﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FileSelectConfirmOptions{

	public string cancelButtonTitle;			// キャンセルボタンのタイトル
	public System.Action cancelButtonDelegate;	// 〃を押したときに実行されるデリゲート
	public string okButtonTitle;				// OKボタンのタイトル
	public System.Action okButtonDelegate;		// 〃を押したときに実行されるデリゲート


}


public class FileSelectConfirmUIController : MonoBehaviour {


	[SerializeField] Text Title;
	[SerializeField] Text Comment;

	[SerializeField] Button cancelButton;
	[SerializeField] Button okButton;


	[SerializeField] Text cancelButtonLabel;
	[SerializeField] Text okButtonLabel;


	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	private static GameObject prefab = null;





	public static FileSelectConfirmUIController Show(bool isMovie, FileSelectConfirmOptions opt){


		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("AddNewPictureDialog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		FileSelectConfirmUIController FileSelectConfirm = obj.GetComponent<FileSelectConfirmUIController>();
		FileSelectConfirm.UpdateContent(isMovie,opt);

		return FileSelectConfirm;

	}


	public void UpdateContent(
		bool isMovie, FileSelectConfirmOptions options=null)
	{

		if(isMovie){
			Title.text = "映像の選択";
			Comment.text = "mp4フォーマットの映像ファイルを選択してください。";
		} else {
			Title.text = "画像の選択";
			Comment.text = "JPGファイル、またはPNGファイルを選択してください。\nサイズはできれば1920x1080をご用意ください。";
		}

		if(options != null)
		{
			// 表示オプションが指定されている場合、オプションの内容に合わせてボタンを表示/非表示する
			cancelButton.transform.parent.gameObject.SetActive(
				options.cancelButtonTitle != null || options.okButtonTitle != null
			);

			cancelButton.gameObject.SetActive(options.cancelButtonTitle != null);
			cancelButtonLabel.text = options.cancelButtonTitle ?? "";
			cancelButtonDelegate = options.cancelButtonDelegate;

			okButton.gameObject.SetActive(options.okButtonTitle != null);
			okButtonLabel.text = options.okButtonTitle ?? "";
			okButtonDelegate = options.okButtonDelegate;
		}
		else
		{
			// 表示オプションが指定されていない場合、デフォルトのボタン表示にする
			cancelButton.gameObject.SetActive(false);
			okButton.gameObject.SetActive(true);
			okButtonLabel.text = "OK";
		}





	}



	// アラートビューを閉じるメソッド
	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{

		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{
		

		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();
	}


}
