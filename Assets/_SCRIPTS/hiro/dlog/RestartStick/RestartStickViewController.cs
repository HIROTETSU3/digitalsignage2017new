﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 店舗の再起動コマンドを送信するものです。
 * 
 * 
 * 
 * */



public class RestartStickViewController : ViewController {


	[SerializeField] Button cancelButton;
	[SerializeField] Button okButton;

	[SerializeField] Text cancelButtonLabel;
	[SerializeField] Text okButtonLabel;

	[SerializeField] Dropdown shopDrop;


	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	private static GameObject prefab = null;
	// Use this for initialization

	static string allShopID = "allshop";
	private string shopidString;

	public static RestartStickViewController Show(){


		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("RestartStickDilog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		RestartStickViewController RestartDlog = obj.GetComponent<RestartStickViewController>();
		RestartDlog.UpdateContent();

		return RestartDlog;
	}


	public void UpdateContent(){

		//

		shopidString = allShopID;
		ShopDropSetup();



	}

	private ClassBox.Staff Me;
	void ShopDropSetup(){


		int shopDefault = 0;

		//shop
		List<Dropdown.OptionData> shopOptions = new List<Dropdown.OptionData> ();
		shopDrop.options.Clear ();
		shopOptions.Add (new Dropdown.OptionData{text = "全店舗"});
		int counter = 0;
		foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
			if (Me != null) { //Edit Panelの場合
				if (shop.shopid == Me.shops) {
					shopDefault = counter + 1; //---の分+1
				}
				counter++;
			}
			shopOptions.Add (new Dropdown.OptionData{text = shop.name});
		}
		shopDrop.options = shopOptions;
		shopDrop.value = shopDefault; //default

	}


	public void ChangeTargetShop(){
		if (shopDrop.value == 0) {
			shopidString = allShopID;
		}else if (0 < shopDrop.value) {
			shopidString = Global.Instance.ShopList [shopDrop.value - 1].shopid;
		}
	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonTitle = "中止";
		opt.okButtonTitle = "実行";
		opt.okButtonDelegate = () =>{
			DoReboot();
		};
		if (shopidString == allShopID) {
			AlertViewController.Show ("全国拠点の端末アプリを一斉にリブートして最新の情報に更新します", "実行しますか？",opt);
		} else {
			AlertViewController.Show ("["+Mskg.API.Instance.getShopByShopID (shopidString).name+"]の店舗に所属する端末アプリをリブートします","よろしいですか?",opt);
		}

	}

	private void DoReboot(){
		
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyPROGID, Cfg.App.RESTART_ID);
		param_list.Add (Cfg.App.keySHOPID, shopidString);

		Mskg.API.Instance.call (
			Cfg.API.insertLocalPlaying + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						
						DataAccessDlog.Show("ショップ端末の再起動を送信中、しばらくすると再起動が実施されます。",3f);

						Dismiss();

					}

					//this.gameObject.SetActive (false);
				}
			}
		);




	}

}
