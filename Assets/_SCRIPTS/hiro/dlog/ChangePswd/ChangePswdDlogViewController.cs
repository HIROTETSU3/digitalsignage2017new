﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePswdDlogOptions
{
	public string cancelButtonTitle;			// キャンセルボタンのタイトル
	public System.Action cancelButtonDelegate;	// 〃を押したときに実行されるデリゲート
	public string okButtonTitle;				// OKボタンのタイトル
	public System.Action okButtonDelegate;		// 〃を押したときに実行されるデリゲート

	//
	public string userId;						// 現在ログイン中のユーザーID
	public string oldLoginPswd;					// 現在ログイン中のパスワード（エンコード済み）
	public bool Expaird;						// 期限切れ状態かどうか
}


public class ChangePswdDlogViewController : ViewController {

	[SerializeField] Text messageLabel;			// メッセージを表示するテキスト

	[SerializeField] Button cancelButton;		// キャンセルボタン
	[SerializeField] Text cancelButtonLabel;	// 〃のタイトルを表示するテキスト
	[SerializeField] Button okButton;			// OKボタン
	[SerializeField] Text okButtonLabel;		// 〃のタイトルを表示するテキスト

	private static GameObject prefab = null;	// アラートビューのプレハブを保持

	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	[SerializeField] InputField oldPassWordField;
	[SerializeField] InputField newPassWordField1;
	[SerializeField] InputField newPassWordField2;
	[SerializeField] GameObject OldPassChk;
	[SerializeField] GameObject NewPassChk;
	[SerializeField] GameObject ExpairdIcon;


	void Start(){
		

	}


	public static ChangePswdDlogViewController Show(ChangePswdDlogOptions options){
		
		if(prefab == null){
			prefab  = Resources.Load("ChangePswdDlog") as GameObject; 
		}


		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		ChangePswdDlogViewController ChangePswdView = obj.GetComponent<ChangePswdDlogViewController>();

		ChangePswdView.UpdateContent(options);

		return ChangePswdView;


	}

	void UpdateContent(ChangePswdDlogOptions options){


		oldPassWordField.text = "";
		oldPassWordField.interactable = true;
		//新しいパスワードフィールドのイニシャライズ
		newPassWordField1.text = "";
		newPassWordField1.interactable = false;
		newPassWordField2.text = "";
		newPassWordField2.interactable = false;

		//チェック表示のイニシャライズ
		OldPassChk.SetActive(false);
		NewPassChk.SetActive(false);

		ExpairdIcon.SetActive(options.Expaird);

		UserId = options.userId;

		okButton.interactable = false;
		cancelButton.interactable = true;

		cancelButtonDelegate = options.cancelButtonDelegate;
		okButtonDelegate = options.okButtonDelegate;

		cancelButtonLabel.text = options.cancelButtonTitle;
		okButtonLabel.text = options.okButtonTitle;

		messageLabel.text = "パスワードを変更してください\nID : "+ options.userId;

	}

	string UserId;

	public void oldPswdInputChange(){
		string inputTxt = Mskg.API.Instance.Aes128CBCEncode (oldPassWordField.text, Global.Instance.pwKey, Global.Instance.pwSalt);
		OldPassChk.SetActive(inputTxt == Global.Instance.Me.password);

		newPassWordField1.interactable = (inputTxt == Global.Instance.Me.password);
		newPassWordField2.interactable = false;
		newPassWordField2.text = "";

	}
	public void newPswdInputChange(){
		//新パスワードの入力がある事に Field2は空っぽに
		newPassWordField2.text = "";
		//８文字以下、もしくは空っぽの場合はField2を入力させない
		newPassWordField2.interactable = !(newPassWordField1.text == "" || newPassWordField1.text.Length < Cfg.App.RANDOM_PASSWORD_NUM);
		okButton.interactable = false;


	}

	public void confiPswdInputChange(){
		//確認用Field2 の入力

		if (newPassWordField1.text == newPassWordField2.text){
			//揃ってる
			NewPassChk.SetActive(true);
			okButton.interactable = true;

		} else {
			NewPassChk.SetActive(false);
			okButton.interactable = false;

		}


	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

		Mskg.API.Instance.sendNewPw (
			newPassWordField2.text,
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					//グローバル変数使います。
					Global.Instance.NewPswd = ret.obj as string;

					if(okButtonDelegate != null)
					{
						okButtonDelegate.Invoke();
					}
					Dismiss();
				}
			}
		);


	}
}
