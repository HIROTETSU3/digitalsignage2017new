﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewProgramDlogViewOptions{

	public System.Action cancelButtonDelegate;	// Cancelを押したときに実行されるデリゲート
	public System.Action<ClassBox.SimpleProgram> okButtonDelegate;	// Okを押したときに実行されるデリゲート

	//動画=1 スライドショー=2
	public int Type = 1;


}

public class NewProgramDlogViewController : ViewController {


	[SerializeField] Text TitleLabel;	
	[SerializeField] Text CommentLabel;	

	[SerializeField] InputField txtName;
	[SerializeField] Text NameData;

	[SerializeField] InputField txtSub;
	[SerializeField] Text SubData;

	[SerializeField] Dropdown TypeDropdown;
	[SerializeField] Text TypeData;

	[SerializeField] Dropdown CategoryDropdown;
	[SerializeField] Text CategoryData;

	[SerializeField] InputField txtRelease;
	[SerializeField] Text ReleaseDate;

	private static GameObject prefab = null;	// ユーザー設定ビューのプレハブを保持

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private ClassBox.SimpleProgram MyProgram;
	private ClassBox.SimpleProgram pBackUp;
	private int Mode;


	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action<ClassBox.SimpleProgram> okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持



	NewProgramDlogViewOptions MyOption;


	public static NewProgramDlogViewController Show(int mode, ClassBox.SimpleProgram p = null, NewProgramDlogViewOptions opt = null){

		//Mode = mode;


		/*
		if(p != null){
			//新規じゃない
//			pBackUp = new ClassBox.SimpleProgram();
//			pBackUp.basic_page_time = p.basic_page_time;
//			pBackUp.bgcolor= p.bgcolor;
//			pBackUp.broadcast = p.broadcast;

			MyProgram = p;
		} else {
			//新規

			MyProgram = new ClassBox.SimpleProgram();
			MyProgram.basic_page_time = 3;



			MyProgram.title = "";
			MyProgram.subtitle = "";
			MyProgram.layout_type = 0;
			MyProgram.bgcolor = "#000";
			MyProgram.content_type = 2;
			MyProgram.transition_type = 1;
			txtTransition_type.text = itemData.transition_type.ToString();
			txtBasic_page_time.text = itemData.basic_page_time.ToString();

			dropContentType.value = int.Parse(txtContent_type.text); //default
			dropTransition.value = int.Parse(txtTransition_type.text);

			txtEditorID.text = itemData.editorid.ToString();
			txtShopID.text = itemData.shopid;
			txtMst_area_id.text = itemData.mst_area_id.ToString();
			txtCampany_id.text = itemData.campany_id.ToString();
			txtBroadcast.text = itemData.broadcast.ToString();

			toggleBroadcast.isOn = (itemData.broadcast == 1);

		}
*/

		if(prefab == null){
			prefab  = Resources.Load("Canvas_New_Program") as GameObject; 
		}

		// プレハブをインスタンス化してビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		NewProgramDlogViewController NewProgramView = obj.GetComponent<NewProgramDlogViewController>();

		NewProgramView.UpdateContent(mode, p, opt);

		return NewProgramView;

	}


	void UpdateContent(int mode, ClassBox.SimpleProgram p, NewProgramDlogViewOptions opt = null){
		Mode = mode;

		MyProgram = p;

		TitleLabel.text = "新規 番組を作成";
		CommentLabel.text = "番組タイトルとサブタイトルを設定し、\n種類を動画、またはスライドショーに設定してください。";

		okButtonDelegate = opt.okButtonDelegate;
		cancelButtonDelegate = opt.cancelButtonDelegate;

		MyOption = opt;


		TypeDropdown.value = p.content_type;

		if (p != null){


			txtName.text = NameData.text = MyProgram.title;
			txtSub.text = SubData.text = MyProgram.subtitle;

		}

		if (Mode == 0){
			//データを表示するだけ

			txtName.gameObject.SetActive(false);
			txtSub.gameObject.SetActive(false);

			NameData.gameObject.SetActive(true);
			SubData.gameObject.SetActive(true);

			TypeData.gameObject.SetActive(true);
			TypeDropdown.gameObject.SetActive(false);


		} else if (Mode == 1){
			//データの編集


			txtName.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);

			NameData.gameObject.SetActive(false);
			SubData.gameObject.SetActive(false);

			TypeData.gameObject.SetActive(false);
			TypeDropdown.gameObject.SetActive(true);


		} else if (Mode == 2){
			//新規

			txtName.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);

			NameData.gameObject.SetActive(false);
			SubData.gameObject.SetActive(false);

			TypeData.gameObject.SetActive(false);
			TypeDropdown.gameObject.SetActive(true);


		}



	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

//		//なんかこれアホかもと思いつつテンポラリに複製。
//
//		Global.Instance.TempSimpleProgram = new ClassBox.SimpleProgram();
//		Global.Instance.TempSimpleProgram.title = MyProgram.title = txtName.text;
//		Global.Instance.TempSimpleProgram.subtitle = MyProgram.subtitle = txtSub.text;
//		Global.Instance.TempSimpleProgram.content_type = MyProgram.content_type;
//
//
//		Global.Instance.TempSimpleProgram.basic_page_time =MyProgram.basic_page_time;
//		Global.Instance.TempSimpleProgram.bgcolor = MyProgram.bgcolor;
//		Global.Instance.TempSimpleProgram.broadcast = MyProgram.broadcast;
//		Global.Instance.TempSimpleProgram.campany_id = MyProgram.campany_id;
//		Global.Instance.TempSimpleProgram.created = MyProgram.created;
//		Global.Instance.TempSimpleProgram.editorid = MyProgram.editorid;
//		Global.Instance.TempSimpleProgram.layout_type = MyProgram.layout_type;
//		Global.Instance.TempSimpleProgram.mst_area_id = MyProgram.mst_area_id;
//		Global.Instance.TempSimpleProgram.programid = MyProgram.programid;
//		Global.Instance.TempSimpleProgram.shopid = MyProgram.shopid;
//
//		Global.Instance.TempSimpleProgram.transition_type = MyProgram.transition_type;


		ClassBox.SimpleProgram _p = new ClassBox.SimpleProgram();
		_p.title = MyProgram.title = txtName.text;
		_p.subtitle = MyProgram.subtitle = txtSub.text;
		_p.content_type = MyProgram.content_type;


		_p.basic_page_time =MyProgram.basic_page_time;
		_p.bgcolor = MyProgram.bgcolor;
		_p.broadcast = MyProgram.broadcast;
		_p.campany_id = MyProgram.campany_id;
		_p.created = MyProgram.created;
		_p.editorid = MyProgram.editorid;
		_p.layout_type = MyProgram.layout_type;
		_p.mst_area_id = MyProgram.mst_area_id;
		_p.programid = MyProgram.programid;
		_p.shopid = MyProgram.shopid;

		_p.transition_type = MyProgram.transition_type;

		//1.1
		_p.category = CategoryDropdown.itemText.text;
		_p.releasedate = txtRelease.text;


		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke(_p);
		}
		Dismiss();

	}



	[SerializeField] Sprite[] Icons;
	[SerializeField] Image TypeIcon;

	public void OnChangeTypeDropdown(Dropdown cell){

		MyOption.Type = cell.value+1;
		TypeIcon.sprite = Icons[cell.value];

		if(cell.value == 0){
			//動画
			MyProgram.content_type = 1;

		} else if (cell.value == 1){
			//スライドショー
			MyProgram.content_type = 2;

		}


	}
}
