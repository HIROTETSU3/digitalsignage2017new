﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginPanelUIOptions{

	public System.Action cancelButtonDelegate;
	public System.Action okButtonDelegate;


}


public class LoginPanelUIController : ViewController {

	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	private static GameObject prefab = null;	// ユーザー設定ビューのプレハブを保持


	[SerializeField] InputField Passwd;
	[SerializeField] Text UID;

	[SerializeField] Button OkButton;
	[SerializeField] Button CancelButton;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static LoginPanelUIController Show(LoginPanelUIOptions opt = null){
		if(prefab == null){
			prefab  = Resources.Load("LoginPanel") as GameObject; 
		}


		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		LoginPanelUIController UserLoginView = obj.GetComponent<LoginPanelUIController>();

		UserLoginView.UpdateContent(opt);

		return UserLoginView;

	}

	void UpdateContent(LoginPanelUIOptions opt = null){


		if (opt != null){
			cancelButtonDelegate = opt.cancelButtonDelegate;
			okButtonDelegate = opt.okButtonDelegate;
		}

		UID.text = Global.Instance.Me.login;



	}

	public void OnPswdFieldChange(InputField pswd){


	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();

	}


}
