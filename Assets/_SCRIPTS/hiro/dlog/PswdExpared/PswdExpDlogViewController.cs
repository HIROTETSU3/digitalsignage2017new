﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// パスワード期限切れの表示オプションを指定するためのクラスを定義
public class PswdExpDlogOptions
{
	public string cancelButtonTitle;			// キャンセルボタンのタイトル
	public System.Action cancelButtonDelegate;	// 〃を押したときに実行されるデリゲート
	public string okButtonTitle;				// OKボタンのタイトル
	public System.Action okButtonDelegate;		// 〃を押したときに実行されるデリゲート
}

public class PswdExpDlogViewController : ViewController {
	[SerializeField] Text messageLabel;			// メッセージを表示するテキスト
	[SerializeField] Button cancelButton;		// キャンセルボタン
	[SerializeField] Text cancelButtonLabel;	// 〃のタイトルを表示するテキスト
	[SerializeField] Button okButton;			// OKボタン
	[SerializeField] Text okButtonLabel;		// 〃のタイトルを表示するテキスト

	private static GameObject prefab = null;	// アラートビューのプレハブを保持
	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	public static PswdExpDlogViewController Show(
		string message, PswdExpDlogOptions options)
	{


		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("PswdExpDlog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		PswdExpDlogViewController PswdExpView = obj.GetComponent<PswdExpDlogViewController>();

		PswdExpView.UpdateContent(message, options);

		return PswdExpView;
	}

	// アラートビューの内容を更新するメソッド
	public void UpdateContent(
		string message, PswdExpDlogOptions options)
	{

		//


		// タイトルとメッセージを設定する
		messageLabel.text = "現在のパスワードは "+message + " まで使用可能です。";

		cancelButton.transform.parent.gameObject.SetActive(
			options.cancelButtonTitle != null || options.okButtonTitle != null
		);

		cancelButton.gameObject.SetActive(options.cancelButtonTitle != null);
		cancelButtonLabel.text = options.cancelButtonTitle ?? "";
		cancelButtonDelegate = options.cancelButtonDelegate;

		okButton.gameObject.SetActive(options.okButtonTitle != null);
		okButtonLabel.text = options.okButtonTitle ?? "";
		okButtonDelegate = options.okButtonDelegate;
	}

	// アラートビューを閉じるメソッド
	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{
		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();
	}
}
