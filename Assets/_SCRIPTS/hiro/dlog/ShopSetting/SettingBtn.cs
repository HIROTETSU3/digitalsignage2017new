﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingBtn : MonoBehaviour {

	// Use this for initialization
	void Start () {

		if (Global.Instance.Me.type != 4){
			Destroy(gameObject);
		}
		
	}

	public void OnPressSetting(){

		ShopSettingUIViewController.Show();

	}

}
