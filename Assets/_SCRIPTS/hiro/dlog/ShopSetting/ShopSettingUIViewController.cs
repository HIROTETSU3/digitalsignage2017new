﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShopSettingUIViewController : ViewController {

	[SerializeField] Text ShopName;
	[SerializeField] Dropdown HourDrop;
	[SerializeField] Dropdown MinutesDrop;

	private static GameObject prefab = null;	// ビューのプレハブを保持

	private int[] HH = {17,18,19,20,21,22,23,24}; 
	private int[] MM = {0,10,20,30,40,50}; 
	private DateTime closeTime = new DateTime (2017, 1, 1, 17, 0, 0); //17:00 default

	public static ShopSettingUIViewController Show(){

		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("ShopSettingDlog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		ShopSettingUIViewController ShopSettingView = obj.GetComponent<ShopSettingUIViewController>();
		ShopSettingView.UpdateContent();

		return ShopSettingView;


	}

	public void UpdateContent(){

		Debug.Log("TYPE = " + Global.Instance.Me.type);

		ShopName.text = Mskg.API.Instance.getShopByShopID(Global.Instance.Me.shops).name + " 店舗設定";

		// 現在のショップ設定の終了時刻を取得して入れてください

		// 取得するまでの表示
		int[] selection = getSelectFromTime (closeTime);
		HourDrop.value = selection[0];
		MinutesDrop.value = selection[1];

		//取得
		Hashtable param_closetime = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_closetime.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);

		Mskg.API.Instance.call (
			Cfg.API.getShopCloseTime + ".php",
			param_closetime, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					
					if (Mskg.API.Instance.checkError(ret.json, null)) {
//						AlertViewController.Show("JSON", ret.json);

						LitJson.JsonData response =  LitJson.JsonMapper.ToObject(ret.json);

						DateTime _closetime = DateTime.Parse(response["closetime"].ToString());
						Debug.Log("////////////");
						Debug.Log(_closetime);
						Debug.Log("////////////");

						int[] selectionFromDB = getSelectFromTime (_closetime);
						HourDrop.value = selectionFromDB[0];
						MinutesDrop.value = selectionFromDB[1];

					}

				}

			}
		);



	}

	public void OnSelectDropDown(){

		Debug.Log("設定時間 : "+HH[HourDrop.value]+":"+MM[MinutesDrop.value]);
		//ここで設定を書き換えるか、OKボタンを押した時に設定を書き換えて下さい。
		closeTime = getTimeFromSelect(HH[HourDrop.value], MM[MinutesDrop.value]);

	}

	public void OnTest(){

		ShopCloseBoard.Show(true);

	}


	// アラートビューを閉じるメソッド
	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// Cancelボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{
		//AlertViewController.Show ("設定時刻", closeTime.ToString());


		Hashtable param_closetime = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_closetime.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		param_closetime.Add (Cfg.App.keyCLOSETIME, closeTime.ToString(Cfg.App.TIME_FORMAT).Replace (":", "-"));

		Mskg.API.Instance.call (
			Cfg.API.updateShopCloseTime + ".php",
			param_closetime, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						AlertViewController.Show("設定時刻", "時刻を設定しました。");
					}

					Dismiss();
				}

			}
		);




	}

	private DateTime getTimeFromSelect(int h, int m){
		DateTime time = new DateTime (2017, 1, 1, HH[HourDrop.value], MM[MinutesDrop.value], 0);
		return time;
	}

	private int[] getSelectFromTime(DateTime _time){
		int selectH, selectM;
		selectH = Array.IndexOf (HH, _time.Hour);
		selectM = Array.IndexOf (MM, _time.Minute);
		int[] rtn = { selectH, selectM };
		return rtn;
	}
}
