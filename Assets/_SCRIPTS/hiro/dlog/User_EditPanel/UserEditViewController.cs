﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UserEditViewOptions{

	public System.Action cancelButtonDelegate;	// Cancelを押したときに実行されるデリゲート
	public System.Action okButtonDelegate;	// Okを押したときに実行されるデリゲート

}


public class UserEditViewController : ViewController {

	GameObject StaffViewTableView;

	[SerializeField] Text TitleLabel;	

	[SerializeField] InputField txtName;
	[SerializeField] Text NameData;

	[SerializeField] Text userID;

	[SerializeField] InputField txtLogin;
	[SerializeField] Text LoginData;

	[SerializeField] InputField txtMail;
	[SerializeField] Text MailData;

	[SerializeField] InputField txtPw;
	[SerializeField] Text PwData;

	//エディット時ユーザーIDが重複している場合に表示するフィールド
	[SerializeField] Text DuplicateUID;

	[SerializeField] private Dropdown shopDrop;
	[SerializeField] Text shopLabel;

	[SerializeField] private Dropdown blockDrop;
	[SerializeField] Text blockLabel;

	[SerializeField] private Dropdown companyDrop;
	[SerializeField] Text campanyLabel;

	//内容によって省略するLINE(vertical Layout Group）
	[SerializeField] GameObject LineShop;
	[SerializeField] GameObject LineBlock;
	[SerializeField] GameObject LinePassWord;


	[SerializeField] Text InfoPswd8Charactor;

	[SerializeField] Text CancelButtonLabel;
	[SerializeField] Text OkButtonLabel;
	//OKボタンは必須の情報が入力されていない限り、または変更がない限りアクティブにならない
	[SerializeField] Button OkButton;
	[SerializeField] Button CopyButton;
	[SerializeField] Button CancelButton;
	[SerializeField] Button EditButton;

	//ランダムパスワード生成ボタン
	[SerializeField] Button RandomPswdButton;


	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持


	//設定されているこのダイアログの自分（入力によって変化）
	private ClassBox.Staff Me;
	//開かれたホンモノデータ
	private ClassBox.Staff MeBase;
	//エラーの時に戻すためのバックアップデータ
	private ClassBox.Staff MeBuckup;

	//変更比較用（最初の読み込み時の記録）
	private string buckUptext;
	private string buckUpUserID;
	//プルダウンによって変更される
	private int SetCampanyId = -1;

	//ダイアログのモード Mode 0=表示 1=編集 2=新規
	private int Mode = -1;

	private bool FieldBuilding = false;

	private static GameObject prefab = null;	// ユーザー設定ビューのプレハブを保持

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	/*public class Staff
	{
		public int id;
		public int type;
		public string name;
		public string login;
		public string mail;
		public string modifieddate;
		public int campany_id;
		public int mst_area_id;
		public string shops;
		public string password;
	}*/

	public static UserEditViewController Show(int mode,ClassBox.Staff itemData=null, UserEditViewOptions options = null){
		if(prefab == null){
			prefab  = Resources.Load("Canvas_User_EditPanel") as GameObject; 
		}


		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		UserEditViewController UserEditView = obj.GetComponent<UserEditViewController>();

		UserEditView.UpdateContent(mode,itemData, options);

		return UserEditView;

	}

	void UpdateContent(int mode, ClassBox.Staff staff=null, UserEditViewOptions options = null){
//		AlertViewController.Show ("shop & block & campany", "shop=" + staff.shops + "block=" + staff.mst_area_id.ToString () + "campany=" + staff.campany_id.ToString ());
		// mode 閲覧モード 0, 編集モード 1, 新規モード 2 //mskg このコメント修正しました。

		if(staff != null)
		{
			// Editの場合
			MeBase = staff;
			Me = new ClassBox.Staff();//staff;
			//複製する（staffをMeに代入しないことで、データを更新しない）
			Me.campany_id = staff.campany_id;
			Me.id = staff.id;
			Me.login = staff.login;
			Me.mail = staff.mail;
			Me.modifieddate = staff.modifieddate;
			Me.mst_area_id = staff.mst_area_id;
			Me.name = staff.name;
			Me.password = staff.password;
			Me.shops = staff.shops;
			Me.type = staff.type;

			//エラー用の保存
			MeBuckup = new ClassBox.Staff();//staff;
			//複製する（staffをMeに代入しないことで、データを更新しない）
			MeBuckup.campany_id = staff.campany_id;
			MeBuckup.id = staff.id;
			MeBuckup.login = staff.login;
			MeBuckup.mail = staff.mail;
			MeBuckup.modifieddate = staff.modifieddate;
			MeBuckup.mst_area_id = staff.mst_area_id;
			MeBuckup.name = staff.name;
			MeBuckup.password = staff.password;
			MeBuckup.shops = staff.shops;
			MeBuckup.type = staff.type;

			DebugMe();

			string plainPw = Mskg.API.Instance.Aes128CBCDecode (Me.password, Global.Instance.pwKey, Global.Instance.pwSalt);

			userID.text = Me.id.ToString();
			NameData.text = txtName.text = Me.name;
			LoginData.text = txtLogin.text = Me.login;
			MailData.text = txtMail.text = Me.mail;
			PwData.text = txtPw.text = plainPw;
			//txtShopID.text = staff.shops;

			//所属ラベル
			ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Me.mst_area_id);
			ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Me.campany_id);
			ClassBox.ShopData shop = Mskg.API.Instance.getShopByShopID (Me.shops);

			blockLabel.text = (area != null) ? area.name : "--";
			campanyLabel.text = (campany != null) ? campany.name : "--";
			shopLabel.text = (shop != null) ? shop.name : "--";


//			try {
//				blockLabel.text = Mskg.API.Instance.getAreaByID (Me.mst_area_id).name;
//				campanyLabel.text = Mskg.API.Instance.getCampanyByID (Me.campany_id).name;
//				shopLabel.text = Mskg.API.Instance.getShopByShopID (Me.shops).name;
//			}catch{
//
//			}

			SetCampanyId = Me.campany_id;

			//txtCompany.text = staff.campany_id.ToString();
			//txtType.text = staff.type.ToString();

			//shopDefault = staff.sho
			//typeDefault = staff.type + 1; //---の分+1

			txtBuckUp();



		} else {
			//staffがnullの想定はなし。

			/*
			Global.Instance.Me = new ClassBox.Staff ();
			Global.Instance.Me.id = -1;
			Global.Instance.Me.type = 0;
			Global.Instance.Me.name = "白石哲也";
			Global.Instance.Me.login = "mushikago";
			Global.Instance.Me.mail = "tshiraishi@mushikago.com";
			Global.Instance.Me.password = "LlvGws8QrpjmTV4xHZRm/g==";
			Global.Instance.Me.modifieddate = "2017-08-05 20:52:25";
			Global.Instance.Me.campany_id = 1;
			Global.Instance.Me.mst_area_id = 1;
			Global.Instance.Me.shops = "12345";
			*/

		}

		FieldBuild(mode, staff.type);

	}

	string UserKind(){

		string[] K = new string[]{
			"システム管理者",
			"管理者",
			"ブロックユーザー",
			"コーポレートユーザー",
			"店舗ユーザー"};


		return "("+K[Me.type]+")";


	}

	void FieldBuild(int mode, int type = 0){
		FieldBuilding = true;
		Mode=mode;

		if (mode == 0){
			//閲覧モード
			TitleLabel.text = "アカウント情報"+UserKind();

			//InputFieldを消す
			txtName.gameObject.SetActive(false);
			txtLogin.gameObject.SetActive(false);
			txtMail.gameObject.SetActive(false);
			txtPw.gameObject.SetActive(false);

			//ログインID重複赤文字
			DuplicateUID.gameObject.SetActive(false);

			//Statick Textを表示
			NameData.gameObject.SetActive(true);
			LoginData.gameObject.SetActive(true);
			MailData.gameObject.SetActive(true);
			PwData.gameObject.SetActive(true);

			//８文字以上
			InfoPswd8Charactor.gameObject.SetActive(false);

			OkButton.gameObject.SetActive(false);
			CopyButton.gameObject.SetActive(false);
			RandomPswdButton.gameObject.SetActive(false);

			CancelButton.gameObject.SetActive(true);
			CancelButtonLabel.text = "閉じる";

			//Block
			int blockId = Me.mst_area_id;

			//ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.ShopList[blockId].mst_area_id);

			shopLabel.gameObject.SetActive(true);

			blockDrop.gameObject.SetActive(false);
			shopDrop.gameObject.SetActive(false);
			companyDrop.gameObject.SetActive(false);

			EditButton.gameObject.SetActive(true);

		} else if (mode==1){
			//編集

			okButtonDelegate = () =>{
				//保存する。
				SaveMe();
			};

			cancelButtonDelegate = null;


			TitleLabel.text = "アカウント編集"+UserKind();

			//InputFieldをアクティブに
			txtName.gameObject.SetActive(true);
			txtLogin.gameObject.SetActive(true);
			txtMail.gameObject.SetActive(true);
			txtPw.gameObject.SetActive(true);

			//ログインID重複赤文字
			DuplicateUID.gameObject.SetActive(false);

			//Statick Textを消す
			NameData.gameObject.SetActive(false);
			LoginData.gameObject.SetActive(false);
			MailData.gameObject.SetActive(false);
			PwData.gameObject.SetActive(false);

			RandomPswdButton.gameObject.SetActive(true);

			//８文字以上
			InfoPswd8Charactor.gameObject.SetActive(true);


			OkButton.gameObject.SetActive(true);
			OkButtonLabel.text = "保存";
			OkButton.interactable = false;

			CopyButton.gameObject.SetActive(false);

			CancelButton.gameObject.SetActive(true);
			CancelButtonLabel.text = "保存せずに閉じる";

			EditButton.gameObject.SetActive(false);

			if (type == 0 || type == 1) { //管理者＆システム管理者

				//DropDown イニシャライズ
				ShopDropSetup();
				CampanyDropSetup ();
				BlockDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(true);
				blockDrop.gameObject.SetActive (true);
				companyDrop.gameObject.SetActive (true);


			} else if (type == 2) { //ブロックユーザ

				//DropDown イニシャライズ
				BlockDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(false);
				blockDrop.gameObject.SetActive (true);
				companyDrop.gameObject.SetActive (false);

			} else if (type == 3) { //コーポレートユーザ

				//DropDown イニシャライズ
				CampanyDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(false);
				blockDrop.gameObject.SetActive (false);
				companyDrop.gameObject.SetActive (true);


			} else if (type == 4) { //店舗ユーザ

				//DropDown イニシャライズ
				ShopDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (true);
				campanyLabel.gameObject.SetActive (true);

				shopDrop.gameObject.SetActive(true);
				blockDrop.gameObject.SetActive (false);
				companyDrop.gameObject.SetActive (false);

			} else {
				Debug.Log("staff.type = null");
			}


			txtBuckUp();

		} else if (mode == 2){
			//新規


			okButtonDelegate = () =>{
				//保存する。
				bool isInsertFlag = true;
				SaveMe(isInsertFlag);
			};

			cancelButtonDelegate = null;



			TitleLabel.text = "アカウント追加"+UserKind();

			//InputFieldをアクティブに
			txtName.gameObject.SetActive(true);
			txtLogin.gameObject.SetActive(true);
			txtMail.gameObject.SetActive(true);
			txtPw.gameObject.SetActive(true);

			//ログインID重複赤文字
			DuplicateUID.gameObject.SetActive(false);

			//Statick Textを消す
			NameData.gameObject.SetActive(false);
			LoginData.gameObject.SetActive(false);
			MailData.gameObject.SetActive(false);
			PwData.gameObject.SetActive(false);

			RandomPswdButton.gameObject.SetActive(true);

			//８文字以上
			InfoPswd8Charactor.gameObject.SetActive(true);


			OkButton.gameObject.SetActive(true);
			OkButtonLabel.text = "保存";
			OkButton.interactable = false;

			CopyButton.gameObject.SetActive(false);

			CancelButton.gameObject.SetActive(true);
			CancelButtonLabel.text = "保存せずに閉じる";

			EditButton.gameObject.SetActive(false);



			if (type == 0 || type == 1) {

				//DropDown イニシャライズ
				ShopDropSetup();
				CampanyDropSetup ();
				BlockDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(true);
				blockDrop.gameObject.SetActive (true);
				companyDrop.gameObject.SetActive (true);


			} else if (type == 2) { //ブロックユーザ

				//DropDown イニシャライズ
				BlockDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(false);
				blockDrop.gameObject.SetActive (true);
				companyDrop.gameObject.SetActive (false);

			} else if (type == 3) { //コーポレートユーザ

				//DropDown イニシャライズ
				CampanyDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (false);
				campanyLabel.gameObject.SetActive (false);

				shopDrop.gameObject.SetActive(false);
				blockDrop.gameObject.SetActive (false);
				companyDrop.gameObject.SetActive (true);


			} else if (type == 4) { //店舗ユーザ

				//DropDown イニシャライズ
				ShopDropSetup();

				shopLabel.gameObject.SetActive(false);
				blockLabel.gameObject.SetActive (true);
				campanyLabel.gameObject.SetActive (true);

				shopDrop.gameObject.SetActive(true);
				blockDrop.gameObject.SetActive (false);
				companyDrop.gameObject.SetActive (false);

			} else {
				Debug.Log("staff.type = null");
			}

			txtBuckUp();
		}

		FieldBuilding = false;
	}

	public void OnPressToggleEditBtn(){
		//閲覧中に編集ボタンを押した。
		FieldBuild(1, Me.type);

	}


	void txtBuckUp(){

		buckUpUserID = Me.login;

		buckUptext = 
			Me.id.ToString()+"|"
			+Me.type + "|"
			+Me.name + "|"
			+Me.login + "|"
			+Me.mail + "|"
			+Me.password + "|"
			+Me.campany_id + "|"
			+Me.mst_area_id + "|"
			+Me.shops;
	}

	bool ChkChangeData(){


		//string EncodePw = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);

		string nowChkText = 
			Me.id.ToString()+"|"
			+Me.type + "|"
			+Me.name + "|"
			+Me.login + "|"
			+Me.mail + "|"
			+Me.password + "|"
			+Me.campany_id + "|"
			+Me.mst_area_id + "|"
			+Me.shops;
		

		return (nowChkText != buckUptext);
	}

	bool FieldChk(){

		return (txtName.text == "" ||
			txtLogin.text == "" ||
			txtMail.text == "" ||
			txtPw.text == "");

		//フィールドに空っぽが一つでもある

	}

	bool chk(){

		//ログインID重複メッセージが表示されているかどうか
		if (DuplicateUID.gameObject.activeInHierarchy) {
			return false;
		}

		//どれかが空っぽ
		if (FieldChk()){
			Debug.Log("-->どれかが空っぽ");
			return false;
		}
		//少し変わってる
		if (ChkChangeData()){
			Debug.Log("-->変更あり");
			return true;
		}

		Debug.Log("-->変わってない");
		//変わってない
		return false;
	}


	public void OnImputFieldNameChangeText(){
		//名前フィールドの変更
		FieldOk();
	}

	public void OnImputFieldMailChangeText(){
		//メールフィールドの変更
		FieldOk();
	}


	public void OnImputFieldPaswdChangeText(){
		//パスワードフィールドの変更
		FieldOk();
	}

	public void OnImputFieldChangeText(){
		// ログイン名フィールドの変更

		Me.login = txtLogin.text;

		if (buckUpUserID == txtLogin.text){
			//このダイアログを開いた時と同じテキストなので重複チェックしない
			//なぜならば重複チェックすれば自分を見つけてexistとしてしまうので。

			DuplicateUID.gameObject.SetActive(false);
			OkButton.interactable = chk();

			return;
		}


		Hashtable param_loginid = new Hashtable ();

		param_loginid.Add (Cfg.App.keyLOGIN, txtLogin.text);

		Mskg.API.Instance.call (
			Cfg.API.checkExistLoginID + ".php",
			param_loginid, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) { //通信成功
						
						LitJson.JsonData response =  LitJson.JsonMapper.ToObject(ret.json);

						bool exist = (bool)response["exist"];
						Debug.Log("////////////");
						Debug.Log(exist);
						Debug.Log("////////////");

						if (exist){
							//重複あり
							DuplicateUID.gameObject.SetActive(true);
							OkButton.interactable = false;
						} else {
							DuplicateUID.gameObject.SetActive(false);
							//はじめて他の問題がないかチェック

							OkButton.interactable = chk();

						}

//						List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
//						staffclasses.AddRange(StaffList);
//
//						tableview.iSelectedUserType = int.Parse(param_stafflist[Cfg.App.keyTYPE].ToString());
//						tableview.UpdateData(staffclasses);
					}

					//					this.gameObject.SetActive (false);
				}
			}
		);

	}

	void FieldOk(){
		
		//フィールド確定

		Me.login = txtLogin.text;
		Me.name = txtName.text;
		Me.mail = txtMail.text;
		Me.password = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);


		OkButton.interactable = chk();

	}

	void OnDropdownChange(){




		OkButton.interactable = chk();

	}

	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();

	}


	void ShopDropSetup(){


		int shopDefault = 0;

		//shop
		List<Dropdown.OptionData> shopOptions = new List<Dropdown.OptionData> ();
		shopDrop.options.Clear ();
//		shopOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
			if (Me != null) { //Edit Panelの場合
				if (shop.shopid == Me.shops) {
					shopDefault = counter;// + 1; //---の分+1
				}
				counter++;
			}
			shopOptions.Add (new Dropdown.OptionData{text = shop.name});
		}
		shopDrop.options = shopOptions;
		shopDrop.value = shopDefault; //default

	}

	public void OnShopDropValueChanged(Dropdown drop) {
//		if (drop.value == 0) { //---の場合
//			campanyLabel.text = "";
//			blockLabel.text = "";
//			shopLabel.text = "";
//			return;
//		}


		//blockLabel.text = Mskg.API.Instance.getAreaByID (Me.mst_area_id).name;
		//campanyLabel.text = Mskg.API.Instance.getCampanyByID (Me.campany_id).name;
		//shopLabel.text = Mskg.API.Instance.getShopByShopID (Me.shops).name;


		int selected = drop.value;// - 1;
		Debug.Log("value = " + Global.Instance.ShopList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		//txtShopID.text = Global.Instance.ShopList[selected].shopid;
		//Me.shops = Global.Instance.ShopList[selected].id.ToString();
		Me.shops = Global.Instance.ShopList[selected].shopid;
		shopLabel.text = Global.Instance.ShopList[selected].name; //Global.Instance.ShopList[selected].name;

		if (!FieldBuilding) { //ユーザが手動で店舗ドロップメニューをプルダウンしたときだけ

			ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.ShopList [selected].mst_area_id);
			//txtBlock.text = area.id.ToString();
			Me.mst_area_id = Global.Instance.ShopList [selected].mst_area_id;
			blockLabel.text = area.name;
			BlockDropSetup ();

			ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Global.Instance.ShopList [selected].campany_id);
			//txtCompany.text = campany.id.ToString();
			Me.campany_id = Global.Instance.ShopList [selected].campany_id;
			campanyLabel.text = campany.name;
			CampanyDropSetup ();

		}

		OnDropdownChange();

		//		Debug.Log("text(options) = " + dropdown.options[value].text);  //リストからテキストを取得
		//		Debug.Log("text(captionText) = " + dropdown.captionText.text); //Labelからテキストを取得
	}

	public void OnNewBlockDropValueChanged(Dropdown drop) {
//		if (drop.value == 0) { //---の場合
//			blockLabel.text = "";
//			return;
//		}

		int selected = drop.value;// - 1;

		ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.AreaList [selected].id);
		//txtBlock.text = area.id.ToString();
		Me.mst_area_id = area.id;
		blockLabel.text = area.name;


		OnDropdownChange();
	}

	public void OnNewCampanyDropValueChanged(Dropdown drop) {
//		if (drop.value == 0) { //---の場合
//			campanyLabel.text = "";
//			return;
//		}

		int selected = drop.value;// - 1;

		ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Global.Instance.CampanyList [selected].id);
		//txtCompany.text = campany.id.ToString();
		Me.campany_id = campany.id;
		campanyLabel.text = campany.name;

		OnDropdownChange();

	}


	void CampanyDropSetup(){


		int campanyDefault = 0;

		//shop
		List<Dropdown.OptionData> campanyOptions = new List<Dropdown.OptionData> ();
		companyDrop.options.Clear ();
//		campanyOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
			if (Me != null) { //Edit Panelの場合
				if (campany.id == Me.campany_id) {
					campanyDefault = counter; // + 1; //---の分+1
				}
				counter++;
			}
			campanyOptions.Add (new Dropdown.OptionData{text = campany.name});
		}
		companyDrop.options = campanyOptions;
		companyDrop.value = campanyDefault; //default

	}
	/*
	public void OnCampanyDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			campanyLabel.text = "";
			blockLabel.text = "";
			shopLabel.text = "";
			return;
		}


		//blockLabel.text = Mskg.API.Instance.getAreaByID (Me.mst_area_id).name;
		//campanyLabel.text = Mskg.API.Instance.getCampanyByID (Me.campany_id).name;
		//shopLabel.text = Mskg.API.Instance.getShopByShopID (Me.shops).name;


		int selected = drop.value - 1;
		Debug.Log("value = " + Global.Instance.ShopList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		//txtShopID.text = Global.Instance.ShopList[selected].shopid;
		//Me.shops = Global.Instance.ShopList[selected].id.ToString();
		Me.shops = Global.Instance.ShopList[selected].shopid;
		shopLabel.text = Global.Instance.ShopList[selected].name; //Global.Instance.ShopList[selected].name;

		ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.ShopList [selected].mst_area_id);
		//txtBlock.text = area.id.ToString();
		Me.mst_area_id = Global.Instance.ShopList [selected].mst_area_id;
		blockLabel.text = area.name;

		ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Global.Instance.ShopList [selected].campany_id);
		//txtCompany.text = campany.id.ToString();
		Me.campany_id = Global.Instance.ShopList [selected].campany_id;
		campanyLabel.text = campany.name;

		OnDropdownChange();

		//		Debug.Log("text(options) = " + dropdown.options[value].text);  //リストからテキストを取得
		//		Debug.Log("text(captionText) = " + dropdown.captionText.text); //Labelからテキストを取得
	}
*/

	void BlockDropSetup(){


		int blockDefault = 0;

		//shop
		List<Dropdown.OptionData> blockOptions = new List<Dropdown.OptionData> ();
		blockDrop.options.Clear ();
//		blockOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		foreach (ClassBox.AreaData block in Global.Instance.AreaList) {
			if (Me != null) { //Edit Panelの場合
				if (block.id == Me.mst_area_id) {
					blockDefault = counter; // + 1; //---の分+1
				}
				counter++;
			}
			blockOptions.Add (new Dropdown.OptionData{text = block.name});
		}
		blockDrop.options = blockOptions;
		blockDrop.value = blockDefault; //default

	}
	/*
	public void OnBlockDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			campanyLabel.text = "";
			blockLabel.text = "";
			shopLabel.text = "";
			return;
		}


		//blockLabel.text = Mskg.API.Instance.getAreaByID (Me.mst_area_id).name;
		//campanyLabel.text = Mskg.API.Instance.getCampanyByID (Me.campany_id).name;
		//shopLabel.text = Mskg.API.Instance.getShopByShopID (Me.shops).name;


		int selected = drop.value - 1;
		Debug.Log("value = " + Global.Instance.ShopList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		//txtShopID.text = Global.Instance.ShopList[selected].shopid;
		//Me.shops = Global.Instance.ShopList[selected].id.ToString();
		Me.shops = Global.Instance.ShopList[selected].shopid;
		shopLabel.text = Global.Instance.ShopList[selected].name; //Global.Instance.ShopList[selected].name;

		ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.ShopList [selected].mst_area_id);
		//txtBlock.text = area.id.ToString();
		Me.mst_area_id = Global.Instance.ShopList [selected].mst_area_id;
		blockLabel.text = area.name;

		ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Global.Instance.ShopList [selected].campany_id);
		//txtCompany.text = campany.id.ToString();
		Me.campany_id = Global.Instance.ShopList [selected].campany_id;
		campanyLabel.text = campany.name;

		OnDropdownChange();

		//		Debug.Log("text(options) = " + dropdown.options[value].text);  //リストからテキストを取得
		//		Debug.Log("text(captionText) = " + dropdown.captionText.text); //Labelからテキストを取得
	}
*/


//	//shop
//	List<Dropdown.OptionData> shopOptions = new List<Dropdown.OptionData> ();
//	shopDrop.options.Clear ();
//	shopOptions.Add (new Dropdown.OptionData{text = "---"});
//	int counter = 0;
//	foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
//		if (staff != null) { //Edit Panelの場合
//			if (shop.shopid == staff.shops) {
//				shopDefault = counter + 1; //---の分+1
//			}
//			counter++;
//		}
//		shopOptions.Add (new Dropdown.OptionData{text = shop.name});
//	}
//	shopDrop.options = shopOptions;
//	shopDrop.value = shopDefault; //default



	void SaveMe(bool isInsert = false){


		//ホンモノデータに更新データをコピー


		MeBase.campany_id = Me.campany_id;
		MeBase.id = Me.id;
		MeBase.login = Me.login;
		MeBase.mail = Me.mail;
		MeBase.modifieddate = Me.modifieddate;
		MeBase.mst_area_id = Me.mst_area_id;
		MeBase.name = Me.name;
		MeBase.password = Me.password;
		MeBase.shops = Me.shops;
		MeBase.type = Me.type;


		DebugMe();

		if (isInsert) {
			insertStaff ();
		} else { //update
			OnPressUpdateButton();
		}


		DataAccessDlog.Show(MeBase.login + " のユーザー情報を更新中",3f);
	}

	void DebugMe(){

		Debug.Log("/////////////////////////////////////");
		Debug.Log("---------------id :" + Me.id);
		Debug.Log("-------------type :" + Me.type);
		Debug.Log("-------------name :" + Me.name);
		Debug.Log("------------login :" + Me.login);
		Debug.Log("-------------mail :" + Me.mail);
		Debug.Log("---------password :" + Me.password);
		Debug.Log("-----modifieddate :" + Me.modifieddate);
		Debug.Log("-------campany_id :" + Me.campany_id);
		Debug.Log("------mst_area_id :" + Me.mst_area_id);
		Debug.Log("------------shops :" + Me.shops);
		Debug.Log("/////////////////////////////////////");


	}

	void OnPressUpdateButton()
	{

		string encodedPw = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);

		Hashtable param_stafflist = new Hashtable ();

		param_stafflist.Add (Cfg.App.keyID, Me.id);
		param_stafflist.Add (Cfg.App.keyNAME, Me.name);
		param_stafflist.Add (Cfg.App.keyLOGIN, Me.login);
		param_stafflist.Add (Cfg.App.keyMAIL, Me.mail);
		param_stafflist.Add (Cfg.App.keyPW, Me.password);
		param_stafflist.Add (Cfg.App.keySHOP, Me.shops);
		param_stafflist.Add (Cfg.App.keyBLOCK, Me.mst_area_id);
		param_stafflist.Add (Cfg.App.keyCOMPANY, Me.campany_id);
		param_stafflist.Add (Cfg.App.keyTYPE, Me.type);

		Mskg.API.Instance.call (
			Cfg.API.updateStaff + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						//うまくいかないのでGAMEオブジェクト名で探すことにした。
						GameObject gsc = GameObject.Find("Canvas_1_1_1_1 getStaffList");
						if (gsc){
							M1_1_1_1 m111 = gsc.GetComponent<M1_1_1_1>();
							if (m111)
								m111.updateMainCanvas(null); //データのリロード
						}
					} else {
						//エラーが起こった時に最初に入力されたデータに戻す

						if(Mode == 1){
							//編集モードの場合
							RevertData();
						}

					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}


	void insertStaff(){


		string encodedPw = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);

		Hashtable param_stafflist = new Hashtable ();

//		param_stafflist.Add (Cfg.App.keyID, Me.id);
		param_stafflist.Add (Cfg.App.keyNAME, Me.name);
		param_stafflist.Add (Cfg.App.keyLOGIN, Me.login);
		param_stafflist.Add (Cfg.App.keyMAIL, Me.mail);
		param_stafflist.Add (Cfg.App.keyPW, Me.password);
		param_stafflist.Add (Cfg.App.keySHOP, Me.shops);
		param_stafflist.Add (Cfg.App.keyBLOCK, Me.mst_area_id);
		param_stafflist.Add (Cfg.App.keyCOMPANY, Me.campany_id);
		param_stafflist.Add (Cfg.App.keyTYPE, Me.type);

		Mskg.API.Instance.call (
			Cfg.API.insertStaff + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) { //OnPressUpdateButton()と同様に

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						//うまくいかないのでGAMEオブジェクト名で探すことにした。
						GameObject gsc = GameObject.Find("Canvas_1_1_1_1 getStaffList");
						if (gsc){
							M1_1_1_1 m111 = gsc.GetComponent<M1_1_1_1>();
							if (m111)
								m111.updateMainCanvas(null); //データのリロード
						}
					} else {
						//エラーが起こった時に最初に入力されたデータに戻す

						if(Mode == 1){
							//編集モードの場合
							RevertData();
						}

					}

//					this.gameObject.SetActive (false);
				}
			}
		);

	}


	void RevertData(){

		MeBase.campany_id = MeBuckup.campany_id;
		MeBase.id = MeBuckup.id;
		MeBase.login = MeBuckup.login;
		MeBase.mail = MeBuckup.mail;
		MeBase.modifieddate = MeBuckup.modifieddate;
		MeBase.mst_area_id = MeBuckup.mst_area_id;
		MeBase.name = MeBuckup.name;
		MeBase.password = MeBuckup.password;
		MeBase.shops = MeBuckup.shops;
		MeBase.type = MeBuckup.type;


	}


	//パスワードをランダムに生成して入力
	public void onMakeRandomPassword(){
		int pass_num = Cfg.App.RANDOM_PASSWORD_NUM;
		txtPw.text = Mskg.API.Instance.randomPass(pass_num, pass_num);

		OnImputFieldChangeText();

	}
}
