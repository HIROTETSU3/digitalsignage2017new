﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SlideValueChanegePanel : ViewController {

	private static GameObject prefab = null;	// ビューのプレハブを保持

	[SerializeField] Text txtTitle;
	[SerializeField] Text txtSubtitle;
	[SerializeField] Text txtDuration;

	[SerializeField] InputField txtComment;
	[SerializeField] Slider ddDuration;

	private SrcListItemTableViewCell myCell;
	private SrcListItemTableViewController myTable;

	public static SlideValueChanegePanel Show(SrcListItemTableViewCell cell, SrcListItemTableViewController tbl){

		//スライドのキャプションや秒数を設定するダイアログです。

		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("SlideValueChangePanel") as GameObject;
		}

		GameObject obj = Instantiate(prefab) as GameObject;
		SlideValueChanegePanel SlideValueChangeBoardView = obj.GetComponent<SlideValueChanegePanel>();
		SlideValueChangeBoardView.UpdateContent(cell,tbl);

		return SlideValueChangeBoardView;


	}


	public void UpdateContent(SrcListItemTableViewCell cell, SrcListItemTableViewController tbl){

		myCell = cell;
		myTable = tbl;

		int d = cell.MyItemData.duration;
		string str = cell.MyItemData.caption;

		txtComment.text = str;
		ddDuration.value = (float)d;

		txtDuration.text = d+"秒";


	}


	public void OnSliderChange(Slider sl){



		txtDuration.text = (int)sl.value+"秒";

	}

	public void Dismiss()
	{
		Destroy(gameObject);
	}

	public void OnPressOK()
	{
		//閉じるときはこれを呼び出してください。

		myCell.MyItemData.duration = (int)ddDuration.value;
		myCell.MyItemData.caption = txtComment.text;


		myTable.OnUpdateCellData(myCell);

		Dismiss();
	}

	public void OnPressCancel()
	{
		//閉じるときはこれを呼び出してください。
		Dismiss();
	}


}
