﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class TextChangeDlogOption{

	public string cancelButtonTitle;			// キャンセルボタンのタイトル
	public System.Action cancelButtonDelegate;	// 〃を押したときに実行されるデリゲート
	public string okButtonTitle;				// OKボタンのタイトル
	public System.Action okButtonDelegate;		// 〃を押したときに実行されるデリゲート

}

public class TextChangeDlogUIController : ViewController {

	[SerializeField] InputField MyText;
	[SerializeField] Text Title;
	[SerializeField] Text Comment;

	[SerializeField] Button cancelButton;
	[SerializeField] Button okButton;


	[SerializeField] Text cancelButtonLabel;
	[SerializeField] Text okButtonLabel;

	string DefString;

	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	private static GameObject prefab = null;


	public static TextChangeDlogUIController Show(
		string title, string message, string DefaultString, TextChangeDlogOption options=null)
	{
		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("TextChangeDlog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		TextChangeDlogUIController TxChangeView = obj.GetComponent<TextChangeDlogUIController>();
		TxChangeView.UpdateContent(title, message, DefaultString,options);

		return TxChangeView;
	}


	public void UpdateContent(
		string title, string message, string DefaultString, TextChangeDlogOption options=null)
	{
		// タイトルとメッセージを設定する
		Title.text = title;
		Comment.text = message;
		DefString = MyText.text = DefaultString;

		if(options != null)
		{
			// 表示オプションが指定されている場合、オプションの内容に合わせてボタンを表示/非表示する
			cancelButton.transform.parent.gameObject.SetActive(
				options.cancelButtonTitle != null || options.okButtonTitle != null
			);

			cancelButton.gameObject.SetActive(options.cancelButtonTitle != null);
			cancelButtonLabel.text = options.cancelButtonTitle ?? "";
			cancelButtonDelegate = options.cancelButtonDelegate;

			okButton.gameObject.SetActive(options.okButtonTitle != null);
			okButtonLabel.text = options.okButtonTitle ?? "";
			okButtonDelegate = options.okButtonDelegate;
		}
		else
		{
			// 表示オプションが指定されていない場合、デフォルトのボタン表示にする
			cancelButton.gameObject.SetActive(false);
			okButton.gameObject.SetActive(true);
			okButtonLabel.text = "OK";
		}

		okButton.interactable = false;

	}

	public void TextChange(){
		//改行が入ればスキップする
		string[] fieldTxs = MyText.text.Split("\n"[0]);
		// Line Feedを削除
		string fData = "";
		foreach (string s in fieldTxs){
			fData = fData +s;
		}
		string[] fieldTxs2 = fData.Split("\r"[0]);

		fData = "";
		foreach (string s in fieldTxs){
			fData = fData +s;
		}


		MyText.text = fData;


		okButton.interactable = (DefString != fData && fData != "");
	}


	// アラートビューを閉じるメソッド
	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{




		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{
		//ここで変更したテキストをどこで受け渡すかよくわかってないのでGlobalを使います。
		//呼び出し側で受け取ってください。
		Global.Instance.TempText = MyText.text;

		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();
	}


}
