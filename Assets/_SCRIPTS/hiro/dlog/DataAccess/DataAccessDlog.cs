﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// データをアクセスしているふう（実際のコールバックは受け取っていません）演出用 Dlogです。

public class DataAccessDlogOptions
{
	public System.Action closeDelegate;	// 〃を押したときに実行されるデリゲート
}


public class DataAccessDlog : ViewController {

	[SerializeField] Text CommentLabel;
	private static GameObject prefab = null;	// アラートビューのプレハブを保持
	private System.Action closeDelegate;	// 終了する時に実行するアクション
	private float ShowTime = 0.5f;
	private string org_message;

	[SerializeField] GameObject Circle;
	[SerializeField] Image CircleValue;

	public static DataAccessDlog Show(
		string message, float sTime = 0f ,DataAccessDlogOptions options = null)
	{
		DataAccessDlog dlog = setPrefab();
		dlog.UpdateContent(message,sTime,options);
		return dlog;
	}

	private static DataAccessDlog setPrefab(){

		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("DataAccess") as GameObject;
		}

		// プレハブをインスタンス化
		GameObject obj = Instantiate(prefab) as GameObject;

		DataAccessDlog DataAccessDlogView = obj.GetComponent<DataAccessDlog>();
		return DataAccessDlogView;
	}


	public void UpdateContent(
		string message, float sTime,DataAccessDlogOptions options)
	{

		Circle.SetActive(false);
		CircleValue.fillAmount = 0f;

		if (options != null){
			closeDelegate = options.closeDelegate;
		} else {
			closeDelegate = null;
		}
		ShowTime = sTime;
		CommentLabel.text = message;
		org_message = message;

		if (ShowTime != 0f) {
			Invoke ("CloseDlog", sTime);
		}
	}

	void CloseDlog(){
		if (closeDelegate != null){
			closeDelegate.Invoke();
		}
		Dismiss();
	}

	public void Dismiss()
	{
		Destroy(gameObject);
	}

	public void progress(int value){
		CommentLabel.text = org_message + " : " + value + "%";


		Circle.SetActive(true);
		CircleValue.fillAmount = (value / 100f);
	}
}
