﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SelectDlogOptions{


	public string TitleString = "";

	public string cancelBtnLabel = "Cancel";
	public string okBtnLabel = "Ok";
	public System.Action cancelButtonDelegate = null;	// Cancelを押したときに実行されるデリゲート
	public System.Action okButtonDelegate = null;	// Okを押したときに実行されるデリゲート


}

public class SelectChannellViewController : ViewController {


	[SerializeField] Button cancelButton;
	[SerializeField] Button okButton;

	[SerializeField] Text Title;
	[SerializeField] Text cancelButtonLabel;
	[SerializeField] Text okButtonLabel;

	[SerializeField] Dropdown ChannelDrop;

	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	private static GameObject prefab = null;
	// Use this for initialization

	public static SelectChannellViewController Show(SelectDlogOptions opt=null){


		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("SelectChannelDlog") as GameObject;
		}

		// プレハブをインスタンス化してアラートビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		SelectChannellViewController SelectChannellDlog = obj.GetComponent<SelectChannellViewController>();
		SelectChannellDlog.UpdateContent(opt);

		return SelectChannellDlog;
	}


	public void UpdateContent(SelectDlogOptions opt=null){

		if (opt != null){
			cancelButtonLabel.text = opt.cancelBtnLabel;
			okButtonLabel.text = opt.okBtnLabel;
			cancelButtonDelegate = opt.cancelButtonDelegate;
			okButtonDelegate = opt.okButtonDelegate;

			Title.text = opt.TitleString;

		}

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{
		
		if(okButtonDelegate != null)
		{
			okButtonDelegate.Invoke();
		}
		Dismiss();

	}


	// DropDown;

	int SelectedChannel = -1;
	public void DropChange(Dropdown d){

		SelectedChannel = d.value;
	}
}
