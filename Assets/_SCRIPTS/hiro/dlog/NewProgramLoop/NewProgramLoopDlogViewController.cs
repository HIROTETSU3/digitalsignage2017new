﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewProgramLoopDlogViewOptions{

	public string LoopTitle = "";
	public string LoopSubtitle = "";
	public string LoopMemo = "";

	public System.Action cancelButtonDelegate;	// Cancelを押したときに実行されるデリゲート
	public System.Action okButtonDelegate;	// Okを押したときに実行されるデリゲート

}

public class NewProgramLoopDlogViewController : ViewController {


	[SerializeField] Text TitleLabel;	
	[SerializeField] Text CommentLabel;	

	[SerializeField] InputField txtName;
	[SerializeField] Text NameData;

	[SerializeField] InputField txtSub;
	[SerializeField] Text SubData;

	[SerializeField] InputField txtMemo;
	[SerializeField] Text MemoData;


	[SerializeField] Button OkButton;
	[SerializeField] Text OkButtonLabel;

	[SerializeField] Button CancelButton;
	[SerializeField] Text CancelButtonLabel;


	private static GameObject prefab = null;	// ユーザー設定ビューのプレハブを保持

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private ClassBox.SimpleProgram MyProgram;
	private ClassBox.SimpleProgram pBackUp;
	private int Mode;


	private System.Action cancelButtonDelegate;	// キャンセルボタンを押したときに
	// 実行されるデリゲートを保持
	private System.Action okButtonDelegate;		// OKボタンを押したときに
	// 実行されるデリゲートを保持

	public static NewProgramLoopDlogViewController Show(int mode, NewProgramLoopDlogViewOptions opt = null){
		
		if(prefab == null){
			prefab  = Resources.Load("Canvas_New_ProgramLoop") as GameObject; 
		}

		// プレハブをインスタンス化してビューを表示する
		GameObject obj = Instantiate(prefab) as GameObject;
		NewProgramLoopDlogViewController NewProgramLoopView = obj.GetComponent<NewProgramLoopDlogViewController>();

		NewProgramLoopView.UpdateContent(mode,opt);

		return NewProgramLoopView;

	}


	NewProgramLoopDlogViewOptions MyOption;

	void UpdateContent(int mode, NewProgramLoopDlogViewOptions opt = null){

		MyOption = opt;

		okButtonDelegate = opt.okButtonDelegate;
		cancelButtonDelegate = opt.cancelButtonDelegate;


		Mode = mode;

		TitleLabel.text = "新規 番組ループを作成";
		CommentLabel.text = "番組ループはカレンダーのその一日に設定される番組のセットです。\n"
			+ "わかりやすいタイトルと、解説を設定してください。";

		if (opt != null){

			txtName.text = NameData.text = opt.LoopTitle;
			txtSub.text = SubData.text = opt.LoopSubtitle;
			txtMemo.text = MemoData.text = opt.LoopMemo;

		}


		if (Mode == 0){
			//データを表示するだけ

			txtName.gameObject.SetActive(false);
			txtSub.gameObject.SetActive(false);
			txtMemo.gameObject.SetActive(false);

			NameData.gameObject.SetActive(true);
			SubData.gameObject.SetActive(true);
			MemoData.gameObject.SetActive(true);


		} else if (Mode == 1){
			//データの編集


			txtName.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);

			NameData.gameObject.SetActive(false);
			SubData.gameObject.SetActive(false);
			MemoData.gameObject.SetActive(false);



		} else if (Mode == 2){
			//新規


			txtName.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);
			txtSub.gameObject.SetActive(true);

			NameData.gameObject.SetActive(false);
			SubData.gameObject.SetActive(false);
			MemoData.gameObject.SetActive(false);


		}



	}


	public void Dismiss()
	{
		Destroy(gameObject);
	}

	// キャンセルボタンが押されたときに呼ばれるメソッド
	public void OnPressCancelButton()
	{
		if(cancelButtonDelegate != null)
		{
			cancelButtonDelegate.Invoke();
		}
		Dismiss();
	}

	// OKボタンが押されたときに呼ばれるメソッド
	public void OnPressOKButton()
	{

		if(okButtonDelegate != null)
		{

			ClassBox.SimpleTodaysLoop t = new ClassBox.SimpleTodaysLoop();

			t.title = txtName.text;
			t.subtitle = txtSub.text;
			t.memo = txtMemo.text;
			Global.Instance.TempSimpleTodaysLoop = t;

			okButtonDelegate.Invoke();
		}
		Dismiss();

	}


}
