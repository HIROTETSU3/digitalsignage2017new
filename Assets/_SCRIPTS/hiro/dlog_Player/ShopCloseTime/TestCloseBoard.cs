﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCloseBoard : MonoBehaviour {

	ShopCloseBoard sc;

	// Use this for initialization
	public void OnPressTest () {
		sc = ShopCloseBoard.Show();

		Invoke("CloseWindow", 10f);

	}

	void CloseWindow(){

		sc.Close();
	}
}
