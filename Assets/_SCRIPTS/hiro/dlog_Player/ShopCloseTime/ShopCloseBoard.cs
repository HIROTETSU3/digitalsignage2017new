﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopCloseBoard : ViewController {

	private static GameObject prefab = null;

	[SerializeField] Image Bg;
	[SerializeField] Image Logo;
	[SerializeField] Text Comment;

	string DefComment;

	void Awake(){

		Bg.color = new Color(0f,0f,0f,0f);
		Logo.color =  new Color(1f,1f,1f,0f);

		DefComment = Comment.text;
		Comment.text = "";

	}

	bool runmode = false;

	public static ShopCloseBoard Show(bool sw = false){
		

		if(prefab == null)
		{
			// プレハブを読み込む
			prefab = Resources.Load("ShopCloseBoard") as GameObject;
		}

		GameObject obj = Instantiate(prefab) as GameObject;
		ShopCloseBoard ShopCloseBoardView = obj.GetComponent<ShopCloseBoard>();
		ShopCloseBoardView.UpdateContent(sw);

		return ShopCloseBoardView;


	}

	public void Dismiss()
	{
		Destroy(gameObject);
	}

	public void Close()
	{
		//閉じるときはこれを呼び出してください。
		Dismiss();
	}

	public void UpdateContent(bool sw){

		runmode = sw;

		Bg.color = new Color(0f,0f,0f,0f);
		Logo.color =  new Color(1f,1f,1f,0f);

		Comment.text = "";

		iTween.ValueTo(gameObject,iTween.Hash(
			"from",0f,
			"to",1f,
			"time",5f,
			"easetype","liner",
			"onupdate","FadeImage",
			"oncomplete","FadeDone"
		));



	}

	void FadeImage(float v){
		Bg.color = new Color(0f,0f,0f,v);
		Logo.color =  new Color(1f,1f,1f,v);

	}

	void FadeDone(){
		
		Comment.text = DefComment;

	}

	public void quitApp(){

		if (runmode){
			//テストで起動
			Dismiss();
		} else {
			Application.Quit();
		}

	}
}
