﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PushText : MonoBehaviour {


	string MyText;
	Text MyField;
	int PushCount = 0;

	//何秒間でプッシュを完了するか
	[SerializeField] float pushTiming = 0.5f;

	// Use this for initialization
	void Awake () {
		MyField = GetComponent<Text>();
		MyText = MyField.text;
	}

	void OnEnable(){

		MyField.text = "";
		PushCount = 0;

		InvokeRepeating("Push",pushTiming/MyText.Length,pushTiming/MyText.Length);

	}

	void OnDisable(){

		MyField.text = "";
		PushCount = 0;
		CancelInvoke("Push");

	}
	
	// Update is called once per frame
	void Push () {
		if (PushCount == MyText.Length){
			CancelInvoke("Push");
		} else {
			if (MyField != null)
				MyField.text = MyText.Substring(0,PushCount+1);
		}
		PushCount++;

	}

	public float time{
		get{
			return pushTiming;
		}
		set {
			pushTiming = value;
		}
	}

	public string text{

		get{
			return MyText;
		}
		set
		{
			CancelInvoke("Push");

			if (MyField != null)
				MyField.text = "";
			MyText = value;
			PushCount = 0;

			InvokeRepeating("Push",pushTiming/MyText.Length,pushTiming/MyText.Length);
		}
	}
}
