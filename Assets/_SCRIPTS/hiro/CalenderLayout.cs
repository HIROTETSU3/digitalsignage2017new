﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CalenderLayout : MonoBehaviour {


	//カレンダーを表示するためのレイアウトです。

	DateTime CurrentDayTime;
	int ThisYear;
	int ThisMonth;
	int ThisDay;

	//↓表示しているカレンダー
	int ViewYear;
	int ViewMonth;

	DayOfWeek CalenderStartDoW;

	[SerializeField] GameObject[] Lines;
	[SerializeField] CalenderCell[] Cells;

	[SerializeField] Text YearTxt;
	[SerializeField] Button ThisMonthBtn;

	//現在選択状態のセル
	public List<CalenderCell> SelectedCells;

	[SerializeField] M1_2_4 m1_2_4;
	[SerializeField] M1_2_4C_Actions m1_2_4c;

	// Use this for initialization
	void Awake(){
		//現在の日付
		CurrentDayTime = DateTime.Now;
		ThisYear = CurrentDayTime.Year;
		ThisMonth = CurrentDayTime.Month;
		ThisDay = CurrentDayTime.Day;


		DateTime ThisMonthStartDay = new DateTime(ThisYear,ThisMonth,1);
		CalenderStartDoW = ThisMonthStartDay.DayOfWeek;

		Debug.Log("DAY "+ThisMonth + "/"+ThisDay + " " + CalenderStartDoW.GetTypeCode());


		//6行文すべてのセル
		Cells = new CalenderCell[42];

		//選択状態のセルの初期化
		SelectedCells = new List<CalenderCell>();

		//編集ダイアログの非表示
		SelectionEditPanel.SetActive(false);

	}

	void Start () {

		//StartでCellSetupが各セルから呼ばれるのでそれが終了したあと、現在のカレンダーを表示する。

	}

	/*****************************
	 * カレンダーの表示に関わるもの
	 ***************************** */
	public void CellSetup(int n,CalenderCell c){

		Cells[n] = c;

		foreach(CalenderCell cell in Cells){
			if (cell == null)
				return;
		}

		//まずは現在のカレンダーを表示します。（ここで表示しないでもいいかも）
		MakeCell();

	}

	public void NextCalender(){


		ViewMonth++;
		if (ViewMonth==13){
			ViewMonth = 1;
			ViewYear++;
		}


		MakeCell(ViewYear,ViewMonth);
	}
	public void PrevCalender(){
		ViewMonth--;
		if (ViewMonth==0){
			ViewMonth = 12;
			ViewYear--;
		}

		MakeCell(ViewYear,ViewMonth);

	}

	public void NowCalender(){
		MakeCell();
	}

	public void ThisCalender(){
		MakeCell(ViewYear,ViewMonth);
	}

	void MakeCell(int y = -1,int m = -1){

		//選択セルの解除
		AllSelectionOff();

		if(y==-1)
			y = ThisYear;

		if(m==-1)
			m = ThisMonth;

		ViewYear = y;
		ViewMonth = m;
		ViewLastYear = y;
		ViewLastMonth = m-1;
		ViewNextYear = y;
		ViewNextMonth = m+1;

		if (ViewLastMonth == 0){
			ViewLastMonth = 12;
			ViewLastYear = y-1;
		}

		if (ViewNextMonth == 13){
			ViewNextMonth = 1;
			ViewNextYear = y+1;
		}


		Debug.Log(ViewYear+"年"+ViewMonth+"月 を表示");

		//表示月の日数
		mDayNum = 31;

		if(ViewMonth == 2){
			if (DateTime.IsLeapYear(ViewYear)){
				mDayNum = 29;
			} else {
				mDayNum = 28;
			}
		} else if (ViewMonth == 4 || ViewMonth == 6 || ViewMonth == 9 || ViewMonth == 11){
			mDayNum = 30;
		}

		//前月の日数
		mLastMonthDayNum = 31;

		if(ViewLastMonth == 2){
			if (DateTime.IsLeapYear(ViewLastYear)){
				mLastMonthDayNum = 29;
			} else {
				mLastMonthDayNum = 28;
			}
		} else if (ViewLastMonth == 4 || ViewLastMonth == 6 || ViewLastMonth == 9 || ViewLastMonth == 11){
			mLastMonthDayNum = 30;
		}

		DateTime ViewMonthStartDay = new DateTime(ViewYear,ViewMonth,1);
		CalenderStartDoW = ViewMonthStartDay.DayOfWeek;

		d1 = (int)CalenderStartDoW;
		//この月のカレンダーの1日が何曜日から始まっているかを取得

		//6段になる場合以外は6段目を消す。 2017-07
		//4段（2月はありうる）はとりあえず考えない

		Lines[5].SetActive(d1 + mDayNum > 35);

		UpdateCalender ();

		m1_2_4.callBackFromCalender (new DateTime[] { Cells [0].MyDateTime, Cells [41].MyDateTime });
		//return;

	}

	int d1;
	int mLastMonthDayNum;
	int ViewLastYear;
	int ViewLastMonth;
	int ViewNextYear;
	int ViewNextMonth;
	int mDayNum;

	public void UpdateCalender(){
		int tempDayNum;
		foreach (CalenderCell cell in Cells){
			if (cell.ID < d1){
				//前月
				tempDayNum = mLastMonthDayNum - d1+cell.ID+1;
				cell.SetUp(true,new DateTime(ViewLastYear,ViewLastMonth,tempDayNum));

//				Debug.Log(ViewLastYear+"/"+ViewLastMonth+"/"+tempDayNum);
			} else if (cell.ID >= mDayNum+d1){
				//翌月
				tempDayNum = (cell.ID-d1)+1-mDayNum;
				cell.SetUp(true,new DateTime(ViewNextYear,ViewNextMonth,tempDayNum));
//				Debug.Log(ViewNextYear+"/"+ViewNextMonth+"/"+tempDayNum);

			} else {
				cell.SetUp(false,new DateTime(ViewYear,ViewMonth,cell.ID-d1+1));
//				Debug.Log(ViewYear+"/"+ViewMonth+"/"+(cell.ID-d1+1));
			}

		}


		if (ViewMonth < 10){
			YearTxt.text = ViewYear+" 0"+ViewMonth;
		} else {
			YearTxt.text = ViewYear+" "+ViewMonth;
		}


		ThisMonthBtn.interactable = !(ViewYear == ThisYear && ViewMonth == ThisMonth);
	}


	/*****************************
	 * セルの選択状態に関わるもの
	 ***************************** */

	[SerializeField] Button SelectionSetUpBtn;
	[SerializeField] Button SelectionOffBtn;

	public void SelectCell(bool isSelect, CalenderCell ClickedCell, bool isAppend){

		if(!isAppend){
			//シフトを押さずクリック

			if(isSelect){
				SelectChange(ClickedCell);
			} else {
				//これだけ外す
				RemoveCell(ClickedCell);
			}

		} else {

			if (isSelect){
				//重複がないか念のため調査
				foreach (CalenderCell cell in SelectedCells){
					if (cell == ClickedCell)
						return;
				}
				SelectedCells.Add(ClickedCell);
			} else {

				//シフトを押しながらこれだけ外す
				RemoveCell(ClickedCell);
			}
		}

		Debug.Log("現在選択されているCELLは " + SelectedCells.Count + "個");

		//項目設定ボタンの表示非表示
		BtnUpdate();
	}

	void BtnUpdate(){
		
		SelectionSetUpBtn.interactable = (SelectedCells.Count > 0);
		SelectionOffBtn.interactable = (SelectedCells.Count > 0);

	}

	void AllSelectionOff(){
		foreach (CalenderCell cell in SelectedCells){
			cell.OffSelect();
		}

		//選択状態のセルの初期化
		SelectedCells = new List<CalenderCell>();
		BtnUpdate();
	}

	void SelectChange(CalenderCell newSelect){

		foreach (CalenderCell cell in SelectedCells){
			if (cell != newSelect)
				cell.OffSelect();
		}

		//選択状態のセルの初期化
		SelectedCells = new List<CalenderCell>();
		SelectedCells.Add(newSelect);

	}

	void RemoveCell(CalenderCell removeCell){
		int i = 0;
		foreach (CalenderCell cell in SelectedCells){
			if (cell == removeCell){
				SelectedCells.RemoveAt(i);
				return;
			}
			i++;
		}

	}

	public void SelectionOff(){
		//SelectionOffBtnから
		AllSelectionOff();
	}

	public void AllSelect(){
		//SelectAllBtnから
		//隠し機能（Shiftを押しながらで今月だけでなく全部選択）
		bool isShift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

		//一度初期化して
		SelectedCells = new List<CalenderCell>();
		foreach (CalenderCell cell in Cells){
			if (cell.MyDateTime.Month == ViewMonth || isShift){
				CalenderCell c = cell.GetComponent<CalenderCell>();
				SelectedCells.Add(c);
				c.OnSelect();
			}
		}


	}


	/*****************************
	 * 選択したセルの編集に関わるもの
	 ***************************** */
	[SerializeField] GameObject SelectionEditPanel;

	public void SelectDlogOpen(){


		AlertViewOptions opt = new AlertViewOptions();
		opt.okButtonTitle = "OK";

		//AlertViewController.Show("【開発中】","この先のダイアログ、デザイン修正予定です。007にて対応します。",opt);


		m1_2_4c.onOpenPanel (null);
		SelectionEditPanel.SetActive(true);
	}

	public void SelectDlogClose(){

		SelectionEditPanel.SetActive(false);
	}



	// Update is called once per frame
	void Update () {
		
	}


	public void RestartStick(){

		RestartStickViewController.Show();

	}

}
