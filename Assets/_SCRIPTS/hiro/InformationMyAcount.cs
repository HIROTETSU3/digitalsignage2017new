﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationMyAcount : MonoBehaviour {


	[SerializeField] Text txtName;
	[SerializeField] Text txtLogin;
	[SerializeField] Text txtTime;
	[SerializeField] Text txtShop;
	[SerializeField] Text txtMail;
	[SerializeField] Text txtBlock;
	[SerializeField] Text txtCompany;
	[SerializeField] Text txtAuthority;

	// Use this for initialization
	void OnEnable () {


		txtName.text = "";
		txtLogin.text = "";
		txtShop.text = "";
		txtBlock.text = "";
		txtMail.text = "";
		txtCompany.text = "";
		txtAuthority.text = "";
		txtTime.text = "";
		//Show();
		Invoke("Show",0.1f);

	}

	void OnDisable(){

		txtName.text = "";
		txtLogin.text = "";
		txtShop.text = "";
		txtBlock.text = "";
		txtMail.text = "";
		txtCompany.text = "";
		txtAuthority.text = "";
		txtTime.text = "";


	}

	void Show(){


		if (!Global.Instance.initLoaded) {
			Invoke("Show",0.3f);

		} else {


			TxPush(txtName,Global.Instance.Me.name);
			TxPush(txtLogin,Global.Instance.Me.login);
			TxPush(txtMail,Global.Instance.Me.mail);
			TxPush(txtShop,Mskg.API.Instance.getShopByShopID(Global.Instance.Me.shops).name);
			TxPush(txtBlock,Mskg.API.Instance.getAreaByID (Global.Instance.Me.mst_area_id).name);
			TxPush(txtCompany,Mskg.API.Instance.getCampanyByID(Global.Instance.Me.campany_id).name);

			string[] types = new string[]{"システム管理者","管理者","ブロックユーザー","コーポレートユーザー","店舗ユーザー"};

			//パスワードの期限の計算
			System.DateTime d = System.DateTime.Parse(Global.Instance.Me.modifieddate);

			System.DateTime mDay = new System.DateTime(d.Year,d.Month,d.Day);
			System.TimeSpan l = new System.TimeSpan(90,0,0,0,0);
			System.DateTime eDay = mDay+l;

			//期限日まであと何日?廣　作成中
			System.DateTime now = System.DateTime.Now;
			//int limitDays = (now - 


			string mDayString = eDay.Year+"年"+eDay.Month+"月"+eDay.Day+"日 (登録日時 : "+ Global.Instance.Me.modifieddate+")";

			TxPush(txtAuthority,types[Global.Instance.Me.type]);
			TxPush(txtTime,mDayString);
		}
	}

	void TxPush(Text t, string str){


			//1文字ずつ表示する奴付けてみた。
		PushText p = t.gameObject.GetComponent<PushText>();
		if (p != null){
			p.time = str.Length*0.02f;
			p.text = str;
		} else{
			p = t.gameObject.AddComponent<PushText>();
			if (p != null){
				p.time = str.Length*0.02f;
				p.text = str;
			} else {
				t.text = str;
			}
		}

	}
}
