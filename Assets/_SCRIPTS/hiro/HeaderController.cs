﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderController : MonoBehaviour {

	[SerializeField] Text VersionNo;
	// Use this for initialization
	void Start () {

		VersionNo.text = "ver. "+Cfg.App.BUILD;

	}

}
