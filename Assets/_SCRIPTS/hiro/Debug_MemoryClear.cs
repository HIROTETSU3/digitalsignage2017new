﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Debug_MemoryClear : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("JumpMemoryClear",40f);
	}

	void JumpMemoryClear () {

		Resources.UnloadUnusedAssets();

		int s = SceneManager.sceneCount;

		for (int i=s-1;i>-1;i--){
			Debug.Log("SCENE "+ SceneManager.GetSceneAt(i).name);
			//if(i>0)
			//	SceneManager.UnloadSceneAsync(i);
		}

		SceneManager.LoadScene("MemoryChkDev",LoadSceneMode.Single);

	}
}
