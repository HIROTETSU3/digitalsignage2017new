﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_1_4 : MainCanvas {

	[SerializeField] public SaveFile saveFile;

	// ナビゲーションビューを保持

	private Hashtable memoriedParam;
	//	bool programInitStandBy = false;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;
		
		#region アイテム一覧画面をナビゲーションビューに対応させる
		//		if(navigationView != null)
		//		{
		//			navigationView.Push(mA);
		//		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

	}


	void ShowAc(){

		DataAccessDlog.Show("パスワードの更新中",1f);


	}

	public void OnPressRunningLog(){
		//仮
		AlertViewController.Show ("【開発中】", "対応までしばらくお待ち下さい。（008対応予定）", 
			new AlertViewOptions {
				cancelButtonTitle = null,
				cancelButtonDelegate = null,
				okButtonTitle = "OK",
				okButtonDelegate = () => {
					Debug.Log("OK");
				},
			});
		return;

	}

	public void OnPressLogCompany(){
		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

					DoExportToCSV("Campany");
//					List<ClassBox.CampanyData> campanyclasses = new List<ClassBox.CampanyData>();
//					campanyclasses.AddRange(Global.Instance.CampanyList);
//					tableview.UpdateData(campanyclasses);

				}

			}
		);
	}

	public void OnPressLogShop(){


		//ショップリスト取得
		Hashtable param_shop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_shop.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllShop + ".php",
			param_shop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.ShopData[] staffList = null;
					try
					{
						Global.Instance.ShopList =  LitJson.JsonMapper.ToObject<ClassBox.ShopData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ShopList);
					Debug.Log("////////////");

					DoExportToCSV("Shop");
//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

//					List<ClassBox.ShopData> shopclasses = new List<ClassBox.ShopData>();
//					shopclasses.AddRange(Global.Instance.ShopList);
//					tableview.UpdateData(shopclasses);

				}

			}
		);
	}


	public void DoExportToCSV(string arg){

		string str = "";
		if (arg == "Shop") {

			str = "" +
				"\"id\"," +
				"\"名称\"," +
				"\"ShopID\"," +
				"\"エリア\"," +
				"\"会社\"\n"
				;
			foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
				string _Campany = Mskg.API.Instance.getCampanyByID(shop.campany_id).name;
				string _Area = Mskg.API.Instance.getAreaByID(shop.mst_area_id).name;

				string s = "";
				s += shop.id + ",";
				s += "\"" + shop.name + "\",";
				s += "\"" + shop.shopid + "\",";
				s += "\"" + _Area + "\",";
				s += "\"" + _Campany + "\"\n";
				str += s;
			}

		} else if (arg == "Campany") {

			str = "" +
				"\"id\"," +
				"\"名称\"\n"
				;
			foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
				string s = "";
				s += campany.id + ",";
				s += "\"" + campany.name + "\"\n";
				str += s;
			}

		}

		saveFile.openSaveDialog (str);
	}

}
