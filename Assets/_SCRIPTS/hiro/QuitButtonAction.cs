﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class QuitButtonAction : MonoBehaviour {

	// Use this for initialization
	void Start () {


		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPressQuit(){

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonDelegate = () => {
			// Nothing
		};
		opt.okButtonDelegate = () => {
			// Quit;
			Quit();
		};
		opt.cancelButtonTitle = "キャンセル";
		opt.okButtonTitle = "終了";

		AlertViewController.Show("アプリケーションを終了します。","よろしいですか?",opt);


	}

	void Quit(){

//		#if UNITY_EDITOR
//		EditorApplication.isPlaying = false;
//		#elif
		Application.Quit();
//		#endif
	}
}
