﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CalenderCell : MonoBehaviour {


	[SerializeField] public int ID;
	[SerializeField] Sprite ImageActive;
	[SerializeField] Sprite ImageDeActive;
	[SerializeField] Sprite ImageSelected;
	Image Bg;
	Image ProgramBg;

	[SerializeField] M1_2_4 m1_2_4;

	//日
	Text DateTxt;

	//プログラムLOOP ID or "BASE"
	Text ProframLoopNumber;
	//プログラムループのタイトル
	Text Comment;

	//特番がある場合の表示アイコン
	GameObject SpIcon;


	//選択状態のための青フレーム
	GameObject IsSelectionFrame;

	CalenderLayout CalenderManager;

	ImageFade ProgramBgIf;

	public DateTime MyDateTime;

	// Use this for initialization
	void Awake(){
		Bg = GetComponent<Image>();
		Bg.sprite = ImageDeActive;
	}

	void Start () {
		
		DateTxt = transform.Find("DateTxt").gameObject.GetComponent<Text>();
		ProframLoopNumber = transform.Find("ProgramObj/Tx_ProgramNum").gameObject.GetComponent<Text>();
		Comment = transform.Find("ProgramObj/Tx_Caption").gameObject.GetComponent<Text>();
		CalenderManager = transform.parent.parent.gameObject.GetComponent<CalenderLayout>();

		SpIcon = transform.Find("Sp").gameObject;
		SpIcon.SetActive(false);

		IsSelectionFrame = transform.Find("IsSelection").gameObject;
		IsSelectionFrame.SetActive(false);

		ProgramBg = transform.Find("ProgramObj").gameObject.GetComponent<Image>();
		ProgramBg.color = new Color(1f,1f,1f,1f);

		ProgramBgIf = transform.Find("ProgramObj").gameObject.GetComponent<ImageFade>();

		CalenderManager.CellSetup(ID,this);

		ProframLoopNumber.text = "BASE";
		Comment.text = "";
		ProframLoopNumber.color = new Color(0f,0f,0f,0.2f);

	}
	
	// このセルに日付を設定する
	public void SetUp (bool isActive, DateTime d) {

		MyDateTime = d;

		if (isActive){
			//先月か来月
			Bg.sprite = ImageDeActive;
			DateTxt.color = new Color(0f,0f,0f,0.1f);

			//日付が設定された後、この日のプログラムループの設定を受け取ってください。（どこから？）
			GetMyProgramLoops(false);

		} else {
			Bg.sprite = ImageActive;
			int DoW = (int)d.DayOfWeek;
			if(DoW == 0){
				//日曜日
				DateTxt.color = new Color(0.7f,0.3f,0f,0.2f);
			} else if (DoW == 6){
				//土曜日
				DateTxt.color = new Color(0.3f,0.5f,1f,0.2f);
			} else {
				DateTxt.color = new Color(0f,0f,0f,0.2f);
			}


			//日付が設定された後、この日のプログラムループの設定を受け取ってください。（どこから？）
			GetMyProgramLoops(true);
		}

		DateTxt.text = ""+d.Day;

	}


	bool isSelection = false;

	void SetSelection(bool tf){
		isSelection = tf;
		IsSelectionFrame.SetActive(tf);
		//CalenderLayout に自分が選択された非選択可を報告

		//Shift Keyを押していたかどうかで分岐
		bool isShift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
		CalenderManager.SelectCell(tf,this,isShift);

	}

	public void ClickCell(){
		//このセルをクリックした時の処理

			
		if (isSelection){
			//選択を解除

			SetSelection(false);
		} else {
			SetSelection(true);

		}

	}

	public void OffSelect(){
		isSelection = false;
		IsSelectionFrame.SetActive(false);
	}

	public void OnSelect(){
		isSelection = true;
		IsSelectionFrame.SetActive(true);
	}


	public void GetMyProgramLoops(bool isThisMonth){

		//プログラムループ番号のカラー（今月かどうか isThisMonth により色の定数変更しています）
		Color NumberColor = (isThisMonth)?new Color(0f,0f,0f,0.5f):new Color(0f,0f,0f,0.1f);
		Color ProgramBgIfColor = (isThisMonth)?new Color(1f,1f,1f,1f):new Color(1f,1f,1f,0.2f);
		Color ProgramBgIfBaseColor = new Color(0.2f,1f,0.2f,0.2f);



		//どこからデータを受け取るか？お願いします
		ClassBox.CalenderItem _item = m1_2_4.getCalenderItem(MyDateTime.ToString(Cfg.App.DATE_FORMAT));

		//デモ（ランダムに番組を設定している風に表示）//////// 以下ランダムに表示するダミーです
		//if (UnityEngine.Random.Range(0,1.0f)<0.3){
		if (_item != null){
			//番組が設定されていた場合の表示は以下２つのフィールドの入力と
			//文字と背景プレートの色設定

			ProframLoopNumber.text = _item.loopset.id.ToString(); //"24";
			Comment.text = _item.loopset.title; //"スズカ8時間耐久＆秋のツーリング、CBR250RR他";
			ProframLoopNumber.color = NumberColor;
			ProgramBgIf.ColorUpdate(ProgramBgIfBaseColor,MyDateTime.Day*0.02f);

		} else {
			//Base番組を表示
			string _base = "";
			if (Global.Instance.CalendarInfoInit) {
				_base = "BASE";
			}
			ProframLoopNumber.text = _base;
			Comment.text = "";
			ProframLoopNumber.color = NumberColor;
			ProgramBgIf.ColorUpdate(ProgramBgIfColor,MyDateTime.Day*0.02f);

		}

//		if (UnityEngine.Random.Range(0,1.0f)<0.05){
//			SpIcon.SetActive(true);
//		} else {
//			SpIcon.SetActive(false);
//		}

		ClassBox.CalenderSpecialProgram _sp = m1_2_4.getCalenderSp(MyDateTime.ToString(Cfg.App.DATE_FORMAT));
		//Debug.Log (_sp);

		if (_sp != null) {
			SpIcon.SetActive (true);
		} else {
			SpIcon.SetActive (false);
		}

		// ダミーここまで


	}
}
