﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillCamera : MonoBehaviour {

	//テスト用のカメラなので実行時 KILL
	void Awake () {
		Camera MyCamera = GetComponent<Camera>();
		MyCamera.enabled = false;
	}
}
