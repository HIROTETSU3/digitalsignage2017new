﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateIcon : MonoBehaviour {

	// ↓ 1回転を何秒で行うか
	[SerializeField] float RotateValue = 10f;
	[SerializeField] bool StartToRotate = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (StartToRotate)
		transform.Rotate(new Vector3(0,0,360/RotateValue*Time.deltaTime));
		
	}

	public void RotateStart(float rotateTime = 0){

		CancelInvoke();

		StartToRotate = true;
		if (rotateTime != 0){
			Invoke("RotateStop",rotateTime);
		}

	}

	public void RotateStop(){
		StartToRotate = false;
	}
}
