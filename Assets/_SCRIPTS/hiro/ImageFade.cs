﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFade : MonoBehaviour {

	Image MyImage;
	[SerializeField,Range(0f, 2.0f)] float FadeTime;

	// Use this for initialization
	void Awake () {
		MyImage = GetComponent<Image>();
	}

	Color SetColor;

	public void ColorUpdate (Color c, float d =0f) {

		iTween.Stop(gameObject);
		SetColor = c;

		MyImage.color = new Color(c.r,c.g,c.b,0f);

		iTween.ValueTo(gameObject,iTween.Hash(
			"delay",d,
			"from",0f,
			"to",c.a,
			"time",FadeTime,
			"easetype","liner",
			"onupdate","FadeImage"
		));

	}

	void FadeImage(float v){

		MyImage.color = new Color(SetColor.r,SetColor.g,SetColor.b,v);

	}
}
