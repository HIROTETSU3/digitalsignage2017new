﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnloadMemories : MonoBehaviour {

	// Use this for initialization
	void Start () {

		Invoke("UnloadMemorie",1f);
		
	}

	void UnloadMemorie () {

		int totalAllocatedMem = (int)UnityEngine.Profiling.Profiler.GetTotalAllocatedMemoryLong();

		Debug.Log("---- > UnloadAssets : totalAllocatedMem="+totalAllocatedMem+"mb");
		Resources.UnloadUnusedAssets();
		totalAllocatedMem = (int)UnityEngine.Profiling.Profiler.GetTotalAllocatedMemoryLong();
		Debug.Log("---- > UnloadAssets2 : totalAllocatedMem="+totalAllocatedMem+"mb");
		
	}
}
