﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoManual : MonoBehaviour {

	public void OnPressManual () {


		switch (Global.Instance.Me.type) {
		case 0: //システム管理者
		case 1: //管理者
			Application.OpenURL(Cfg.App.BASE_URL+"manual/system_doc201710.pdf");
			break;
		case 2: //ブロックユーザ
			Application.OpenURL(Cfg.App.BASE_URL+"manual/block_doc201710.pdf");
			break;
		case 3: //コーポレートユーザ
			Application.OpenURL(Cfg.App.BASE_URL+"manual/corp_doc201710.pdf");
			break;
		case 4: //店舗ユーザ
			Application.OpenURL(Cfg.App.BASE_URL+"manual/shop_document201710.pdf");
			break;
		}


	}
}
