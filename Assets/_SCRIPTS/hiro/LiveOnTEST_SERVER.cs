﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//テストサーバーの設定時のみ生き残ります。

public class LiveOnTEST_SERVER : MonoBehaviour {

	// Use this for initialization
	void Start () {


		#if !TEST_SERVER

		Destroy(gameObject);

		#endif
		
	}

}
