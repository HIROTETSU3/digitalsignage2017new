using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SFB;

[RequireComponent(typeof(Button))]
public class SaveFile : MonoBehaviour, IPointerDownHandler {
    public string Title = "";
    public string Directory = "";
    public string FileName = "";
    public string Extension = "";

    // Sample text data
    public string _data = "";

#if UNITY_WEBGL && !UNITY_EDITOR
    //
    // WebGL
    //
    [DllImport("__Internal")]
    private static extern void DownloadFile(string id, string filename, byte[] byteArray, int byteArraySize);

    // Broser plugin should be called in OnPointerDown.
    public void OnPointerDown(PointerEventData eventData) {
        var bytes = Encoding.UTF8.GetBytes(_data);
        DownloadFile(gameObject.name, FileName + "." + Extension, bytes, bytes.Length);
    }

    // Called from browser
    public void OnFileDownloaded() {
        //
    }
#else
    //
    // Standalone platforms & editor
    //
    public void OnPointerDown(PointerEventData eventData) { }

    // Listen OnClick event in standlone builds
    void Start() {
//        var button = GetComponent<Button>();
//        button.onClick.AddListener(OnClick);
    }

//	public void openSaveDialog(){
//		OnClick();
//	}

    //public void OnClick() {
	public void openSaveDialog(string data) {
		if (data != null) {
			_data = data;
		}
        var path = StandaloneFileBrowser.SaveFilePanel(Title, Directory, FileName, Extension);
        if (!string.IsNullOrEmpty(path)) {
            File.WriteAllText(path, _data);
        }
    }

	public string getSaveDialogPath(string defaultFileName = "data.csv"){
		string _defaultFileName = FileName;
		if (defaultFileName != null) {
			_defaultFileName = defaultFileName;
		}

		string filePath = StandaloneFileBrowser.SaveFilePanel(Title, Directory, _defaultFileName, Extension);

		if (filePath != "") {
			return Path.GetDirectoryName (filePath);
		} else {
			return "";
		}

	}

#endif
}