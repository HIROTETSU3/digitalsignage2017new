﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loginPanel : MonoBehaviour {

	[SerializeField] InputField inputUserID;
	[SerializeField] InputField inputPw;
	[SerializeField] KanriStarter starter;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetKeyDown( KeyCode.Return ) )
			pushButtonGo();
	}

	public void pushButtonGo(){
		Global.Instance.userid = inputUserID.text;
		Mskg.API.Instance.setUserID (Global.Instance.userid);
		string encPw = Mskg.API.Instance.Aes128CBCEncode (inputPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);
		this.gameObject.SetActive (false);
		starter.login (Global.Instance.userid, encPw);
	}

	public void clearPwField(){
		#if TEST_SERVER
		inputPw.text = "11111111"; //test用に"a"を入れてます。
		#endif
	}
}
