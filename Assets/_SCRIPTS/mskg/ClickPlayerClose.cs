﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickPlayerClose : MonoBehaviour, IPointerClickHandler {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerClick (PointerEventData eventData)
	{
//		if( eventData.clickCount > 1 ){
//			Debug.Log(eventData.clickCount);
//		}
		if (Global.Instance.PlayerPreviewMode) {
			Mskg.API.Instance.unloadAllPlayer ();
		} else {

            //この部分はクリックで終了確認ではなく、コンフィグダイアログを出すようなUIに変更します。（タスク）

            AlertViewController.Show ("終了確認", "プレイヤーを終了しますか？\n（自動終了時刻 : " + Global.Instance.closetime.ToString() + "）", 
				new AlertViewOptions {
					cancelButtonTitle = "キャンセル",
					cancelButtonDelegate = () => {
						Debug.Log("キャンセルしました。");
						return;
					},
					okButtonTitle = "はい",
					okButtonDelegate = () => {
						Quit();
					},
				});
		}
	}

	void Quit(){

		//		#if UNITY_EDITOR
		//		EditorApplication.isPlaying = false;
		//		#elif
		Application.Quit();
		//		#endif
	}
}
