﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LitJson;
using System.Text;

public class MainStart : MonoBehaviour {

	[SerializeField] GameObject colorBar;
	[SerializeField] Text userName;
	[SerializeField] Text shopName;
	[SerializeField] Text campanyName;

	private string _shopName = "";
	private string _campanyName = "";
	private string _areaName = "";
	public ClassBox.Staff me; // = Global.Instance.Me;

	private bool initShopLoaded = false;
	private bool initAreaLoaded = false;
	private bool initCampanyLoaded = false;


	// Use this for initialization
	void Start () {
		/*
		Global.Instance.Me = new ClassBox.Staff ();
		Global.Instance.Me.id = 10;
		Global.Instance.Me.type = 0;
		Global.Instance.Me.name = "白石哲也";
		Global.Instance.Me.login = "mushikago";
		Global.Instance.Me.mail = "tshiraishi@mushikago.com";
		Global.Instance.Me.password = "LlvGws8QrpjmTV4xHZRm/g==";
		Global.Instance.Me.modifieddate = "2017-08-05 20:52:25";
		Global.Instance.Me.campany_id = 1;
		Global.Instance.Me.mst_area_id = 1;
		Global.Instance.Me.shops = "12345";
		*/
		me = Global.Instance.Me;

		Global.Instance.mytype = me.type; //Global.Instance.Me.type;

		//バー着色
		Color newCol;
		if (ColorUtility.TryParseHtmlString(Cfg.UI.BAR_COLOR[me.type], out newCol))
			colorBar.GetComponent<Image>().color = newCol;

		//バーに名前とメール
		userName.text = me.name + " | " + me.mail;

		//バーにショップ表示
//		shopName.text = me.shops;

		//AlertViewController.Show (Global.Instance.Me.name + "でログインしました", Global.Instance.Me.mail);

		//ショップリスト取得
		Hashtable param_shop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_shop.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllShop + ".php",
			param_shop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

//					ClassBox.ShopData[] staffList = null;
					try
					{
						Global.Instance.ShopList =  LitJson.JsonMapper.ToObject<ClassBox.ShopData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ShopList);
					Debug.Log("////////////");

					ClassBox.ShopData shop =  Mskg.API.Instance.getShopByShopID(me.shops);
					if (shop != null){
						_shopName = shop.name;
						initShopLoaded = true;
						outputAfterStandby();
					}

				}

			}
		);




		//カンパニーリスト取得
		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

//					foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
//						if (campany.id == me.campany_id){
//							_canpanyName = campany.name;
//							outputAfterStandby();
//						}
//					}

					ClassBox.CampanyData campany =  Mskg.API.Instance.getCampanyByID(me.campany_id);
					if (campany != null){
						_campanyName = campany.name;
						initCampanyLoaded = true;
						outputAfterStandby();
					}

				}

			}
		);


		//エリアリスト取得
		Hashtable param_area = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_area.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllArea + ".php",
			param_area, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.AreaData[] areaList = null;
					try
					{
						Global.Instance.AreaList =  LitJson.JsonMapper.ToObject<ClassBox.AreaData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.AreaList);
					Debug.Log("////////////");

//					foreach (ClassBox.AreaData area in Global.Instance.AreaList) {
//						if (area.id == me.mst_area_id){
//							_areaName = area.name;
//							outputAfterStandby();
//						}
//					}

					ClassBox.AreaData area =  Mskg.API.Instance.getAreaByID(me.mst_area_id);
					if (area != null){
						_areaName = area.name;
						initAreaLoaded = true;
						outputAfterStandby();
					}

				}

			}
		);



	}

	void outputAfterStandby(){
		if (initShopLoaded && initAreaLoaded && initCampanyLoaded) {
			shopName.text = _shopName;
			campanyName.text = _areaName + " : " + _campanyName;
			ClassBox.ShopData theShop = Mskg.API.Instance.getShopAllInfo (me.shops);

//			Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

			Global.Instance.initLoaded = true;
			Debug.Log(theShop);


		}
	}


	// Update is called once per frame
	void Update () {
		
	}


	public void onRestart(){
//		Mskg.API.Instance.restart ();
	}
}
