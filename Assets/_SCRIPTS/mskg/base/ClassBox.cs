﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassBox {

	public enum fileNameX {
		samba,
		lupin,
		bunny
	}

//	[Serializable]
	public class MyClass
	{
		public int level;
		public float timeElapsed;
		public string playerName;
	}
		

	/// <summary>
	/// Get Program
	/// </summary

	public class Src
	{
		public int srcid;
		public string path;
	}

	public class SrcListItem
	{
		public Src src;
		public int duration;
		public string caption;
	}

	public class SrcListItemWithSrc
	{
		public int SrcListItem_id;
		public int Src_id;
		public int duration;
		public string caption;
		public string path;

		public bool IsError(){
			return path == "";
		}

		public bool IsMovie(){
			if (!IsError()){
				string[] filepaths = path.Split("."[0]);
				string ex = filepaths[filepaths.Length-1];
				return (ex == "mp4");

			} else {
				return false;
			}

		}
	}

	public class Program
	{
		public int programid;
		public string created;
		public string title;
		public string subtitle;
		public string bgcolor;
		public int editorid;
		public int basic_page_time;
		public int content_type;
		public int layout_type;
		public int transition_type;
		public SrcListItem[] src_list;
		public int playedid;
	}


	/// <summary>
	/// ShopInit
	/// </summary>
	public class ProgramsListItem
	{
		public int id;
		public Program program;
		public string time;
		public bool done = false;
	}

	public class TodaysLoop
	{
		public int id;
		public ProgramsListItem[] specialprograms;
		public ProgramsListItem[] programs;
		public string created;
	}

	public class UserSettings
	{
		public int id;
		public int shopid;
		public string closetime;
	}
		
	public class ShopSettings
	{
		public TodaysLoop todays_loop;
//		public UserSettings user_setting;
	}

	/// <summary>
	/// ShopPlayerLogin
	/// </summary>
	public class ShopData
	{
		public int id;
		public string shopid;
		public string name;
		public string uuid;
		public int campany_id;
		public int mst_area_id;
		public string campanyName;
		public string areaName;
	}

	public class ShopPlayerLogin
	{
		public bool success;
		public ShopData shop_data;
	}

	/////////// 以下 管理画面用 ////////////
	public class SuccessObj //SQLの成功など
	{
		public bool success;
	}

	public class Staff
	{
		public int id;
		public int type;
		public string name;
		public string login;
		public string mail;
		public string modifieddate;
		public int campany_id;
		public int mst_area_id;
		public string shops;
		public string password;
	}

	public class BuildObject
	{
		public int id;
		public Double build;
		public string filepath;
		public string message;
		public DateTime created;
	}

	public class UserLogin
	{
		public bool success;
		public Staff user;
	}

	public class CampanyData
	{
		public int id;
		public string name;
	}

	public class AreaData
	{
		public int id;
		public string name;
	}

	public class SimpleProgram
	{
		public int programid;
		public string created;

		public string title;
		public string subtitle; //*

		public int layout_type; //*
		public string bgcolor; //*
		public int content_type; //*
		public int transition_type; //*
		public int basic_page_time; //*

		public int editorid;
		public string shopid;
		public int mst_area_id;
		public int campany_id;
		public int broadcast;

		public int src_num;

		public string firstsrc;

		//1.1
		public string category;
		public string releasedate;
		public int playable;

	}



	public class SimpleTodaysLoop
	{
		public int id;
		public string title;
		public string subtitle;
		public string date;
		public string memo;
		public int channel;
	}

	/////////// 以下 カレンダー ////////////

	public class CalenderItem
	{
		public int id;
		public DateTime date;
		public SimpleTodaysLoop loopset;
	}

	public class CalenderSpecialProgram
	{
		public int id;
		public DateTime dateinfo;
		public DateTime timeinfo;
		public SimpleProgram program;
	}

	//// 1.1 ////

	public class DeviceData
	{
		public int id;
		public string name;
		public string uuid;
		public string memo;
		public int channel;
	}

	public class ChannelData
	{
		public int id;
		public string name;
		public string color;
	}

	public class CollectionItem : SimpleProgram{
		public int collectionid;
	}
}
