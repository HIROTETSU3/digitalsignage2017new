﻿using UnityEngine;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Cfg {

	public class MyClass
	{
		public int level;
		public float timeElapsed;
		public string playerName;
	}

	public static class App {
		public const Double BUILD = 0.90;

		public const string TEST = "TEST STRING";
		public static string LOCAL_DIR = Application.persistentDataPath;
		#if UNITY_EDITOR
		public const string BASE_URL = "http://hmjsignage.work/";
		//public const string BASE_URL = "http://ds.d1.hm-japan.com/";

		#elif TEST_SERVER
		public const string BASE_URL = "http://hmjsignage.work/";
		#else
		public const string BASE_URL = "http://ds.d1.hm-japan.com/";
		//		public const string BASE_URL = "http://hornet72.d1.hm-japan.com/";
		//		public const string BASE_URL = "http://mushikago.com/";
		#endif

		public const string API_DIR = "api/";
		public const string ASSETS_DIR = "assets/";
		public const string BUILD_DIR = "builds/";
		public const string THUMBNAIL = "_thumb";
		public const int THUMBNAIL_W = 600;
		public const Environment.SpecialFolder OPEN_DIALOG_DEFAULT_DIR = Environment.SpecialFolder.DesktopDirectory;
		public const int DEFAULT_CHANNEL = 1;

		public const string RANDOM_PASSWORD_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789";
		public const int RANDOM_PASSWORD_NUM = 8; //パスワードの文字数



		//Calender
		public const string DATE_FORMAT = "yyyy-MM-dd";
		public const string TIME_FORMAT = "HH:mm";

		//Player
		public const float PLAYER_CHECK_INTERVAL = 5.0f; //秒
		public const float CLOSING_MESSAGE_TIME = 1800f; //300秒(5分)

		public static readonly ReadOnlyCollection<float> CONST_LIST = 
			Array.AsReadOnly(new float[] {1.0f, 2.0f, 3.0f});

		public const int shopid_num = 9;
		public const string keySHOPID = "shopid";
		public const string keyUUID = "uuid";
		public const string keyTODAY = "today";
		public const string keyTHISMONTH = "thismonth";
		public const string keyPREVIEWPROG = "preview_program";
		public const string keyPLAYEDID = "playedid";
		public const float fadeBaseTime = 1.0f;
		public const float fadeSlideTime = 1.0f;
		public const float PlayerEndTime = 2.0f;

		public const int RESTART_ID = -1234;

		///// Kanri /////

		public const string tableStaff = "Staff";
		public const string tableShop = "Shop";
		public const string tableCampany = "Campanies";
		public const string tableProgram = "Program";
		public const string tableProgramsListItem = "ProgramsListItem";
		public const string tableTodaysLoop = "TodaysLoop";
		public const string tableSrcListItem = "SrcListItem";
		public const string tableSpecialProgram = "SpecialProgram";

		public const string keyMYTYPE = "mytype";
		public const string keyTABLE = "table";

		public const string keyID = "id";
		public const string keyNAME = "name";
		public const string keyLOGIN = "login";
		public const string keyMAIL = "mail";
		public const string keyPW = "password";
		public const string keySHOP = "shops";
		public const string keyBLOCK = "mst_area_id";
		public const string keyCOMPANY = "campany_id";
		public const string keyTYPE = "type";
		public const string keyCAPTION = "caption";
		public const string keyDURATION = "duration";
		public const string keyPATH = "path";

		public const string keySUPER = "super"; //放送(super)かカセットか
		///// Login
		public const string keyUSERID = "userid";
		public const string keyBUILD = "build";

		// Program
		public const string keyTITLE = "title";
		public const string keySUBTITLE = "subtitle";
		public const string keyEDITOR = "editor";
		public const string keyPARENT = "parent";
		public const string keyPROGID = "programid";
		public const string keySPECIAL = "special";
		public const string keyTIME = "time";
		public const string keyDATE = "date";
		public const string keyMEMO = "memo";

		public const string keyLAYOUT = "layout";
		public const string keyBGCOLOR = "bg";
		public const string keyCONTENT = "content";
		public const string keyCONTENTTYPE = "content_type";
		public const string keyTRANSITION = "trans";
		public const string keyPAGETIME = "pagetime";

		public const string keyBROADCAST = "broadcast";
		public const string keyLOOPSET = "loopset";

		public const string keyFILENAME = "filename";

		//1.1
		public const string keyCHANNEL = "channel";
		public const string keyCATEGORY = "category";
		public const string keyRELEASEDATE = "release";
		public const string keySHOPMENU = "shopmenu";
		public const string keyPLAYABLE = "playable";
		public const string keyCOLLECTIONID = "collectionid";

		public const string defaultTodaysLoopTitle = "新規プログラムループ";
		public const string defaultSrcListItemCaption = "新規キャプション";

		// Calender
		public const string keyCALENDERSTART = "start";
		public const string keyCALENDEREND = "end";
		public const string keyDATELIST = "d";

		//close setting
		public const string keyCLOSETIME = "closetime";

		//1.1
		public const string keyCALENDERID = "calenderid";
	}

	public static class UI {
		public class SideMenuItemClass {
			public string symbol;
			public string label; //
			public bool selected = false;
			public bool enableCell = true;
			public bool notyet; //Cfgの設定に従う
		}

		public static readonly ReadOnlyCollection<string> BAR_COLOR = 
			Array.AsReadOnly(new string[] {"#000000", "#707ebd", "#00752b", "#19a6de", "#3060ab"});

		public static string sidemenuTopMenuLabel = "トップ画面";
		//トップ
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1 = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_1", label = "管理者用メニュー" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_2", label = "プログラム作成メニュー" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_3", label = "マイアカウント" }//notyet = true は未作成
				//new Cfg.UI.SideMenuItemClass { symbol = "1_4", label = "運用ログ出力"}
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1x = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_2", label = "プログラム（番組）" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_3", label = "マイアカウント"}//,
//				new Cfg.UI.SideMenuItemClass { symbol = "1_4", label = "アプリ設定", notyet = true }
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1t = 
		Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
		new Cfg.UI.SideMenuItemClass { symbol = "1_2", label = "プログラム（番組）" },
		new Cfg.UI.SideMenuItemClass { symbol = "1_3", label = "マイアカウント"},
		new Cfg.UI.SideMenuItemClass { symbol = "1_4", label = "端末設定"} //1.1
		});


		//管理者用メニュー menu1_1 : sidemenu1[0]
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_1 = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1", label = "アカウント管理" },
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_2", label = "運営会社管理" },
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_3", label = "店舗管理" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_4", label = "運用ログ出力"}
			});

		//アカウント管理 menu1_1_1 : sidemenu1_1[0]
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_1_1 = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_1", label = "システム管理者" }
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_2", label = "管理者" },
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_3", label = "ブロックユーザー" },
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_4", label = "コーポレートユーザー" },
//				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_5", label = "店舗ユーザー" }
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_1_1x = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_2", label = "管理者" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_3", label = "ブロックユーザー" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_4", label = "コーポレートユーザー" },
				new Cfg.UI.SideMenuItemClass { symbol = "1_1_1_5", label = "店舗ユーザー" }
			});


		//プログラム作成メニュー menu1_2 : sidemenu1[1]
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_2 = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_2", label = "放送用プログラムリスト"}, //1
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_3", label = "プログラムループリスト"}, //2
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_4", label = "カレンダー" } //A
 //		new Cfg.UI.SideMenuItemClass { symbol = "1_2_5", label = "特番配信"},//, //A
//		new Cfg.UI.SideMenuItemClass { symbol = "1_2_1s", label = "店舗他登録番組リスト"} //3
//				new Cfg.UI.SideMenuItemClass { symbol = "1_2_6", label = "緊急特番配信", notyet = true}, //A
//				new Cfg.UI.SideMenuItemClass { symbol = "1_2_0", label = "運用履歴", notyet = true}
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_2block = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_1", label = "ブロック登録番組"} //1
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_2campany = 
			Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_1", label = "会社登録番組"} //1
			});
		public static readonly ReadOnlyCollection<Cfg.UI.SideMenuItemClass> sidemenu1_2shop = 
		Array.AsReadOnly(new Cfg.UI.SideMenuItemClass[] {
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_0", label = "公開番組一覧"},//, //1
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_1", label = "店舗作成番組一覧・追加"},//, //1
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_6", label = "ブロック番組一覧"},//, //1
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_7", label = "会社作成番組一覧"},//, //1
				new Cfg.UI.SideMenuItemClass { symbol = "1_2_8", label = "番組コレクション ★"}//, //1
			});

		// 1_2_6 〜 1_2_8を追加しました。（1.1向け）


	}


	public static class API {
		public const string SAMPLE = "sample";
		public const string SAMPLE2 = "sample2";
		public const string getShopSettings = "getShopSettings";
		public const string shopplayerlogin = "shopplayerlogin";
		public const string shopplayerloginError = "shopplayerloginError";
		public const string getLocalPlayingProgram = "getLocalPlayingProgram";

		///// Kanri /////
		public const string deleteItemAtTable = "deleteItemAtTable";
		public const string deleteSrcListItemThenUpdateSrc = "deleteSrcListItemThenUpdateSrc";
		public const string getStaffList = "getStaffList";
		public const string getStaff = "getStaff";
		public const string insertStaff = "insertStaff";
		public const string updateStaff = "updateStaff";

		public const string userlogin = "userlogin";
		public const string checkBuild = "checkBuild";
		public const string getAllShop = "getAllShop";
		public const string getAllCampanies = "getAllCampanies";
		public const string getAllArea = "getAllArea";
		public const string insertShop = "insertShop";
		public const string updateShop = "updateShop";
		public const string insertCampany = "insertCampany";
		public const string updateCampany = "updateCampany";
		public const string updateSrcListItem = "updateSrcListItem";

		public const string checkExistLoginID = "checkExistLoginID";

		//1.1
		public const string getDevicesLive = "getDevicesLive";
		public const string updateDeviceInfo = "updateDeviceInfo";
		public const string getChannelList = "getChannelList";


		////// Program //////
		//		public const string getSrcList = "getSrcList";
		public const string getSimpleProgram = "getSimpleProgram";
		public const string getSimpleTodaysLoop = "getSimpleTodaysLoop";
		public const string getProgramsListItem = "getProgramsListItem";
		public const string getBroadcast = "getBroadcast";
		public const string getSrcListItemWithSrc = "getSrcListItemWithSrc";
		//		public const string getProgram = "getProgram";
		public const string insertSimpleProgram = "insertSimpleProgram";
		public const string insertProgramsListItem = "insertProgramsListItem";
		public const string insertTodaysLoop = "insertTodaysLoop";
		public const string insertSrcListItem = "insertSrcListItem";
		public const string insertLocalPlaying = "insertLocalPlaying";
		public const string updateTodaysLoop = "updateTodaysLoop";
		public const string updateProgram = "updateProgram";
		public const string updateDefaultChannel = "updateDefaultChannel";
		public const string updateSrc = "updateSrc";
		public const string updatePassword = "updatePassword";
		public const string uploadFile = "uploadFile";
		// Calender
		public const string getCalenderTodaysLoop = "getCalenderTodaysLoop";
		public const string getCalenderSpecialPrograms = "getCalenderSpecialPrograms";
		public const string insertCalendarLoop = "insertCalendarLoop";
		public const string deleteCalendarLoop = "deleteCalendarLoop";
		public const string insertSpecialProgram = "insertSpecialProgram";

		//shop setting
		public const string updateShopCloseTime = "updateShopCloseTime";
		public const string getShopCloseTime = "getShopCloseTime";

		//1.1
		public const string insertCollection = "insertCollection";
		public const string getCollection = "getCollection";
	}

	///// Kanri /////
	public static class K {
		public const string TEST = "Kanri";
	}
}
