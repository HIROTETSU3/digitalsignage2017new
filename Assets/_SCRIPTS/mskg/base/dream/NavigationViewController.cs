﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasGroup))]
public class NavigationViewController : ViewController	// ViewControllerクラスを継承
{
	private Stack<ViewController> stackedViews = 
		new Stack<ViewController>();				// ビューの階層を保持するスタック
	private ViewController currentView = null;		// 現在のビューを保持
	
	[SerializeField] private Text titleLabel;		// ナビゲーションバーのタイトルを表示するテキスト
	[SerializeField] public Button backButton;		// ナビゲーションバーのバックボタン //public mskg
	[SerializeField] private Text backButtonLabel;	// バックボタンのテキスト
//	[SerializeField] private Button topBackButton;	// トップのバックボタン mskg
	[SerializeField] public float MoveTime = 0.3f;

	// インスタンスのロード時に呼ばれる
	void Awake()
	{
		// バックボタンのイベントリスナーを設定する
		if (backButton){
			backButton.onClick.AddListener(OnPressBackButton);
			// 最初はバックボタンを非表示にする
			backButton.gameObject.SetActive(false);
		}
	}

	public void RestView(){
		//外部からリセットする（ページを消す）
		stackedViews = new Stack<ViewController>();
		if (currentView != null){

		}
		currentView = null;

	}
	
	// バックボタンが押されたときに呼ばれるメソッド
	public void OnPressBackButton()
	{
		// 1つ前の階層のビューに戻る
		Pop();
	}
	
	// ユーザーのインタラクションを有効/無効にするメソッド
	private void EnableInteraction(bool isEnabled)
	{
		GetComponent<CanvasGroup>().blocksRaycasts = isEnabled;
	}
	
	// 次の階層のビューへ遷移する処理をおこなうメソッド
	public void Push(ViewController newView)
	{
		if(currentView == null)
		{
			// 最初のビューはアニメーションなしで表示する
			newView.gameObject.SetActive(true);
			currentView = newView;


			if(titleLabel){
				//1文字ずつ表示する奴付けてみた。
				PushText p = titleLabel.gameObject.GetComponent<PushText>();
				if (p != null){
					p.text = currentView.Title;
				} else{
					p = titleLabel.gameObject.AddComponent<PushText>();
					if (p != null){
						p.text = currentView.Title;
					} else {
						titleLabel.text = currentView.Title;
					}
				}
			}


			return;
		}
		
		// アニメーションの最中はユーザーのインタラクションを無効にする
		EnableInteraction(false);
		
		// 現在表示されているビューを画面左外に移動する
		ViewController lastView = currentView;
		stackedViews.Push(lastView);
		Vector2 lastViewPos = lastView.CachedRectTransform.anchoredPosition;
		lastViewPos.x = -this.CachedRectTransform.rect.width;
		lastView.CachedRectTransform.MoveTo(
			lastViewPos, MoveTime, 0.0f, iTween.EaseType.easeOutSine, ()=>{
				// 移動が終わったらビューを非アクティブにする
				lastView.gameObject.SetActive(false);
		});

		// 新しいビューを画面右外から中央に移動する
		newView.gameObject.SetActive(true);
		Vector2 newViewPos = newView.CachedRectTransform.anchoredPosition;
		newView.CachedRectTransform.anchoredPosition = 
			new Vector2(this.CachedRectTransform.rect.width, newViewPos.y);
		newViewPos.x = 0.0f;
		newView.CachedRectTransform.MoveTo(
			newViewPos, MoveTime, 0.0f, iTween.EaseType.easeOutSine, ()=>{
				// 移動が終わったらユーザーのインタラクションを有効にする
				EnableInteraction(true);
				newView.gameObject.SendMessage("StartAnim",SendMessageOptions.DontRequireReceiver);
		});
		
		// 新しいビューを現在のビューとして保持して、ナビゲーションバーのタイトルを変更する
		currentView = newView;

		if(titleLabel){
			//1文字ずつ表示する奴付けてみた。
			PushText p = titleLabel.gameObject.GetComponent<PushText>();
			if (p != null){
				p.text = newView.Title;
			} else{
				p = titleLabel.gameObject.AddComponent<PushText>();
				if (p != null){
					p.text = newView.Title;
				} else {
					titleLabel.text = newView.Title;
				}
			}
		}

		// バックボタンのラベルを変更する
		if(backButtonLabel)
			backButtonLabel.text = lastView.Title;
		// バックボタンをアクティブにする
		if(backButton)
			backButton.gameObject.SetActive(true);

		// mskg トップバックボタンを非アクティブに
//		if(topBackButton)
//			topBackButton.gameObject.SetActive(false);
	}
	
	// 前の階層のビューへ戻る処理をおこなうメソッド
	public void Pop()
	{
		if(stackedViews.Count < 1)
		{
			// 前の階層のビューがないので何もしない
			return;
		}
		
		// アニメーションの最中はユーザーのインタラクションを無効にする
		EnableInteraction(false);
		
		// 現在表示されているビューを画面右外に移動する
		ViewController lastView = currentView;
		Vector2 lastViewPos = lastView.CachedRectTransform.anchoredPosition;
		lastViewPos.x = this.CachedRectTransform.rect.width;
		lastView.CachedRectTransform.MoveTo(
			lastViewPos, MoveTime, 0.0f, iTween.EaseType.easeOutSine, ()=>{
				// 移動が終わったらビューを非アクティブにする
				lastView.gameObject.SetActive(false);
				lastView.OnPopped(); //mskg
		});
		
		// 前の階層のビューをスタックから戻し、画面左外から中央に移動する
		ViewController poppedView = stackedViews.Pop();
		poppedView.gameObject.SetActive(true);
		Vector2 poppedViewPos = poppedView.CachedRectTransform.anchoredPosition;
		poppedViewPos.x = 0.0f;
		poppedView.CachedRectTransform.MoveTo(
			poppedViewPos, MoveTime, 0.0f, iTween.EaseType.easeOutSine, ()=>{
				// 移動が終わったらユーザーのインタラクションを有効にする
				EnableInteraction(true);
		});

		// スタックから戻したビューを現在のビューとして保持して、ナビゲーションバーのタイトルを変更する
		currentView = poppedView;
		if(titleLabel){
			//1文字ずつ表示する奴付けてみた。
			PushText p = titleLabel.gameObject.GetComponent<PushText>();
			if (p != null){
				p.text = poppedView.Title;
			} else{
				p = titleLabel.gameObject.AddComponent<PushText>();
				if (p != null){
					p.text = poppedView.Title;
				} else {
					titleLabel.text = poppedView.Title;
				}
			}
		}

		// 前の階層のビューがある場合、バックボタンのラベルを変更してアクティブにする
		if(stackedViews.Count >= 1)
		{
			if (backButtonLabel){
				backButtonLabel.text = stackedViews.Peek().Title;
				backButton.gameObject.SetActive(true);
			}
		}
		else
		{
			if(backButton){
				backButton.gameObject.SetActive(false);
				// mskg
//				topBackButton.gameObject.SetActive(true);
			}
		}
	}
}
