﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MskgTileViewCellButton<T> : MonoBehaviour {

	private T iData;
	// データプロパティ
	public T DataSource
	{
		get { return iData; }
		set {
			iData = value;
		}
	}

	// セル内ボタンの内容を更新するメソッド
	public virtual void SetContent(T value)
	{
		DataSource = value;
		// 継承したクラスでオーバーライド
	}
}
