﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System;

[System.Serializable]
public class MainCanvasEvent : UnityEvent<string, Hashtable, Hashtable>
{
}

public class Global : SingletonMonoBehaviour<Global> {

	// ----------
	public delegate void callback( Mskg.RtnObj ret );
	public void Awake()
	{
		if(this != Instance)
		{
			Destroy(this);
			return;
		}
		DontDestroyOnLoad(this.gameObject);
	}
	void OnDestroy ()
	{
		Destroy(this);
	}
	// ----------

	public bool logoFadeComp = false;

	public string uuid = "";
	public string shopid = "";
	public DateTime closetime = new DateTime(2100,DateTime.Now.Month,DateTime.Now.Day,23,59,0);
	public string filenameX = "";

	public ClassBox.Program nowProgram;
	public bool isSpecialProgram = false;

	public bool dlExistenceCheck = true;
	public bool dlAutoDelete = true;

	public ClassBox.ShopSettings shopSettingsObj = null;
	public int currentProgram = 0;

	public void OnActiveSceneChanged( Scene i_preChangedScene, Scene i_postChangedScene )
	{
		Debug.LogFormat( "OnActiveSceneChanged() preChangedScene:{0} postChangedScene:{1}", i_preChangedScene.name, i_postChangedScene.name );
	}

	public UnityEvent onFoundPlayingProgram;
	public UnityEvent onReleasePlayingProgram;
	public UnityEvent onPlayerDone;

	public Texture2D thisTexture;
	public Texture2D nextTexture;

	///// Kanri /////
	public string pwKey = "HONDA";
	public string pwSalt = "DreamsPower";
		
	public int mytype = -1;
	public string mymail = "";

	public List<MainCanvas> mainCanvases = new List<MainCanvas>();
//	public MainCanvasEvent onOpenMainCanvas;

	public string userid = ""; 
	public ClassBox.Staff Me = null;

	public bool initLoaded = false;

	public ClassBox.ShopData[] ShopList;
	public ClassBox.CampanyData[] CampanyList;
	public ClassBox.AreaData[] AreaList;
	public List<ClassBox.Program> LocalProgramList = new List<ClassBox.Program>();

	public ClassBox.DeviceData[] DeviceList; //1.1

	public List<MainCanvas> programMainCanvases = new List<MainCanvas> ();

	///// Program /////
	public ClassBox.Src[] SrcList;
	public ClassBox.SimpleProgram[] SimpleProgramList;
	public ClassBox.SimpleTodaysLoop[] SimpleTodaysLoopList;
	public ClassBox.ProgramsListItem[] ProgramsListItemList;
	public ClassBox.Program[] BroadcastList;
	public ClassBox.SrcListItemWithSrc[] SrcListItemList;

	//1.1
	public ClassBox.CollectionItem[] CollectionList;

	public string isSuper = "0";

	public string openDialogPath = "";
	public bool PlayerPreviewMode = false;
	public int PlayerPreviewProgramID = -1;

	public bool CalendarInfoInit = false;

	//ヒロ追加
	public string NewPswd = "";
	public string TempText = "";


	public ClassBox.SimpleProgram TempSimpleProgram;
	public ClassBox.SimpleTodaysLoop TempSimpleTodaysLoop;

	//1.1
	public ClassBox.ChannelData[] ChannelDataArray = null;
	public string CalenderID = "";
}
