﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using WWWKit;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.SceneManagement;

namespace Mskg {
	
	public class RtnObj {
		public bool success; //成功/失敗
		public object obj; //文字列返信
		public string json;

		public RtnObj(bool success, object obj = null, string json = null)
		{
			this.success = success;
			this.obj = obj;
			this.json = json;
		}
	}


	public class DownLoadObject
	{
		public int uid;
		public int progress;
		public bool complete = false;
		public bool error = false;
		public bool existence = false;
	}

	/// 

	public class API : SingletonMonoBehaviour<API> {
		
		// ----------
		public delegate void callback( RtnObj ret );
		public void Awake()
		{
			if(this != Instance)
			{
				Destroy(this);
				return;
			}
			DontDestroyOnLoad(this.gameObject);
		}
		void OnDestroy ()
		{
			Destroy(this);
		}
		// ----------

		public void restart(){
			SceneManager.LoadScene ("Restart", LoadSceneMode.Single);
		}

		// Public Functions
		public void call( // PHPのAPI呼び出し
			string api, 
			Hashtable param, 
			callback onCallback
		){
			string url = Cfg.App.BASE_URL + Cfg.App.API_DIR + api;
//			AlertViewController.Show ("URL", url);
			string append = "";
			append += "?uuid=" + Global.Instance.uuid;
			append += "&r=" + UnityEngine.Random.Range (0, 9999);
			if (param != null) {
				foreach (DictionaryEntry de in param) {
					append += "&" + de.Key + "=" + WWW.EscapeURL(de.Value.ToString(), System.Text.Encoding.UTF8);
				}
			}
			url += append.Replace (" ", "+");
			Debug.Log (url);

			WWWClient client = new WWWClient (this, url);
			client.OnDone = (WWW www) => {
				if (onCallback != null){
					onCallback(new RtnObj(true, null, www.text));
				}
			};
			client.OnFail = (WWW www) => {
				Debug.Log ("WWW Fail:" + www.error);
				if (onCallback != null){
					onCallback(new RtnObj(false, null, null));
				}
			};
			client.OnDisposed = () => {
				Debug.Log("WWW Timeout");
			};
			client.Request();
		}


		public void download( // 指定したファイルをダウンロード
			string url,
			string localDirectory,
			callback onProgress,
			callback onComplete,
			int uid,
			bool existenceCheck
		){
			if (existenceCheck && checkExistence (url, localDirectory)) {
				DownLoadObject dlo = new DownLoadObject ();
				dlo.uid = uid;
				dlo.existence = true;
				onComplete (new RtnObj (true, dlo));
			} else {
				StartCoroutine (coDownload (url, localDirectory, onProgress, onComplete, uid));
			}
		}

		public void downloadSrcList(
			string[] urlList,
			string localDirectory,
			callback onStatus,
			bool existenceCheck,
			bool autoDelete,
            bool oneByOne = true
		){
			for (int i = 0; i < urlList.Length; i++)
			{
				if (autoDelete) {
					deleteUnusedFile (urlList, localDirectory);
				}
                if (!oneByOne)
                {
                    //旧ダウンロードシステム
                    Mskg.API.Instance.download(
                        urlList[i],
                        localDirectory,
                        (Mskg.RtnObj ret) => { //progress
                            if (ret.success)
                            {
                                onStatus(new RtnObj(true, (DownLoadObject)ret.obj));
                            }
                        },
                        (Mskg.RtnObj ret) => { //complete
                            onStatus(new RtnObj(ret.success, (DownLoadObject)ret.obj));
                        },
                        i, //.ToString(),
                        Global.Instance.dlExistenceCheck
                    );
                }
			}


            if (oneByOne)
            {
                //新ダウンロードシステム（ひとつずつダウンロード）
                if (0 < urlList.Length)
                {
                    this.doDownload(0, urlList, localDirectory, onStatus);
                }
            }


        }

        private void doDownload(
            int now,
            string[] urlList,
            string localDirectory,
            callback onStatus
            )
        {
            //新ダウンロードシステム（ひとつずつダウンロード）
            Mskg.API.Instance.download(
                        urlList[now],
                        localDirectory,
                        (Mskg.RtnObj ret) => { //progress
                            if (ret.success)
                            {
                                Debug.Log("++++++++++++++++");
                                Debug.Log(ret.obj);
                                onStatus(new RtnObj(true, (DownLoadObject)ret.obj));
                            }
                        },
                        (Mskg.RtnObj ret) => { //complete
                            Debug.Log("==========COMP===========");
                            Debug.Log(ret.obj);
                            Debug.Log("=========================");
                            onStatus(new RtnObj(ret.success, (DownLoadObject)ret.obj));
                            now++;
                            if(now < urlList.Length)
                            {
                                doDownload(now, urlList, localDirectory, onStatus);
                            }
                        },
                        now, //.ToString(),
                        Global.Instance.dlExistenceCheck
                    );
        }


		public string newUUID(){
			string uuid = System.Guid.NewGuid().ToString();
			PlayerPrefsX.SetString (Cfg.App.keyUUID, uuid);
			return uuid;
		}

		public void clearPrefs(){
			PlayerPrefs.DeleteAll ();
		}

		public string getUUID(){
//			PlayerPrefs.DeleteAll ();
			string uuid = PlayerPrefsX.GetString (Cfg.App.keyUUID);
			if (uuid != "") {
				return uuid;
			} else {
				return newUUID ();
			}
		}

		public void setShopID(string idstr){
			PlayerPrefsX.SetString (Cfg.App.keySHOPID, idstr);
		}

		public void getShopID(
			callback onGetShopID
		){
			string storeid = PlayerPrefsX.GetString (Cfg.App.keySHOPID);
			if (storeid != "") {
				onGetShopID (new RtnObj(true, storeid));
			} else {
				onGetShopID (new RtnObj(false));
			}
		}

		//指定した矩形内に収まるwidth,heightを返す
		public Vector2 calcRect( Vector2 rectSize, Vector2 imageSize ){
			Vector2 retVec2 = new Vector2( imageSize.x, imageSize.y );
			if(imageSize.x > imageSize.y){
				//横長画像
				retVec2.x = rectSize.x;
				retVec2.y = rectSize.y * ( imageSize.y / imageSize.x );
			}else{
				//縦長画像
				retVec2.x = rectSize.x * ( imageSize.x / imageSize.y );
				retVec2.y = rectSize.y;
			}
			return retVec2;
		}


		//暗号化
		//参考: http://developer.wonderpla.net/entry/entry/blog/engineer/AESwithUnity/
		public string Aes128CBCEncode(string txt, string _pw, string _salt)
		{
			if (txt == "" || txt == null) {
				return "";
			}

			byte[] src = Encoding.UTF8.GetBytes (txt);

			RijndaelManaged rijndael = new RijndaelManaged ();
			rijndael.KeySize = 128;
			rijndael.BlockSize = 128;

			// パスワードから共有キーと初期化ベクターを作成
			string pw = _pw; //“passward”;
			string salt = _salt; //“salt”;

			byte[] bSalt = Encoding.UTF8.GetBytes (salt);
			Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes (pw, bSalt);
			deriveBytes.IterationCount = 1000;        // 反復回数

			rijndael.Key = deriveBytes.GetBytes (rijndael.KeySize / 8);
			rijndael.IV = deriveBytes.GetBytes (rijndael.BlockSize / 8);

			// 暗号化
			ICryptoTransform encryptor = rijndael.CreateEncryptor ();
			byte[] encrypted = encryptor.TransformFinalBlock (src, 0, src.Length);

			encryptor.Dispose ();

//			return System.Convert.ToBase64String(encrypted);
//			return Encoding.UTF8.GetString (encrypted);
			return System.Convert.ToBase64String(encrypted);
		}

		//複合化
		public string Aes128CBCDecode(string txt, string _pw, string _salt)
		{
			if (txt == "" || txt == null) {
				return "";
			}

			byte[] src = System.Convert.FromBase64String(txt);

			RijndaelManaged rijndael = new RijndaelManaged ();
			rijndael.KeySize = 128;
			rijndael.BlockSize = 128;

			// パスワードから共有キーと初期化ベクターを作成
			string pw = _pw; //“passward”;
			string salt = _salt; //“salt”;

			byte[] bSalt = Encoding.UTF8.GetBytes (salt);
			Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes (pw, bSalt);
			deriveBytes.IterationCount = 1000;        // 反復回数

			rijndael.Key = deriveBytes.GetBytes (rijndael.KeySize / 8);
			rijndael.IV = deriveBytes.GetBytes (rijndael.BlockSize / 8);

			// 復号化
			ICryptoTransform decryptor = rijndael.CreateDecryptor ();
			byte[] plain = decryptor.TransformFinalBlock (src, 0, src.Length);

			decryptor.Dispose ();
//			return System.Convert.ToBase64String(plain);
			return Encoding.UTF8.GetString (plain);
		}


		public bool checkError(string json, string message){

			ClassBox.SuccessObj successObj = null;
			try
			{
				successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
			}
			catch (NullReferenceException ex)
			{
				Debug.Log(ex);
			}

			if (successObj.success) {
				return true;
			}else{
				string _message = "処理中にエラーが発生しました";
				if (message != null) {
					_message = message;
				}
				
				AlertViewController.Show("エラー", _message);
				return false;
			}

		}


		public void upload(
			string localFilePath, 
			string api, 
			Hashtable param,
			callback onCallback,
			callback onProgress = null
		)
		{

			string url = Cfg.App.BASE_URL + Cfg.App.API_DIR + api;
			string append = "";
			append += "?uuid=" + Global.Instance.uuid;
			append += "&r=" + UnityEngine.Random.Range (0, 9999);
			if (param != null) {
				foreach (DictionaryEntry de in param) {
					append += "&" + de.Key + "=" + WWW.EscapeURL(de.Value.ToString(), System.Text.Encoding.UTF8);
				}
			}
			url += append.Replace (" ", "+");
			Debug.Log (url);

			StartCoroutine(coUpload(localFilePath, url, onCallback, onProgress));
		}


		public string randomPass(int minCharAmount, int maxCharAmount){
			string _string = "";
			string glyphs = Cfg.App.RANDOM_PASSWORD_CHARS;
//			int minCharAmount = 1;
//			int maxCharAmount = 10;
			int charAmount = UnityEngine.Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
			for(int i=0; i<charAmount; i++)
			{
				_string += glyphs[UnityEngine.Random.Range(0, glyphs.Length)];
			}

			return _string;
		}


		public void sendNewPw(string rawNewPw, callback onCallback){

			Hashtable param = new Hashtable (); // jsonでリクエストを送るのへッダ例
			param.Add (Cfg.App.keyUSERID, Global.Instance.Me.id);
			string encordedNewPw = Mskg.API.Instance.Aes128CBCEncode (rawNewPw, Global.Instance.pwKey, Global.Instance.pwSalt);
			param.Add (Cfg.App.keyPW, encordedNewPw);
			Mskg.API.Instance.call (
				Cfg.API.updatePassword + ".php",
				param, 
				(Mskg.RtnObj ret) => { //callback
					if (ret.success) {

						Debug.Log("////////////");
						Debug.Log(ret);
						Debug.Log("////////////");


						if (Mskg.API.Instance.checkError(ret.json, null)) {
							if (onCallback != null){
								onCallback(new RtnObj(true, encordedNewPw, null));
							}
//							AlertViewController.Show("確認", "パスワードを更新しました");
						}

//						this.gameObject.SetActive (false);
					}
				}
			);


		}


		//local Functions
		IEnumerator coDownload( //ダウンローダーのコルーチン
			string url,
			string localDirectory,
			callback onProgress,
			callback onComplete,
			int uid
		){
			WWW www = new WWW(url);

			DownLoadObject dlo = new DownLoadObject();

			while (!www.isDone) { // ダウンロードの進捗を表示
				dlo.uid = uid;
				dlo.progress = Mathf.CeilToInt (www.progress * 100);
				onProgress(new RtnObj(true, dlo));
				yield return null;
			}

			if (!string.IsNullOrEmpty(www.error)) { // ダウンロードでエラーが発生した
				print(www.error);

				dlo.complete = true;
				dlo.error = true;

			} else { // ダウンロードが正常に完了した
				//print(Application.persistentDataPath);
				string localPath = localDirectory + "/" + Path.GetFileName(www.url);
				File.WriteAllBytes(localPath, www.bytes);

				dlo.progress = Mathf.CeilToInt (www.progress * 100);
				dlo.complete = true;
				dlo.error = false;
			}

			onComplete (new RtnObj (true, dlo));

		}


		bool checkExistence(string url, string localDirectory){
			string filename = Path.GetFileName (url);
			return File.Exists (localDirectory + "/" + filename);
		}

		void deleteUnusedFile(string[] urlList, string localDirectory){

			List<string> NeedFileNames = new List<string>();
			foreach (string urlfile in urlList) {
				string filename = Path.GetFileName (urlfile);
				NeedFileNames.Add (filename);
			}

			DirectoryInfo info = new DirectoryInfo(localDirectory);
			FileInfo[] fileInfo = info.GetFiles();
//			List<string> DirectoryFileNames = new List<string>();
			foreach (FileInfo file in fileInfo) {
				if (System.Array.IndexOf (NeedFileNames.ToArray(), file.Name) == -1) {
//					Debug.Log("いらない");
					File.Delete(file.DirectoryName + "/" + file.Name);
					//File.Delete (file);
				}
			}

			//File.Exists (localDirectory + "/" + filename);


		}


		IEnumerator coUpload(string localFilePath, string uploadURL, callback onCallback, callback onProgress = null) //アップローダーのコルーチン
		{
			WWW localFile = new WWW(localFilePath); //new WWW("file:///" + localFileName);
			yield return localFile;
			if (localFile.error == null)
				Debug.Log("Loaded file successfully");
			else
			{
				Debug.Log("Open file error: "+localFile.error);
				yield break; // stop the coroutine here
			}
			WWWForm postForm = new WWWForm();
			// version 1
			//postForm.AddBinaryData("theFile",localFile.bytes);
			// version 2
//			postForm.AddBinaryData("userfile",localFile.bytes,localFilePath,"image/png");
			postForm.AddBinaryData("userfile",localFile.bytes,localFilePath);

			WWW upload = new WWW(uploadURL,postForm);  

			if (onProgress != null) {
				DownLoadObject dlo = new DownLoadObject ();

				while (!upload.isDone) { // ダウンロードの進捗を表示
					dlo.uid = 0; //固定
					dlo.progress = Mathf.CeilToInt (upload.uploadProgress * 100);
					onProgress (new RtnObj (true, dlo));
					//					Debug.Log ("?????????????????????");
					//					Debug.Log (localFile.progress * 100);
					yield return null;
				}
			}


			yield return upload;
			if (upload.error == null) {
				Debug.Log ("upload done :" + upload.text);
				if (onCallback != null) {
					onCallback (new RtnObj (true, null, upload.text));
				}
			} else {
				Debug.Log ("Error during upload: " + upload.error);
				if (onCallback != null){
					onCallback(new RtnObj(false, null, null));
				}
			}


		}



		//以下、管理アプリ用

		public void getUserID(
			callback onGetUserID
		){
			string userid = PlayerPrefsX.GetString (Cfg.App.keyUSERID);
			if (userid != "") {
				onGetUserID (new RtnObj(true, userid));
			} else {
				onGetUserID (new RtnObj(false));
			}
		}

		public void setUserID(string idstr){
			PlayerPrefsX.SetString (Cfg.App.keyUSERID, idstr);
		}

		public ClassBox.ShopData getShopAllInfo(string shopid){
			
			ClassBox.ShopData returnShopData = new ClassBox.ShopData ();
			foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
				if (shop.shopid == shopid) {
					returnShopData = shop;
					foreach (ClassBox.AreaData area in Global.Instance.AreaList) {
						if (area.id == shop.mst_area_id) {
							returnShopData.areaName = area.name;
						}
					}

					foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
						if (campany.id == shop.campany_id) {
							returnShopData.campanyName = campany.name;
						}
					}

				}
			}

			return returnShopData;
		}

		public ClassBox.ShopData getShopByShopID(string shopid){
			if (shopid == "") {
				ClassBox.ShopData shop = new ClassBox.ShopData();
				shop.name = "---";
				return shop; //空のDataを返す
			}

			foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
				if (shop.shopid == shopid) {
					return shop;
				}
			}
			return null;

		}

		public ClassBox.AreaData getAreaByID(int id){
			if (id == -1) {
				ClassBox.AreaData area = new ClassBox.AreaData();
				area.name = "---";
				return area; //空のDataを返す
			}

			foreach (ClassBox.AreaData area in Global.Instance.AreaList) {
				if (area.id == id) {
					return area;
				}
			}
			return null;
		}

		public ClassBox.CampanyData getCampanyByID(int id){
			if (id == -1) {
				ClassBox.CampanyData campany = new ClassBox.CampanyData();
				campany.name = "---";
				return campany; //空のDataを返す
			}

			foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
				if (campany.id == id) {
					return campany;
				}
			}
			return null;
		}

		public string getTypeNameByInt(int arg){
			return Cfg.UI.sidemenu1_1_1 [arg].label;
		}

		public bool isSuperLevel(){
			return (Global.Instance.Me.type == 0);
		}

		public bool isKanriLevel(){
			return (Global.Instance.Me.type <= 1);
		}

		public bool isTempUser(){
			return (Global.Instance.Me.type == 4);
		}

		public void unloadAllPlayer(){
			unloadScene ("PlayerStarter");
			unloadScene ("PrgLoopPlayer");
			unloadScene ("PrgPlayerType1");
			unloadScene ("PrgPlayerType2");
		}

		private void unloadScene(string name){
			Debug.Log (SceneManager.GetSceneByName (name).IsValid());
			if (SceneManager.GetSceneByName(name).IsValid()){
				SceneManager.UnloadSceneAsync(name);
			}
		}

	}
}