﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log ("called RestartScene.cs");
		// 明示的にDestroyするもの

		Destroy (Global.Instance.thisTexture);
		Destroy (Global.Instance.nextTexture);

		Destroy (Global.Instance);

		Debug.Log ("load PlayerStarter");
		SceneManager.LoadScene ("PlayerStarter", LoadSceneMode.Single);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
