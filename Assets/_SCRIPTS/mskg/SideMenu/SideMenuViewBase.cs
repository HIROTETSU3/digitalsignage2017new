﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Collections.ObjectModel;

public class SideMenuViewBase : ViewController {

	[SerializeField] protected NavigationViewController navigationView;
	[SerializeField] protected SideMenuTableViewController tableview;
	[SerializeField] protected bool initView = false;

	protected ReadOnlyCollection<Cfg.UI.SideMenuItemClass> uiArray;

	//	[SerializeField] private SideMenuView1_1 push2nd;

	// Use this for initialization
	protected virtual void Start () {

		#region メニューUI生成・初期化

		//		ReadOnlyCollection<Cfg.UI.SideMenuItemClass> uiArray = Cfg.UI.sidemenu1_1;

		this.gameObject.SetActive (false);

		//		List<Cfg.UI.SideMenuItemClass> sidemenuclasses = new List<Cfg.UI.SideMenuItemClass>();

		//		List<Cfg.UI.SideMenuItemClass> sidemenuclasses = new List<Cfg.UI.SideMenuItemClass>() {
		//			new Cfg.UI.SideMenuItemClass { symbol="menu0", label=Cfg.UI.SideMenu1[0]}, 
		//			new Cfg.UI.SideMenuItemClass { symbol="menu1", label=Cfg.UI.SideMenu1[1]}, 
		//			new Cfg.UI.SideMenuItemClass { symbol="menu2", label=Cfg.UI.SideMenu1[2]}, 
		//			new Cfg.UI.SideMenuItemClass { symbol="menu3", label=Cfg.UI.SideMenu1[3]}
		//		};


		//メニューUI作成
		List<Cfg.UI.SideMenuItemClass> sidemenuclasses = new List<Cfg.UI.SideMenuItemClass> ();
		foreach (Cfg.UI.SideMenuItemClass menu in uiArray) {
			sidemenuclasses.Add (menu);
		}
		tableview.UpdateData (sidemenuclasses);


		if(initView && navigationView != null)
		{
			navigationView.Push(this);
		}

		navigationView.backButton.onClick.AddListener (onNavBackButtonClick);

		#endregion

	}

	// Update is called once per frame
	void Update () {

	}

	public virtual void OnPressButton(SideMenuTableViewCell cell)
	{
		//		navigationView.Push(push2nd);
		Debug.Log(cell.symbol);
	}

	protected virtual void selectCell(SideMenuTableViewCell cell)
	{
		if (cell == null) {
			OnPopped ();
		}

		tableview.selectCell (cell);
	}
		
	private void onNavBackButtonClick(){
		selectCell (null);
	}

	public override void OnPopped ()
	{
		base.OnPopped ();

		foreach (MainCanvas canvas in Global.Instance.mainCanvases)
		{
			canvas.gameObject.SetActive(false);
		}

		foreach (MainCanvas canvas in Global.Instance.programMainCanvases)
		{
			canvas.gameObject.SetActive(false);
		}

	}
}

