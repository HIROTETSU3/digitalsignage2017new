﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;



public class SideMenuView1_1_1 : SideMenuViewBase {

	[SerializeField] private SideMenuViewBase[] views;
	[SerializeField] private MainCanvas[] mainCanvases;

	[SerializeField] Dropdown BlockSelectDropdown;

	public override string Title { get { return Cfg.UI.sidemenu1_1[0].label; } } //[]内は1引いた数

	// Use this for initialization
	protected override void Start()
	{
		if (Mskg.API.Instance.isSuperLevel()) {
			uiArray = Cfg.UI.sidemenu1_1_1;
		} else {
			uiArray = Cfg.UI.sidemenu1_1_1x;
		}



		// ベースクラスのStartメソッドを呼ぶ
		base.Start();
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnPressButton(SideMenuTableViewCell cell)
	{
		/*
		Hashtable param = new Hashtable ();
		param.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
		Global.Instance.onOpenMainCanvas.Invoke (cell.symbol, null, param);
//			maincanvas.gameObject.SetActive (!maincanvas.gameObject.activeInHierarchy);

		//		navigationView.Push(push2nd);
		selectCell (cell);
		base.OnPressButton(cell);
		*/

		Hashtable param = new Hashtable ();
		bool targetOn = false;
		switch (cell.symbol) {
		case "1_1_1_1":
			param.Add (Cfg.App.keyTYPE, 0);
			targetOn = true;
			BlockSelectDropdown.gameObject.SetActive(false);
			break;

			case "1_1_1_2":
			param.Add (Cfg.App.keyTYPE, 1);
			targetOn = true;
			BlockSelectDropdown.gameObject.SetActive(false);
			break;

			case "1_1_1_3":
			param.Add (Cfg.App.keyTYPE, 2);
			targetOn = true;
			BlockSelectDropdown.gameObject.SetActive(false);
			break;

			case "1_1_1_4":
			param.Add (Cfg.App.keyTYPE, 3);
			targetOn = true;
			BlockSelectDropdown.gameObject.SetActive(false);
			break;

			case "1_1_1_5":
			param.Add (Cfg.App.keyTYPE, 4);
			targetOn = true;
			BlockSelectDropdown.value = 0;
			BlockSelectDropdown.gameObject.SetActive(true);
			break;
		}

		if (targetOn) {
			param.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
//			mainCanvases[0].onOpenMainCanvas(cell.symbol, null, param);
			mainCanvases[0].onOpenMainCanvas("1_1_1_1", null, param); //viewは"1_1_1_1"に
			selectCell (cell);
		}

		base.OnPressButton(cell);
	}

}
