﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SideMenuView1_2 : SideMenuViewBase {

	[SerializeField] private SideMenuViewBase[] views;

	public override string Title { get { return Cfg.UI.sidemenu1[1].label; } } //[]内は1引いた数

	// Use this for initialization
	protected override void Start()
	{
		switch (Global.Instance.Me.type) {
		case 0: //システム管理者
		case 1: //管理者
			uiArray = Cfg.UI.sidemenu1_2;
			break;
		case 2: //ブロックユーザ
			uiArray = Cfg.UI.sidemenu1_2block;
			break;
		case 3: //コーポレートユーザ
			uiArray = Cfg.UI.sidemenu1_2campany;
			break;
		case 4: //店舗ユーザ
			uiArray = Cfg.UI.sidemenu1_2shop;
			break;
		}
			
		SceneManager.LoadScene ("KanriProgram", LoadSceneMode.Additive);

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnPressButton(SideMenuTableViewCell cell)
	{
		switch (cell.symbol) {
		case "1_2_1s": //店舗他登録番組リスト
			Hashtable param = new Hashtable ();
			//			param.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
			Global.Instance.isSuper = "0";
			param.Add (Cfg.App.keySUPER, Global.Instance.isSuper);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param);
			selectCell (cell);
			break;
		case "1_2_2": //放送
			Hashtable param2 = new Hashtable ();
			Global.Instance.isSuper = "1";
			param2.Add (Cfg.App.keySUPER, Global.Instance.isSuper);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param2);
			selectCell (cell);
			break;
		case "1_2_3": //放送・特番設定
			Hashtable param3 = new Hashtable ();
//			param3.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_3", null, param3);
			selectCell (cell);
			break;
		case "1_2_4": //再生アプリコントローラ
			Hashtable param4 = new Hashtable ();
//			param4.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_4", null, param4);
			selectCell (cell);
			break;
		case "1_2_5": //チャンネル設定
			Hashtable param5 = new Hashtable ();
//			param5.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_5", null, param5);
			selectCell (cell);
			break;

		//1.1
		case "1_2_0": //公開番組一覧 shopmenu0
			Hashtable param0 = new Hashtable ();
			param0.Add (Cfg.App.keySHOPMENU, 0);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param0);
			selectCell (cell);
			break;
		case "1_2_1": //店舗作成番組一覧・追加 shopmenu1
			Hashtable param1 = new Hashtable ();
			//			param.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
			Global.Instance.isSuper = "0";
			param1.Add (Cfg.App.keySHOPMENU, 1);
			param1.Add (Cfg.App.keySUPER, Global.Instance.isSuper);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param1);
			selectCell (cell);
			break;
		case "1_2_6": //ブロック番組一覧 shopmenu6
			Hashtable param6 = new Hashtable ();
			param6.Add (Cfg.App.keySHOPMENU, 6);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param6);
			selectCell (cell);
			break;
		case "1_2_7": //会社作成番組一覧 shopmenu7
			Hashtable param7 = new Hashtable ();
			param7.Add (Cfg.App.keySHOPMENU, 7);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_1", null, param7);
			selectCell (cell);
			break;
		case "1_2_8": //番組コレクション shopmenu8
			Hashtable param8 = new Hashtable ();
			param8.Add (Cfg.App.keySHOPMENU, 8);
			Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_8", null, param8);
			selectCell (cell);
			break;

		}


		base.OnPressButton(cell);
	}


}
