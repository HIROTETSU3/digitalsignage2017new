﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;


//クラス定義はCfgへ

[RequireComponent(typeof(ScrollRect))]
public class SideMenuTableViewController : TableViewController<Cfg.UI.SideMenuItemClass>
{

	public void selectCell(SideMenuTableViewCell cell)
	{
		if (cell == null){
			foreach (Cfg.UI.SideMenuItemClass item in tableData) {
				item.enableCell = true;
			}
		}else{
			foreach (Cfg.UI.SideMenuItemClass item in tableData) {
				if (tableData [cell.DataIndex] == item) {
					item.selected = true;
					item.enableCell = false;
				} else {
					item.selected = false;
					item.enableCell = !item.notyet;
				}
			}
		}
		UpdateContents();
	}

	public void UpdateData(List<Cfg.UI.SideMenuItemClass> itemclasses){

		tableData.Clear ();

		tableData = itemclasses;

		/*
		foreach(DataRow d in dt.Rows){
//			Debug.Log(d["name"].ToString());

			string prefecture_name = (d ["prefecture_name"] != null) ? d ["prefecture_name"].ToString () : "";

			tableData.Add( new FindStationItemData { 
				id = d["id"].ToString(), 
				name = d["name"].ToString(), 
				kana = (d["kana"] != null) ? d["kana"].ToString() : "", 
				address = prefecture_name + d["address"].ToString(), 
				url = (d["url"] != null) ? d["url"].ToString() : "", 
				lat = d["latitude"].ToString(), 
				lng = d["longitude"].ToString(), 
				distance = d["distance"].ToString(), 
				mst_prefecture_id = d["mst_prefecture_id"].ToString(),
				arrived = (d["arrived"] != null) ? (int)d["arrived"] : 0, //int
				tel = (d["tel"] != null) ? d["tel"].ToString() : ""  //tel
			} );

		}
*/


		// スクロールさせる内容のサイズを更新する
		UpdateContents();

		//return (0 < tableData.Count) ? int.Parse(tableData[0].mst_prefecture_id) : -1;
	}

	// Use this for initialization
	protected override void Start()
	{
		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

//		tableData = new List<itemClass>() {
//			new itemClass { name="drink1"}, 
//			new itemClass { name="drink2"}, 
//			new itemClass { name="drink3"}, 
//			new itemClass { name="drink4"}, 
//			new itemClass { name="drink5"}
//		};


		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	public override string Title { get { return "1stView Table"; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(SideMenuTableViewCell cell)
	{
		if(navigationView != null)
		{
			AlertViewController.Show (tableData [
				cell.DataIndex].label, 
				tableData [cell.DataIndex].symbol
			);
		}
	}
	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

