﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SideMenuView1_1 : SideMenuViewBase {

	[SerializeField] private SideMenuViewBase[] views;
	[SerializeField] private MainCanvas[] mainCanvases;

	public override string Title { get { return Cfg.UI.sidemenu1[0].label; } } //[]内は1引いた数

	// Use this for initialization
	protected override void Start()
	{
		
		uiArray = Cfg.UI.sidemenu1_1;

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnPressButton(SideMenuTableViewCell cell)
	{
		Hashtable param = new Hashtable ();
		switch (cell.symbol) {
		case "1_1_1":
			navigationView.Push (views [0]);
			selectCell (null);
			break;
		case "1_1_2":
//			param.Add (Cfg.App.keyTYPE, 0);
			mainCanvases[0].onOpenMainCanvas("1_1_2", null, param);
			selectCell (cell);
			break;
		case "1_1_3":
			mainCanvases[1].onOpenMainCanvas("1_1_3", null, param);
			selectCell (cell);
			break;
		case "1_1_4":
			mainCanvases[2].onOpenMainCanvas("1_1_4", null, param);
			selectCell (cell);
			break;
		}

		base.OnPressButton(cell);
	}

}
