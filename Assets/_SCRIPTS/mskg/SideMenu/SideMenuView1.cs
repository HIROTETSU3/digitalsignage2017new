﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SideMenuView1 : SideMenuViewBase {

	[SerializeField] private SideMenuViewBase[] views;
	[SerializeField] private MainCanvas[] mainCanvases;

	public override string Title { get { return Cfg.UI.sidemenuTopMenuLabel; } }

	// Use this for initialization
	protected override void Start()
	{
		if (Mskg.API.Instance.isKanriLevel()) {
			uiArray = Cfg.UI.sidemenu1;
		} else if(Mskg.API.Instance.isTempUser()) {
			uiArray = Cfg.UI.sidemenu1t;
		}else{
			uiArray = Cfg.UI.sidemenu1x;
		}

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();
	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnPressButton(SideMenuTableViewCell cell)
	{
		
		//AlertViewController.Show (cell.symbol, "message text");

		Hashtable param = new Hashtable ();
		switch (cell.symbol) {
		case "1_1":
			selectCell (null);
			navigationView.Push (views [0]);
			break;
		case "1_2":
			selectCell (null);
			navigationView.Push (views [1]);
			break;
		case "1_3":
			mainCanvases[0].onOpenMainCanvas("1_3", null, param);
			selectCell (cell);
			break;
		case "1_4":
			mainCanvases[0].onOpenMainCanvas("1_4", null, param);
			selectCell (cell);
			break;
		}

		base.OnPressButton(cell);
	}

}
