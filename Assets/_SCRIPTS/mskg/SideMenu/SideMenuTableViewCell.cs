﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SideMenuTableViewCell : TableViewCell<Cfg.UI.SideMenuItemClass>
{
	[SerializeField] private Text nameLabel;	// アイテム名を表示するテキスト

	[System.NonSerialized] public string symbol;

	public override void UpdateContent(Cfg.UI.SideMenuItemClass itemData)
	{
		symbol = itemData.symbol;
		nameLabel.text = itemData.label;
		if (itemData.selected) {
			this.enableCell (itemData.enableCell);
		} else {
			this.enableCell (!itemData.notyet);
			if (itemData.notyet)
				nameLabel.text += "【未】";
		}

	}

}

