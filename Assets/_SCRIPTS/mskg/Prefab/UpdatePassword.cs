﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdatePassword : MonoBehaviour {

	[SerializeField] InputField nowPw;
	[SerializeField] InputField newPw;
	[SerializeField] InputField confirmPw;

	public string encordedPw = "";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void onGo(){

		if (encordedPw == Mskg.API.Instance.Aes128CBCEncode (nowPw.text, Global.Instance.pwKey, Global.Instance.pwSalt)) {
			if (newPw.text == confirmPw.text) {
				
				Mskg.API.Instance.sendNewPw (
					newPw.text,
					(Mskg.RtnObj ret) => { //callback
						if (ret.success) {

							Debug.Log("////////////");
							Debug.Log(ret);
							Debug.Log("////////////");

							AlertViewController.Show("確認", "パスワードを更新しました");

							this.gameObject.SetActive (false);
						}
					}
				);
					
			} else {
				AlertViewController.Show ("エラー", "新しいパスワードと確認のための入力が異なります。");
			}
		} else {
			AlertViewController.Show ("エラー", "現在のパスワードが間違っています。");
		}
	}



}
