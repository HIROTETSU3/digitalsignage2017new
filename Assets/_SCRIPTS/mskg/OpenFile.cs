using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using SFB;
using System.IO;
using RenderHeads.Media.AVProVideo;
using UnityEngine.Networking;

[RequireComponent(typeof(Button))]
public class OpenFile : MonoBehaviour, IPointerDownHandler {
    public string Title = "";
    public string FileName = "";
    public string Directory = "";
//    public string Extension = "";
    public bool Multiselect = false;

    public RawImage output;
	public string filepath;

	[SerializeField] M1_2_1BB_SrcEditor m1_2_1BB;
	[SerializeField] InputField txtID;
	[SerializeField] InputField txtPath;

	[SerializeField] MediaPlayer _mediaPlayer;
	[SerializeField] VCR_ProgramVideo VCR;
//	[SerializeField] M1_2_1C_Actions m1_2_1c;

	[SerializeField] RawImage previewImage;
	[SerializeField] DisplayUGUI previewMovie;

	[SerializeField] GameObject uploadingMark;

	private bool isMovie = false;
	[SerializeField] Button captureButton;
	[SerializeField] GameObject movieControllerPanel;

	private bool isMovieDialog = false;

	private DataAccessDlog hud;

#if UNITY_WEBGL && !UNITY_EDITOR
    //
    // WebGL
    //
    [DllImport("__Internal")]
    private static extern void UploadFile(string id);

    public void OnPointerDown(PointerEventData eventData) {
        UploadFile(gameObject.name);
    }

    // Called from browser
    public void OnFileUploaded(string url) {
        StartCoroutine(OutputRoutine(url));
    }
#else
    //
    // Standalone platforms & editor
    //
    public void OnPointerDown(PointerEventData eventData) { }

    void Start() {

		previewImage.gameObject.SetActive (false);
		previewMovie.gameObject.SetActive (false);

//		uploadingMark.SetActive (false);
		if (hud != null)
			hud.Dismiss();

		captureButton.gameObject.SetActive (isMovie);
		movieControllerPanel.SetActive (isMovie);

//        var button = GetComponent<Button>();
//        button.onClick.AddListener(OnClick);
    }

	public bool OnClick() {

		if (Global.Instance.openDialogPath == "") {
			Directory = System.Environment.GetFolderPath (Cfg.App.OPEN_DIALOG_DEFAULT_DIR);
		} else {
			Directory = Global.Instance.openDialogPath;
		}

//		extensions = new [] {
//			new ExtensionFilter("Image Files", "png", "jpg", "jpeg" ),
//			new ExtensionFilter("Movie Files", "mp4" ),
//			new ExtensionFilter("All Files", "*" ),
//		};

		ExtensionFilter[] extensions;
		if (isMovieDialog) { //動画指定の場合
			extensions = new [] {
				new ExtensionFilter("Movie Files", "mp4" )
			};
		} else { //画像指定の場合
			extensions = new [] {
				new ExtensionFilter("Image Files", "png", "jpg", "jpeg" )
			};
		}
		

		var paths = StandaloneFileBrowser.OpenFilePanel(Title, Directory, extensions, Multiselect);
        if (paths.Length > 0) {
			filepath = paths[0];
			Debug.Log("filepath: " + filepath);

			if (filepath == "" || filepath ==null)
				return false;

			string ext = Path.GetExtension (filepath);

			string localFilePath;
			string serverFileName;

			switch (ext)
			{
			case ".mp4":
			case ".MP4":
//				AlertViewController.Show ("動画", null);
				//src
//				string thisSrcPath = Cfg.App.LOCAL_DIR + "/" + thisProgram.src_list [0].src.path;
//				MediaPlayer.FileLocation _location = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
//				m1_2_1c.gameObject.SetActive (true);
				Global.Instance.openDialogPath = Path.GetDirectoryName (filepath);



				isMovie = true;
				previewImage.gameObject.SetActive (false);
				previewMovie.gameObject.SetActive (isMovie);
				//captureButton.gameObject.SetActive (isMovie);
				//movieControllerPanel.SetActive (isMovie);

				MediaPlayer.FileLocation _location = MediaPlayer.FileLocation.AbsolutePathOrURL;
				bool _autoStart = false;
				//				_mediaPlayer.OpenVideoFromFile (_location, filepath, _autoStart);

				//				VCR.LoadingPlayer = _mediaPlayer;
				VCR._location = _location;
				VCR._videoFiles [0] = filepath;
				VCR._videoFiles [1] = filepath;

				//オートスタートしないようにした
				VCR._mediaPlayer.m_AutoStart = false;
				VCR.OnOpenVideoFile();
				// 10秒の場所へ
				VCR._mediaPlayer.Control.Seek(10f);

				Debug.Log (VCR.PlayingPlayer);

				//				Take ();
				//				StartCoroutine(OutputRoutine(new System.Uri(filepath).AbsoluteUri, false));

				localFilePath = new System.Uri (filepath).AbsoluteUri;
				serverFileName = txtID.text + Path.GetExtension (localFilePath);



				//Fileサイズ
				long FileSize = getFileSize (filepath);



				if (FileSize / 1024 / 1024 < 100){
					//100MB未満は警告無しでアップロード

					VCR.OnOpenVideoFile ();

					StartCoroutine(OutputRoutine(localFilePath, serverFileName, false,  
						(Mskg.RtnObj ret) => { //callback
							if (ret.success) {
								Invoke("onCaptureThumbnail",0.3f);
								//AlertViewController.Show("成功", "アップロードに成功しました。");
							}else{
								AlertViewController.Show("エラー", "アップロード中にエラーが発生しました。");
							}
						}
					));

				} else if (FileSize / 1024 / 1024 > 280){
					//250MBまでのアップロード制限

					AlertViewController.Show("ファイルサイズ制限を超えています","size = " + (FileSize / 1024 / 1024) + "MB 最大280MBまで",
						new AlertViewOptions{
							cancelButtonTitle = "",
							okButtonTitle = "閉じる"
						});

				} else {
					//警告次第でアップロード

				AlertViewController.Show ("アップロード確認", "ファイルサイズが" + (getFileSize (filepath) / 1000 / 1000) + "MBあります。\n本当にアップロードしますか？", 
					new AlertViewOptions {
						cancelButtonTitle = "キャンセル",
						cancelButtonDelegate = () => {
							Debug.Log("キャンセルしました。");
							return;
						},
						okButtonTitle = "はい",
						okButtonDelegate = () => {
								//ハイのときだけ再生
								//VCR.OnOpenVideoFile ();
							StartCoroutine(OutputRoutine(localFilePath, serverFileName, false,  
								(Mskg.RtnObj ret) => { //callback
									if (ret.success) {
											//onCaptureThumbnail();
											Invoke("onCaptureThumbnail",0.3f);
										//AlertViewController.Show("成功", "アップロードに成功しました。");
									}else{
										AlertViewController.Show("エラー", "アップロード中にエラーが発生しました。");
									}
								}
							));

						},
					});

				}

				break;
			case ".png":
			case ".PNG":
			case ".jpg":
			case ".JPG":
				Global.Instance.openDialogPath = Path.GetDirectoryName (filepath);

				isMovie = false;
				previewImage.gameObject.SetActive (true);
				previewMovie.gameObject.SetActive (isMovie);
				captureButton.gameObject.SetActive (isMovie);
				movieControllerPanel.SetActive (isMovie);

//				Take ();
//				AlertViewController.Show ("filepath", filepath);
				localFilePath = new System.Uri(filepath).AbsoluteUri;
				serverFileName = txtID.text + Path.GetExtension (localFilePath);


				StartCoroutine(OutputRoutine(localFilePath, serverFileName, false,
//				StartCoroutine(OutputRoutine(filepath, serverFileName, false,  
					(Mskg.RtnObj ret) => { //callback
						if (ret.success) {
							onCaptureThumbnail();
							//AlertViewController.Show("成功", "アップロードに成功しました。");
						}else{
							AlertViewController.Show("エラー", "アップロード中にエラーが発生しました。");
						}
					}
				));
				break;
			default:
				//AlertViewController.Show ("その他", null);
				break;
			}
            
        }

		return true;
    }
		

#endif

	private IEnumerator OutputRoutine(string localFilePath, string serverFileName, bool isThumbnail ,Mskg.API.callback CallBack) {
//		AlertViewController.Show("アップロード", "URL: " + localFilePath);
		Debug.Log("アップロード URL: " + localFilePath);
		var loader = new WWW(localFilePath);
        yield return loader;
		if (!isThumbnail)
        	output.texture = loader.texture;

		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyFILENAME, serverFileName);

//		uploadingMark.SetActive (true);
		hud = DataAccessDlog.Show ("アップロード中です。", 0f, null);
		Mskg.API.Instance.upload (
			localFilePath, Cfg.API.uploadFile + ".php", 
			param_list,
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
//						AlertViewController.Show("成功", "アップロードに成功しました。");
						CallBack(new Mskg.RtnObj(true));

						if(!isThumbnail){
							updatePathInfo(serverFileName);
						}

					}else{
						CallBack(new Mskg.RtnObj(false));
					}
				}else{
					CallBack(new Mskg.RtnObj(false));
				}

//				uploadingMark.SetActive(false);
				hud.Dismiss();
			},
			(Mskg.RtnObj ret) => { //progress
				if (ret.success) {
					outputProgress ((Mskg.DownLoadObject)ret.obj);
				}
			}
		);
    }

	/// <summary>
	/// 画面をキャプチャーします
	/// </summary>
	protected string Take(string _name, bool isVideo = false)
	{

		Texture2D tex;

		if (isVideo) {
			tex = previewMovie.mainTexture as Texture2D;
		} else {
			tex = previewImage.texture as Texture2D;
		}

		float rate = tex.texelSize.x / tex.texelSize.y;
		int H = Mathf.RoundToInt(Cfg.App.THUMBNAIL_W * rate);
		TextureScale.Bilinear (tex, Cfg.App.THUMBNAIL_W, H);
		byte[] bytes = tex.EncodeToJPG ();
		string thisSrcPath = Cfg.App.LOCAL_DIR + "/" + _name + Cfg.App.THUMBNAIL + ".jpg";
		Debug.Log (thisSrcPath);
//		AlertViewController.Show ("Take " + Cfg.App.THUMBNAIL + ".jpg", thisSrcPath);
		File.WriteAllBytes (thisSrcPath, bytes);




		return "file://" + thisSrcPath;

//		saved_screen_capture = true;
	}

	public void onCaptureThumbnail(){
		
		string localThumbnailPath = Take (txtID.text, isMovie);
		string serverFileName = txtID.text + Cfg.App.THUMBNAIL + ".jpg";
		StartCoroutine(OutputRoutine(localThumbnailPath, serverFileName, true, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					VCR.OnPauseButton();
					AlertViewController.Show("アップロード", "アップロードを完了しました。");
				}else{
					VCR.OnPauseButton();
					AlertViewController.Show("エラー", "サムネイルのアップロード中にエラーが発生しました。");
				}
			}
		));

	}

	private void updatePathInfo(string pathname){

		txtPath.text = pathname;
		m1_2_1BB.onUpdateButton ();

	}

	public void setDialogType(bool _isMovieDialog){
		isMovieDialog = _isMovieDialog;
	}

	private long getFileSize(string _localPath){
		var fileInfo = new System.IO.FileInfo(_localPath.Replace("file://",""));
//		AlertViewController.Show ("ファイルサイズ", (fileInfo.Length / 1000 / 1000).ToString() + " MB");
//		Debug.Log(fileInfo.Length);
		return fileInfo.Length;
	}

	void outputProgress(object obj){
		Mskg.DownLoadObject dlo = (Mskg.DownLoadObject)obj;
//		AlertViewController.Show ("進捗", dlo.progress.ToString());
//		Debug.Log(dlo.progress.ToString());
		hud.progress (dlo.progress);
	}

	IEnumerator newUploader() { //未使用
		byte[] myData = System.Text.Encoding.UTF8.GetBytes("This is some test data");
		UnityWebRequest www = UnityWebRequest.Put("http://www.my-server.com/upload", myData);
		yield return www.Send();

		if(www.isError) {
			Debug.Log(www.error);
		}
		else {
			Debug.Log("Upload complete!");
		}
	}

}