﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LitJson;

public class KanriStarter : MonoBehaviour {
	
	[SerializeField] private Text appInfoText; 
	[SerializeField] private Text loadingText; 
//	[SerializeField] private string filename; 
//	[SerializeField] string filename = ""; //ClassBox.fileNameX filename;
	[SerializeField] loginPanel loginPanel;

	[SerializeField] private Text pregressOutput;
	[SerializeField] LogoFadeIn logoFader;
	[SerializeField] GameObject reTryButton;
	[SerializeField] InputField inputUserID;

	[SerializeField] SaveFile saveFile;

	List<string> dllist;
	Hashtable compHashTable = new Hashtable();
	int dlerrorCount;

	bool programInitStandBy = false;


	void Start () {

		SceneManager.activeSceneChanged += Global.Instance.OnActiveSceneChanged;

		dlerrorCount = 0;
		Global.Instance.uuid = Mskg.API.Instance.getUUID ();

		showReTryButton (false);


		Mskg.API.Instance.getUserID ((Mskg.RtnObj ret) => {

			if (ret.success) {
				Global.Instance.userid = ret.obj as string;
				inputUserID.text = Global.Instance.userid;
			}

			openLoginPanal();


			//同時にバージョンチェック
			Hashtable param_builds = new Hashtable (); // jsonでリクエストを送るのへッダ例
			param_builds.Add (Cfg.App.keyBUILD, Cfg.App.BUILD);
			Mskg.API.Instance.call (
				Cfg.API.checkBuild + ".php",
				param_builds, 
				(Mskg.RtnObj returnObj) => { //callback
					if (returnObj.success) {

						ClassBox.BuildObject[] buildObjects = null;
						try
						{
							buildObjects = LitJson.JsonMapper.ToObject<ClassBox.BuildObject[]>(returnObj.json);
						}
						catch (Exception ex)
						{
							Debug.Log(ex);
						}

						Debug.Log("////////////");
						Debug.Log(buildObjects);
						Debug.Log("////////////");

						ClassBox.BuildObject latestBuild = null;
						foreach (ClassBox.BuildObject build in buildObjects){
							latestBuild = build;
						}

						if (latestBuild != null){
							
								AlertViewController.Show ("アップデート", "最新ビルドは、build " + latestBuild.build.ToString() + " です。ダウンロードしますか？", 
								new AlertViewOptions {
									cancelButtonTitle = "いいえ",
									cancelButtonDelegate = () => {
										Debug.Log("いいえを選択しました。");
										return;
									},
									okButtonTitle = "はい",
									okButtonDelegate = () => {
										string saveDir = saveFile.getSaveDialogPath(latestBuild.filepath);
										if (saveDir == ""){ //cancel
											return;
										}
										downloadNewBuild(latestBuild, saveDir);
									},
								});


						}

					}

				}
			);

		});

		//チャンネル取得
		Hashtable param_channel = new Hashtable (); // jsonでリクエストを送るのへッダ例
//		param_channel.Add (Cfg.App.keyBUILD, Cfg.App.BUILD);
		Mskg.API.Instance.call (
			Cfg.API.getChannelList + ".php",
			param_channel, 
			(Mskg.RtnObj returnObj) => { //callback
				if (returnObj.success) {

//					ClassBox.ChannelData[] channelObjects = null;
					try
					{
						Global.Instance.ChannelDataArray = LitJson.JsonMapper.ToObject<ClassBox.ChannelData[]>(returnObj.json);
					}
					catch (Exception ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ChannelDataArray);
					Debug.Log("////////////");

				}

			}
		);
	}

	private void downloadNewBuild(ClassBox.BuildObject newBuild, string saveDir){
		Mskg.API.Instance.download (
			Cfg.App.BASE_URL + Cfg.App.BUILD_DIR + newBuild.filepath,
			//System.Environment.GetFolderPath (Cfg.App.OPEN_DIALOG_DEFAULT_DIR),
			saveDir, 
			(Mskg.RtnObj ret) => { //progress
				if (ret.success) {
					outputProgress ((Mskg.DownLoadObject)ret.obj);
				}
			},
			(Mskg.RtnObj ret) => { //complete
				outputProgress ((Mskg.DownLoadObject)ret.obj);
			},
			1000000,
			false
		);
		Debug.Log (System.Environment.GetFolderPath (Cfg.App.OPEN_DIALOG_DEFAULT_DIR));
	}

	void outputProgress(object obj){
		Mskg.DownLoadObject dlo = (Mskg.DownLoadObject)obj;
		Debug.Log (dlo.progress);
	}

	public void clearPrefs(){
		Mskg.API.Instance.clearPrefs ();
	}

	public void showReTryButton(bool arg){
		reTryButton.SetActive (arg);
	}

	public void openLoginPanal(){
		showReTryButton (false);
		loadingText.text = "UserIDとパスワードを入力してください。";
		loginPanel.clearPwField ();
		loginPanel.gameObject.SetActive(true);
	}

	//パスワード変更パネル用に保存
	private string pwEncBackup;
	private string userIdBackup;

	public void login(string useridString, string pwEncString){

		loadingText.text = "Logging ...";

		//パスワード変更ダイアログ用に記録
		userIdBackup = useridString;


		Hashtable param_staff = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_staff.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.userlogin + ".php",
			param_staff, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.UserLogin loginObj = null;
					try
					{
						loginObj =  LitJson.JsonMapper.ToObject<ClassBox.UserLogin>(ret.json);
					}
					catch (Exception ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(loginObj);
					Debug.Log("////////////");
					string test = Mskg.API.Instance.Aes128CBCDecode(pwEncString, Global.Instance.pwKey, Global.Instance.pwSalt);

//					Debug.Log("loginObj.user.password = " + loginObj.user.password);
//					Debug.Log("pwEncString            = " + pwEncString);

					if (loginObj.success && loginObj.user.password == pwEncString) {
						Global.Instance.Me = loginObj.user;

						pwEncBackup = pwEncString;
						//ココらへんで期限切れ、または期限切れ前30日かをチェックして分岐してください。
						//その際、パスワードをこのクラスにでも保存してください。（入力比較用）


						//StartCoroutine("ChkPasswordExp");
						StartCoroutine(ChkPasswordExp(loginObj.user));

					}else{
						showReTryButton (true);
						loadingText.text = "エラーが発生しました。ログインIDとパスワードをご確認ください。";
					}

				}

			}
		);


	}

	//ヒロ追加　パスワードの期限切れのチェック
	IEnumerator	ChkPasswordExp(ClassBox.Staff loginUser){

		// ★★★★白石さん
		//パスワードの期限切れをチェックして、PasswordModifiedDateStringに最終更新日をいれてください。









		// 0.5秒待つ （ダミー）↓ほんとはAPIの戻りで
		yield return new WaitForSeconds (0.2f);  

		//パスワードのmodified 日付を現在の日付と比較
		//↓実際はDBから受け取ったmodifieddateを入れてください。
		string PasswordModifiedDateString = loginUser.modifieddate; //"2017-09-17 06:00:00"; //<-- 日付　modifieddate

		DateTime PasswordModifiedDate = DateTime.Parse(PasswordModifiedDateString);
		//↓期限日
		DateTime LimitDate = PasswordModifiedDate + TimeSpan.FromDays(90d);
		//↓現在
		DateTime Today = DateTime.Now;

		TimeSpan PassLifeTime = Today-PasswordModifiedDate;

		Debug.Log("PassLifeTime.TotalDays = " + PassLifeTime.TotalDays);
		Debug.Log("期限日 " + LimitDate.ToString());


		if (PassLifeTime.TotalDays > 90d){
			//期限切れ
			OpenChangePassword(true);
		} else if(PassLifeTime.TotalDays > 60d){
			//期限が近づいている
			PswdExpDlogViewController.Show(
				LimitDate.ToString(),
				new PswdExpDlogOptions{
					cancelButtonTitle = "そのままログイン",
					cancelButtonDelegate = ()=>{
						Debug.Log("---> ログイン");
						StartLogin();
					},
					okButtonTitle = "パスワードを変更",
					okButtonDelegate = ()=>{
						Debug.Log("---> パスワードを変更");
						OpenChangePassword();
					}
			});
		} else {
			//PswdExpDlogViewController
			StartLogin();

		}


	}

	//ヒロ追加　パスワードの変更、もしくは変更が終了しログイン
	public void StartLogin(){

		loadingText.gameObject.SetActive(false);
		appInfoText.gameObject.SetActive(false);


		logoFader.FadeStart (() => { //callback
			programInitStandBy = true;
		});

	}

	public void OpenChangePassword(bool expaird = false){

		Debug.Log("---> ChangePassWord");
		StartCoroutine("ChangePasswordDlogOpen",expaird);

	}

	IEnumerator	ChangePasswordDlogOpen(bool expaird){
		//一瞬間を空けるためだけのyield
		yield return new WaitForSeconds (0.1f);  

		if (expaird){
			//期限切れの場合の強制変更用
			ChangePswdDlogViewController.Show(
				new ChangePswdDlogOptions{
					cancelButtonTitle = "終了",
					cancelButtonDelegate = ()=>{
						Debug.Log("---> 終了");
						Quit();
					},
					okButtonTitle = "パスワードを変更",
					okButtonDelegate = ()=>{
						Debug.Log("---> パスワードを変更");

						TryChangePassword();
					},
					userId =userIdBackup,
					oldLoginPswd =pwEncBackup,
					Expaird = expaird
				});

		} else {
			//期限内の場合の任意変更用
			ChangePswdDlogViewController.Show(
				new ChangePswdDlogOptions{
					cancelButtonTitle = "変更せずにログイン",
					cancelButtonDelegate = ()=>{
						Debug.Log("---> ログイン");
						StartLogin();
					},
					okButtonTitle = "パスワードを変更",
					okButtonDelegate = ()=>{
						Debug.Log("---> パスワードを変更");
						TryChangePassword();
					},
					userId =userIdBackup,
					oldLoginPswd =pwEncBackup,
					Expaird = expaird
				});

		}

		// ★★★★白石さん　↑ 
		// マイアカウントからのパスワード変更は 
		// cancelButtonTitle = "キャンセル"
		// cancelButtonDelegate = ()=>{Debug.Log("Cancel")}
		// でいいと思います。

	}

	public void TryChangePassword(){

		StartCoroutine("CallChangePassword");
	}

	IEnumerator CallChangePassword(){
		Debug.Log("TryChangePassword");
		string newPswd = Global.Instance.NewPswd;

		// ★★★★白石さん、ここでパスワードを更新してください。
		// newPswdはエンコードされています。






		//ダミーで待ちを↓ほんとはAPIの戻りで
		yield return new WaitForSeconds (0.1f);

		StartLogin();
	}

	public void Quit(){

		Debug.Log("Quit");

		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif

	}



	void Update(){

		if (programInitStandBy && Global.Instance.logoFadeComp) {
			programInit();
			programInitStandBy = false;
		}
	}

	public void programInit(){
		SceneManager.LoadScene ("KanriMain");
	}

	//パスワード変更ダイアログ用
	public bool oldPwCheck(string pw){
		return (pwEncBackup == pw);
	}

}
