﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemListTableViewCell : TableViewCell<itemClass>
{
	[SerializeField] private Text nameLabel;	// アイテム名を表示するテキスト

	public override void UpdateContent(itemClass itemData)
	{
		nameLabel.text = itemData.name;
	}


}

