﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PushUIManager : MonoBehaviour {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private Push1stView push1st;
	[SerializeField] private Push2ndView push2nd;

	// Use this for initialization
	void Start () {

		push1st.gameObject.SetActive (false);
		push2nd.gameObject.SetActive (false);

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(push1st);
		}
		#endregion
	}

}
