﻿using UnityEngine;
using System.Collections;

public class Push2ndView : ViewController {

	[SerializeField] private NavigationViewController navigationView;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPressButton()
	{
		navigationView.Pop();
	}
}
