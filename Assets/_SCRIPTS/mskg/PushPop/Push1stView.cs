﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Push1stView : ViewController {

	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private Push2ndView push2nd;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPressButton()
	{
		navigationView.Push(push2nd);
	}

}
