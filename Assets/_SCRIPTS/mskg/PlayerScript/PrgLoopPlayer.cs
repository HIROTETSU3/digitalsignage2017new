﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PrgLoopPlayer : MonoBehaviour {

	ClassBox.ProgramsListItem[] todaysPrograms;
	ClassBox.ProgramsListItem[] specialPrograms;

	public float timeOut;

	private string nowSceneName = "";

	private bool requesting = false;
	List<string> dllist;
	Hashtable compHashTable = new Hashtable();
	int dlerrorCount;


	// Use this for initialization
	void Start () {
		if (Global.Instance.onFoundPlayingProgram == null)
			Global.Instance.onFoundPlayingProgram = new UnityEvent ();
		if (Global.Instance.onReleasePlayingProgram == null)
			Global.Instance.onReleasePlayingProgram = new UnityEvent ();


		todaysPrograms = Global.Instance.shopSettingsObj.todays_loop.programs;
		specialPrograms = Global.Instance.shopSettingsObj.todays_loop.specialprograms;

		//時刻順に並べる
		Array.Sort(specialPrograms, (a, b) => (int)DateTime.Parse(a.time).Ticks - (int)DateTime.Parse(b.time).Ticks);

		if (Global.Instance.onPlayerDone == null)
			Global.Instance.onPlayerDone = new UnityEvent ();
		Global.Instance.onPlayerDone.AddListener (onPlayerDone);

		loadProgramPlayer ();

		if (!Global.Instance.PlayerPreviewMode) { //PlayerApp only
			StartCoroutine (CoPlayCheck (Cfg.App.PLAYER_CHECK_INTERVAL));
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void onPlayerDone(){
		Debug.Log ("onPlayerDone");
		loadProgramPlayer ();
	}

	void loadProgramPlayer(){

//		foreach (ClassBox.ProgramsListItem programItem in Global.Instance.shopSettingsObj.todays_loop.programs) 
//		{
//			Debug.Log (programItem.program.content_type);
//		}

//		switch (Global.Instance.shopSettingsObj.todays_loop.programs [Global.Instance.currentProgram].program.content_type) {
//		case 1:
//			SceneManager.LoadSceneAsync("ProgramPlayerType2", LoadSceneMode.Additive);
//			break;
//		case 2:
//			SceneManager.LoadSceneAsync("ProgramPlayerType2", LoadSceneMode.Additive);
//			break;
//		default:
//			break;
//		}

		if (todaysPrograms.Length <= Global.Instance.currentProgram) { //現在のプログラム番号が総プログラム数を上回った場合ループ
			Global.Instance.currentProgram = 0;
		}

		if (0 < Global.Instance.LocalProgramList.Count) { //ローカル再生が存在する場合
			
			Global.Instance.nowProgram = Global.Instance.LocalProgramList[0];
			Global.Instance.LocalProgramList.RemoveAt (0);

		} else {
			//スペシャルプログラムの確認
			foreach (ClassBox.ProgramsListItem specialProgram in Global.Instance.shopSettingsObj.todays_loop.specialprograms) 
			{
				if (specialProgram.done == false && (DateTime.Parse (specialProgram.time)) < DateTime.Parse (DateTime.Now.ToString("HH:mm:ss"))) {
					Global.Instance.nowProgram = specialProgram.program;
					Global.Instance.isSpecialProgram = true;
					specialProgram.done = true;
					break;
				} else {
					Global.Instance.isSpecialProgram = false;
				}
			}

			if (Global.Instance.isSpecialProgram == false){ //通常のプログラム
				Global.Instance.nowProgram = Global.Instance.shopSettingsObj.todays_loop.programs [Global.Instance.currentProgram].program;
			}
		}

		int type = Global.Instance.nowProgram.content_type; //todaysPrograms [Global.Instance.currentProgram].program.content_type;
		nowSceneName = "PrgPlayerType" + type;
		//SceneManager.LoadSceneAsync(nowSceneName, LoadSceneMode.Additive);
		SceneManager.LoadScene(nowSceneName, LoadSceneMode.Additive);
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}


	//再生すべきコンテンツがないかチェック
	IEnumerator CoPlayCheck(float time) {
		yield return new WaitForSeconds (time);

		if (!isAutoClosing)
			checkCloseTime ();

		Debug.Log ("CHECK!!");
		//再度ローカル再生検知を開始
		StartCoroutine(CoPlayCheck(Cfg.App.PLAYER_CHECK_INTERVAL) );

		if (!requesting) {

			int defaultPlayedID = 0;
			if (PlayerPrefsX.GetInt (Cfg.App.keyPLAYEDID) != null) {
				defaultPlayedID = PlayerPrefsX.GetInt (Cfg.App.keyPLAYEDID);
			}

			Hashtable param_localprog = new Hashtable (); // jsonでリクエストを送るのへッダ例
			param_localprog.Add (Cfg.App.keySHOPID, Global.Instance.shopid);
			param_localprog.Add (Cfg.App.keyPLAYEDID, defaultPlayedID);
			requesting = true;
			Mskg.API.Instance.call (
				Cfg.API.getLocalPlayingProgram + ".php",
				param_localprog, 
				(Mskg.RtnObj ret) => { //callback
					requesting = false;
					if (ret.success) {

						ClassBox.Program[] localPrograms = null;
						try {
							localPrograms = LitJson.JsonMapper.ToObject<ClassBox.Program[]> (ret.json);
						} catch (NullReferenceException ex) {
							Debug.Log (ex);
						}



						//					if (0 < Global.Instance.LocalProgramList.Count){ //ローカル再生が存在する場合
						if (0 < localPrograms.Length) { //ローカル再生が存在する場合

							Global.Instance.LocalProgramList.AddRange (localPrograms);

							Debug.Log ("////////////");
							Debug.Log (Global.Instance.LocalProgramList);
							Debug.Log ("////////////");

							PlayerPrefsX.SetInt(Cfg.App.keyPLAYEDID, localPrograms[0].playedid);

							if (localPrograms[0].programid == Cfg.App.RESTART_ID){
//								AlertViewController.Show("再起動", "再起動信号を受信しました。");
								Mskg.API.Instance.restart();
								return;
							}

							foundPlayingProgram ();
						}

					}

				}
			);

		}
//
//		//StartCoroutine( CoPlayCheck(Cfg.App.PLAYER_CHECK_INTERVAL) );
	}



	void foundPlayingProgram(){
		Global.Instance.onFoundPlayingProgram.Invoke ();

		dllist = new List<string>();
		foreach (ClassBox.Program _program in Global.Instance.LocalProgramList)
		{
			foreach (ClassBox.SrcListItem srcListItem in _program.src_list)
			{
				dllist.Add(Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + srcListItem.src.path);
			}
		}

		Debug.Log("----SRC----");
		foreach (string item in dllist){
			Debug.Log(item);
		}
		Debug.Log(Cfg.App.LOCAL_DIR);
		Debug.Log("--------");

		Mskg.API.Instance.downloadSrcList(
			dllist.ToArray(),
			Cfg.App.LOCAL_DIR,
			(Mskg.RtnObj ret1) => {
				outputProgress(ret1.obj);
			},
			Global.Instance.dlExistenceCheck,
			false
		);
	}
	//

	void outputProgress(object obj){
		Mskg.DownLoadObject dlo = (Mskg.DownLoadObject)obj;
		if (dlo.complete || dlo.existence) {
			string randomStr = Mskg.API.Instance.randomPass(8,8);
			compHashTable.Add ("asset" + randomStr + dlo.uid, dlo.complete);
			Debug.Log (compHashTable.ToString ());
//			pregressOutput.text += "id : " + dlo.uid + 
			string _str = "id : " + dlo.uid + 
				" pregoress : " + dlo.progress + 
				" complete : " + dlo.complete + 
				" error : " + dlo.error + 
				" existence : " + dlo.existence + 
				"\n";
			Debug.Log (_str);

			if (dlo.error) {
				dlerrorCount++;
			}

			if (dllist.Count == compHashTable.Count) {

				if (0 < dlerrorCount) {
//					pregressOutput.text += "エラーがあります。" +
//						"\n";
					Debug.Log ("ダウンロードエラーがあります。");
				} else {
//					SceneManager.LoadScene("PrgLoopPlayer", LoadSceneMode.Single);
					compHashTable = new Hashtable();
					Debug.Log ("★★★★★★★★");
					SceneManager.UnloadSceneAsync (nowSceneName); //"ProgramPlayerType2");
					Global.Instance.onPlayerDone.Invoke ();
				}
				Global.Instance.onReleasePlayingProgram.Invoke ();

			} else {
				Debug.Log ("★A★A★A★A★A★A★A★");
				Debug.Log ("" + dllist.Count.ToString() + " : " + compHashTable.Count.ToString());
				Debug.Log ("★A★A★A★A★A★A★A★");
			}
		}

	}


	ShopCloseBoard sc;
	bool isAutoClosing = false;

	void checkCloseTime(){

		if (Global.Instance.closetime < DateTime.Now) {
			isAutoClosing = true;
			sc = ShopCloseBoard.Show();
			Invoke("CloseWindow", Cfg.App.CLOSING_MESSAGE_TIME);
		}

	}

	void CloseWindow(){
//		sc.Close();
		Application.Quit();
	}


}
