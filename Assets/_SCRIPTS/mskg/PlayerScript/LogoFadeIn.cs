﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LogoFadeIn : MonoBehaviour {

	public delegate void callback();
	public float FadeTime = 1.0f;
	public float StartDelay = 0f;
	public float FadeOutDelay = 0.7f;

	Image MyImage;
	Color StartColor;

	// Use this for initialization
	void Awake () {

		MyImage = GetComponent<Image>();
		StartColor = MyImage.color;
		MyImage.color = new Color (StartColor.r, StartColor.g, StartColor.b, 0f);
	}

	void OnEnable(){
		//FadeStart ();
	}

	public void FadeStart(callback callback){
		Global.Instance.logoFadeComp = false;
		MyImage.color = new Color(StartColor.r,StartColor.g,StartColor.b,0f);
		iTween.ValueTo(gameObject,
			iTween.Hash(
				"delay",StartDelay,
				"from", 0f, 
				"to", 1f, 
				"time", FadeTime, 
				"onupdate", "SetValue",
				"oncomplete", "FadeInComplete",
				"oncompleteparams", callback
			)
		);
	}

	public void FadeOut(callback callback){
		Global.Instance.logoFadeComp = false;
		MyImage.color = new Color(StartColor.r,StartColor.g,StartColor.b,1f);
		iTween.ValueTo(gameObject,
			iTween.Hash(
				"delay",FadeOutDelay,
				"from", 1f, 
				"to", 0f, 
				"time", FadeTime / 2, 
				"onupdate", "SetValue",
				"oncomplete", "FadeOutComplete",
				"oncompleteparams", callback
			)
		);
	}


	void OnDiable(){
		iTween.Stop(gameObject);
	}

	private void FadeInComplete(callback callback){
		FadeOut (callback);
	}

	private void FadeOutComplete(callback callback){
		Global.Instance.logoFadeComp = true;
		callback ();
	}

	void SetValue (float f) {
		MyImage.color = new Color(StartColor.r,StartColor.g,StartColor.b,f);
	}
}
