﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using RenderHeads.Media.AVProVideo;

public class PrgPlayerType1 : PrgPlayerBase {
	
	[SerializeField] GameObject mediaDisplay;
	public MediaPlayer _mediaPlayer;

	protected override void Start ()
	{
		thisSceneName = "PrgPlayerType1";
		base.Start ();
		_mediaPlayer.Events.AddListener(OnVideoEvent);
		mediaDisplay.SetActive (false);

		//src
		string thisSrcPath = Cfg.App.LOCAL_DIR + "/" + thisProgram.src_list [0].src.path;
		MediaPlayer.FileLocation _location = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
		bool _autoStart = false;
		_mediaPlayer.OpenVideoFromFile(_location, thisSrcPath, _autoStart);
	}


	protected override void playBasePage(){
		base.playBasePage ();
		StartCoroutine(startMovieScene((float)thisProgram.basic_page_time));
	}


	IEnumerator startMovieScene (float time){
		yield return new WaitForSeconds (time);
		mediaDisplay.SetActive (true);
		fadeBaseObjects(0.0f, Cfg.App.fadeBaseTime);

		_mediaPlayer.Control.Play();
	}

	public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
	{
		switch (et)
		{
		case MediaPlayerEvent.EventType.ReadyToPlay:
			break;
		case MediaPlayerEvent.EventType.Started:
			break;
		case MediaPlayerEvent.EventType.FirstFrameReady:
			break;
		case MediaPlayerEvent.EventType.FinishedPlaying:
			StartCoroutine(coDoEnding(0.0f));
			break;
		}

		Debug.Log("PrgPlayerType1 Event: " + et.ToString());
	}

}
