﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerTopLayer : MonoBehaviour {
	
	void Start () {

	}

	void Update(){
	}

	public void onCloseButton(){
		unloadSceneByName("PlayerStarter");
		unloadSceneByName("PrgLoopPlayer");
		unloadSceneByName("PrgPlayerType1");
		unloadSceneByName("PrgPlayerType2");
	}

	private void unloadSceneByName(string name){
		if (SceneManager.GetSceneByName (name) != null) {
			SceneManager.UnloadSceneAsync (name);
		}
	}
}
