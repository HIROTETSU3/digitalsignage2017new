﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

/*
transition_type = 0 がトランジッションなしで切り替え
0 クロスフェード
1 ワイプ →
2 ワイプ ←
3 ワイプ ↑
4 ワイプ ↓
5 押出 →
・・・
*/


public class PrgPlayerType2 : PrgPlayerBase {
	
	protected Vector2 baseDelta;
	protected Vector2 rawimgSizeDelta;
	protected bool lastFlag = false;
	protected int counter = 0;

	[SerializeField] protected GameObject slideScene;
	[SerializeField] protected RawImage thisImage;
	[SerializeField] protected RawImage nextImage;
	[SerializeField] protected Text thisCaption;
	[SerializeField] protected Text nextCaption;

	[SerializeField] Image TextBg1;
	[SerializeField] Image TextBg2;

//	Texture2D thisTexture; // = new Texture2D(200, 200);
//	Texture2D nextTexture; // = new Texture2D(200, 200);

	Rect thisImageRect;
	protected override void Start ()
	{
		Global.Instance.thisTexture = new Texture2D(1,1); //thisTexture = new Texture2D(1,1);
		Global.Instance.nextTexture = new Texture2D(1,1); //nextTexture = new Texture2D(1,1);
		thisImage.texture = Global.Instance.thisTexture; //new Texture2D(200, 200);
		nextImage.texture = Global.Instance.nextTexture; //new Texture2D(200, 200);

		thisSceneName = "PrgPlayerType2";
		base.Start ();
	}

	protected override void playBasePage(){
		base.playBasePage ();
		StartCoroutine(startSlideScene((float)thisProgram.basic_page_time));
	}


	IEnumerator startSlideScene (float time){
		yield return new WaitForSeconds (time);

		fadeBaseObjects(0.0f, Cfg.App.fadeBaseTime);
		slideScene.SetActive (true);

		playSlideScene ();
	}

	void playSlideScene(){
		thisSrcListItem = thisProgram.src_list [currentSrcNum];
		nextSrcListItem = thisProgram.src_list [currentSrcNum + 1];

		if ((currentSrcNum + 2) == thisProgram.src_list.Length) {
			lastFlag = true;
		}

		initImage ();

		StartCoroutine(showImage((float)thisSrcListItem.duration));
	}

	IEnumerator showImage(float time) {
		yield return new WaitForSeconds (time);

		counter++;
//		Debug.Log (counter);

		iTween.ValueTo (gameObject,
			iTween.Hash (
				"from", 1.0f,
				"to", 0.0f,
				"time", Cfg.App.fadeSlideTime,
				"easetype", "easeOutCubic",
				"onUpdate", "TransitionUpdate",
				"oncomplete", "TransitionComplete"
			)
		);

	}


	private void initImage(){
		


		setImageToRawImage (thisImage, Cfg.App.LOCAL_DIR + "/" + thisSrcListItem.src.path);
		setImageToRawImage (nextImage, Cfg.App.LOCAL_DIR + "/" + nextSrcListItem.src.path);

		/*
		string thisImagePath = new System.Uri (Cfg.App.LOCAL_DIR + "/" + thisSrcListItem.src.path).AbsoluteUri;
		string nextImagePath = new System.Uri (Cfg.App.LOCAL_DIR + "/" + nextSrcListItem.src.path).AbsoluteUri;

		StartCoroutine (setImageToRawImage2 (thisImage, thisImagePath ));
		StartCoroutine (setImageToRawImage2 (nextImage, nextImagePath ));
*/
		//		nextImage.GetComponent<CanvasGroup> ().alpha = 0.0f;

		//caption
		thisCaption.text = thisSrcListItem.caption;
		nextCaption.text = nextSrcListItem.caption;

		TextBg1.enabled = !(thisSrcListItem.caption == "");
		TextBg2.enabled = !(nextSrcListItem.caption == "");

		thisImageRect = thisImage.gameObject.GetComponent<RectTransform> ().rect;

		TransitionInit ();
	}

	private void showAll(){
		thisImage.color = colorShow;
		nextImage.color = colorShow;

		thisCaption.color = colorShow;
		nextCaption.color = colorShow;
	}

	private void TransitionInit(){

		thisProgram.transition_type = 0; //どんな場合でもクロスフェード
		switch (thisProgram.transition_type) {
		case 0:
			//		color.a = 0.0f;
			thisImage.color = colorShow;
			nextImage.color = colorHidden;

			thisCaption.color = colorShow;
			nextCaption.color = colorHidden;

			TextBg1.color = bgcolorShow;
			TextBg2.color = bgcolorHidden;

			break;

		case 1:
			showAll ();
			nextImage.transform.position = new Vector2 (thisImageRect.width * -1, 0);
			break;

		case 2:
			showAll ();
			nextImage.transform.position = new Vector2 (thisImageRect.width, 0);
			break;
		case 3:
			showAll ();
			nextImage.transform.position = new Vector2 (0, thisImageRect.height * -1);
			break;
		case 4:
			showAll ();
			nextImage.transform.position = new Vector2 (0,thisImageRect.height);
			break;
		case 5:
			showAll ();
			nextImage.transform.position = new Vector2 (thisImageRect.width * -1, 0);
			break;
		case 6:
			showAll ();
			nextImage.transform.position = new Vector2 (thisImageRect.width, 0);
			break;
		case 7:
			showAll ();
			nextImage.transform.position = new Vector2 (0, thisImageRect.height * -1);
			break;
		case 8:
			showAll ();
			nextImage.transform.position = new Vector2 (0,thisImageRect.height);
			break;
		}


	}

	private void TransitionUpdate(float fade){
		Vector3 t_pos = thisImage.transform.localPosition;
		Vector3 n_pos = nextImage.transform.localPosition;

		thisProgram.transition_type = 0; //どんな場合でもクロスフェード
		switch (thisProgram.transition_type) {
		case 0:
			Color color = thisImage.color;
			color.a = fade;
			thisImage.color = color;
			thisCaption.color = color;

			Color bgcolor = TextBg1.color;
			bgcolor.a = (fade * 0.6f);
			TextBg1.color = bgcolor;


			Color color2 = nextImage.color;
			color2.a = 1.0f - fade;
			nextImage.color = color2;
			nextCaption.color = color2;

			Color bgcolor2 = TextBg2.color;
			bgcolor2.a = 0.6f - fade;
			TextBg2.color = bgcolor2;

			break;
		case 1:
			n_pos.x = thisImageRect.width * (1.0f - fade) + (thisImageRect.width * -1);
			nextImage.transform.localPosition = n_pos;
			break;
		case 2:
			n_pos.x = (thisImageRect.width) - thisImageRect.width * (1.0f - fade);
			nextImage.transform.localPosition = n_pos;
			break;
		case 3:
			n_pos.y = thisImageRect.height * (1.0f - fade) + (thisImageRect.height * -1);
			nextImage.transform.localPosition = n_pos;
			break;
		case 4:
			n_pos.y = (thisImageRect.height) - thisImageRect.height * (1.0f - fade);
			nextImage.transform.localPosition = n_pos;
			break;
		case 5:
			n_pos.x = thisImageRect.width * (1.0f - fade) + (thisImageRect.width * -1);
			nextImage.transform.localPosition = n_pos;
			t_pos.x = thisImageRect.width * (1.0f - fade);
			thisImage.transform.localPosition = t_pos;
			break;
		case 6:
			n_pos.x = (thisImageRect.width) - thisImageRect.width * (1.0f - fade);
			nextImage.transform.localPosition = n_pos;
			t_pos.x = 0 - thisImageRect.width * (1.0f - fade);
			thisImage.transform.localPosition = t_pos;
			break;
		case 7:
			n_pos.y = thisImageRect.height * (1.0f - fade) + (thisImageRect.height * -1);
			nextImage.transform.localPosition = n_pos;
			t_pos.y = thisImageRect.height * (1.0f - fade);
			thisImage.transform.localPosition = t_pos;
			break;
		case 8:
			n_pos.y = (thisImageRect.height) - thisImageRect.height * (1.0f - fade);
			nextImage.transform.localPosition = n_pos;
			t_pos.y = 0 - thisImageRect.height * (1.0f - fade);
			thisImage.transform.localPosition = t_pos;
			break;
		}
	}

	private void TransitionComplete(){
		//		thisImage.SetActive (false);
		//		nextImage.GetComponent<CanvasGroup>().alpha = 1.0f;

		if (lastFlag) {
//			unloadTexture(thisTexture);
//			unloadTexture(nextTexture);
			doEnding ();
		} else {
			currentSrcNum++;
			playSlideScene ();
		}

	}





	void setImageToRawImage(RawImage rawimage, string path){
		byte[] bytes = File.ReadAllBytes(path);

//		rawimage.texture = new Texture2D(1,1); //new Texture2D(200, 200);
		rawimage.texture.filterMode = FilterMode.Trilinear;
		(rawimage.texture as Texture2D).LoadImage(bytes);
//		rawimage.texture = texture;
		rawimage.SetNativeSize();

//		MonoBehaviour.Destroy(texture);
//		unloadTexture (texture);
		//rawimage.GetComponent<RectTransform> ().sizeDelta = Mskg.API.Instance.calcRect (baseDelta, rawimgSizeDelta);

	}

	void unloadTexture(Texture2D texture){
		texture = null;
		GC.Collect ();
	}

	/*
	IEnumerator setImageToRawImage2(RawImage rawimage, string path){

		// wwwクラスのコンストラクタに画像URLを指定
		string url = path;
		WWW www = new WWW(url);

		// 画像ダウンロード完了を待機
		yield return www;

		// webサーバから取得した画像をRaw Imagで表示する
		//		RawImage rawImage = GetComponent<RawImage>();
		rawimage.texture = www.textureNonReadable;

		//ピクセルサイズ等倍に
		rawimage.SetNativeSize();
	}
	*/

	override protected IEnumerator coUnload(float time){
		yield return new WaitForSeconds (time);

		if (Global.Instance.isSpecialProgram == false){
			Global.Instance.currentProgram++;
		}

		Destroy(Global.Instance.thisTexture);
		Destroy(Global.Instance.nextTexture);

		SceneManager.UnloadSceneAsync(thisSceneName); //"ProgramPlayerType2");
		Global.Instance.onPlayerDone.Invoke ();
	}

}
