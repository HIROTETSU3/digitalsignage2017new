﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LitJson;

public class PlayerStarter : MonoBehaviour {
	
	[SerializeField] private Text appInfoText; 
	[SerializeField] private Text loadingText; 
//	[SerializeField] private string filename; 
//	[SerializeField] string filename = ""; //ClassBox.fileNameX filename;
	[SerializeField] GameObject inputPanel;

	[SerializeField] private Text pregressOutput;
	[SerializeField] LogoFadeIn logoFader;
	[SerializeField] GameObject reTryButton;
	[SerializeField] GameObject progresBar;
    [SerializeField] Text targetFileText;
    [SerializeField] Text progresText;
	[SerializeField] GameObject contentsObjects;

	List<string> dllist;
	Hashtable compHashTable = new Hashtable();
	int dlerrorCount;

	bool programInitStandBy = false;
	Vector2 barSizeDelta;
	float StageW = 0f;
//	float cnt = 0f;

	void Start () {
		//progressBar init
		StageW = (float)Screen.width;
		barSizeDelta = new Vector2 (0f, 4f);
		progresBar.GetComponent<RectTransform>().sizeDelta = barSizeDelta;


		if (Global.Instance.PlayerPreviewMode) {
//			AlertViewController.Show("Preview Mode",null);
			logoFader.StartDelay = 0f;
			logoFader.FadeTime = 0f;

		}

		SceneManager.activeSceneChanged += Global.Instance.OnActiveSceneChanged;

		dlerrorCount = 0;
		Global.Instance.uuid = Mskg.API.Instance.getUUID ();

		showReTryButton (false);

		Mskg.API.Instance.getShopID ((Mskg.RtnObj ret) => {

			if (ret.success) {
				inputPanel.SetActive(false);
				Global.Instance.shopid = ret.obj as string;
				login();
			}else{
				openInputIdPanal();
			}

		});

	}







	public void clearPrefs(){
		Mskg.API.Instance.clearPrefs ();
	}

	public void showReTryButton(bool arg){
		reTryButton.SetActive (arg);
	}

	public void openInputIdPanal(){

        //ここはダイアログに変更予定。（タスク）

		showReTryButton (false);
		loadingText.text = "ShopIDを入力してください。";
		inputPanel.SetActive(true);
	}

	public void login(){
		loadingText.text = "Loading ...";

		Hashtable param_shopplayerlogin = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_shopplayerlogin.Add (Cfg.App.keySHOPID, Global.Instance.shopid);
		Mskg.API.Instance.call (
			Cfg.API.shopplayerlogin + ".php", //".php"は仮 
			param_shopplayerlogin, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.ShopPlayerLogin loginObj = null;
					try
					{
						loginObj =  LitJson.JsonMapper.ToObject<ClassBox.ShopPlayerLogin>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(loginObj);
					Debug.Log(LitJson.JsonMapper.ToJson(loginObj));
					//					Debug.Log(jsonObject);
					Debug.Log("////////////");
					if (loginObj.success) {
						loadingText.gameObject.SetActive(false);
						appInfoText.gameObject.SetActive(false);

						if (Global.Instance.PlayerPreviewMode) { //プレビューモードではロゴ表示なし
							programInitStandBy = true;
						}else{
							logoFader.FadeStart (() => { //callback
								programInitStandBy = true;
							});
						}

					}else{
						showReTryButton (true);
						loadingText.text = "エラーが発生しました。ShopIDをご確認ください。";
					}

				}
			}
		);

	}

	void Update(){

		if (programInitStandBy && Global.Instance.logoFadeComp) {
			programInit();
			programInitStandBy = false;
		}
	}

	public void programInit(){

		//カーソルを消す
		//Cursor.visible = false;


		/// 店情報取得初期APIサンプル
		Hashtable param_shopinit = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_shopinit.Add (Cfg.App.keySHOPID, Global.Instance.shopid);
		param_shopinit.Add(Cfg.App.keyTODAY, DateTime.Now.ToString(Cfg.App.DATE_FORMAT)); //2017-07-28
		//		param_shopinit.Add (Cfg.App.keyUUID, Global.Instance.uuid);

		if (Global.Instance.PlayerPreviewMode && Global.Instance.PlayerPreviewProgramID != -1) {
			param_shopinit.Add (Cfg.App.keyPREVIEWPROG, Global.Instance.PlayerPreviewProgramID);
		}

		Mskg.API.Instance.call (
			Cfg.API.getShopSettings + ".php", //".php"は仮 
			param_shopinit, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					try
					{
						Global.Instance.shopSettingsObj =  LitJson.JsonMapper.ToObject<ClassBox.ShopSettings>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					//					//データの取得
					////					string message = (string)jsonData["message"];
					////					int num = (int)jsonData["num"];

					Debug.Log("########");
					Debug.Log(Global.Instance.shopSettingsObj);
					Debug.Log(LitJson.JsonMapper.ToJson(Global.Instance.shopSettingsObj));
					//					Debug.Log(jsonObject);
					Debug.Log("########");

//					string now = DateTime.Now.ToString();
//					Debug.Log(now);
//					DateTime.Parse("01/01/2017");
//					Debug.Log(DateTime.Parse("01/01/2017"));
//
//
//					System.DateTime back1 = System.DateTime.Parse("07/08/2017 21:23:10");
//					System.DateTime back2 = System.DateTime.Parse("21:23:10");
//					System.DateTime back3 = System.DateTime.Parse("07/08/2017");
//					System.DateTime back4 = System.DateTime.ParseExact("2017-07-08 21:23:10","yyyy-MM-dd HH:mm:ss",null);
//					string timenow = System.DateTime.Now.ToString();

					dllist = new List<string>();
					foreach (ClassBox.ProgramsListItem progListItem in Global.Instance.shopSettingsObj.todays_loop.programs)
					{
						foreach (ClassBox.SrcListItem srcListItem in progListItem.program.src_list)
						{
							dllist.Add(Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + srcListItem.src.path);
						}
					}
					foreach (ClassBox.ProgramsListItem progListItem in Global.Instance.shopSettingsObj.todays_loop.specialprograms)
					{
						foreach (ClassBox.SrcListItem srcListItem in progListItem.program.src_list)
						{
							dllist.Add(Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + srcListItem.src.path);
						}
					}
					Debug.Log("----SRC----");
					foreach (string item in dllist){
						Debug.Log(item);
					}
					Debug.Log(Cfg.App.LOCAL_DIR);
					Debug.Log("--------");

					Mskg.API.Instance.downloadSrcList(
						dllist.ToArray(),
						Cfg.App.LOCAL_DIR,
						(Mskg.RtnObj ret1) => {
							outputProgress(ret1.obj);
						},
						Global.Instance.dlExistenceCheck,
						Global.Instance.dlAutoDelete
					);



				}
			}
		);




		/// 閉店時刻取得
		//取得
		Hashtable param_closetime = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_closetime.Add (Cfg.App.keySHOPID, Global.Instance.shopid);

		Mskg.API.Instance.call (
			Cfg.API.getShopCloseTime + ".php",
			param_closetime, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						//						AlertViewController.Show("JSON", ret.json);

						LitJson.JsonData response =  LitJson.JsonMapper.ToObject(ret.json);

						if (response["closetime"] != null) {
							DateTime _closetime = DateTime.Parse(response["closetime"].ToString());

							//DateTime.Parse(response["closetime"].ToString());
							Global.Instance.closetime = new DateTime(
								DateTime.Now.Year,
								DateTime.Now.Month,
								DateTime.Now.Day,
								_closetime.Hour,
								_closetime.Minute,
								0);

							Debug.Log("////////////");
							Debug.Log(Global.Instance.closetime);
							Debug.Log("////////////");

	//						int[] selectionFromDB = getSelectFromTime (_closetime);
	//						HourDrop.value = selectionFromDB[0];
	//						MinutesDrop.value = selectionFromDB[1];
						}else{
							Debug.Log("////////////");
							Debug.Log(Global.Instance.closetime);
							Debug.Log("////////////");
						}

//						AlertViewController.Show("閉店時刻", _closetime.ToString());

					}

				}

			}
		);

	}

//	string[] lines = new string[24];
	void outputProgress(object obj){
		Mskg.DownLoadObject dlo = (Mskg.DownLoadObject)obj;
		if (dlo.complete || dlo.existence) {
			compHashTable.Add ("asset" + dlo.uid, dlo.complete);
//			Debug.Log (compHashTable.ToString ());
//			pregressOutput.text += "id : " + dlo.uid + 
//				" pregoress : " + dlo.progress + 
//				" complete : " + dlo.complete + 
//				" error : " + dlo.error + 
//				" existence : " + dlo.existence + 
//				"\n";

			if (dlo.error) {
				dlerrorCount++;
			}

			if (dllist.Count == compHashTable.Count) {

				if (0 < dlerrorCount) {
					pregressOutput.text += "エラーがあります。" +
					"\n";
				} else {
					SceneManager.LoadScene("PrgLoopPlayer", LoadSceneMode.Additive);
					contentsObjects.SetActive (false);
				}


			}
		}

		barSizeDelta = new Vector2 (StageW * (dlo.progress / 100f), 4f);
        //		Debug.Log (dlo.progress);
        targetFileText.text = "Downloading ... ( uid = " + dlo.uid.ToString() + " : downloads = " + dllist.Count.ToString() + " )";
		progresText.text = (dlo.progress != 0) ? dlo.progress.ToString () : "";
		progresBar.GetComponent<RectTransform>().sizeDelta = barSizeDelta;

//		lines [dlo.uid] = dlo.progress.ToString();
//		Debug.Log(dlo.uid);
//		pregressOutput.text = "START ... \n";
			
//		foreach(string line in lines){
//			pregressOutput.text = lines[dlo.uid] + '\n';
//		}

	}

}
