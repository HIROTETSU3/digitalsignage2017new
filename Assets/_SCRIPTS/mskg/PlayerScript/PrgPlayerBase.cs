﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PrgPlayerBase : MonoBehaviour {

	protected string thisSceneName = "";

	protected ClassBox.Program thisProgram;
	protected int currentSrcNum = 0;
	protected ClassBox.SrcListItem thisSrcListItem;
	protected ClassBox.SrcListItem nextSrcListItem;
	// Use this for initialization
	protected Color colorHidden = new Color(1,1,1,0); //nextImage.color;
	protected Color colorShow = new Color(1,1,1,1); //nextImage.color;
	protected Color bgcolorHidden = new Color(0,0,0,0); //nextImage.color;
	protected Color bgcolorShow = new Color(0,0,0,0.6f); //nextImage.color;

	[SerializeField] protected GameObject basePageBG;
	[SerializeField] protected Text Title;
	[SerializeField] protected Text SubTitle;
	[SerializeField] public GameObject foundMark;

	protected virtual void Start () {
		foundMark.SetActive (false);
		Global.Instance.onFoundPlayingProgram.AddListener (onFoundPlayingProgram);
		Global.Instance.onReleasePlayingProgram.AddListener (onReleasePlayingProgram);

//		Debug.Log (Global.Instance.currentProgram);
		thisProgram = Global.Instance.nowProgram;

		//baseDelta = slideScene.GetComponent<RectTransform> ().sizeDelta;
		//rawimgSizeDelta = thisImage.GetComponent<RectTransform> ().sizeDelta;

		//init
		fadeBaseObjects(0.0f, 0.0f);
//		basePageBG thisProgram.bgcolor
		Color newCol;
		if (ColorUtility.TryParseHtmlString(thisProgram.bgcolor, out newCol))
			basePageBG.GetComponent<Image>().color = newCol;

		//start
		playBasePage ();
	}

	protected void fadeBaseObjects(float alpha, float duration){
		basePageBG.GetComponent<Image>().CrossFadeAlpha (alpha, duration, true);
		Title.CrossFadeAlpha (alpha, duration, true);
		SubTitle.CrossFadeAlpha (alpha, duration, true);
	}

	protected void fadeBaseBgOnly(float alpha, float duration){
		basePageBG.GetComponent<Image>().CrossFadeAlpha (alpha, duration, true);
	}

	protected virtual void playBasePage(){

		Title.text = thisProgram.title;
		SubTitle.text = thisProgram.subtitle;

		fadeBaseObjects(1.0f, Cfg.App.fadeBaseTime);

//		StartCoroutine(startSlideScene((float)thisProgram.basic_page_time));
	}

	protected void doEnding(){
		StartCoroutine(coDoEnding((float)thisSrcListItem.duration));
	}

	protected IEnumerator coDoEnding(float time){
		yield return new WaitForSeconds (time);

		fadeBaseBgOnly(1.0f, Cfg.App.fadeBaseTime);
		StartCoroutine(coUnload(Cfg.App.PlayerEndTime));
	}

	protected virtual IEnumerator coUnload(float time){
		yield return new WaitForSeconds (time);

		if (Global.Instance.isSpecialProgram == false){
			Global.Instance.currentProgram++;
		}
		SceneManager.UnloadSceneAsync(thisSceneName); //"ProgramPlayerType2");
		Global.Instance.onPlayerDone.Invoke ();
	}


	void onFoundPlayingProgram(){
//		AlertViewController.Show ("Found", null);
		foundMark.SetActive (true);
	}

	void onReleasePlayingProgram(){
//		AlertViewController.Show ("Found", null);
		foundMark.SetActive (false);
	}

}
