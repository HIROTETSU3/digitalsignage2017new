﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputStoreIdPanel : MonoBehaviour {

	[SerializeField] Text tf;
	[SerializeField] PlayerStarter starter;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void pushButton(int arg){
		if (tf.text.Length < Cfg.App.shopid_num) {
			tf.text += arg.ToString ();
		}
	}

	public void pushButtonBs(){
		tf.text = tf.text.Remove (tf.text.Length - 1);
	}

	public void pushButtonGo(){

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonTitle = "再入力";
		opt.okButtonTitle = "はい";
		opt.okButtonDelegate = () =>{

			Go();
		};

		AlertViewController.Show("拠点番号:" + tf.text,
			"上記の拠点番号で間違いはありませんか?",
			opt);
		
	}

	void Go(){
		Global.Instance.shopid = tf.text;
		Mskg.API.Instance.setShopID (Global.Instance.shopid);
		this.gameObject.SetActive (false);
		starter.login ();
	}
}
