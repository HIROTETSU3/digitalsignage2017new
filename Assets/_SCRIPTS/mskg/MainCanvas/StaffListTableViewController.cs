﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using SFB;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class StaffListTableViewController : TableViewController<Staff>
{

	[SerializeField] private M1_1_1_1 getStaffListCanvas;

	[SerializeField] private M1_1_1_1B_Detail mB;
	[SerializeField] private Button insertButton;

	[SerializeField] private M1_1_1_1C_Actions updatePanel;
	[SerializeField] private Text userID;
	[SerializeField] private InputField txtName;
	[SerializeField] private InputField txtLogin;
	[SerializeField] private InputField txtMail;
	[SerializeField] private InputField txtPw;
	[SerializeField] private InputField txtShopID;
	[SerializeField] private InputField txtBlock;
	[SerializeField] private InputField txtCompany;
	[SerializeField] private InputField txtType;

	[SerializeField] SaveFile saveFile;
	[SerializeField] GameObject NoContent;

	//1_1_1_5 Blockセグメント用にピックアップされたフルメンバーのリストを保存しておく
	private List<Staff> backUpAllStaffs;

	[NonSerialized] public int iSelectedUserType = -1; //指定されてなければデフォルト-1

	public void UpdateData(List<Staff> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		//複製を作る（これでいいのか？）
		backUpAllStaffs = new List<Staff>();
		foreach (Staff s in tableData){
			backUpAllStaffs.Add(s);
		}

		NoContent.SetActive (tableData.Count == 0);
			

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{

		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}

	public void onExportTableDataToCSV(){

		//CSV書き出しデータが押されたので選択

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonTitle = "現在のリスト";
		opt.cancelButtonDelegate = () =>{
			DoExportTableDataToCSV();
		};
		opt.okButtonTitle = "全アカウント";
		opt.cancelButtonDelegate = () =>{
			DoExportAllUserDataToCSV();
		};

		AlertViewController.Show("CSV出力","出力ファイルを選択してください。",opt);

	}

	public void DoExportAllUserDataToCSV(){

		//仮　実際は全ユーザーデータを出力する

	}

	public void DoExportTableDataToCSV(){

		string str = "" +
			"\"id\"," +
			"\"type\"," +
			"\"name\"," +
			"\"login\"," +
			"\"mail\"," +
			"\"modifieddate\"," +
			"\"campany_id\"," +
			"\"mst_area_id\"," +
			"\"shops\"," +
			"\"password\"\n"
			;
		foreach (Staff staff in tableData) {
			string s = "";
			s += staff.id + ",";
			s += staff.type + ",";
			s += "\"" + staff.name + "\",";
			s += "\"" + staff.login + "\",";
			s += "\"" + staff.mail + "\",";
			s += "\"" + staff.modifieddate + "\",";
			s += staff.campany_id + ",";
			s += staff.mst_area_id + ",";
			s += "\"" + staff.shops + "\",";
			s += "\"" + staff.password + "\"\n";
			str += s;
		}
		saveFile.openSaveDialog (str);
	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressCellEditButton(StaffListTableViewCell cell)
	{
		ClassBox.Staff staff = tableData [cell.DataIndex];
		/* OLD
		updatePanel.onOpenPanel (staff);
		updatePanel.gameObject.SetActive(true);
		*/

		if(navigationView != null)
		{
			//mB.UpdateContent (tableData [cell.DataIndex]);
			//navigationView.Push(mB);

		}

		//ダイアログを表示（表示するだけなので閉じるだけ）

		UserEditViewOptions opt = new UserEditViewOptions{

			cancelButtonDelegate = () => {
				Debug.Log("---> NO WORK");
			},
			okButtonDelegate = () =>{
				Debug.Log("---> CLOSE Dlog");
			}
		};

		UserEditViewController.Show(1,staff, opt);

	}


	public void OnPressDeleteButton(StaffListTableViewCell cell)
	{

		ClassBox.Staff staff = tableData [cell.DataIndex];

		AlertViewOptions opt = new AlertViewOptions();
		opt.okButtonTitle = "はい";
		opt.okButtonDelegate = () =>{
			Debug.Log("削除する");
			RemoveUser(staff);
		};

		opt.cancelButtonTitle = "削除しない";
		opt.cancelButtonDelegate = () =>{
			Debug.Log("削除をやめる");

		};

		AlertViewController.Show("削除の確認",staff.login + "("+staff.name+") のアカウントを\n本当に削除しても本当によろしいですか？",opt);


	}


	public void OnPressInsertButton(){

		//

		AlertViewOptions opt2 = new AlertViewOptions();
		opt2.okButtonTitle = "閉じる";
		opt2.okButtonDelegate = () =>{
			
		};

//		AlertViewController.Show("修正中","一時的にユーザー新規追加を止めています（廣)",opt2);
//
//		return ;


		ClassBox.Staff newStaff = new ClassBox.Staff();
		//新規のデフォルトユーザーを作成（タイプ別）
		if (iSelectedUserType == 0 || iSelectedUserType == 1) {
			
			newStaff.campany_id = 1; //1
			newStaff.id = -1;
			newStaff.login = "";
			newStaff.mail = "";
			newStaff.modifieddate = "";
			newStaff.mst_area_id = 8; //8
			newStaff.name = "";
			newStaff.password = "";
			newStaff.shops = "0"; //"0"
			newStaff.type = iSelectedUserType; //Global.Instance.mytype;

		} else if (iSelectedUserType == 2) { //ブロックユーザ
			
			newStaff.campany_id = -1; //対象なし
			newStaff.id = -1;
			newStaff.login = "";
			newStaff.mail = "";
			newStaff.modifieddate = "";
			newStaff.mst_area_id = 0; //デフォルト
			newStaff.name = "";
			newStaff.password = "";
			newStaff.shops = ""; //対象なし
			newStaff.type = iSelectedUserType; //Global.Instance.mytype;

		} else if (iSelectedUserType == 3) { //コーポレートユーザ
			
			newStaff.campany_id = 0; //デフォルト
			newStaff.id = -1;
			newStaff.login = "";
			newStaff.mail = "";
			newStaff.modifieddate = "";
			newStaff.mst_area_id = -1; //対象なし
			newStaff.name = "";
			newStaff.password = "";
			newStaff.shops = ""; //対象なし
			newStaff.type = iSelectedUserType; //Global.Instance.mytype;

		} else if (iSelectedUserType == 4) { //店舗ユーザ

			newStaff.campany_id = 0; //デフォルト？
			newStaff.id = -1;
			newStaff.login = "";
			newStaff.mail = "";
			newStaff.modifieddate = "";
			newStaff.mst_area_id = 0; //デフォルト？
			newStaff.name = "";
			newStaff.password = "";
			newStaff.shops = ""; //デフォルト？
			newStaff.type = iSelectedUserType; //Global.Instance.mytype;


		} else {
			Debug.Log("staff.type = null");
		}







		UserEditViewOptions opt = new UserEditViewOptions{

			cancelButtonDelegate = () => {
				Debug.Log("---> NO WORK");
			},
			okButtonDelegate = () =>{
				Debug.Log("---> CLOSE Dlog");
			}
		};

		UserEditViewController.Show(2, newStaff);


	}



	void RemoveUser(ClassBox.Staff staff)
	{
		Hashtable param_stafflist = new Hashtable ();

		param_stafflist.Add (Cfg.App.keyID, staff.id);
		param_stafflist.Add (Cfg.App.keyTABLE, Cfg.App.tableStaff);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						//getStaffListCanvas.updateMainCanvas(null); //データのリロード

						//うまくいかないのでGAMEオブジェクト名で探すことにした。
						GameObject gsc = GameObject.Find("Canvas_1_1_1_1 getStaffList");
						if (gsc){
							M1_1_1_1 m111 = gsc.GetComponent<M1_1_1_1>();
							if (m111)
								m111.updateMainCanvas(null); //データのリロード
						}

					}

//					this.gameObject.SetActive (false);
				}
			}
		);

	}

	bool checkError(string json){

		Debug.Log("------> checkError \n"+ json);

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");

			//エラー時：例えばlogin が重複している場合などに
			return false;
		}

	}


	//Blockセレクトのドロップダウンが実行されたとき
	public void OnBlockDropValueChanged(Dropdown drop) {

		Debug.Log("-----> drop.value = " + drop.value);

		if (drop.value == 0) { //---全国の場合（全国はmst_area_idでは8）
			tableData = new List<Staff>();
			foreach (Staff s in backUpAllStaffs){
				tableData.Add(s);
			}

		} else {
			

			tableData = new List<Staff>();
			foreach (Staff s in backUpAllStaffs){
				if (s.mst_area_id == drop.value){
					tableData.Add(s);
				}
			}
		}

		NoContent.SetActive (tableData.Count == 0);

		UpdateContents();
	}

	/*
	public void OnPressTableViewCell(StaffListTableViewCell cell)
	{
		AlertViewController.Show (tableData [cell.DataIndex].name, "message text");
	}
	*/

	public void OnPressTableViewCell(StaffListTableViewCell cell)
	{
		if(navigationView != null)
		{
			//mB.UpdateContent (tableData [cell.DataIndex]);
			//navigationView.Push(mB);

		}

		//ダイアログを表示（表示するだけなので閉じるだけ）

		UserEditViewOptions opt = new UserEditViewOptions{
			
			cancelButtonDelegate = () => {
				Debug.Log("---> NO WORK");
			},
			okButtonDelegate = () =>{
				Debug.Log("---> CLOSE Dlog");
			}
		};

		UserEditViewController.Show(0,tableData [cell.DataIndex], opt);

	}


	public void OnPressEditButton(StaffListTableViewCell cell)
	{


		//ダイアログを表示（表示するだけなので閉じるだけ）

		UserEditViewOptions opt = new UserEditViewOptions{

			cancelButtonDelegate = () => {
				Debug.Log("---> NO WORK");
			},
			okButtonDelegate = () =>{
				Debug.Log("---> CLOSE Dlog");
			}
		};

		UserEditViewController.Show(1,tableData [cell.DataIndex], opt);



	}


	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion



}

