﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class M1_1_3C_Actions : MonoBehaviour {
	[SerializeField] private bool defaultHidden = true;
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_1_3 shopListCanvas;
	[SerializeField] private M1_1_3C_Actions insertPanel;
	[SerializeField] private Text iD;
	[SerializeField] private InputField txtShopID;
	[SerializeField] private InputField txtName;
	[SerializeField] private InputField txtCompany;
	[SerializeField] private InputField txtBlock;

	[SerializeField] private Dropdown campanyDrop;
	[SerializeField] private Dropdown blockDrop;

	private bool changedCampany = false;
	private bool changedBlock = false;

	#region GUI基本処理

	void Start () {
		if (defaultHidden)
			this.gameObject.SetActive (false);
	}

	public void OnPressCloseButton()
	{
		this.gameObject.SetActive (false);
	}

	public void OnPressOpenInsertPanelButton()
	{

		AlertViewController.Show("調整中","開発版のため008にて対応いたします。");
		return;


		onOpenPanel(null);
		insertPanel.gameObject.SetActive(true);
	}

	#endregion


	#region 初期化とドロップダウン操作

	public void onOpenPanel(ClassBox.ShopData aShop){

		int campanyDefault = 0;
		int blockDefault = 0;

		if (aShop != null) { //Edit Panelの場合
			
			iD.text = aShop.id.ToString();
			txtShopID.text = aShop.shopid;
			txtName.text = aShop.name;
			txtCompany.text = aShop.campany_id.ToString();
			txtBlock.text = aShop.mst_area_id.ToString();

//			typeDefault = shop.type + 1; //---の分+1
		}

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("ショップリストを取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		//campanyとblockのプルダウン作る必要あり。


		//campany
		List<Dropdown.OptionData> campanyOptions = new List<Dropdown.OptionData> ();
		campanyDrop.options.Clear ();
		campanyOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		foreach (ClassBox.CampanyData campany in Global.Instance.CampanyList) {
			if (aShop != null) { //Edit Panelの場合
				if (campany.id == aShop.campany_id) {
					campanyDefault = counter + 1; //---の分+1
				}
				counter++;
			}
			campanyOptions.Add (new Dropdown.OptionData{text = campany.name});
		}
		campanyDrop.options = campanyOptions;
		campanyDrop.value = campanyDefault; //default

		//block
//		List<Dropdown.OptionData> blockOptions = new List<Dropdown.OptionData> ();
//		blockDrop.options.Clear ();
//		blockOptions.Add (new Dropdown.OptionData{text = "---"});
//		foreach (Cfg.UI.SideMenuItemClass utype in Cfg.UI.sidemenu1_1_1) {
//			blockOptions.Add (new Dropdown.OptionData{text = utype.label});
//		}
//		blockDrop.options = blockOptions;
//		blockDrop.value = typeDefault; //default

		//campany
		List<Dropdown.OptionData> blockOptions = new List<Dropdown.OptionData> ();
		blockDrop.options.Clear ();
		blockOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter2 = 0;
		foreach (ClassBox.AreaData area in Global.Instance.AreaList) {
			if (aShop != null) { //Edit Panelの場合
				if (area.id == aShop.mst_area_id) {
					blockDefault = counter2 + 1; //---の分+1
				}
				counter2++;
			}
			blockOptions.Add (new Dropdown.OptionData{text = area.name});
		}
		blockDrop.options = blockOptions;
		blockDrop.value = blockDefault; //default

	}

	public void OnCampanyDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			changedCampany = false;
			txtCompany.text = "";
			return;
		}
		int selected = drop.value - 1;
		Debug.Log("value = " + Global.Instance.CampanyList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		txtCompany.text = Global.Instance.CampanyList[selected].id.ToString();

		changedCampany = true;

	}

	public void OnBlockDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			changedBlock = false;
			txtBlock.text = "";
			return;
		}
		int selected = drop.value - 1;
		Debug.Log("value = " + Global.Instance.AreaList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		txtBlock.text = Global.Instance.AreaList[selected].id.ToString();

		changedBlock = true;

	}

	#endregion


	#region WebAPI系ボタン操作

	public void OnPressInsertButton()
	{
		if (
			!changedCampany || 
			!changedBlock ||
			txtName.text == "" ||
			txtShopID.text == ""
		) {
			AlertViewController.Show ("確認", "必須項目が入力されていません。");
			return;
		}
			
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keySHOPID, txtShopID.text);
		param_list.Add (Cfg.App.keyNAME, txtName.text);
		param_list.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
		param_list.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));

		Mskg.API.Instance.call (
			Cfg.API.insertShop + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");
					/*
					ClassBox.Staff[] StaffList = null;
					try
					{
						StaffList =  LitJson.JsonMapper.ToObject<ClassBox.Staff[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(StaffList);
					Debug.Log("////////////");

					List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
					staffclasses.AddRange(StaffList);

					tableview.UpdateData(staffclasses);
					//					sidemenutableview.UpdateData(sidemenuclasses);
					*/



					if (checkError(ret.json)) {
						shopListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressUpdateButton()
	{
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, int.Parse(iD.text));
		param_list.Add (Cfg.App.keySHOPID, txtShopID.text);
		param_list.Add (Cfg.App.keyNAME, txtName.text);
		param_list.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
		param_list.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));

		Mskg.API.Instance.call (
			Cfg.API.updateShop + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						shopListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressDeleteButton()
	{
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, int.Parse(iD.text));
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableShop);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						shopListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}

	#endregion
}
