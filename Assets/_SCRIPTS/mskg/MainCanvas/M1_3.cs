﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_3 : MainCanvas {

	[SerializeField] private Text loadingText; 
	[SerializeField] GameObject reTryButton;
	[SerializeField] GameObject accountView;
	[SerializeField] GameObject loginView;
	[SerializeField] M1_3_loginPanel loginpanel;

	[SerializeField] UpdatePassword updatePwDialog;

	[SerializeField] Text txtID;
	[SerializeField] Text txtName;
	[SerializeField] Text txtLoginID;
	[SerializeField] InputField inputPW;
	[SerializeField] Text txtMail;
	[SerializeField] Text txtDate;
	[SerializeField] Text txtShop;
	[SerializeField] Text txtArea;
	[SerializeField] Text txtCampany;
	[SerializeField] Text txtType;

	// ナビゲーションビューを保持

	private Hashtable memoriedParam;
//	bool programInitStandBy = false;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		showReTryButton (false);







		#region アイテム一覧画面をナビゲーションビューに対応させる
//		if(navigationView != null)
//		{
//			navigationView.Push(mA);
//		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}
			

		/*
		//ショップリスト取得
		Hashtable param_shop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_shop.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllShop + ".php",
			param_shop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.ShopData[] staffList = null;
					try
					{
						Global.Instance.ShopList =  LitJson.JsonMapper.ToObject<ClassBox.ShopData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ShopList);
					Debug.Log("////////////");

//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.ShopData> shopclasses = new List<ClassBox.ShopData>();
					shopclasses.AddRange(Global.Instance.ShopList);
					tableview.UpdateData(shopclasses);

				}

			}
		);
		*/
	}

	public void login(string useridString, string pwEncString){

		loadingText.text = "Logging ...";

		Hashtable param_staff = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_staff.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.userlogin + ".php",
			param_staff, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.UserLogin loginObj = null;
					try
					{
						loginObj =  LitJson.JsonMapper.ToObject<ClassBox.UserLogin>(ret.json);
					}
					catch (Exception ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(loginObj);
					Debug.Log("////////////");
					string test = Mskg.API.Instance.Aes128CBCDecode(pwEncString, Global.Instance.pwKey, Global.Instance.pwSalt);

					if (loginObj.success && loginObj.user.password == pwEncString) {
						ClassBox.Staff me = loginObj.user;

						updatePwDialog.encordedPw = pwEncString;

						txtID.text = me.id.ToString();
						txtName.text = me.name;
						txtLoginID.text = me.login;
						inputPW.text = me.password;
						txtMail.text = me.mail;
						txtDate.text = me.modifieddate;
						txtShop.text = Mskg.API.Instance.getShopByShopID(me.shops).name;
						txtArea.text = Mskg.API.Instance.getAreaByID(me.mst_area_id).name;
						txtCampany.text = Mskg.API.Instance.getCampanyByID(me.campany_id).name;
						txtType.text = Mskg.API.Instance.getTypeNameByInt(me.type);


						loadingText.gameObject.SetActive(false);
//						appInfoText.gameObject.SetActive(false);

//						logoFader.FadeStart ();
//						programInitStandBy = true;
						loginView.gameObject.SetActive(false);
						accountView.gameObject.SetActive(true);

					}else{
						showReTryButton (true);
						loadingText.text = "エラーが発生しました。ログインIDとパスワードをご確認ください。";
					}

				}

			}
		);


	}

	public void openLoginPanal(){
		showReTryButton (false);
		loadingText.text = "UserIDとパスワードを入力してください。";
		loginpanel.clearPwField ();
		loginpanel.gameObject.SetActive(true);
	}

	public void showReTryButton(bool arg){
		reTryButton.SetActive (arg);
	}
		
	public void showUpdatePasswordPanel(){
//		AlertViewController.Show ("制作中", "パスワードアップデートパネルを開く");
//		UpdatePasswordController.Show("制作中", "パスワードアップデートパネルを開く");

		ChangePswdDlogOptions options = new ChangePswdDlogOptions ();

		options.userId = Global.Instance.Me.login;

		options.oldLoginPswd = inputPW.text;
		options.okButtonTitle = "設定";
		options.cancelButtonTitle = "キャンセル";

		options.okButtonDelegate = () => {
			//あえてここでWebAPIを叩かなくてもよさそう？
			Debug.Log("パスワード再設定完了");
			ShowAc();
		};


		ChangePswdDlogViewController.Show (options);
	}

	void ShowAc(){

		DataAccessDlog.Show("パスワードの更新中",1f);


	}
}
