﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class M1_2_1B_ProgramEditor : MainCanvasChild {

	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_2_1 simpleProgListCanvas;

	[SerializeField] SrcListItemTableViewController tableview;

	//[SerializeField] private InputField txtTitle;
	[SerializeField] private Text txtTitle;

	//[SerializeField] private InputField txtSubTitle;
	[SerializeField] private Text txtSubTitle;

	[SerializeField] private InputField txtLayoutType;
	[SerializeField] private InputField txtBGColor;
	[SerializeField] private InputField txtContent_type;
	[SerializeField] private InputField txtTransition_type;
	[SerializeField] private InputField txtBasic_page_time;

	[SerializeField] private InputField txtEditorID;
	[SerializeField] private InputField txtShopID;
	[SerializeField] private InputField txtMst_area_id;
	[SerializeField] private InputField txtCampany_id;

	[SerializeField] private InputField txtBroadcast;

	[SerializeField] private Dropdown dropContentType;
	[SerializeField] private Dropdown dropTransition;

	[SerializeField] private Toggle toggleBroadcast;

	[SerializeField] private Text lblAddSrcButton;
	[SerializeField] private Button btnAddSrcButton;
	[SerializeField] private Button updateAndBack;

	string BackUpTitleSetting = "";

	[SerializeField] private OpenFile openfile;

	//1.1
	[SerializeField] private Toggle togglePlayable;

	public ClassBox.SimpleProgram simpleprog;
//	ClassBox.Program program;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onBackButton(){
		navigationView.Pop ();
	}

	public void onDeleteButton()
	{

		AlertViewController.Show ("削除確認", "本当に削除しますか？", 
			new AlertViewOptions {
				cancelButtonTitle = "キャンセル",
				cancelButtonDelegate = () => {
					Debug.Log("キャンセルしました。");
					return;
				},
				okButtonTitle = "はい",
				okButtonDelegate = () => {
					doDelete();
				},
			});
		
	}

	private void doDelete(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, simpleprog.programid);
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableProgram);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
						navigationView.Pop();
					}

					this.gameObject.SetActive (false);
				}
			}
		);
	}

	public void onUpdateButton(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, simpleprog.programid);

		param_list.Add (Cfg.App.keyTITLE, txtTitle.text);
		param_list.Add (Cfg.App.keySUBTITLE, txtSubTitle.text);

		param_list.Add (Cfg.App.keyLAYOUT, txtLayoutType.text);
		param_list.Add (Cfg.App.keyBGCOLOR, txtBGColor.text);
		param_list.Add (Cfg.App.keyCONTENT, txtContent_type.text);
		param_list.Add (Cfg.App.keyTRANSITION, txtTransition_type.text);
		param_list.Add (Cfg.App.keyPAGETIME, txtBasic_page_time.text);

		param_list.Add (Cfg.App.keyBROADCAST, txtBroadcast.text);

		//1.1
		string _playable = "0";
		if (togglePlayable.isOn == true) {
			_playable = "1";
		}
		param_list.Add(Cfg.App.keyPLAYABLE, _playable);

		Mskg.API.Instance.call (
			Cfg.API.updateProgram + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
						//						navigationView.Pop();
						AlertViewController.Show("更新", "情報を更新しました。");
					}

				}
			}
		);
	}


	public bool chkChanged(){
		string _playableStr = "0";
		if (togglePlayable.isOn == true) {
			_playableStr = "1";
		}
		string temp = txtTitle.text+
			txtSubTitle.text+
			txtLayoutType.text+
			txtBGColor.text+
			txtTransition_type.text+
			txtBasic_page_time.text+
			_playableStr;

		updateAndBack.interactable = (temp != BackUpTitleSetting);

		return (temp != BackUpTitleSetting);

	}

	public void UpdateContent(ClassBox.SimpleProgram itemData, Mskg.API.callback addedCallback = null)
	{
		if (itemData != null) {
			
			simpleprog = itemData;

			updateAndBack.interactable = false;

			string _playableStr = "0";
			if (itemData.playable == 1) {
				_playableStr = "1";
			}

			BackUpTitleSetting = itemData.title+
				itemData.subtitle+
				itemData.layout_type.ToString()+
				itemData.bgcolor+
				itemData.transition_type.ToString()+
				itemData.basic_page_time.ToString()+
				_playableStr;




			txtTitle.text = itemData.title;
			txtSubTitle.text = itemData.subtitle;

			txtLayoutType.text = itemData.layout_type.ToString();
			txtBGColor.text = itemData.bgcolor;
			txtContent_type.text = itemData.content_type.ToString();
			txtTransition_type.text = itemData.transition_type.ToString();
			txtBasic_page_time.text = itemData.basic_page_time.ToString();

			dropContentType.value = int.Parse(txtContent_type.text); //default
			dropTransition.value = int.Parse(txtTransition_type.text);

			txtEditorID.text = itemData.editorid.ToString();
			txtShopID.text = itemData.shopid;
			txtMst_area_id.text = itemData.mst_area_id.ToString();
			txtCampany_id.text = itemData.campany_id.ToString();
			txtBroadcast.text = itemData.broadcast.ToString();

			toggleBroadcast.isOn = (itemData.broadcast == 1);

			//1.1
			togglePlayable.isOn = (itemData.playable == 1);

			btnAddSrcButton.interactable = false;
			if (itemData.content_type == 2) {
				lblAddSrcButton.text = "画像の追加";
				btnAddSrcButton.interactable = true;
				openfile.setDialogType (false);
			}else if(itemData.content_type == 1) {
				lblAddSrcButton.text = "動画の追加";
				openfile.setDialogType (true);
			}

		} else {
			itemData = simpleprog;
		}

		//SrcListItemの取得
		Hashtable param_srclistitem = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_srclistitem.Add (Cfg.App.keyPARENT, itemData.programid);

		Mskg.API.Instance.call (
			Cfg.API.getSrcListItemWithSrc + ".php",
			param_srclistitem, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					try
					{
						Global.Instance.SrcListItemList =  LitJson.JsonMapper.ToObject<ClassBox.SrcListItemWithSrc[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.SrcListItemList);
					Debug.Log("////////////");

					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.SrcListItemWithSrc> srclistitemclasses = new List<ClassBox.SrcListItemWithSrc>();
					srclistitemclasses.AddRange(Global.Instance.SrcListItemList);
					tableview.UpdateData(srclistitemclasses);

					if (addedCallback != null){
						addedCallback(new Mskg.RtnObj(true, srclistitemclasses, null));
					}

					if(itemData.content_type == 1){ //動画の場合
						btnAddSrcButton.interactable = (Global.Instance.SrcListItemList.Length < 1); //Srcは1未満のみ
					}

				}

			}
		);


	}


	public void OnTypeDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			txtContent_type.text = "";
			return;
		}
		txtContent_type.text = drop.value.ToString ();
	}

	public void OnTransitionDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			txtTransition_type.text = "";
			return;
		}
		txtTransition_type.text = drop.value.ToString ();
	}

	public void OnToggleBroadcastChanged(Toggle toggle){
		txtBroadcast.text = (toggle.isOn) ? "1" : "0";
	}


	public void OnPressPreviewButton()
	{
		Global.Instance.PlayerPreviewMode = true;
		Global.Instance.PlayerPreviewProgramID = simpleprog.programid;
		SceneManager.LoadScene("PlayerStarter", LoadSceneMode.Additive);
	}




	public void OnPressTitleChange(){

		ChangeTxtDlog(txtTitle, "番組タイトルの変更","わかりやすい番組タイトルを付けてください");
	}
	public void OnPressSubTitleChange(){

		ChangeTxtDlog(txtSubTitle, "サブタイトルの変更","内容がわかるコメントを記述ください。");
	}

	void ChangeTxtDlog(Text TxField, string TitleString, string Caption){
		

		TextChangeDlogOption opt = new TextChangeDlogOption();
		opt.cancelButtonTitle = "変更しない";
		opt.okButtonTitle = "変更";
		opt.okButtonDelegate = () =>{

			Debug.Log("NEW TEXT = " + Global.Instance.TempText);
			TxField.text = Global.Instance.TempText;
			//ここではまだ情報更新しない
			chkChanged();

		};

		TextChangeDlogUIController.Show(TitleString,Caption,TxField.text,opt);


	}

	public void onClickToggleButton(){
		chkChanged ();
	}

//	public  getTable(){
//
//	}
}
