﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainCanvas : ViewController {

	[SerializeField] public string canvasId;
	[SerializeField] protected bool programCanvas = false;
	protected bool initialized = false;


	// Use this for initialization
	protected virtual void Start () {
		/*
		if (Global.Instance.onOpenMainCanvas == null) {
			Global.Instance.onOpenMainCanvas = new MainCanvasEvent ();
			Global.Instance.onOpenMainCanvas.AddListener (onOpenMainCanvas);
		}
		*/

		if (programCanvas) {
			Global.Instance.programMainCanvases.Add (this);
		} else {
			Global.Instance.mainCanvases.Add (this);
		}


		this.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void initMainCanvas(Hashtable param){
		
	}

	public virtual void updateMainCanvas(Hashtable param){

	}

	public virtual void onOpenMainCanvas(string canvasId, Hashtable initparam, Hashtable updateparam){

		foreach (MainCanvas canvas in Global.Instance.mainCanvases)
		{
			if (canvas.canvasId == canvasId){
				canvas.gameObject.SetActive(true);
				canvas.initMainCanvas (initparam);
				canvas.updateMainCanvas (updateparam);
			}else{
				canvas.gameObject.SetActive(false);
			}
		}

		foreach (MainCanvas canvas in Global.Instance.programMainCanvases)
		{
			canvas.gameObject.SetActive(false);
		}

	}

	public void onOpenProgramMainCanvas(string canvasId, Hashtable initparam, Hashtable updateparam){

		foreach (MainCanvas canvas in Global.Instance.programMainCanvases)
		{
			if (canvas.canvasId == canvasId){
				canvas.gameObject.SetActive(true);
				canvas.initMainCanvas (initparam);
				canvas.updateMainCanvas (updateparam);
			}else{
				canvas.gameObject.SetActive(false);
			}
		}

		foreach (MainCanvas canvas in Global.Instance.mainCanvases)
		{
			canvas.gameObject.SetActive(false);
		}

	}

}
