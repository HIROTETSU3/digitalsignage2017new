﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class M1_2_3B_TodaysLoopEditor : MainCanvasChild {

	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_2_3 TodaysLoopListCanvas; 

	[SerializeField] ProgramsListItemTableViewController tableview;

	[SerializeField] private InputField txtTitle;
	[SerializeField] private InputField txtSubTitle;
	[SerializeField] private InputField txtDate;
	[SerializeField] private InputField txtMemo;

	[SerializeField] private Button UpdateBtn;

//	[SerializeField] private Text lblPw;
//	[SerializeField] private Text lblShopID;
//	[SerializeField] private Text lblBlock;
//	[SerializeField] private Text lblCompany;
//	[SerializeField] private Text lblType;

	ClassBox.SimpleTodaysLoop simpleTodaysLoop;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onBackButton(){
		navigationView.Pop ();
	}

	public void onDeleteButton()
	{

		AlertViewController.Show ("削除確認", "本当に削除しますか？", 
			new AlertViewOptions {
				cancelButtonTitle = "キャンセル",
				cancelButtonDelegate = () => {
					Debug.Log("キャンセルしました。");
					return;
				},
				okButtonTitle = "はい",
				okButtonDelegate = () => {
					doDelete();
				},
			});
		
	}

	private void doDelete(){

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, simpleTodaysLoop.id);
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableTodaysLoop);
//		Debug.Log (simpleTodaysLoop.id + " : " + Cfg.App.tableTodaysLoop);
//		return;
			
		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						TodaysLoopListCanvas.updateMainCanvas(null); //データのリロード
						navigationView.Pop();
					}

//					this.gameObject.SetActive (false);
				}
			}
		);
	}

	public void onPlusButton(){
		AlertViewController.Show ("＋ボタン", "未処理");
	}

	public void onUpdateButton(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, simpleTodaysLoop.id);
		param_list.Add (Cfg.App.keyTITLE, txtTitle.text);
		param_list.Add (Cfg.App.keySUBTITLE, txtSubTitle.text);
//		param_list.Add (Cfg.App.keyDATE, txtDate.text);
		param_list.Add (Cfg.App.keyMEMO, txtMemo.text);

		Mskg.API.Instance.call (
			Cfg.API.updateTodaysLoop + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						TodaysLoopListCanvas.updateMainCanvas(null); //データのリロード
//						navigationView.Pop();
						AlertViewController.Show("更新", "情報を更新しました。");
					}

					//					this.gameObject.SetActive (false);
				}
			}
		);
	}

	public void OnCallBackFromAddPanel(Hashtable param)
	{

		int parentid = simpleTodaysLoop.id;

		Hashtable param_list = param;
		param_list.Add (Cfg.App.keyPARENT, parentid); //int.Parse());
		//		param_list.Add (Cfg.App.keyPROGID, param[Cfg.App.keyPROGID]);
		//		param_list.Add (Cfg.App.keySPECIAL, null);
		//		param_list.Add (Cfg.App.keyTIME, null);

		Mskg.API.Instance.call (
			Cfg.API.insertProgramsListItem + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


//					if (checkError(ret.json)) {
//						TodaysLoopListCanvas.updateMainCanvas(null); //データのリロード
//					}

//					this.gameObject.SetActive (false);

					this.UpdateContent(simpleTodaysLoop);

				}
			}
		);


	}

	private string backString;

	public void UpdateContent(ClassBox.SimpleTodaysLoop itemData)
	{
		if (itemData != null) {
			simpleTodaysLoop = itemData;

			UpdateBtn.interactable = false;

			backString = txtTitle.text+txtSubTitle.text+txtMemo.text;

			//
			txtTitle.text = itemData.title;
			txtSubTitle.text = itemData.subtitle;
			txtDate.text = itemData.date;
			txtMemo.text = itemData.memo;
			//		lblShopID.text = Mskg.API.Instance.getShopByShopID(itemData.shops).name;//itemData.shops;
			//		lblBlock.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name; //itemData.mst_area_id.ToString();
			//		lblCompany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name; //itemData.campany_id.ToString();
			//		lblType.text = Mskg.API.Instance.getTypeNameByInt(itemData.type); //itemData.type.ToString();
			//
			//		AlertViewController.Show (itemData.name, "M1_1_1_1B");
		} else {
			itemData = simpleTodaysLoop;
		}

		Hashtable param_programlist = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_programlist.Add (Cfg.App.keyPARENT, itemData.id);

		Mskg.API.Instance.call (
			Cfg.API.getProgramsListItem + ".php",
			param_programlist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					try
					{
						Global.Instance.ProgramsListItemList =  LitJson.JsonMapper.ToObject<ClassBox.ProgramsListItem[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ProgramsListItemList);
					Debug.Log("////////////");

					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.ProgramsListItem> proglistitemclasses = new List<ClassBox.ProgramsListItem>();
					proglistitemclasses.AddRange(Global.Instance.ProgramsListItemList);
					tableview.UpdateData(proglistitemclasses);

				}

			}
		);

	}


	public void UpdateField(){

		string s = txtTitle.text+txtSubTitle.text+txtMemo.text;


		UpdateBtn.interactable = !(backString == s);


	}
	

	
}
