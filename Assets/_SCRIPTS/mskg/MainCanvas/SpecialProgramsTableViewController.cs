﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class SpecialProgramsTableViewController : TableViewController<CalenderSpecialProgram>
{
//	[SerializeField] private M1_1_1_1B_Detail mB;
//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

//	[SerializeField] private M1_1_3C_Actions updatePanel;
	[SerializeField] private M1_2_5 m1_2_5;
	[SerializeField] private Button insertButton;

	public void UpdateData(List<CalenderSpecialProgram> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region ボタンアクション
	public void OnPressNewButton(){
		Hashtable param4 = new Hashtable ();
		//			param4.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);
		Global.Instance.programMainCanvases [0].onOpenProgramMainCanvas ("1_2_4", null, param4);
	}


	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(ShopListTableViewCell cell)
	{
		if(navigationView != null)
		{
//			mB.UpdateContent (tableData [cell.DataIndex]);
//			navigationView.Push(mB);

		}
	}

	public void OnPressCellEditButton(SpecialProgramsTableViewCell cell)
	{
		ClassBox.CalenderSpecialProgram shop = tableData [cell.DataIndex];

//		updatePanel.onOpenPanel (shop);
//		updatePanel.gameObject.SetActive(true);
	}

	public void OnPressCellDeleteButton(SpecialProgramsTableViewCell cell)
	{

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonTitle = "やめる";
		opt.okButtonTitle = "削除する";
		opt.okButtonDelegate = ()=>{
			DeleteSpProgram(cell);
		};

		AlertViewController.Show("特別番組の削除","この特番設定を削除してもよろしいですか?\n※番組は削除されません",opt);

	}

	void DeleteSpProgram(SpecialProgramsTableViewCell cell)
	{


		Hashtable param_sp = new Hashtable ();

		param_sp.Add (Cfg.App.keyID, tableData[cell.DataIndex].id);
		param_sp.Add (Cfg.App.keyTABLE, Cfg.App.tableSpecialProgram + Global.Instance.CalenderID.ToString());

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_sp, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
//						getStaffListCanvas.updateMainCanvas(null); //データのリロード
						m1_2_5.updateMainCanvas(null);
					}

//					this.gameObject.SetActive (false);
				}
			}
		);
	}

	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

