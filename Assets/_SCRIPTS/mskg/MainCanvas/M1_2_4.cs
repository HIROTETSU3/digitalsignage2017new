﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//using UnityEditor;

public class M1_2_4 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;
	[SerializeField] private CalenderLayout calender;
	[SerializeField] private Dropdown dropChannel;

//	[SerializeField] SimpleProgramListTableViewController tableview;

	private DateTime[] memoriedFirstDayAndLastDay;

	public Dictionary<String,ClassBox.CalenderItem> calenderData;
	public Dictionary<String,ClassBox.CalenderSpecialProgram> calenderSPData;

	// Use this for initialization
	protected override void Start () {

		mA.gameObject.SetActive (true);

//		Global.Instance.programMainCanvases.Add (this);

//		switch (Global.Instance.Me.type) {
//		case 0: //システム管理者
//		case 1: //管理者
//			mA.Title = Cfg.UI.sidemenu1_2[0].label;
//			break;
//		case 2: //ブロックユーザ
//			mA.Title = Cfg.UI.sidemenu1_2block[0].label;
//			break;
//		case 3: //コーポレートユーザ
//			mA.Title = Cfg.UI.sidemenu1_2campany[0].label;
//			break;
//		case 4: //店舗ユーザ
//			mA.Title = Cfg.UI.sidemenu1_2shop[0].label;
//			break;
//		}

		calenderData = new Dictionary<String,ClassBox.CalenderItem> ();
		calenderSPData = new Dictionary<String,ClassBox.CalenderSpecialProgram> ();




		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
//		if(navigationView != null)
//		{
//			navigationView.Push(mA);
//		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){
//		navigationView.Pop ();
//		navigationView.Pop ();

		//1.1
		//channel
		List<Dropdown.OptionData> channelOptions = new List<Dropdown.OptionData> ();
		dropChannel.options.Clear ();
		//		channelOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		int channelDefault = 0;
		foreach (ClassBox.ChannelData channel in Global.Instance.ChannelDataArray) {
			int ch = 0;
			if (Global.Instance.CalenderID != "") {
				ch = int.Parse (Global.Instance.CalenderID);
			}
			if (channel.id == ch) {
				//channelDefault = counter + 1; //---の分+1
				channelDefault = counter;
			}
			counter++;
			channelOptions.Add (new Dropdown.OptionData{text = channel.id.ToString() + " : " + channel.name});
		}
		dropChannel.options = channelOptions;
		dropChannel.value = channelDefault; //default



		Debug.Log (param);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}





		Debug.Log (calenderData);

//		calender.UpdateCalenderDate ();
//		calender.NowCalender(); //makeCellを呼ぶ

	}

	public void callBackFromCalender(DateTime[] firstDayAndLastDay){
		updateCalenderInfo (firstDayAndLastDay);
	}

	public void updateCalenderInfo(DateTime[] firstDayAndLastDay){
		
		DateTime[] _firstDayAndLastDay;
		if (firstDayAndLastDay == null){
			_firstDayAndLastDay = memoriedFirstDayAndLastDay;
		}else{
			_firstDayAndLastDay = firstDayAndLastDay;
			memoriedFirstDayAndLastDay = firstDayAndLastDay;
		}

		Debug.Log (firstDayAndLastDay);

		Hashtable param_calender = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_calender.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		param_calender.Add (Cfg.App.keyCALENDERSTART, _firstDayAndLastDay[0].ToString(Cfg.App.DATE_FORMAT));
		param_calender.Add (Cfg.App.keyCALENDEREND, _firstDayAndLastDay[1].ToString(Cfg.App.DATE_FORMAT));

		//1.1
		param_calender.Add (Cfg.App.keyCALENDERID, Global.Instance.CalenderID);

		Mskg.API.Instance.call (
			Cfg.API.getCalenderTodaysLoop + ".php",
			param_calender, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					
					ClassBox.CalenderItem[] CalenderItemList = null;
					try
					{
						CalenderItemList =  LitJson.JsonMapper.ToObject<ClassBox.CalenderItem[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(CalenderItemList);
					Debug.Log("////////////");

					calenderData.Clear ();
					foreach (ClassBox.CalenderItem item in CalenderItemList){
						calenderData.Add (
							item.date.ToString(Cfg.App.DATE_FORMAT) , item
						);
					}

//					for(int i = 1; i < 9; i++)
//					{
//						calenderData.Add ("2017-09-0" + i.ToString() ,new ClassBox.CalenderItem { 
//							id = i,
//							date = "2017-09-0" + i.ToString(),
//							loop = (new ClassBox.SimpleTodaysLoop{
//								id = i,
//								title = "title" + i.ToString()
//							})
//						});
//					}
					Global.Instance.CalendarInfoInit = true;
					calender.UpdateCalender ();
				}

			}
		);

		Mskg.API.Instance.call (
			Cfg.API.getCalenderSpecialPrograms + ".php",
			param_calender, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.CalenderSpecialProgram[] CalenderSPList = null;
					try
					{
						CalenderSPList =  LitJson.JsonMapper.ToObject<ClassBox.CalenderSpecialProgram[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(CalenderSPList);
					Debug.Log("////////////");

					calenderSPData.Clear ();
					foreach (ClassBox.CalenderSpecialProgram sp in CalenderSPList){
//						string keyStr = sp.dateinfo.ToString(Cfg.App.DATE_FORMAT.Replace ("-", "_")) + "_" + sp.timeinfo.ToString(Cfg.App.TIME_FORMAT.Replace (":", "_"));
						string keyStr = sp.dateinfo.ToString(Cfg.App.DATE_FORMAT);
						if (getCalenderSp(keyStr) == null) {
							calenderSPData.Add (
								keyStr , sp
							);
						}
					}

//					Global.Instance.CalendarInfoInit = true;
					calender.UpdateCalender ();
				}

			}
		);


	}

	public ClassBox.CalenderItem getCalenderItem(string _date){
		ClassBox.CalenderItem val = new ClassBox.CalenderItem { };
		if (calenderData != null) {
			calenderData.TryGetValue (_date, out val);
		} else {
			val = null;
		}
		return val;
	}

	public ClassBox.CalenderSpecialProgram getCalenderSp(string _date){
		ClassBox.CalenderSpecialProgram val = new ClassBox.CalenderSpecialProgram { };
		if (calenderSPData != null) {
			calenderSPData.TryGetValue (_date, out val);
		} else {
			val = null;
		}
		return val;
	}

	//1.1
	public void onChangeChannel(){
		if (dropChannel.value == 0) {
			Global.Instance.CalenderID = "";
		} else {
			Global.Instance.CalenderID = dropChannel.value.ToString();
		}

		calender.ThisCalender ();
	}
}

