﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;

public class TodaysLoopForCalendarTableViewCell : TableViewCell<SimpleTodaysLoop>
{
//	[SerializeField] private Button updateButton;
	[SerializeField] private M1_2_4 mMainCanvas;

	[SerializeField] private Image imgChannel;
	[SerializeField] private Button btnSetChannel;
	[SerializeField] private Text txtId;
	[SerializeField] private Text txtTitle;
	[SerializeField] private Text txtSubTitle;
	[SerializeField] private Text txtDate;
	[SerializeField] private Text txtStaff;
	[SerializeField] private Text txtMemo;

	private int thisID = 0;
	void Start(){
//		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(SimpleTodaysLoop itemData)
	{
		thisID = itemData.id;
//		imgChannel.gameObject.SetActive ((itemData.channel == Cfg.App.DEFAULT_CHANNEL));
//		btnSetChannel.gameObject.SetActive ((itemData.channel != Cfg.App.DEFAULT_CHANNEL));

//		txtId.text = itemData.id.ToString ();
		txtTitle.text = thisID.ToString() + " : " + itemData.title;
//		txtSubTitle.text = itemData.subtitle;
		txtDate.text = itemData.date;
//		txtMemo.text = itemData.memo;


	}
	/*
	public void onSetChannelButton(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyLOOPSET, thisID);

		Mskg.API.Instance.call (
			Cfg.API.updateDefaultChannel + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, "他のチャンネルに割り当てられている可能性があります。")) {
						mMainCanvas.updateMainCanvas(null); //データのリロード
					}
				}
			}
		);
	}
	*/
}

