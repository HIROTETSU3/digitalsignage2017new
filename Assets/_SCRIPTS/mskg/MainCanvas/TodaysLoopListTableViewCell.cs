﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;

public class TodaysLoopListTableViewCell : TableViewCell<SimpleTodaysLoop>
{
//	[SerializeField] private Button updateButton;
	[SerializeField] private M1_2_3 mMainCanvas;

	[SerializeField] private Image imgChannel;
	[SerializeField] private Text baseTxt;
	[SerializeField] private Button btnSetChannel;
	[SerializeField] private Text txtId;
	[SerializeField] private Text txtTitle;
	[SerializeField] private Text txtSubTitle;
	[SerializeField] private Text txtDate;
	[SerializeField] private Text txtStaff;
	[SerializeField] private Text txtMemo;

	private int thisID = 0;
	void Start(){
//		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(SimpleTodaysLoop itemData)
	{
		thisID = itemData.id;
		//imgChannel.gameObject.SetActive ((itemData.channel == Cfg.App.DEFAULT_CHANNEL));
		baseTxt.enabled = imgChannel.enabled = (itemData.channel == Cfg.App.DEFAULT_CHANNEL);

		//btnSetChannel.gameObject.SetActive ((itemData.channel != Cfg.App.DEFAULT_CHANNEL));
		btnSetChannel.interactable = (itemData.channel != Cfg.App.DEFAULT_CHANNEL);

		txtId.text = itemData.id.ToString ();
		txtTitle.text = itemData.title;
		txtSubTitle.text = itemData.subtitle;
		txtDate.text = itemData.date;
		txtMemo.text = itemData.memo;

//		txtTitle.text = itemData.title;
//
//		ClassBox.ShopData _shop = Mskg.API.Instance.getShopByShopID (itemData.shopid);
//		ClassBox.AreaData _area = Mskg.API.Instance.getAreaByID (itemData.mst_area_id);
//		ClassBox.CampanyData _campany = Mskg.API.Instance.getCampanyByID (itemData.campany_id);
//
//		txtShopid.text = (_shop != null) ? _shop.name : "---";
//		txtArea.text = (_area != null) ? _area.name : "---";
//		txtCampany.text = (_campany != null) ? _campany.name : "---";

//		txtShopid.text = itemData.shopid; //.name;
//		txtArea.text = .name;
//		txtCampany.text = .name;

//		txtCampany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name;
//		txtArea.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name;
	}

	public void onSetChannelButton(){


		SelectDlogOptions opt = new SelectDlogOptions();
		opt.cancelBtnLabel = "やめる";
		opt.okBtnLabel = "選択";
		opt.okButtonDelegate = () =>{

			okSetChannel();
		};
		opt.TitleString = "BASE番組に指定するチャンネルを選択してください";


		SelectChannellViewController.Show(opt);


		/*
		//確認する

		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonTitle = "やめる";
		opt.okButtonTitle = "変更する";
		opt.okButtonDelegate=()=>{
			okSetChannel();
		};

		AlertViewController.Show("BASE番組の変更","設定していない日付で基本番組として再生される番組ループを\n["+
			txtTitle.text+"]に変更しますか。",opt);
		*/

	}

	void okSetChannel(){


		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyLOOPSET, thisID);

		Mskg.API.Instance.call (
			Cfg.API.updateDefaultChannel + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, "他のチャンネルに割り当てられている可能性があります。")) {
						mMainCanvas.updateMainCanvas(null); //データのリロード
					}
				}
			}
		);
	}
}

