﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_1_2 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;

	[SerializeField] CampanyListTableViewController tableview;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {
		
		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		//この括りいるか？
//		Hashtable _param;
//		if (param != null) {
//			_param = param;
//			memoriedParam = param;
//		} else {
//			_param = memoriedParam;
//		}

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}


		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

					List<ClassBox.CampanyData> campanyclasses = new List<ClassBox.CampanyData>();
					campanyclasses.AddRange(Global.Instance.CampanyList);
					tableview.UpdateData(campanyclasses);

				}

			}
		);


		/*

		//SideMenuでparamを作成
		Hashtable param_stafflist = param;

		Mskg.API.Instance.call (
			Cfg.API.getStaffList + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.Staff[] StaffList = null;
					try
					{
						StaffList =  LitJson.JsonMapper.ToObject<ClassBox.Staff[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(StaffList);
					Debug.Log("////////////");

					List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
					staffclasses.AddRange(StaffList);

					tableview.UpdateData(staffclasses);
					//					sidemenutableview.UpdateData(sidemenuclasses);




				}
			}
		);

		*/

	}
}
