﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class BroadcastTableViewController : TableViewController<Program>
{
//	[SerializeField] private M1_1_1_1B_Detail mB;
//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

//	[SerializeField] private M1_2_3X_Actions updatePanel;
	[SerializeField] private M1_2_3X_Actions addPanel;
	[SerializeField] private M1_2_3B_TodaysLoopEditor mB;


//	[SerializeField] private Button insertButton;
//	[SerializeField] private M1_2_3C_ProgramListItemEditor mC;

	public void UpdateData(List<Program> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
//		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(BroadcastTableViewCell cell)
	{
		if(navigationView != null)
		{
			//AlertViewController.Show (tableData [cell.DataIndex].title, null);
//			addPanel.gameObject.SetActive (false);

//			mB.UpdateContent (tableData [cell.DataIndex]);
//			navigationView.Push(mB);



			Hashtable param_list = new Hashtable ();
//			param_list.Add (Cfg.App.keyPARENT, int.Parse()); //int.Parse());
			param_list.Add (Cfg.App.keyPROGID, tableData [cell.DataIndex].programid);
			param_list.Add (Cfg.App.keySPECIAL, 0);
			param_list.Add (Cfg.App.keyTIME, "");

			mB.OnCallBackFromAddPanel (param_list);

			addPanel.gameObject.SetActive (false);
		}
	}



	public void onAddButton(){
		addPanel.onOpenPanel (null);
		addPanel.gameObject.SetActive(true);
	}


	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

