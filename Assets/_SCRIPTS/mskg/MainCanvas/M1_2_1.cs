﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//using UnityEditor;

public class M1_2_1 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;

	[SerializeField] SimpleProgramListTableViewController tableview;

	//1.1
	[SerializeField] Button InsertButton;
	[SerializeField] Toggle PlayableToggle;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {

//		Global.Instance.programMainCanvases.Add (this);

		switch (Global.Instance.Me.type) {
		case 0: //システム管理者
		case 1: //管理者
			mA.Title = Cfg.UI.sidemenu1_2[0].label;
			break;
		case 2: //ブロックユーザ
			mA.Title = Cfg.UI.sidemenu1_2block[0].label;
			break;
		case 3: //コーポレートユーザ
			mA.Title = Cfg.UI.sidemenu1_2campany[0].label;
			break;
		case 4: //店舗ユーザ
			mA.Title = Cfg.UI.sidemenu1_2shop[0].label;
			break;
		}


		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){
		navigationView.Pop ();
		navigationView.Pop ();

		memoriedParam = param;
		Debug.Log (memoriedParam);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		/*
		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

					List<ClassBox.CampanyData> campanyclasses = new List<ClassBox.CampanyData>();
					campanyclasses.AddRange(Global.Instance.CampanyList);
					tableview.UpdateData(campanyclasses);

				}

			}
		);
		*/


		Hashtable param_program = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_program.Add (Cfg.App.keyUSERID, Global.Instance.Me.id);
		param_program.Add (Cfg.App.keyTYPE, Global.Instance.Me.type);
		param_program.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		param_program.Add (Cfg.App.keyBLOCK, Global.Instance.Me.mst_area_id);
		param_program.Add (Cfg.App.keyCOMPANY, Global.Instance.Me.campany_id);
		param_program.Add (Cfg.App.keySUPER, Global.Instance.isSuper);

		//1.1
		if (memoriedParam != null) {
			if (memoriedParam [Cfg.App.keySHOPMENU] != null) {
				InsertButton.interactable = ((int)param [Cfg.App.keySHOPMENU] == 1);
				param_program.Add (Cfg.App.keySHOPMENU, memoriedParam [Cfg.App.keySHOPMENU]);
			} else {
				InsertButton.interactable = (int.Parse (Global.Instance.isSuper) == 1);
			}
		}

		//1.1 PlayableToggle
		PlayableToggle.gameObject.SetActive(Global.Instance.isSuper == "1");


		Mskg.API.Instance.call (
			Cfg.API.getSimpleProgram + ".php",
			param_program, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					
					try
					{
						Global.Instance.SimpleProgramList =  LitJson.JsonMapper.ToObject<ClassBox.SimpleProgram[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.SimpleProgramList);
					Debug.Log("////////////");

//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.SimpleProgram> programclasses = new List<ClassBox.SimpleProgram>();
					programclasses.AddRange(Global.Instance.SimpleProgramList);

					//仮です。ヒロ
					if (tableview != null)
						tableview.UpdateData(programclasses);

				}

			}
		);

	}


	//新番組追加
	public ClassBox.SimpleProgram p;
	public void onNewButton(){


		p = new ClassBox.SimpleProgram();

		NewProgramDlogViewOptions opt = new NewProgramDlogViewOptions();
		opt.cancelButtonDelegate = ()=>{

		};
		opt.okButtonDelegate = (callBackSimpleProgram)=>{
			Debug.Log(callBackSimpleProgram.title);
			//AddNewProgram(p);
			AddNewProgram(callBackSimpleProgram);
		};

		//初期化
		p.basic_page_time = 3;
		p.bgcolor = "#000";
		p.broadcast = 1; //放送用
		p.campany_id = Global.Instance.Me.campany_id;
		p.content_type = 1; // 一旦動画で
		p.editorid = Global.Instance.Me.id;
		p.layout_type = 0;//一旦0で
		p.mst_area_id = Global.Instance.Me.mst_area_id;
		p.shopid = Global.Instance.Me.shops;
		p.subtitle = "番組内容の解説";
		p.title = "新規プログラム";
		p.transition_type = 0;//とりあえず0で。


		NewProgramDlogViewController.Show(2,p, opt);

	}

	/*

	if (
		txtTitle.text == ""
	) {
		AlertViewController.Show ("確認", "必須項目が入力されていません。");
		return;
	}
	Hashtable param_list = new Hashtable ();
	param_list.Add (Cfg.App.keyTITLE, txtTitle.text);
	param_list.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
	param_list.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));
	param_list.Add (Cfg.App.keySHOPID, txtShopID.text);
	param_list.Add (Cfg.App.keyEDITOR, txtEditorID.text);

	Mskg.API.Instance.call (
		Cfg.API.insertSimpleProgram + ".php",
		param_list, 
		(Mskg.RtnObj ret) => { //callback
			if (ret.success) {

				Debug.Log("////////////");
				Debug.Log(ret);
				Debug.Log("////////////");


				if (checkError(ret.json)) {
					simpleProgListCanvas.updateMainCanvas(null); //データのリロード
				}

				this.gameObject.SetActive (false);
			}
		}
	);
*/


	void AddNewProgram(ClassBox.SimpleProgram p){


//		ClassBox.SimpleProgram temp = Global.Instance.TempSimpleProgram;
		ClassBox.SimpleProgram temp = p;

		//AlertViewController.Show(temp.title, temp.subtitle);


		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyTITLE, temp.title);
		param_list.Add (Cfg.App.keySUBTITLE, temp.subtitle);
		param_list.Add (Cfg.App.keyBROADCAST,temp.broadcast);
		param_list.Add (Cfg.App.keyCONTENTTYPE, temp.content_type);
		param_list.Add (Cfg.App.keyCOMPANY, Global.Instance.Me.campany_id);
		param_list.Add (Cfg.App.keyBLOCK, Global.Instance.Me.mst_area_id);
		param_list.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		param_list.Add (Cfg.App.keyEDITOR, Global.Instance.Me.id);

		//1.1
		param_list.Add(Cfg.App.keyCATEGORY, temp.category);
		param_list.Add (Cfg.App.keyRELEASEDATE, temp.releasedate);

		Mskg.API.Instance.call (
			Cfg.API.insertSimpleProgram + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						//mMainCanvas.updateMainCanvas(null); //データのリロード
						//						navigationView.Pop();
						updateMainCanvas(memoriedParam);
						AlertViewController.Show("作成成功", "プログラム["+p.title+"]を作成しました。");
					}

				}
			}
		);
	}


	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}


	void AddNewProgramList(){
		


		Hashtable param_todaysloop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_todaysloop.Add (Cfg.App.keyTITLE, Cfg.App.defaultTodaysLoopTitle);

		Mskg.API.Instance.call (
			Cfg.API.insertTodaysLoop + ".php",
			param_todaysloop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						//mMainCanvas.updateMainCanvas(null); //データのリロード
						//						navigationView.Pop();
						AlertViewController.Show("新規", "プログラムループを作成しました。");
					}

				}
			}
		);
	}


}
