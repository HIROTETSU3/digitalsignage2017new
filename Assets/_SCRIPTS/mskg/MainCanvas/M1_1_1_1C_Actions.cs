﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class M1_1_1_1C_Actions : MonoBehaviour {
	[SerializeField] private bool defaultHidden = true;
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_1_1_1 getStaffListCanvas;
	[SerializeField] private M1_1_1_1C_Actions insertPanel;
	[SerializeField] private Text userID;
	[SerializeField] private InputField txtName;
	[SerializeField] private InputField txtLogin;
	[SerializeField] private InputField txtMail;
	[SerializeField] private InputField txtPw;
	[SerializeField] private InputField txtShopID;
	[SerializeField] private InputField txtBlock;
	[SerializeField] private InputField txtCompany;
	[SerializeField] private InputField txtType;

	[SerializeField] private Dropdown shopDrop;
	[SerializeField] private Text lblBlock;
	[SerializeField] private Text lblCompany;
	[SerializeField] private Dropdown typeDrop;

	private bool changedShop = false;
	private bool changedType = false;

	// Use this for initialization
	void Start () {
		if (defaultHidden)
			this.gameObject.SetActive (false);
	}


	public void onOpenPanel(ClassBox.Staff staff){

		int shopDefault = 0;
		int typeDefault = 0;

		if (staff != null) { //Edit Panelの場合

			string plainPw = Mskg.API.Instance.Aes128CBCDecode (staff.password, Global.Instance.pwKey, Global.Instance.pwSalt);

			userID.text = staff.id.ToString();
			txtName.text = staff.name;
			txtLogin.text = staff.login;
			txtMail.text = staff.mail;
			txtPw.text = plainPw;
			txtShopID.text = staff.shops;
			txtBlock.text = staff.mst_area_id.ToString();
			txtCompany.text = staff.campany_id.ToString();
			txtType.text = staff.type.ToString();

			//shopDefault = staff.sho
			typeDefault = staff.type + 1; //---の分+1
		}

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("ショップリストを取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		//shop
		List<Dropdown.OptionData> shopOptions = new List<Dropdown.OptionData> ();
		shopDrop.options.Clear ();
		shopOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		foreach (ClassBox.ShopData shop in Global.Instance.ShopList) {
			if (staff != null) { //Edit Panelの場合
				if (shop.shopid == staff.shops) {
					shopDefault = counter + 1; //---の分+1
				}
				counter++;
			}
			shopOptions.Add (new Dropdown.OptionData{text = shop.name});
		}
		shopDrop.options = shopOptions;
		shopDrop.value = shopDefault; //default

		//type
		List<Dropdown.OptionData> typeOptions = new List<Dropdown.OptionData> ();
		typeDrop.options.Clear ();
		typeOptions.Add (new Dropdown.OptionData{text = "---"});
		foreach (Cfg.UI.SideMenuItemClass utype in Cfg.UI.sidemenu1_1_1) {
			typeOptions.Add (new Dropdown.OptionData{text = utype.label});
		}
		typeDrop.options = typeOptions;
		typeDrop.value = typeDefault; //default


		this.gameObject.SetActive (true);
	}


	public void OnShopDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			changedShop = false;
			txtShopID.text = "";
			txtBlock.text = "";
			lblBlock.text = "";
			txtCompany.text = "";
			lblCompany.text = "";
			return;
		}
		int selected = drop.value - 1;
		Debug.Log("value = " + Global.Instance.ShopList[selected].name);  //値を取得（先頭から連番(0～n-1)）
		txtShopID.text = Global.Instance.ShopList[selected].shopid;

		ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (Global.Instance.ShopList [selected].mst_area_id);
		txtBlock.text = area.id.ToString();
		lblBlock.text = area.name;

		ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (Global.Instance.ShopList [selected].campany_id);
		txtCompany.text = campany.id.ToString();
		lblCompany.text = campany.name;

		changedShop = true;

//		Debug.Log("text(options) = " + dropdown.options[value].text);  //リストからテキストを取得
//		Debug.Log("text(captionText) = " + dropdown.captionText.text); //Labelからテキストを取得
	}

	public void OnTypeDropValueChanged(Dropdown drop) {
		if (drop.value == 0) { //---の場合
			changedType = false;
			txtType.text = "";
			return;
		}
		int selected = drop.value - 1;
		Debug.Log("value = " + Cfg.UI.sidemenu1_1_1[selected].label);  //値を取得（先頭から連番(0～n-1)）
		txtType.text = selected.ToString();

		changedType = true;
		//		Debug.Log("text(options) = " + dropdown.options[value].text);  //リストからテキストを取得
		//		Debug.Log("text(captionText) = " + dropdown.captionText.text); //Labelからテキストを取得
	}



	// Update is called once per frame
	void Update () {
	
	}

	public void OnPressCloseButton()
	{
		this.gameObject.SetActive (false);
	}

	public void OnPressOpenInsertPanelButton()
	{
		//		navigationView.Push(mB);
		onOpenPanel(null);
			
		insertPanel.gameObject.SetActive(true);
	}

	public void OnPressInsertButton()
	{
		if (
			!changedShop || 
			!changedType ||
			txtName.text == "" ||
			txtLogin.text == "" ||
			txtMail.text == "" ||
			txtPw.text == ""
		) {
			AlertViewController.Show ("確認", "必須項目が入力されていません。");
			return;
		}


		string encodedPw = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);

		Hashtable param_stafflist = new Hashtable ();
		param_stafflist.Add (Cfg.App.keyNAME, txtName.text);
		param_stafflist.Add (Cfg.App.keyLOGIN, txtLogin.text);
		param_stafflist.Add (Cfg.App.keyMAIL, txtMail.text);
		param_stafflist.Add (Cfg.App.keyPW, encodedPw);
		param_stafflist.Add (Cfg.App.keySHOP, txtShopID.text);
		param_stafflist.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));
		param_stafflist.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
		param_stafflist.Add (Cfg.App.keyTYPE, int.Parse(txtType.text));

		Mskg.API.Instance.call (
			Cfg.API.insertStaff + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");
					/*
					ClassBox.Staff[] StaffList = null;
					try
					{
						StaffList =  LitJson.JsonMapper.ToObject<ClassBox.Staff[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(StaffList);
					Debug.Log("////////////");

					List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
					staffclasses.AddRange(StaffList);

					tableview.UpdateData(staffclasses);
					//					sidemenutableview.UpdateData(sidemenuclasses);
					*/



					if (checkError(ret.json)) {
						getStaffListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressUpdateButton()
	{

		string encodedPw = Mskg.API.Instance.Aes128CBCEncode (txtPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);

		Hashtable param_stafflist = new Hashtable ();

		param_stafflist.Add (Cfg.App.keyID, int.Parse(userID.text));
		param_stafflist.Add (Cfg.App.keyNAME, txtName.text);
		param_stafflist.Add (Cfg.App.keyLOGIN, txtLogin.text);
		param_stafflist.Add (Cfg.App.keyMAIL, txtMail.text);
		param_stafflist.Add (Cfg.App.keyPW, encodedPw);
		param_stafflist.Add (Cfg.App.keySHOP, txtShopID.text);
		param_stafflist.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));
		param_stafflist.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
		param_stafflist.Add (Cfg.App.keyTYPE, int.Parse(txtType.text));

		Mskg.API.Instance.call (
			Cfg.API.updateStaff + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						getStaffListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressDeleteButton()
	{
		Hashtable param_stafflist = new Hashtable ();

		param_stafflist.Add (Cfg.App.keyID, int.Parse(userID.text));
		param_stafflist.Add (Cfg.App.keyTABLE, Cfg.App.tableStaff);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						getStaffListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}


	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}


	public void onMakeRandomPassword(){
		int pass_num = Cfg.App.RANDOM_PASSWORD_NUM;
		txtPw.text = Mskg.API.Instance.randomPass(pass_num, pass_num);
	}
}
