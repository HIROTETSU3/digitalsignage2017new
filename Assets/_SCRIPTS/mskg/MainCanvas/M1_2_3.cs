﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using UnityEngine;
//using UnityEditor;

public class M1_2_3 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;

	[SerializeField] TodaysLoopListTableViewController tableview;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {

//		Global.Instance.programMainCanvases.Add (this);

		switch (Global.Instance.Me.type) {
		case 0: //システム管理者
		case 1: //管理者
			mA.Title = Cfg.UI.sidemenu1_2[0].label;
			break;
		case 2: //ブロックユーザ
			mA.Title = Cfg.UI.sidemenu1_2block[0].label;
			break;
		case 3: //コーポレートユーザ
			mA.Title = Cfg.UI.sidemenu1_2campany[0].label;
			break;
		case 4: //店舗ユーザ
			mA.Title = Cfg.UI.sidemenu1_2shop[0].label;
			break;
		}


		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		Hashtable param_program = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_program.Add (Cfg.App.keyUSERID, Global.Instance.Me.id);

		Mskg.API.Instance.call (
			Cfg.API.getSimpleTodaysLoop + ".php",
			param_program, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					
					try
					{
						Global.Instance.SimpleTodaysLoopList =  LitJson.JsonMapper.ToObject<ClassBox.SimpleTodaysLoop[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.SimpleTodaysLoopList);
					Debug.Log("////////////");

//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.SimpleTodaysLoop> todaysloopclasses = new List<ClassBox.SimpleTodaysLoop>();
					todaysloopclasses.AddRange(Global.Instance.SimpleTodaysLoopList);
					tableview.UpdateData(todaysloopclasses);

				}

			}
		);

	}




}
