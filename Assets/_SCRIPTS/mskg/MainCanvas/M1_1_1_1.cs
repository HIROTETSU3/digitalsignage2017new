﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_1_1_1 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;
	[SerializeField] private M1_1_1_1B_Detail mB;

	[SerializeField] StaffListTableViewController tableview;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {
		
		mA.gameObject.SetActive (false);
		mB.gameObject.SetActive (false);
		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log("----- > updateMainCanvas.updateMainCanvas");

		navigationView.Pop ();

		Hashtable _param;
		if (param != null) {
			_param = param;
			memoriedParam = param;
		} else {
			_param = memoriedParam;
		}

		Debug.Log (_param);
		//SideMenuでparamを作成
		//		Hashtable param_stafflist = new Hashtable ();
		//		param_stafflist.Add (Cfg.App.keyMYTYPE, Global.Instance.mytype);

		//key type は 選択されたユーザのタイプ
		//key mytype は ログインしているユーザのタイプ
		Hashtable param_stafflist = _param;
		Mskg.API.Instance.call (
			Cfg.API.getStaffList + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.Staff[] StaffList = null;
					try
					{
						StaffList =  LitJson.JsonMapper.ToObject<ClassBox.Staff[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(StaffList);
					Debug.Log("////////////");

					List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
					staffclasses.AddRange(StaffList);

					tableview.iSelectedUserType = int.Parse(param_stafflist[Cfg.App.keyTYPE].ToString());
					tableview.UpdateData(staffclasses);

					//					sidemenutableview.UpdateData(sidemenuclasses);


				}
			}
		);

	}
}
