﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ClassBox;

public class CampanyListTableViewCell : TableViewCell<CampanyData>
{
	[SerializeField] private Text txtID;
	[SerializeField] private Button updateButton;
	[SerializeField] private Button deleteButton;
	[SerializeField] public InputField txtName;

	string LastName;

	public int id;

	void Start(){
		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		deleteButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(CampanyData itemData)
	{
		id = itemData.id;
		txtID.text = id.ToString ();
		LastName = txtName.text = itemData.name;

		updateButton.interactable = false;
	}

	//ヒロ追加
	public void ChangeCampanyNameField(){

		if (txtName.text == LastName){
			updateButton.interactable = false;
		} else {
			updateButton.interactable = true;
		}

	}

	public void UpdateChangeCampanyName(){

	}
}

