﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;

public class ShopListTableViewCell : TableViewCell<ShopData>
{
	[SerializeField] private Button updateButton;
	[SerializeField] private Text txtName;
	[SerializeField] private Text txtShopID;
	[SerializeField] private Text txtCampany;
	[SerializeField] private Text txtArea;

	void Start(){
		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(ShopData itemData)
	{
		txtName.text = itemData.name;
		txtShopID.text = itemData.shopid;
		txtCampany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name;
		txtArea.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name;
	}


}

