﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ClassBox;

public class StaffListTableViewCell : TableViewCell<Staff>
{
	

	[SerializeField] private Button editButton;
	[SerializeField] private Button deleteButton;
	[SerializeField] private Text iDLabel;
	[SerializeField] private Text nameLabel;	// アイテム名を表示するテキスト
	[SerializeField] private Text loginNameLabel;
	[SerializeField] private Text blockLabel;
	[SerializeField] private Text campanyLabel;
	[SerializeField] private Text shopLabel;

	[SerializeField] Image Bg;
	[SerializeField] Color BgColor;

	void Start(){
		//editButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		//deleteButton.interactable = !(Mskg.API.Instance.isSuperLevel ());

		if (!Mskg.API.Instance.isSuperLevel ()){
			editButton.gameObject.SetActive(false);
			deleteButton.gameObject.SetActive(false);
		}
	}

	public override void UpdateContent(Staff itemData)
	{
		iDLabel.text = itemData.id.ToString();
		nameLabel.text = itemData.name;
		loginNameLabel.text = itemData.login;

		ClassBox.AreaData area = Mskg.API.Instance.getAreaByID (itemData.mst_area_id);
		ClassBox.CampanyData campany = Mskg.API.Instance.getCampanyByID (itemData.campany_id);
		ClassBox.ShopData shop = Mskg.API.Instance.getShopByShopID (itemData.shops);

		blockLabel.text = (area != null) ? area.name : "";
		campanyLabel.text = (campany != null) ? campany.name : "";
		shopLabel.text = (shop != null) ? shop.name : "";

		deleteButton.interactable = false;

		if (Mskg.API.Instance.isSuperLevel () ){

			//自分のアカウントは消せないようにする。
			if (Global.Instance.Me.id == itemData.id){
				deleteButton.interactable = false;
				Bg.color = BgColor;
			} else {
				deleteButton.interactable = true;
				Bg.color = new Color(1f,1f,1f);
			}

		}

	}


}

