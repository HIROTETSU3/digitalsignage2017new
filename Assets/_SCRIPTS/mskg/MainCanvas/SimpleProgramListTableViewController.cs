﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class SimpleProgramListTableViewController : TableViewController<SimpleProgram>
{
//	[SerializeField] private M1_1_1_1B_Detail mB;
//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

	[SerializeField] private M1_2_1C_Actions updatePanel;
	[SerializeField] private Button insertButton;
	[SerializeField] private M1_2_1B_ProgramEditor mB;

	public void UpdateData(List<SimpleProgram> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
//		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(SimpleProgramListTableViewCell cell)
	{



		///★デバッグ （客先に見せるのに殺していたのを復活）

		#if !TEST_SERVER

//		AlertViewOptions opt = new AlertViewOptions();
//		opt.okButtonTitle = "OK";
//
//		AlertViewController.Show("【開発中】","調整中です。006にて対応します。",opt);
//
//		return;
		#endif

		//Global.Instance.Me.type

		if(!cell.Editorble){

			AlertViewOptions opt = new AlertViewOptions();
			opt.cancelButtonTitle = "";
			opt.okButtonTitle = "閉じる";

			AlertViewController.Show("編集できません。","店舗で作成した番組のみ改廃が可能です。",opt);
			return;

		}


		if(navigationView != null)
		{
			//AlertViewController.Show (tableData [cell.DataIndex].title, null);
			mB.UpdateContent (tableData [cell.DataIndex]);
			navigationView.Push(mB);

		}
	}

	public void OnPressCellEditButton(SimpleProgramListTableViewCell cell)
	{
		ClassBox.SimpleProgram program = tableData [cell.DataIndex];

		updatePanel.onOpenPanel (program);
		updatePanel.gameObject.SetActive(true);
	}

	public void OnPressPreviewButton(SimpleProgramListTableViewCell cell)
	{
		Global.Instance.PlayerPreviewMode = true;
		Global.Instance.PlayerPreviewProgramID = tableData [cell.DataIndex].programid;
		SceneManager.LoadScene("PlayerStarter", LoadSceneMode.Additive);
	}

	public void OnPressPlayButton(SimpleProgramListTableViewCell cell) //if (Mskg.API.Instance.isTempUser()) {
	{
		
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyPROGID, tableData[cell.DataIndex].programid);
		param_list.Add (Cfg.App.keySHOPID, tableData[cell.DataIndex].shopid);

		Mskg.API.Instance.call (
			Cfg.API.insertLocalPlaying + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						AlertViewController.Show("再生", "動画情報を再生端末に送信しました。");
					}

					//this.gameObject.SetActive (false);
				}
			}
		);

	}
	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

