﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class M1_1_2C_Actions : MonoBehaviour {
	[SerializeField] private bool defaultHidden = true;
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_1_2 campanyListCanvas;
	[SerializeField] private M1_1_2C_Actions insertPanel;
	[SerializeField] private InputField txtName;


	#region GUI基本処理

	void Start () {
		if (defaultHidden)
			this.gameObject.SetActive (false);
	}

	public void OnPressCloseButton()
	{
		this.gameObject.SetActive (false);
	}

	public void OnPressOpenInsertPanelButton()
	{
//		onOpenPanel(null); //初期化不要
		insertPanel.gameObject.SetActive(true);
	}

	#endregion


	#region 初期化とドロップダウン操作

//	public void onOpenPanel(ClassBox.ShopData aShop){
//		
//	}

	#endregion


	#region WebAPI系ボタン操作

	public void OnPressInsertButton()
	{
		if (
			txtName.text == ""
		) {
			AlertViewController.Show ("確認", "必須項目が入力されていません。");
			return;
		}
			
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyNAME, txtName.text);

		Mskg.API.Instance.call (
			Cfg.API.insertCampany + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						campanyListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressUpdateButton(CampanyListTableViewCell cell)
	{

		
		DataAccessDlog.Show("データ更新中",1f,
			new DataAccessDlogOptions{
			closeDelegate = ()=>{
					Debug.Log("---> データ更新終了");
				}}
		);

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, cell.id);
		param_list.Add (Cfg.App.keyNAME, cell.txtName.text);

		Mskg.API.Instance.call (
			Cfg.API.updateCampany + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						campanyListCanvas.updateMainCanvas(null); //データのリロード
					}

					//this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressDeleteButton(CampanyListTableViewCell cell)
	{

		//確認ダイアログを表示

		AlertViewController.Show ("削除確認", "本当に削除しますか？", 
			new AlertViewOptions {
				cancelButtonTitle = "キャンセル",
				cancelButtonDelegate = () => {
					Debug.Log("キャンセルしました。");
					return;
				},
				okButtonTitle = "はい",
				okButtonDelegate = () => {
					doDelete(cell);
				},
			});


	}


	void doDelete(CampanyListTableViewCell cell){


		//応急措置
		#if !TEST_SERVER

		AlertViewController.Show ("【開発中】", "スタッフ移籍などの仕組み構築のため削除機能を一旦OFFにしております。", 
		new AlertViewOptions {
		cancelButtonTitle = null,
		okButtonTitle = "はい",
		okButtonDelegate = () => {
		Debug.Log("OK");
		},
		});
		return;

		#endif

		////// ↓ //////
		AlertViewController.Show ("【開発中】", "※ スタッフ移籍などの仕組み構築のため削除機能を一旦OFFにしております。" +
			"\n（開発スタッフ向け）=> 本当に削除して良い場合のみ「はい」を押してください", 
			new AlertViewOptions {
				cancelButtonTitle = "キャンセル",
				cancelButtonDelegate = () => {
					Debug.Log("キャンセルしました。");
					return;
				},
				okButtonTitle = "はい",
				okButtonDelegate = () => {
					Debug.Log("OK");

					////// ↑ ////// ここまでと↓

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, cell.id);
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableCampany);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						campanyListCanvas.updateMainCanvas(null); //データのリロード
					}

					//this.gameObject.SetActive (false);
				}
			}
		);

					////// ↓ ここから//////
				},
			});
		////// ↑ ////// ここまでを削除
	}

	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}

	#endregion
}
