﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_4 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;
	[SerializeField] private M1_4 deviceListCanvas;

	[SerializeField] DeviceListTableViewController tableview;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {
		
		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		//この括りいるか？
//		Hashtable _param;
//		if (param != null) {
//			_param = param;
//			memoriedParam = param;
//		} else {
//			_param = memoriedParam;
//		}

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}


		Hashtable _param = new Hashtable (); // jsonでリクエストを送るのへッダ例
		_param.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		Mskg.API.Instance.call (
			Cfg.API.getDevicesLive + ".php",
			_param, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.DeviceList =  LitJson.JsonMapper.ToObject<ClassBox.DeviceData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.DeviceList);
					Debug.Log("////////////");

					List<ClassBox.DeviceData> deviceclasses = new List<ClassBox.DeviceData>();
					deviceclasses.AddRange(Global.Instance.DeviceList);
					tableview.UpdateData(deviceclasses);

				}

			}
		);


		/*

		//SideMenuでparamを作成
		Hashtable param_stafflist = param;

		Mskg.API.Instance.call (
			Cfg.API.getStaffList + ".php",
			param_stafflist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.Staff[] StaffList = null;
					try
					{
						StaffList =  LitJson.JsonMapper.ToObject<ClassBox.Staff[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(StaffList);
					Debug.Log("////////////");

					List<ClassBox.Staff> staffclasses = new List<ClassBox.Staff>();
					staffclasses.AddRange(StaffList);

					tableview.UpdateData(staffclasses);
					//					sidemenutableview.UpdateData(sidemenuclasses);




				}
			}
		);

		*/

	}




	#region WebAPI系ボタン操作

	public void OnPressUpdateButton(DeviceListTableViewCell cell)
	{


		DataAccessDlog.Show("データ更新中",1f,
			new DataAccessDlogOptions{
				closeDelegate = ()=>{
					Debug.Log("---> データ更新終了");
				}}
		);

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, cell.id);
		param_list.Add (Cfg.App.keyNAME, cell.txtName.text);
		param_list.Add (Cfg.App.keyMEMO, cell.txtMemo.text);
		param_list.Add (Cfg.App.keyCHANNEL, cell.dropChannel.value);

		Mskg.API.Instance.call (
			Cfg.API.updateDeviceInfo + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						deviceListCanvas.updateMainCanvas(null); //データのリロード
					}

					//this.gameObject.SetActive (false);
				}
			}
		);

	}


	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}

	#endregion

}
