﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainCanvasChild : ViewController {

	[SerializeField] private string titleText;

	public override string Title { get { return titleText; } }

	// Use this for initialization
	protected virtual void Start () {
//		this.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
