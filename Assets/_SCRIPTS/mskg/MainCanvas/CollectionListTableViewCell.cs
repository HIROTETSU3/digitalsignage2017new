﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;

public class CollectionListTableViewCell : TableViewCell<CollectionItem>
{
//	[SerializeField] private Button updateButton;
	[SerializeField] private Text txtId;
	[SerializeField] private Text txtTitle;
	[SerializeField] private Text txtShopid;
	[SerializeField] private Text txtArea;
	[SerializeField] private Text txtEditor;
	[SerializeField] private Text txtCampany;
	[SerializeField] private Button btnPlay; //only tenpouser
	[SerializeField] private Image IconKind;
	[SerializeField] private Sprite IconMovie;
	[SerializeField] private Sprite IconSlideshow;
	[SerializeField] private Image Bg;
	[SerializeField] private Image LockIcon;
	[SerializeField] private Color[] BgColors;

	[SerializeField] private M1_2_8 mA;

	void Start(){
//		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
		btnPlay.interactable = (Mskg.API.Instance.isTempUser());
		btnPlay.gameObject.SetActive(Mskg.API.Instance.isTempUser());
	}


	public CollectionItem myItemData;
	public bool Editorble = false;

	public override void UpdateContent(CollectionItem itemData)
	{
		myItemData = itemData;

		if (Global.Instance.Me.id == itemData.editorid){
			//自分が作成、編集したもの
			 Editorble = true;
		}


		txtId.text = "No." + itemData.collectionid + "\nEDITOR : "+itemData.editorid;
		txtTitle.text = "Prog." + itemData.programid + " : " + itemData.title;

		ClassBox.ShopData _shop = Mskg.API.Instance.getShopByShopID (itemData.shopid);
		ClassBox.AreaData _area = Mskg.API.Instance.getAreaByID (itemData.mst_area_id);
		ClassBox.CampanyData _campany = Mskg.API.Instance.getCampanyByID (itemData.campany_id);

		txtShopid.text = (_shop != null) ? _shop.name : "---";
		txtArea.text = (_area != null) ? _area.name : "---";
		txtCampany.text = (_campany != null) ? _campany.name : "---";

		txtEditor.text = itemData.editorid.ToString() + " === " + Global.Instance.Me.id;

		//店舗ユーザーアクセスとして
		//if (Global.Instance.Me.shops == _shop.id.ToString()){

		if(itemData.firstsrc == null){
			//srcが一個もない
		}


		Button b = GetComponent<Button>();

		if(itemData.editorid == Global.Instance.Me.id)
		{
			Editorble = true;
			LockIcon.enabled = false;
//			if (b)
//				b.interactable = true; //Collectionは常にfalse

		} else {
			//ボタンとして押せないように
			//スクロールすると反映できないのでやめた。

			Editorble = false;
			if (b)
				b.interactable = false;
			LockIcon.enabled = true;
			

		}

		if (myItemData.content_type == 1){
			//動画
			IconKind.sprite = IconMovie;

			//Bg.color = BgColors[0];
		} else if (myItemData.content_type == 2){
			// スライドショー
			IconKind.sprite = IconSlideshow;
			//Bg.color = BgColors[1];

		}



//		txtShopid.text = itemData.shopid; //.name;
//		txtArea.text = .name;
//		txtCampany.text = .name;

//		txtCampany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name;
//		txtArea.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name;

		//サムネールを読み込みたい


	}


	public void onRemoveButton(){
		
		AlertViewOptions opt = new AlertViewOptions();
		opt.cancelButtonDelegate = () => {
			// Nothing
		};
		opt.okButtonDelegate = () => {
//			Debug.Log ("onAddCollection");
			removeCollection();
		};
		opt.cancelButtonTitle = "キャンセル";
		opt.okButtonTitle = "はい";

		AlertViewController.Show("番組コレクションから取り除いてもいいですか？",myItemData.title,opt);

	}


	private void removeCollection(){
		
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyID, myItemData.collectionid);
		param_list.Add (Cfg.App.keyTABLE, "Collection");

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						AlertViewController.Show("番組コレクションから取り除かれました。", myItemData.title);
						mA.updateMainCanvas(null);
					}

				}
			}
		);
	}

}

