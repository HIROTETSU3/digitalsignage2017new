﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class ProgramsListItemTableViewController : TableViewController<ProgramsListItem>
{
	[SerializeField] private M1_2_3B_TodaysLoopEditor mB;
//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

	[SerializeField] private M1_2_3X_Actions updatePanel;
	[SerializeField] private M1_2_3X_Actions addPanel;
//	[SerializeField] private Button insertButton;
//	[SerializeField] private M1_2_3C_ProgramListItemEditor mC;

	public void UpdateData(List<ProgramsListItem> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
//		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(ProgramsListItemTableViewCell cell)
	{
		if(navigationView != null)
		{
			//AlertViewController.Show (tableData [cell.DataIndex].title, null);
//			mB.UpdateContent (tableData [cell.DataIndex]);
//			navigationView.Push(mB);

		}
	}

	public void OnPressCellDeleteButton(ProgramsListItemTableViewCell cell)
	{
		ClassBox.ProgramsListItem pListItem = tableData [cell.DataIndex];
//		AlertViewController.Show (pListItem.program.title,null);

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, pListItem.id);
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableProgramsListItem);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						mB.UpdateContent(null); //データのリロード
					}

					//this.gameObject.SetActive (false);
				}
			}
		);





	}



	public void onAddButton(){
		addPanel.onOpenPanel (null);
		addPanel.gameObject.SetActive(true);
	}

	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

