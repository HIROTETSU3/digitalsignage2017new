﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//using UnityEditor;

public class M1_2_5 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;
	[SerializeField] SpecialProgramsTableViewController tableview;
	[SerializeField] private Dropdown dropChannel;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {

//		Global.Instance.programMainCanvases.Add (this);

		switch (Global.Instance.Me.type) {
		case 0: //システム管理者
		case 1: //管理者
			mA.Title = Cfg.UI.sidemenu1_2[0].label;
			break;
		case 2: //ブロックユーザ
			mA.Title = Cfg.UI.sidemenu1_2block[0].label;
			break;
		case 3: //コーポレートユーザ
			mA.Title = Cfg.UI.sidemenu1_2campany[0].label;
			break;
		case 4: //店舗ユーザ
			mA.Title = Cfg.UI.sidemenu1_2shop[0].label;
			break;
		}



		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		//1.1
		//channel
		List<Dropdown.OptionData> channelOptions = new List<Dropdown.OptionData> ();
		dropChannel.options.Clear ();
		//		channelOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		int channelDefault = 0;
		foreach (ClassBox.ChannelData channel in Global.Instance.ChannelDataArray) {
			int ch = 0;
			if (Global.Instance.CalenderID != "") {
				ch = int.Parse (Global.Instance.CalenderID);
			}
			if (channel.id == ch) {
				//channelDefault = counter + 1; //---の分+1
				channelDefault = counter;
			}
			counter++;
			channelOptions.Add (new Dropdown.OptionData{text = channel.id.ToString() + " : " + channel.name});
		}
		dropChannel.options = channelOptions;
		dropChannel.value = channelDefault; //default


		navigationView.Pop ();
		navigationView.Pop ();

		Debug.Log (param);

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		/*
		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

					List<ClassBox.CampanyData> campanyclasses = new List<ClassBox.CampanyData>();
					campanyclasses.AddRange(Global.Instance.CampanyList);
					tableview.UpdateData(campanyclasses);

				}

			}
		);
		*/


		Hashtable param_program = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//param_program.Add (Cfg.App.keyTHISMONTH, DateTime.Now.ToString("yyyy-MM"));

		//1.1
		param_program.Add (Cfg.App.keyCALENDERID, Global.Instance.CalenderID);

		Mskg.API.Instance.call (
			Cfg.API.getCalenderSpecialPrograms + ".php",
			param_program, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.CalenderSpecialProgram[] CalenderSPList = null;
					try
					{
						CalenderSPList =  LitJson.JsonMapper.ToObject<ClassBox.CalenderSpecialProgram[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(CalenderSPList);
					Debug.Log("////////////");

					List<ClassBox.CalenderSpecialProgram> spclasses = new List<ClassBox.CalenderSpecialProgram>();
					spclasses.AddRange(CalenderSPList);

					tableview.UpdateData(spclasses);
				}

			}
		);

	}


	//1.1
	public void onChangeChannel(){
		if (dropChannel.value == 0) {
			Global.Instance.CalenderID = "";
		} else {
			Global.Instance.CalenderID = dropChannel.value.ToString();
		}

		updateMainCanvas (null);
	}

}
