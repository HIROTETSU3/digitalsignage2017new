﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class M1_1_1_1B_Detail : MainCanvasChild {

	[SerializeField] private NavigationViewController navigationView;

	[SerializeField] private Text lblUserID;
	[SerializeField] private Text lblName;
	[SerializeField] private Text lblLogin;
	[SerializeField] private Text lblMail;
	[SerializeField] private Text lblPw;
	[SerializeField] private Text lblShopID;
	[SerializeField] private Text lblBlock;
	[SerializeField] private Text lblCompany;
	[SerializeField] private Text lblType;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void UpdateContent(ClassBox.Staff itemData)
	{

		lblUserID.text = itemData.id.ToString();
		lblName.text = itemData.name;
		lblLogin.text = itemData.login;
		lblMail.text = itemData.mail;
		lblShopID.text = Mskg.API.Instance.getShopByShopID(itemData.shops).name;//itemData.shops;
		lblBlock.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name; //itemData.mst_area_id.ToString();
		lblCompany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name; //itemData.campany_id.ToString();
		lblType.text = Mskg.API.Instance.getTypeNameByInt(itemData.type); //itemData.type.ToString();

//		AlertViewController.Show (itemData.name, "M1_1_1_1B");
	}
}
