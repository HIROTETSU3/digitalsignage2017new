﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class M1_2_3X_Actions : MonoBehaviour {
	[SerializeField] private bool defaultHidden = true;
	[SerializeField] private NavigationViewController navigationView;

	[SerializeField] private BroadcastTableViewController tableview;

	[SerializeField] private M1_2_3 simpleProgListCanvas;
	[SerializeField] private M1_2_3X_Actions addPanel;
	[SerializeField] private Text programID;

	[SerializeField] private InputField txtTitle;
	[SerializeField] private InputField txtCompany;
	[SerializeField] private InputField txtBlock;
	[SerializeField] private InputField txtShopID;
	[SerializeField] private InputField txtEditorID;


	#region GUI基本処理

	void Start () {
		if (defaultHidden)
			this.gameObject.SetActive (false);
	}

	public void OnPressCloseButton()
	{
		this.gameObject.SetActive (false);
	}

	public void OnPressOpenInsertPanelButton()
	{
		onOpenPanel(null);
		addPanel.gameObject.SetActive(true);
	}

	#endregion


	#region 初期化とドロップダウン操作

	public void onOpenPanel(ClassBox.Program program){
		
		if (program != null) { //Edit Panelの場合
//			programID.text = simpleTodaysLoop.id.ToString();
//			txtTitle.text = simpleTodaysLoop.title;
//			txtCompany.text = simpleTodaysLoop.campany_id.ToString();
//			txtBlock.text = simpleTodaysLoop.mst_area_id.ToString();
//			txtShopID.text = simpleTodaysLoop.shopid;
		}

//		if (!Global.Instance.initLoaded) {
//			AlertViewController.Show ("ショップリストを取得中", "取得が完了するまでしばらくお待ちください。");
//			return;
//		}

//		ClassBox.Staff me = Global.Instance.Me;
//
//		txtCompany.text = me.campany_id.ToString();
//		txtBlock.text = me.mst_area_id.ToString();
//		txtShopID.text = me.shops;
//		txtEditorID.text = me.id.ToString();



		Hashtable param_broadcastlist = new Hashtable (); // jsonでリクエストを送るのへッダ例
//		param_broadcastlist.Add (Cfg.App.keyPARENT, itemData.id);

		Mskg.API.Instance.call (
			Cfg.API.getBroadcast + ".php",
			param_broadcastlist, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					try
					{
						Global.Instance.BroadcastList =  LitJson.JsonMapper.ToObject<ClassBox.Program[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.BroadcastList);
					Debug.Log("////////////");

					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.Program> programclasses = new List<ClassBox.Program>();
					programclasses.AddRange(Global.Instance.BroadcastList);
					tableview.UpdateData(programclasses);

				}

			}
		);

	}


	#endregion


	#region WebAPI系ボタン操作

	public void OnPressAddButton()
	{
		AlertViewController.Show ("制作中", "この追加ボタンは現在制作中です。");
		return;

		if (
			txtTitle.text == ""
		) {
			AlertViewController.Show ("確認", "必須項目が入力されていません。");
			return;
		}


		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyTITLE, txtTitle.text);
		param_list.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
		param_list.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));
		param_list.Add (Cfg.App.keySHOPID, txtShopID.text);
		param_list.Add (Cfg.App.keyEDITOR, txtEditorID.text);

		Mskg.API.Instance.call (
			Cfg.API.insertSimpleProgram + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (checkError(ret.json)) {
						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	public void OnPressUpdateButton()
	{
//		Hashtable param_list = new Hashtable ();
//
//		param_list.Add (Cfg.App.keyID, int.Parse(programID.text));
//		param_list.Add (Cfg.App.keySHOPID, txtShopID.text);
//		param_list.Add (Cfg.App.keyNAME, txtName.text);
//		param_list.Add (Cfg.App.keyCOMPANY, int.Parse(txtCompany.text));
//		param_list.Add (Cfg.App.keyBLOCK, int.Parse(txtBlock.text));
//
//		Mskg.API.Instance.call (
//			Cfg.API.updateShop + ".php",
//			param_list, 
//			(Mskg.RtnObj ret) => { //callback
//				if (ret.success) {
//
//					Debug.Log("////////////");
//					Debug.Log(ret);
//					Debug.Log("////////////");
//
//					if (checkError(ret.json)) {
//						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
//					}
//
//					this.gameObject.SetActive (false);
//				}
//			}
//		);

	}

	public void OnPressDeleteButton()
	{

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, int.Parse(programID.text));
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableProgram);

		Mskg.API.Instance.call (
			Cfg.API.deleteItemAtTable + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (checkError(ret.json)) {
						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}

	bool checkError(string json){

		ClassBox.SuccessObj successObj = null;
		try
		{
			successObj =  LitJson.JsonMapper.ToObject<ClassBox.SuccessObj>(json);
		}
		catch (NullReferenceException ex)
		{
			Debug.Log(ex);
		}

		if (successObj.success) {
			return true;
		}else{
			AlertViewController.Show("エラー", "処理中にエラーが発生しました");
			return false;
		}

	}

	#endregion
}
