﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ClassBox;
using System.Collections.Generic;

public class DeviceListTableViewCell : TableViewCell<DeviceData>
{
	[SerializeField] private Text txtID;
	[SerializeField] private Button updateButton;
	[SerializeField] private Button deleteButton;
	[SerializeField] public InputField txtName;
	[SerializeField] public InputField txtMemo;
	[SerializeField] public Text txtUuid;
	[SerializeField] public Dropdown dropChannel;

	string LastName;
	string LastMemo;
	int LastChannel;

	public int id;

	void Start(){
		//updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		//deleteButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		//txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(DeviceData itemData)
	{
		id = itemData.id;
		txtID.text = id.ToString ();
		LastName = txtName.text = itemData.name;
		LastMemo = txtMemo.text = itemData.memo;

			
		txtUuid.text = itemData.uuid;


		updateButton.interactable = false;

		//channel
		List<Dropdown.OptionData> channelOptions = new List<Dropdown.OptionData> ();
		dropChannel.options.Clear ();
//		channelOptions.Add (new Dropdown.OptionData{text = "---"});
		int counter = 0;
		int channelDefault = 0;
		foreach (ClassBox.ChannelData channel in Global.Instance.ChannelDataArray) {
			if (channel.id == itemData.channel) {
				//channelDefault = counter + 1; //---の分+1
				channelDefault = counter;
			}
			counter++;
			channelOptions.Add (new Dropdown.OptionData{text = channel.id.ToString() + " : " + channel.name});
		}
		dropChannel.options = channelOptions;
		dropChannel.value = channelDefault; //default
		LastChannel = itemData.channel;
		ChangeNameMemoField();
	}

	//ヒロ追加 白石修正
	public void ChangeNameMemoField(){

		if (txtName.text != LastName || txtMemo.text != LastMemo || dropChannel.value != LastChannel){
			updateButton.interactable = true;
		} else {
			updateButton.interactable = false;
		}

	}

	public void UpdateChangeCampanyName(){

	}
}

