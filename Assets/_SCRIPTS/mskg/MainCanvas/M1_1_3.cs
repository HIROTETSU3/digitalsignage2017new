﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M1_1_3 : MainCanvas {

	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private MainCanvasChild mA;

	[SerializeField] ShopListTableViewController tableview;

	private Hashtable memoriedParam;

	// Use this for initialization
	protected override void Start () {
		
		base.Start ();

	}

	public override void initMainCanvas(Hashtable param){
		if (initialized) //initialized check
			return;

		#region アイテム一覧画面をナビゲーションビューに対応させる
		if(navigationView != null)
		{
			navigationView.Push(mA);
		}
		#endregion

		this.initialized = true;
	}

	public override void updateMainCanvas(Hashtable param){

		Debug.Log (param);

		//この括りいるか？
//		Hashtable _param;
//		if (param != null) {
//			_param = param;
//			memoriedParam = param;
//		} else {
//			_param = memoriedParam;
//		}

		if (!Global.Instance.initLoaded) {
			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
			return;
		}

		/*
		Hashtable param_campany = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_campany.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllCampanies + ".php",
			param_campany, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.CampanyData[] campanyList = null;
					try
					{
						Global.Instance.CampanyList =  LitJson.JsonMapper.ToObject<ClassBox.CampanyData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.CampanyList);
					Debug.Log("////////////");

					List<ClassBox.CampanyData> campanyclasses = new List<ClassBox.CampanyData>();
					campanyclasses.AddRange(Global.Instance.CampanyList);
					tableview.UpdateData(campanyclasses);

				}

			}
		);
		*/


		//ショップリスト取得
		Hashtable param_shop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//		param_shop.Add (Cfg.App.keyUSERID, useridString);
		Mskg.API.Instance.call (
			Cfg.API.getAllShop + ".php",
			param_shop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					//					ClassBox.ShopData[] staffList = null;
					try
					{
						Global.Instance.ShopList =  LitJson.JsonMapper.ToObject<ClassBox.ShopData[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.ShopList);
					Debug.Log("////////////");

//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.ShopData> shopclasses = new List<ClassBox.ShopData>();
					shopclasses.AddRange(Global.Instance.ShopList);
					tableview.UpdateData(shopclasses);

				}

			}
		);

	}
}
