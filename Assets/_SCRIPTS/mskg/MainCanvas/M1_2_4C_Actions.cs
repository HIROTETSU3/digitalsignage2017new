﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class M1_2_4C_Actions : MonoBehaviour {

	[SerializeField] private M1_2_4 m1_2_4;
	[SerializeField] private CalenderLayout calenderBase;
	[SerializeField] TodaysLoopForCalendarTableViewController tableviewLoop;
	[SerializeField] SpecialProgramForCalendarTableViewController tableviewSpecial;

	[SerializeField] NavigationViewController navigation2;
	[SerializeField] MainCanvasChild dialogA;
	[SerializeField] MainCanvasChild dialogB;

	string dateStrings = "";

	#region ダイアログの切り替え(Navigation2)

	void Start () {
		navigation2.Push (dialogA);
	}

	public void toSelectSpecial(){
		navigation2.Push (dialogB);
	}

	public void toSelectProgramLoop(){
		navigation2.Pop ();
	}

	#endregion

	#region GUI基本処理


	public void OnPressCloseButton()
	{
		this.gameObject.SetActive (false);
	}



	#endregion


	#region 初期化とドロップダウン操作

//	public void onOpenPanel(ClassBox.SimpleProgram simplePrg){
//		
//
//
//	}

	public void onOpenPanel(Hashtable param){
		navigation2.Pop ();

		Debug.Log (param);

//		if (!Global.Instance.initLoaded) {
//			AlertViewController.Show ("初期情報を取得中", "取得が完了するまでしばらくお待ちください。");
//			return;
//		}

		makeDateStringArray ();

		getLoop ();
		getSpecial ();

	}

	private void getLoop(){

		Hashtable param_program = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_program.Add (Cfg.App.keyUSERID, Global.Instance.Me.id);

		Mskg.API.Instance.call (
			Cfg.API.getSimpleTodaysLoop + ".php",
			param_program, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					try
					{
						Global.Instance.SimpleTodaysLoopList =  LitJson.JsonMapper.ToObject<ClassBox.SimpleTodaysLoop[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.SimpleTodaysLoopList);
					Debug.Log("////////////");

					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.SimpleTodaysLoop> todaysloopclasses = new List<ClassBox.SimpleTodaysLoop>();
					todaysloopclasses.AddRange(Global.Instance.SimpleTodaysLoopList);
					tableviewLoop.UpdateData(todaysloopclasses);

				}

			}
		);

	}

	private void getSpecial(){

		Hashtable param_program = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_program.Add (Cfg.App.keyUSERID, Global.Instance.Me.id);
		param_program.Add (Cfg.App.keyTYPE, Global.Instance.Me.type);
		param_program.Add (Cfg.App.keySHOPID, Global.Instance.Me.shops);
		param_program.Add (Cfg.App.keyBLOCK, Global.Instance.Me.mst_area_id);
		param_program.Add (Cfg.App.keyCOMPANY, Global.Instance.Me.campany_id);
		param_program.Add (Cfg.App.keySUPER, "1");

		Mskg.API.Instance.call (
			Cfg.API.getSimpleProgram + ".php",
			param_program, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					try
					{
						Global.Instance.SimpleProgramList =  LitJson.JsonMapper.ToObject<ClassBox.SimpleProgram[]>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(Global.Instance.SimpleProgramList);
					Debug.Log("////////////");

					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();

					List<ClassBox.SimpleProgram> programclasses = new List<ClassBox.SimpleProgram>();
					programclasses.AddRange(Global.Instance.SimpleProgramList);


					tableviewSpecial.UpdateData(programclasses);

				}

			}
		);

	}





	#endregion


	#region WebAPI系ボタン操作

	private void makeDateStringArray(){
		string[] dateStringArray;
		List<string> dateStringsList = new List<string>();
		foreach (CalenderCell cell in calenderBase.SelectedCells) {
			dateStringsList.Add (cell.MyDateTime.ToString(Cfg.App.DATE_FORMAT));
		}
		dateStringArray = dateStringsList.ToArray();
		dateStrings = string.Join("_", dateStringArray);
	}


	public void OnInsertAction(int programloopid)
	{
		Debug.Log (calenderBase.SelectedCells);

//		if (
//			txtTitle.text == ""
//		) {
//			AlertViewController.Show ("確認", "必須項目が入力されていません。");
//			return;
//		}

//		string[] dateStringArray;
//		List<string> dateStringsList = new List<string>();
//		foreach (CalenderCell cell in calenderBase.SelectedCells) {
//			dateStringsList.Add (cell.MyDateTime.ToString(Cfg.App.DATE_FORMAT));
//		}
//		dateStringArray = dateStringsList.ToArray();
//		string dateStrings = string.Join("_", dateStringArray);

		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyLOOPSET, programloopid);
		param_list.Add (Cfg.App.keyDATELIST, dateStrings);

		//1.1
		param_list.Add (Cfg.App.keyCALENDERID, Global.Instance.CalenderID);

		Mskg.API.Instance.call (
			Cfg.API.insertCalendarLoop + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						m1_2_4.updateCalenderInfo(null);
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}
		
	public void OnDeleteAction()
	{
		
//		string[] dateStringArray;
//		List<string> dateStringsList = new List<string>();
//		foreach (CalenderCell cell in calenderBase.SelectedCells) {
//			dateStringsList.Add (cell.MyDateTime.ToString(Cfg.App.DATE_FORMAT));
//		}
//		dateStringArray = dateStringsList.ToArray();
//		string dateStrings = string.Join("_", dateStringArray);

		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyDATELIST, dateStrings);

		//1.1
		param_list.Add (Cfg.App.keyCALENDERID, Global.Instance.CalenderID);

		Mskg.API.Instance.call (
			Cfg.API.deleteCalendarLoop + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						m1_2_4.updateCalenderInfo(null);
					}

					this.gameObject.SetActive (false);
				}
			}
		);

	}


	public void OnInsertSpecial(int programid, string timeString){
//		AlertViewController.Show ("OnInsertSpecial programid=" + programid.ToString() + ",time=" + timeString, dateStrings);
//		return;
		Hashtable param_list = new Hashtable ();
		param_list.Add (Cfg.App.keyPROGID, programid);
		param_list.Add (Cfg.App.keyTIME, timeString.Replace (":", "-"));
		param_list.Add (Cfg.App.keyDATELIST, dateStrings);

		//1.1
		param_list.Add (Cfg.App.keyCALENDERID, Global.Instance.CalenderID);

		Mskg.API.Instance.call (
			Cfg.API.insertSpecialProgram + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");


					if (Mskg.API.Instance.checkError(ret.json, null)) {
						m1_2_4.updateCalenderInfo(null);
					}

					this.gameObject.SetActive (false);
				}
			}
		);
	}

	public void OnDeleteSpecial(){
		AlertViewController.Show ("OnDeleteSpecial", dateStrings);
	}


	#endregion
}
