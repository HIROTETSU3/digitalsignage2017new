﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class M1_3_loginPanel : MonoBehaviour {

	[SerializeField] InputField inputUserID;
	[SerializeField] InputField inputPw;
	[SerializeField] M1_3 m1_3;
	// Use this for initialization
	void Start () {
		inputUserID.text = Global.Instance.userid;
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetKeyDown( KeyCode.Return ) )
			pushButtonGo();
	}

	public void pushButtonGo(){
		Global.Instance.userid = inputUserID.text;
//		Mskg.API.Instance.setUserID (Global.Instance.userid);
		string encPw = Mskg.API.Instance.Aes128CBCEncode (inputPw.text, Global.Instance.pwKey, Global.Instance.pwSalt);
		this.gameObject.SetActive (false);
		m1_3.login (Global.Instance.userid, encPw);
	}

	public void clearPwField(){
//		inputPw.text = "a"; //test用に"a"を入れてます。
	}
}
