﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class SrcListItemTableViewController : TableViewController<SrcListItemWithSrc>
{
	[SerializeField] private M1_2_1B_ProgramEditor mB;
	[SerializeField] private M1_2_1BB_SrcEditor mBB;
	[SerializeField] private OpenFile openfile;

//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

//	[SerializeField] private M1_2_3X_Actions updatePanel;
//	[SerializeField] private M1_2_3X_Actions addPanel;
//	[SerializeField] private Button insertButton;
//	[SerializeField] private M1_2_3C_ProgramListItemEditor mC;

	public void UpdateData(List<SrcListItemWithSrc> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
//		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(SrcListItemTableViewCell cell)
	{

		SlideValueChanegePanel.Show(cell,this);



		//OnUpdateCellData に移動

		/*
		if(navigationView != null)
		{


			return; //ここはインターフェースとして
			//AlertViewController.Show (tableData [cell.DataIndex].title, null);
			mBB.UpdateContent (tableData [cell.DataIndex]);
			navigationView.Push(mBB);

		}
*/

	}

	public void OnUpdateCellData(SrcListItemTableViewCell cell)
	{

		OnPressUpdateButton(cell);

	}


	public void OnPressCellDeleteButton(SrcListItemTableViewCell cell)
	{
		ClassBox.SrcListItemWithSrc pListItem = tableData [cell.DataIndex];
//		AlertViewController.Show (pListItem.program.title,null);

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, pListItem.SrcListItem_id);
		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableSrcListItem);

		Mskg.API.Instance.call (
			Cfg.API.deleteSrcListItemThenUpdateSrc + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						mB.UpdateContent(null); //データのリロード
					}

					//this.gameObject.SetActive (false);
				}
			}
		);





	}

	public void OnPressUpdateButton(SrcListItemTableViewCell cell)
	{
		ClassBox.SrcListItemWithSrc pListItem = tableData [cell.DataIndex];

		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, pListItem.SrcListItem_id);
		param_list.Add (Cfg.App.keyCAPTION, cell.MyItemData.caption);
		param_list.Add (Cfg.App.keyDURATION, cell.MyItemData.duration);

		Mskg.API.Instance.call (
			Cfg.API.updateSrcListItem + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						mB.UpdateContent(null); //データのリロード
						AlertViewController.Show("更新", "情報を更新しました。");
					}

					//this.gameObject.SetActive (false);
				}
			}
		);

	}

//	public void onAddButton(){
//		addPanel.onOpenPanel (null);
//		addPanel.gameObject.SetActive(true);
//	}

	public void onNewButton(M1_2_1B_ProgramEditor p){


		FileSelectConfirmOptions opt = new FileSelectConfirmOptions();
		opt.cancelButtonDelegate = () =>{
			Debug.Log("--CANCEL");
		};
		opt.cancelButtonTitle = "キャンセル";
			
		opt.okButtonDelegate = () =>{

			NewEmptyCard();

		};
		opt.okButtonTitle = "カードを追加";

		//ムービーかどうか
		if (p.simpleprog.content_type == 1){
			//MOVIE
			FileSelectConfirmUIController.Show(true,opt);
		} else if (p.simpleprog.content_type == 2){
			FileSelectConfirmUIController.Show(false,opt);
		} else {
			AlertViewOptions opt2 = new AlertViewOptions();
			opt.cancelButtonTitle = null;
			AlertViewController.Show("ERROR","開発エラー:" +
				"content_type=" + p.simpleprog.content_type + " で開こうとしています。",opt2);
		}


	}


	void NewEmptyCard(){


		//if (!openfile.OnClick())
		//	return;


		Hashtable param_srclistitem = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_srclistitem.Add (Cfg.App.keyPARENT, mB.simpleprog.programid);
		param_srclistitem.Add (Cfg.App.keyTITLE, "");//Cfg.App.defaultSrcListItemCaption);
		//param_srclistitem.Add (Cfg.App.keyDURATION, 3);//Cfg.App.defaultSrcListItemCaption);

		Mskg.API.Instance.call (
			Cfg.API.insertSrcListItem + ".php",
			param_srclistitem, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						mB.UpdateContent(null,
							(Mskg.RtnObj ret2) => { //確実に追加された後に処理を
								if (ret2.success) {

									mBB.UpdateContent (tableData[tableData.Count-1]);
									navigationView.Push(mBB);

									//
									openfile.OnClick();
								}
							}
						); //データのリロード



						//						navigationView.Pop();
						//AlertViewController.Show("新規", "プログラム素材を作成しました。"+src[src.Length-1].SrcListItem_id + "/" +src[src.Length-1].Src_id);
					}

				}
			}
		);
	}

	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 120.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

