﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;
using System.IO;

public class SrcListItemTableViewCell : TableViewCell<SrcListItemWithSrc>
{
	//	[SerializeField] private Button updateButton;
	[SerializeField] public Text txtId;
	[SerializeField] public Text txtCaption;
	//[SerializeField] public InputField txtCaption;
	//[SerializeField] public InputField txtDuration;
	[SerializeField] public Text txtDuration;
	[SerializeField] public Text txtPath;
//	[SerializeField] private Text txtShopid;
//	[SerializeField] private Text txtArea;
//	[SerializeField] private Text txtCampany;

	[SerializeField] private RawImage thumbnail;

	[SerializeField] Color DisableTxtColor;
	[SerializeField] Color EnableTxtColor;

	void Start(){
//		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public SrcListItemWithSrc MyItemData;

	public override void UpdateContent(SrcListItemWithSrc itemData)
	{
		MyItemData = itemData;

		txtId.text = itemData.SrcListItem_id.ToString();
		txtPath.text = "server file name : "+itemData.path;

		Button b = GetComponent<Button>();
		if (itemData.IsError()){
			//ファイルパスが設定されていない（ERROR)
			txtPath.text = "ERROR ファイルが設定されていません";

			txtCaption.text = "";
			txtCaption.color = DisableTxtColor;

		} else {
			if (itemData.IsMovie()){
				txtDuration.text = "MOVIE";
				txtCaption.text = "(Movie にキャプションは使用できません)";
				txtCaption.color = DisableTxtColor;

				if (b != null)
					b.interactable = false;

			} else {
				
				if (b != null)
					b.interactable = true;
				
				if (itemData.caption == "" || itemData.caption == null){
					txtCaption.text = "(キャプションを追加できます)";
					txtCaption.color = DisableTxtColor;
				} else {

					txtCaption.color = EnableTxtColor;
					txtCaption.text = itemData.caption;
				}
				txtDuration.text = itemData.duration.ToString () + "sec.";
			}
		}

		StartCoroutine (setThumbnail (GetPathWithoutExtension(itemData.path)));



//
//		ClassBox.ShopData _shop = Mskg.API.Instance.getShopByShopID (itemData.shopid);
//		ClassBox.AreaData _area = Mskg.API.Instance.getAreaByID (itemData.mst_area_id);
//		ClassBox.CampanyData _campany = Mskg.API.Instance.getCampanyByID (itemData.campany_id);
//
//		txtShopid.text = (_shop != null) ? _shop.name : "---";
//		txtArea.text = (_area != null) ? _area.name : "---";
//		txtCampany.text = (_campany != null) ? _campany.name : "---";

//		txtShopid.text = itemData.shopid; //.name;
//		txtArea.text = .name;
//		txtCampany.text = .name;

//		txtCampany.text = Mskg.API.Instance.getCampanyByID(itemData.campany_id).name;
//		txtArea.text = Mskg.API.Instance.getAreaByID(itemData.mst_area_id).name;
	}


	IEnumerator setThumbnail(string id){

		// wwwクラスのコンストラクタに画像URLを指定

		string url = Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + id + Cfg.App.THUMBNAIL + ".jpg";
		Debug.Log (url);
		WWW www = new WWW(url);

		// 画像ダウンロード完了を待機
		yield return www;

		// webサーバから取得した画像をRaw Imagで表示する
		//		RawImage rawImage = GetComponent<RawImage>();
		thumbnail.texture = www.textureNonReadable;

		//ピクセルサイズ等倍に
//		thumbnail.SetNativeSize();
	}

	private string GetPathWithoutExtension( string path )
	{
		var extension = Path.GetExtension( path );
		if ( string.IsNullOrEmpty( extension ) )
		{
			return path;
		}
		return path.Replace( extension, string.Empty );
	}
}

