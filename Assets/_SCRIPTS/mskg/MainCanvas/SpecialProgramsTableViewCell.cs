﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ClassBox;

public class SpecialProgramsTableViewCell : TableViewCell<CalenderSpecialProgram>
{
	//	[SerializeField] private Button updateButton;
	[SerializeField] private M1_2_5 mMainCanvas;

	[SerializeField] private Text txtTitle;
	[SerializeField] private Text txtDate;
	[SerializeField] private Text txtTime;

	private int thisID = 0;
	void Start(){
		//		updateButton.interactable = (Mskg.API.Instance.isSuperLevel ());
		//		txtName.interactable = (Mskg.API.Instance.isSuperLevel ());
	}

	public override void UpdateContent(CalenderSpecialProgram itemData)
	{
		thisID = itemData.id;
		//		imgChannel.gameObject.SetActive ((itemData.channel == Cfg.App.DEFAULT_CHANNEL));
		//		btnSetChannel.gameObject.SetActive ((itemData.channel != Cfg.App.DEFAULT_CHANNEL));

		//		txtId.text = itemData.id.ToString ();
		txtTitle.text = thisID.ToString() + " : " + itemData.program.title;

		txtDate.text = itemData.dateinfo.ToString(Cfg.App.DATE_FORMAT);
		txtTime.text = itemData.timeinfo.ToString(Cfg.App.TIME_FORMAT);
			//		txtSubTitle.text = itemData.subtitle;
			//		txtDate.text = itemData.date;
			//		txtMemo.text = itemData.memo;


			}
			/*
	public void onSetChannelButton(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyLOOPSET, thisID);

		Mskg.API.Instance.call (
			Cfg.API.updateDefaultChannel + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, "他のチャンネルに割り当てられている可能性があります。")) {
						mMainCanvas.updateMainCanvas(null); //データのリロード
					}
				}
			}
		);
	}
	*/
			}

