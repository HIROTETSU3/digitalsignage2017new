﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

//アイテムクラス定義はClassBoxで
using ClassBox;

[RequireComponent(typeof(ScrollRect))]
public class TodaysLoopListTableViewController : TableViewController<SimpleTodaysLoop>
{
//	[SerializeField] private M1_1_1_1B_Detail mB;
//	[SerializeField] private Button insertButton;
//
//	[SerializeField] private M1_1_1_1C_Actions updatePanel;
//	[SerializeField] private Text userID;
//	[SerializeField] private InputField txtName;
//	[SerializeField] private InputField txtLogin;
//	[SerializeField] private InputField txtMail;
//	[SerializeField] private InputField txtPw;
//	[SerializeField] private InputField txtShopID;
//	[SerializeField] private InputField txtBlock;
//	[SerializeField] private InputField txtCompany;
//	[SerializeField] private InputField txtType;

	[SerializeField] private M1_2_3 mMainCanvas;
	[SerializeField] private M1_2_3B_TodaysLoopEditor mB;
	[SerializeField] private M1_2_3X_Actions updatePanel;
	[SerializeField] private Button insertButton;

	public void UpdateData(List<SimpleTodaysLoop> itemclasses){

		tableData.Clear ();
		tableData = itemclasses;

		// スクロールさせる内容のサイズを更新する
		UpdateContents();
	}

	// Use this for initialization
	protected override void Start()
	{
		insertButton.interactable = (Mskg.API.Instance.isSuperLevel ());

		// ベースクラスのStartメソッドを呼ぶ
		base.Start();

		// スクロールさせる内容のサイズを更新する
		UpdateContents();

	}


	#region アイテム一覧画面をナビゲーションビューに対応させる
	// ナビゲーションビューを保持
	[SerializeField] private NavigationViewController navigationView;

	// ビューのタイトルを返す
	//public override string Title { get { return ""; } }
	#endregion


	#region アイテム詳細画面に遷移させる処理の実装

	//セルが選択されたときに呼ばれるメソッド
	public void OnPressTableViewCell(TodaysLoopListTableViewCell cell)
	{

//		///★デバッグ（客先に見せるのに殺していたのを復活
		#if !TEST_SERVER
//		AlertViewOptions opt = new AlertViewOptions();
//		opt.okButtonTitle = "OK";
//
//		AlertViewController.Show("【開発中】","調整中です。006にて対応します。",opt);
//
//		return;
		#endif
		if(navigationView != null)
		{
			//AlertViewController.Show (tableData [cell.DataIndex].title, null);
			mB.UpdateContent (tableData [cell.DataIndex]);
			navigationView.Push(mB);

		}
	}

//	public void OnPressCellEditButton(TodaysLoopListTableViewCell cell)
//	{
//		ClassBox.SimpleTodaysLoop todaysloop = tableData [cell.DataIndex];
//
////		updatePanel.onOpenPanel (todaysloop);
//		updatePanel.gameObject.SetActive(true);
//	}

	public void onNewButton(){

		//新規番組ループを作成

		NewProgramLoopDlogViewOptions opt = new NewProgramLoopDlogViewOptions();
		opt.cancelButtonDelegate = ()=>{

		};
		opt.okButtonDelegate = ()=>{
			AddNewProgram();
		};

		NewProgramLoopDlogViewController.Show(2,opt);

	}


	void AddNewProgram(){

		ClassBox.SimpleTodaysLoop t = Global.Instance.TempSimpleTodaysLoop;
		
		Hashtable param_todaysloop = new Hashtable (); // jsonでリクエストを送るのへッダ例
		//param_todaysloop.Add (Cfg.App.keyTITLE, Cfg.App.defaultTodaysLoopTitle);
		param_todaysloop.Add (Cfg.App.keyTITLE,t.title);
		param_todaysloop.Add (Cfg.App.keySUBTITLE,t.subtitle);
		param_todaysloop.Add (Cfg.App.keyMEMO,t.memo);



		Mskg.API.Instance.call (
			Cfg.API.insertTodaysLoop + ".php",
			param_todaysloop, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						mMainCanvas.updateMainCanvas(null); //データのリロード
						//						navigationView.Pop();
						AlertViewController.Show("新規", "プログラムループを作成しました。");
					}

				}
			}
		);
	}


	#endregion


	#region Layout要素
	// リスト項目に対応するセルの高さを返すメソッド
	protected override float CellHeightAtIndex(int index)
	{
		return 60.0f;
	}

	// インスタンスのロード時に呼ばれる
	protected override void Awake()
	{
		// ベースクラスのAwakeメソッドを呼ぶ
		base.Awake();
	}
	#endregion

}

