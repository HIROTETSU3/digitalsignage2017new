﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class M1_2_1BB_SrcEditor : MainCanvasChild {

	[SerializeField] private NavigationViewController navigationView;
	[SerializeField] private M1_2_1B_ProgramEditor programEditorCanvas;

//	[SerializeField] SrcListItemTableViewController tableview;

	[SerializeField] private InputField txtID;
	[SerializeField] private InputField txtPath;

	public ClassBox.SrcListItemWithSrc srcListItemWithSrc;
//	ClassBox.Program program;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onBackButton(){
		navigationView.Pop ();
	}
	/*
	public void onDeleteButton()
	{

//		AlertViewController.Show ("削除確認", "本当に削除しますか？", 
//			new AlertViewOptions {
//				cancelButtonTitle = "キャンセル",
//				cancelButtonDelegate = () => {
//					Debug.Log("キャンセルしました。");
//					return;
//				},
//				okButtonTitle = "はい",
//				okButtonDelegate = () => {
//					doDelete();
//				},
//			});
		
	}

	private void doDelete(){
//		Hashtable param_list = new Hashtable ();
//
//		param_list.Add (Cfg.App.keyID, simpleprog.programid);
//		param_list.Add (Cfg.App.keyTABLE, Cfg.App.tableProgram);
//
//		Mskg.API.Instance.call (
//			Cfg.API.deleteItemAtTable + ".php",
//			param_list, 
//			(Mskg.RtnObj ret) => { //callback
//				if (ret.success) {
//
//					Debug.Log("////////////");
//					Debug.Log(ret);
//					Debug.Log("////////////");
//
//					if (Mskg.API.Instance.checkError(ret.json, null)) {
//						simpleProgListCanvas.updateMainCanvas(null); //データのリロード
//						navigationView.Pop();
//					}
//
//					this.gameObject.SetActive (false);
//				}
//			}
//		);
	}
*/

	public void onUpdateButton(){
		Hashtable param_list = new Hashtable ();

		param_list.Add (Cfg.App.keyID, srcListItemWithSrc.Src_id);
		param_list.Add (Cfg.App.keyPATH, txtPath.text);


		Mskg.API.Instance.call (
			Cfg.API.updateSrc + ".php",
			param_list, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					Debug.Log("////////////");
					Debug.Log(ret);
					Debug.Log("////////////");

					if (Mskg.API.Instance.checkError(ret.json, null)) {
						programEditorCanvas.UpdateContent(null); //データのリロード
						navigationView.Pop();
						//AlertViewController.Show("更新", "アップロードに成功し、情報を更新しました。");
					}

				}
			}
		);
	}
		
	public void UpdateContent(ClassBox.SrcListItemWithSrc itemData)
	{
//		AlertViewController.Show("制作中", null);
//		return;

		if (itemData != null) {
			srcListItemWithSrc = itemData;


			txtID.text = Mskg.API.Instance.randomPass (16, 16); //itemData.Src_id.ToString();
			txtPath.text = itemData.path;

		} else {
			itemData = srcListItemWithSrc;
		}
//
//		//SrcListItemの取得
//		Hashtable param_srclistitem = new Hashtable (); // jsonでリクエストを送るのへッダ例
//		param_srclistitem.Add (Cfg.App.keyPARENT, itemData.programid);
//
//		Mskg.API.Instance.call (
//			Cfg.API.getSrcListItemWithSrc + ".php",
//			param_srclistitem, 
//			(Mskg.RtnObj ret) => { //callback
//				if (ret.success) {
//
//					try
//					{
//						Global.Instance.SrcListItemList =  LitJson.JsonMapper.ToObject<ClassBox.SrcListItemWithSrc[]>(ret.json);
//					}
//					catch (NullReferenceException ex)
//					{
//						Debug.Log(ex);
//					}
//
//					Debug.Log("////////////");
//					Debug.Log(Global.Instance.SrcListItemList);
//					Debug.Log("////////////");
//
//					//					Mskg.API.Instance.putCampanyAndAreaNameIntoShopList ();
//
//					List<ClassBox.SrcListItemWithSrc> srclistitemclasses = new List<ClassBox.SrcListItemWithSrc>();
//					srclistitemclasses.AddRange(Global.Instance.SrcListItemList);
//					tableview.UpdateData(srclistitemclasses);
//
//				}
//
//			}
//		);


	}
}
