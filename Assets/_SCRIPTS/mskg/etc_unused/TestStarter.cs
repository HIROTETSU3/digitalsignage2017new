﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LitJson;

public class TestStarter : MonoBehaviour {

	[SerializeField] private Text tf; 
//	[SerializeField] private string filename; 
	[SerializeField] string filename = ""; //ClassBox.fileNameX filename;
	[SerializeField] GameObject inputPanel;

	[SerializeField] private Text pregressOutput;

	List<string> dllist;
	Hashtable compHashTable = new Hashtable();
	int dlerrorCount;

	void Start () {

		SceneManager.activeSceneChanged += Global.Instance.OnActiveSceneChanged;


		dlerrorCount = 0;
		Global.Instance.uuid = Mskg.API.Instance.getUUID ();

		//Global.Instance.filenameX = filename.ToString() + ".mp4";
		/*
		/// ダウンロードサンプル
		Mskg.API.Instance.download (
			Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + Global.Instance.filenameX,
			Cfg.App.LOCAL_DIR,
			(Mskg.RtnObj ret) => { //progress
				//Debug.Log (ret);
				tf.text = ret.obj.ToString();
			},
			(Mskg.RtnObj ret) => { //complete
				if (ret.success) {
//					text.text = Cfg.App.LOCAL_DIR;
//					SceneManager.LoadScene("03_Demo_VideoControls 1", LoadSceneMode.Single);
				}
			}
		);
		*/

		Mskg.API.Instance.getShopID ((Mskg.RtnObj ret) => {

			if (ret.success) {
				inputPanel.SetActive(false);
				Global.Instance.shopid = ret.obj as string;
				login();
			}else{
				inputPanel.SetActive(true);
			}

		});



	}

	//callback delegate sample
	void onCallback( Mskg.RtnObj ret ){
		Debug.Log (ret.obj);
	}


	public void login(){

		/// 初期ログインサンプル（成功例）
		Hashtable param_shopplayerlogin = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_shopplayerlogin.Add (Cfg.App.keySHOPID, Global.Instance.shopid);
		//		param_shopplayerlogin.Add (Cfg.App.keyUUID, Global.Instance.uuid);
		Mskg.API.Instance.call (
			Cfg.API.shopplayerlogin + ".php", //".php"は仮 
			param_shopplayerlogin, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.ShopPlayerLogin loginObj = null;
					try
					{
						loginObj =  LitJson.JsonMapper.ToObject<ClassBox.ShopPlayerLogin>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(loginObj);
					Debug.Log(LitJson.JsonMapper.ToJson(loginObj));
					//					Debug.Log(jsonObject);
					Debug.Log("////////////");

					programInit();
				}
			}
		);

		/// 初期ログインサンプル（エラー）
		Mskg.API.Instance.call (
			Cfg.API.shopplayerloginError + ".php", //".php"は仮 
			param_shopplayerlogin, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {

					ClassBox.ShopPlayerLogin loginObj = null;
					try
					{
						loginObj =  LitJson.JsonMapper.ToObject<ClassBox.ShopPlayerLogin>(ret.json);
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					Debug.Log("////////////");
					Debug.Log(loginObj);
					Debug.Log(LitJson.JsonMapper.ToJson(loginObj));
					//					Debug.Log(jsonObject);
					Debug.Log("////////////");

				}
			}
		);

		//		Debug.Log (Guid.NewGuid ());
		Debug.Log("####");
		Debug.Log (Global.Instance.uuid);
		Debug.Log("####");

	}


	public void programInit(){

		/// 店情報取得初期APIサンプル
		Hashtable param_shopinit = new Hashtable (); // jsonでリクエストを送るのへッダ例
		param_shopinit.Add (Cfg.App.keySHOPID, Global.Instance.shopid);
		//		param_shopinit.Add (Cfg.App.keyUUID, Global.Instance.uuid);
		Mskg.API.Instance.call (
			Cfg.API.getShopSettings + ".php", //".php"は仮 
			param_shopinit, 
			(Mskg.RtnObj ret) => { //callback
				if (ret.success) {
					//sampleObj = new ClassBox.ShopInit();
					//					try
					//					{
					//						sampleObj = JsonUtility.FromJson<ClassBox.ShopInit>(ret.json);
					//					}
					//					catch (NullReferenceException ex)
					//					{
					//						Debug.Log("error");
					//					}

//					ClassBox.ShopSettings shopinitObj = null;
					try
					{
						//					//JSONテキストのデコード.
						//LitJson.JsonData jsonObject =  LitJson.JsonMapper.ToObject(ret.json);
						Global.Instance.shopSettingsObj =  LitJson.JsonMapper.ToObject<ClassBox.ShopSettings>(ret.json);
						//sampleObj.user_setting.id = (int)jsonObject["user_setting"]["id"];
					}
					catch (NullReferenceException ex)
					{
						Debug.Log(ex);
					}

					//					//データの取得
					////					string message = (string)jsonData["message"];
					////					int num = (int)jsonData["num"];

					Debug.Log("########");
					Debug.Log(Global.Instance.shopSettingsObj);
					Debug.Log(LitJson.JsonMapper.ToJson(Global.Instance.shopSettingsObj));
					//					Debug.Log(jsonObject);
					Debug.Log("########");

//					string now = DateTime.Now.ToString();
//					Debug.Log(now);
//					DateTime.Parse("01/01/2017");
//					Debug.Log(DateTime.Parse("01/01/2017"));
//
//
//					System.DateTime back1 = System.DateTime.Parse("07/08/2017 21:23:10");
//					System.DateTime back2 = System.DateTime.Parse("21:23:10");
//					System.DateTime back3 = System.DateTime.Parse("07/08/2017");
//					System.DateTime back4 = System.DateTime.ParseExact("2017-07-08 21:23:10","yyyy-MM-dd HH:mm:ss",null);
//					string timenow = System.DateTime.Now.ToString();

					dllist = new List<string>();
					foreach (ClassBox.ProgramsListItem progListItem in Global.Instance.shopSettingsObj.todays_loop.programs)
					{
						foreach (ClassBox.SrcListItem srcListItem in progListItem.program.src_list)
						{
							dllist.Add(Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + srcListItem.src.path);
						}
					}
					foreach (ClassBox.ProgramsListItem progListItem in Global.Instance.shopSettingsObj.todays_loop.specialprograms)
					{
						foreach (ClassBox.SrcListItem srcListItem in progListItem.program.src_list)
						{
							dllist.Add(Cfg.App.BASE_URL + Cfg.App.ASSETS_DIR + srcListItem.src.path);
						}
					}
					Debug.Log("----SRC----");
					foreach (string item in dllist){
						Debug.Log(item);
					}
					Debug.Log(Cfg.App.LOCAL_DIR);
					Debug.Log("--------");

					Mskg.API.Instance.downloadSrcList(
						dllist.ToArray(),
						Cfg.App.LOCAL_DIR,
						(Mskg.RtnObj ret1) => {
							outputProgress(ret1.obj);
						},
						Global.Instance.dlExistenceCheck,
						Global.Instance.dlAutoDelete
					);

				}
			}
		);




		/// プログラム情報取得APIサンプル

	}


	void outputProgress(object obj){
		Mskg.DownLoadObject dlo = (Mskg.DownLoadObject)obj;
		if (dlo.complete || dlo.existence) {
			compHashTable.Add ("asset" + dlo.uid, dlo.complete);
			Debug.Log (compHashTable.ToString ());
			pregressOutput.text += "id : " + dlo.uid + 
				" pregoress : " + dlo.progress + 
				" complete : " + dlo.complete + 
				" error : " + dlo.error + 
				" existence : " + dlo.existence + 
				"\n";

			if (dlo.error) {
				dlerrorCount++;
			}

			if (dllist.Count == compHashTable.Count) {

				if (0 < dlerrorCount) {
					pregressOutput.text += "エラーがあります。" +
					"\n";
				} else {
					SceneManager.LoadScene("PrgLoopPlayer", LoadSceneMode.Single);
				}


			}
		}

	}
}
