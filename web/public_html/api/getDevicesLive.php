<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//GetAllCampanies

	$devicelist = array();
	foreach (execSql(sqlGetDevicesLive($_GET['shopid'])) as $_device) {
		$device = new DeviceData();
		$device->id = (int)$_device["id"];
		$device->name = $_device["name"];
		$device->uuid = $_device["uuid"];
		$device->memo = $_device["memo"];
		$device->channel = (int)$_device["channel"];
		$devicelist[] = $device;
	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($devicelist, true); // 配列をJSON形式に変換してくれる
exit();

?>