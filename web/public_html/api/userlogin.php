<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");


$success = false;
$userlogin = new UserLogin();


$stafflist = array();
foreach (execSql(sqlGetStaffByUserID($_GET['userid'])) as $_staff) {
	$staff = new StaffData();
	$staff->id = (int)$_staff["id"];
	$staff->type = (int)$_staff["type"];
	$staff->name = $_staff["name"];
	$staff->login = $_staff["login"];
	$staff->mail = $_staff["mail"];
	$staff->modifieddate = $_staff["modifieddate"];
	$staff->campany_id = (int)$_staff["campany_id"];
	$staff->mst_area_id = (int)$_staff["mst_area_id"];
	$staff->shops = $_staff["shops"];
	$staff->password = $_staff["password"];
	$stafflist[] = $staff;
}

if (0 < count($stafflist)){
	$userlogin->user = $stafflist[0];
	$success = true;
}

$userlogin->success = $success;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($userlogin, true); // 配列をJSON形式に変換してくれる
exit();


?>
