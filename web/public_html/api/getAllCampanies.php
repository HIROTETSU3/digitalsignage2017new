<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//GetAllCampanies

	$campanylist = array();
	foreach (execSql(sqlGetAllCampanies()) as $_campany) {
		$campany = new CampanyData();
		$campany->id = (int)$_campany["id"];
		$campany->name = $_campany["name"];
		$campanylist[] = $campany;
	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($campanylist, true); // 配列をJSON形式に変換してくれる
exit();

?>