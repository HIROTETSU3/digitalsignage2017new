<?php
require_once("config.php");

//$cfg_DataDir.$_GET['filename'];
try {
$pdo = new PDO('mysql:host='.$db_host.';dbname='.$db_name.';charset=utf8', $db_user, $db_pw,
array(PDO::ATTR_EMULATE_PREPARES => false));
// echo "connected";
} catch (PDOException $e) {
 exit('データベース接続失敗。'.$e->getMessage());
}



	function execSql($sql)
{
	// // var_dump($pdo);
	// //sample1
	// // "SELECT * FROM program WHERE id=1 ORDER BY id ASC"
	// $stmt = $pdo->query($sql);
	// while($row = $stmt -> fetch(PDO::FETCH_ASSOC)) {
	//  $ttitle = $row["title"];
	//  // $tsubtitle = $row["subtitle"];
	//  // $ttype = $row["type"];
	//  // $tcreated = $row["created"];
	//  $tid = (int)$row["id"];
	//  // echo $ttitle;
	// // echo<<<EOF
	// // EOF;
	// }

	//sample2
	// クエリーの実行
	// $sql = "SELECT id, name, address FROM members";

	global $pdo;

	// $statement = $GLOBALS['pdo']->query($sql);
	$statement = $pdo->query($sql);
	// 結果の取得
	$members = array();
	foreach ($statement as $row) {
	    $members[] = $row;
	}
	//var_dump($members);

	// バージョンによってはきちんとcloseしないと次のSQLが実行されない
	$statement->closeCursor();


    return $members;
}

// Select

//shopplayerlogin
//$_GET['shopid']
function sqlGetPlayer($uuid)
{
	$sql = "SELECT 
	    Player.id AS Player_id,
	    Shop.id AS Shop_id,
	    Player.*, Shop.* 
		 FROM `Player`, `Shop` 
		 WHERE Player.shopid = Shop.shopid 
		 AND Player.uuid = '".$uuid."';";
		 // var_dump($sql);
	return $sql;
}

function sqlGetShop($shopid)
{
	return "SELECT * FROM `Shop` WHERE shopid = '".$shopid."'";
}

function sqlShopPlayerLogin($shopid) //未使用
{
	return "SELECT * FROM `User` WHERE uuid = 'fa0e9597-7cf1-4b8b-8628-406e38c083b9&r=2849&shopid=987654'".$shopid;
}

//getShopSettings

function sqlProgramsListItem($channel)
{
	//channel指定でデフォルトのループセットを特定
	return "SELECT * 
		FROM  `ProgramsListItem` 
		WHERE  `parentTodaysLoop` = ( 
			SELECT id
			FROM TodaysLoop
			WHERE TodaysLoop.id =  ( 
				SELECT loopset
				FROM Channel
				WHERE Channel.id =  ".$channel." 
			)
		) ;";

	//日付と店舗iDでループセットを返す版 - old
	/*
	if (isset($shopid)) {
		return "SELECT * 
			FROM  `ProgramsListItem` 
			WHERE  `parentTodaysLoop` = ( 
			SELECT id
			FROM TodaysLoop
			WHERE TodaysLoop.date =  ".$today." 
			AND TodaysLoop.shopid =  ".$shopid." ) ;";
	}else{
		return "SELECT * 
			FROM  `ProgramsListItem` 
			WHERE  `parentTodaysLoop` = ( 
			SELECT id
			FROM TodaysLoop
			WHERE TodaysLoop.date =  ".$today." ) ;";
	}
	*/

	// return "SELECT * 
	// 	FROM  `ProgramsListItem` 
	// 	WHERE  `parentTodaysLoop` = ( 
	// 	SELECT id
	// 	FROM TodaysLoop
	// 	WHERE TodaysLoop.date =  ".$today." ) ;";
}

function sqlGetPlayerChannel($uuid){
	return "SELECT channel FROM Player WHERE uuid = '".$uuid."'";
}


function sqlProgramsListItemByCalendarLoopset($myChannelID)
{
	return "SELECT * 
		FROM  `ProgramsListItem` 
		WHERE  `parentTodaysLoop` = ( 
			SELECT loopset 
			FROM  `Calender".$myChannelID."` 
			WHERE  `date` = CURRENT_DATE()
			) ;";
}

function sqlChannelByShopId($shopid)
{
	return "SELECT * 
		FROM  `Channel` 
		WHERE  `shopid` = '".$shopid."'";
}


function sqlLocalPlaying($shopid, $playedid)
{
	// return "SELECT * 
	// 	FROM  `Program` 
	// 	WHERE Program.parentProgramsListItem = ".$proglistitemid;

	// return "SELECT * 
	// 	FROM  `LocalPlaying`  
	// 	WHERE played = 0 AND shopid = '".$shopid."'";

	/*
	return "SELECT * FROM `LocalPlaying`
		WHERE id = (SELECT max(id) FROM `LocalPlaying`
			WHERE shopid = '".$shopid."') 
			AND ".$playedid." < (SELECT max(id) FROM `LocalPlaying`
			WHERE shopid = '".$shopid."') 
			AND CURRENT_DATE() < created";
	*/
			
	return "SELECT * FROM `LocalPlaying`
		WHERE id = (SELECT max(id) FROM `LocalPlaying`
			WHERE shopid = '".$shopid."' OR shopid = 'allshop') 
			AND ".$playedid." < (SELECT max(id) FROM `LocalPlaying`
			WHERE shopid = '".$shopid."' OR shopid = 'allshop') 
			AND CURRENT_DATE() < created";
}

// function sqlProgram($proglistitemid)
function sqlProgram($programid)
{
	// return "SELECT * 
	// 	FROM  `Program` 
	// 	WHERE Program.parentProgramsListItem = ".$proglistitemid;

	return "SELECT * 
		FROM  `Program` 
		WHERE Program.id = ".$programid;
}

function sqlSrcListItem($programid) //結合SQLで一気にSrcも取得
{
	/*
	return "SELECT * 
		FROM  `SrcListItem` 
		WHERE parentProgram = ".$programid;
		*/
	return "SELECT 
	    SrcListItem.id AS SrcListItem_id,
	    Src.id AS Src_id,
	    SrcListItem.*, Src.* 
			FROM  `SrcListItem` ,  `Src` 
			WHERE Src.parentSrcListItem = SrcListItem.id 
			AND SrcListItem.parentProgram = ".$programid;
}

function sqlSrc($srclistitemid) //未使用
{
	return "SELECT * 
		FROM  `Src` 
		WHERE Src.parentSrcListItem = ".$srclistitemid;
}


/// <summary>
/// Kannri
/// </summary>

function sqlStaffList($mytype, $type) 
{
	// return "SELECT  `id` ,  `type` ,  `name` ,  `login` , 
	// 	 `mail` ,  `modifieddate` ,  `campany_id` ,  `mst_area_id` ,  `shops` 
	// 	FROM  `Staff` 
	// 	WHERE TYPE >= ".$mytype;

	if (isset($type)) {
		return "SELECT  * FROM  `Staff` 
		WHERE TYPE >= ".$mytype." AND TYPE = ".$type;
	}else{
		return "SELECT  * FROM  `Staff` 
		WHERE TYPE >= ".$mytype;
	}
	
}

function sqlGetStaffByUserID($userid){
	return "SELECT * 
		FROM  `Staff` 
		WHERE  `login` =  '".$userid."'";
}

function sqlGetCampanyByID($id){
	return "SELECT * 
		FROM  `Campanies` 
		WHERE  `id` =  '".$id."'";
}

function sqlGetAreaByID($id){
	return "SELECT * 
		FROM  `Mst_areas` 
		WHERE  `id` =  '".$id."'";
}

function sqlGetTodaysLoopByID($id){
	return "SELECT * 
		FROM  `TodaysLoop` 
		WHERE  `id` =  '".$id."'";
}

function sqlSpecialProgram()
{
	// return "SELECT * 
	// 	FROM  `Program` 
	// 	WHERE Program.parentProgramsListItem = ".$proglistitemid;

	return "SELECT * 
		FROM  `SpecialProgram` 
		WHERE `dateinfo` = CURRENT_DATE()";
}

/// Program
function sqlSrcList() 
{

	// if (isset($type)) {
	// 	return "SELECT  * FROM  `Staff` 
	// 	WHERE TYPE >= ".$mytype." AND TYPE = ".$type;
	// }else{
	// 	return "SELECT  * FROM  `Staff` 
	// 	WHERE TYPE >= ".$mytype;
	// }
	return "SELECT * FROM  `Src` ";
}

// SimpleProgram
function sqlSuperProgramListForSuperUser($userid) //放送
{
	//1.1 放送用プログラムリスト
	// return "SELECT *
	//  FROM  `Program`
	//  WHERE broadcast = 1 
	//  ORDER BY id DESC";

	$whereString = "WHERE broadcast = 1
	 	AND (
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 0)
	 	OR 
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 1)
	 	)";

	return "SELECT *
	 FROM  `Program`
	 ".$whereString."
	 ORDER BY id DESC";
}
function sqlProgramListForSuperUser($userid) //カセット
{
	//1.1 店舗他登録番組リスト
	// return "SELECT *
	//  FROM  `Program`
	//  WHERE broadcast = 0 
	//  ORDER BY id DESC";

	$whereString = "WHERE broadcast = 1
	 	AND (
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 3)
	 	OR 
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 4)
	 	OR 
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 5)
	 	)";

	return "SELECT *
	 FROM  `Program`
	 ".$whereString."
	 ORDER BY id DESC";
}
function sqlProgramListForBlockUser($userid, $blockid) 
{
	return "SELECT *
	 FROM  `Program`
	 WHERE mst_area_id = '".$blockid."' 
	 ORDER BY id DESC";
}
function sqlProgramListForCampanyUser($userid, $campanyid) 
{
	return "SELECT *
	 FROM  `Program`
	 WHERE campany_id = '".$campanyid."' 
	 ORDER BY id DESC";
}
function sqlProgramListForShopUser($userid, $shopid, $blockid, $campanyid, $shopmenu) 
{
	// return "SELECT `id`, `created`, `title`, `editorid`, `shopid`, `mst_area_id`, `campany_id`
	//  FROM  `Program`
	//  WHERE shopid = '".$shopid."' OR mst_area_id = '".$blockid."' OR campany_id = '".$campanyid."'";



	// return "SELECT * 
	//  FROM  `Program`
	//  WHERE shopid = '".$shopid."' OR mst_area_id = '".$blockid."' OR campany_id = '".$campanyid."' 
	//  ORDER BY id DESC";

	//デフォルトは1.0時のWHERE句
	$whereString = "WHERE shopid = '".$shopid."' 
	 OR (mst_area_id = '".$blockid."' AND (SELECT `type` FROM Staff WHERE id = Program.editorid) = 2)
	 OR (campany_id = '".$campanyid."' AND (SELECT `type` FROM Staff WHERE id = Program.editorid) = 3)";

	 if ($shopmenu == 0) { //公開番組一覧
	 	$whereString = "WHERE broadcast = 1 and playable = 1 
	 	AND (
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 0)
	 	OR 
	 	((SELECT `type` FROM Staff WHERE id = Program.editorid) = 1)
	 	)";
	 }else if ($shopmenu == 1) { //店舗作成番組一覧
	 	$whereString = "WHERE shopid = '".$shopid."'";
	 }else if ($shopmenu == 6) { //ブロック番組一覧
	 	$whereString = "WHERE  
		 (mst_area_id = '".$blockid."' )
		 ";
	 }else if ($shopmenu == 7) { //会社作成番組一覧
	 	$whereString = "WHERE  
	 	(campany_id = '".$campanyid."' )
	 	";
	 }

	return "SELECT * 
	 FROM  `Program`
	 ".$whereString."
	 ORDER BY id DESC";
}

function sqlTodaysLoopWithChannel() 
{
	return "SELECT 
		(SELECT id FROM Channel WHERE loopset = TodaysLoop.id) AS channel,
	    TodaysLoop.* 
		FROM  `TodaysLoop`";
}

function sqlTodaysLoopByID($id) 
{
	return "SELECT * FROM  `TodaysLoop` WHERE id = ".$id;
}

function sqlGetAllTable($tableName, $conditions) 
{
	if (isset($conditions)){
		return "SELECT * FROM  `".$tableName."` WHERE ".$conditions;
	}else{
		return "SELECT * FROM  `".$tableName."`";
	}
	
}


/// <summary>
/// getAll
/// </summary>

function sqlGetAllShop(){
	return "SELECT * FROM  `Shop` ";
}
function sqlGetAllCampanies(){
	return "SELECT * FROM  `Campanies` ";
}
function sqlGetAllArea(){
	return "SELECT * FROM  `Mst_areas` ";
}

//1.1
function sqlGetDevicesLive($shopid){
	// return "SELECT * FROM  `Player` 
	// 		WHERE shopid = '".$shopid."' ') 
	// 		AND CURRENT_DATE() < created";
	return "SELECT * FROM  `Player` 
			WHERE CURRENT_DATE() < updated
			AND shopid = '".$shopid."'";
}

function sqlGetChannelList(){
	return "SELECT * FROM  `Mst_channel` ";
}

function sqlCollectionList($shopid)
{
	return "SELECT * FROM  `Collection` WHERE shopid = ".$shopid; //." ORDER BY id DESC";
}


// Insert
function sqlSetPlayer($shopid, $uuid)
{
	// $sql = "INSERT INTO  `Player` (
	// 	`shopid` ,
	// 	`uuid`
	// 	)
	// 	VALUES (
	// 	'".$shopid."',  '".$uuid."'
	// 	);
	// 	".sqlGetUser($uuid);
	// 	 // var_dump($sql);
	// return $sql;

	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Player (shopid, uuid) VALUES (:shopid, :uuid)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(':shopid' => $shopid, ':uuid' => $uuid);
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertStaff($name, $login, $mail, $password, $shops, $mst_area_id, $campany_id, $type)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Staff (name, login, mail, password, shops, mst_area_id, campany_id, type) VALUES (:name, :login, :mail, :password, :shops, :mst_area_id, :campany_id, :type)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':name' => $name, 
		':login' => $login, 
		':mail' => $mail, 
		':password' => $password, 
		':shops' => $shops, 
		':mst_area_id' => $mst_area_id, 
		':campany_id' => $campany_id, 
		':type' => $type)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertShop($shopid, $name, $campany_id, $mst_area_id)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Shop (shopid, name, campany_id, mst_area_id) VALUES (:shopid, :name, :campany_id, :mst_area_id)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':shopid' => $shopid, 
		':name' => $name, 
		':campany_id' => $campany_id, 
		':mst_area_id' => $mst_area_id)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertCampany($name)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Campanies (name) VALUES (:name)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':name' => $name
		)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}


function sqlInsertSimpleProgram(
	$title, 
	$subtitle, 
	$broadcast, 
	$content_type, 
	$campany_id, 
	$mst_area_id, 
	$shopid, 
	$editor,
	$category,
	$release
)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Program (
		title, 
		subtitle, 
		broadcast, 
		content_type, 
		campany_id, 
		mst_area_id, 
		shopid, 
		editorid, 
		`category`, 
		`release`
	) VALUES (
		:title, 
		:subtitle, 
		:broadcast, 
		:content_type, 
		:campany_id, 
		:mst_area_id, 
		:shopid, 
		:editor,
		:category,
		:release
	)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':title' => $title, 
		':subtitle' => $subtitle, 
		':broadcast' => $broadcast, 
		':content_type' => $content_type, 
		':campany_id' => $campany_id, 
		':mst_area_id' => $mst_area_id, 
		':shopid' => $shopid, 
		':editor' => $editor,
		':category' => $category,
		':release' => $release
	)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertProgramsListItem($parent, $programid, $special, $timeinfo)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO ProgramsListItem (`parentTodaysLoop`,`programid`, `special`, `time`) VALUES (:parentTodaysLoop, :programid, :special, :timeinfo)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':parentTodaysLoop' => $parent, 
		':programid' => $programid, 
		':special' => $special, 
		':timeinfo' => $timeinfo)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertTodaysLoop($title, $subtitle, $memo)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO TodaysLoop (title, subtitle, memo) VALUES (:title, :subtitle, :memo)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':title' => $title,
		':subtitle' => $subtitle,
		':memo' => $memo
		);
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertSrcListItemWithSrc($parent, $caption)
{
	
	global $pdo;

	// SrcListItemに新規作成後、Srcもすぐ追加。
	$sql = "
	INSERT INTO SrcListItem (parentProgram, caption) VALUES (:parent, :caption)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':parent' => $parent,
		':caption' => $caption
		);
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	$sql2 = "INSERT INTO Src (parentSrcListItem) VALUES ((SELECT MAX(id) FROM SrcListItem))";

	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt2 = $pdo->prepare($sql2);
	 
	// 挿入する値を配列に格納する
	$params2 = array();
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag2 = $stmt2->execute($params2);

	$flag3 = ($flag == true && $flag2 == true);
	// 登録完了のメッセージ
	return $flag3;
}

function sqlInsertLocalPlaying($programid, $shopid)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO LocalPlaying (programid, shopid) VALUES (:programid, :shopid)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':programid' => $programid, 
		':shopid' => $shopid)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}


function sqlInsertCalendarLoop($loopset, $dateinfo, $calenderid)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Calender".$calenderid." (`loopset`, `date`) VALUES (:loopset, :dateinfo)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':loopset' => $loopset, 
		':dateinfo' => $dateinfo)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertSpecialProgram($programid, $timeinfo, $dateinfo, $calenderid)
{
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO SpecialProgram".$calenderid." (`programid`, `timeinfo`, `dateinfo`) VALUES (:programid, :timeinfo, :dateinfo)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':programid' => $programid, 
		':timeinfo' => $timeinfo, 
		':dateinfo' => $dateinfo)
	;
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

function sqlInsertCollection($shopid, $programid){
	global $pdo;

	// INSERT文を変数に格納
	$sql = "INSERT INTO Collection (shopid, programid) VALUES (:shopid, :programid)";
	 
	// 挿入する値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 挿入する値を配列に格納する
	$params = array(
		':shopid' => $shopid,
		':programid' => $programid
	);
	 
	// 挿入する値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 登録完了のメッセージ
	return $flag;
}

// Update
function sqlUpdateStaff($id, $name, $login, $mail, $password, $shops, $mst_area_id, $campany_id, $type)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Staff SET 
	name = :name, 
	login = :login, 
	mail = :mail, 
	password = :password, 
	shops = :shops, 
	mst_area_id = :mst_area_id, 
	campany_id = :campany_id, 
	type = :type 
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':name' => $name, 
		':login' => $login, 
		':mail' => $mail, 
		':password' => $password, 
		':shops' => $shops, 
		':mst_area_id' => $mst_area_id, 
		':campany_id' => $campany_id, 
		':type' => $type
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateShop($id, $shopid, $name, $campany_id, $mst_area_id)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Shop SET 
	shopid = :shopid, 
	name = :name, 
	campany_id = :campany_id, 
	mst_area_id = :mst_area_id 
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':shopid' => $shopid, 
		':name' => $name, 
		':campany_id' => $campany_id, 
		':mst_area_id' => $mst_area_id
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateShopCloseTime($shopid, $closetime)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Shop SET 
	closetime = :closetime 
	WHERE shopid = :shopid";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':shopid' => $shopid, 
		':closetime' => $closetime
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateCampany($id, $name)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Campanies SET 
	name = :name  
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':name' => $name
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateTodaysLoop($id, $title, $subtitle, $memo)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE TodaysLoop SET 
	`title` = :title, 
	`subtitle` = :subtitle, 
	`memo` = :memo 
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':title' => $title,
		':subtitle' => $subtitle,
		':memo' => $memo,
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateProgram(
	$id,
	$title,
	$subtitle,
	$layout,
	$bg,
	$content,
	$trans,
	$pagetime,
	$broadcast,
	$playable
	)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Program SET 
	`title` = :title, 
	`subtitle` = :subtitle, 
	`layout_type` = :layout, 
	`bgcolor` = :bg, 
	`content_type` = :content, 
	`transition_type` = :trans, 
	`basic_page_time` = :pagetime, 
	`broadcast` = :broadcast, 
	`playable` = :playable
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':title' => $title,
		':subtitle' => $subtitle,
		':layout' => $layout,
		':bg' => $bg,
		':content' => $content,
		':trans' => $trans,
		':pagetime' => $pagetime,
		':broadcast' => $broadcast,
		':playable' => $playable
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}


function sqlUpdateDefaultChannel($loopset)
{
	global $pdo, $DEFALT_CHANNEL;
	
	// UPDATE文を変数に格納
	$sql = "UPDATE Channel SET 
	loopset = :loopset  
	WHERE id = :default_channel";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':loopset' => $loopset, 
		':default_channel' => $DEFALT_CHANNEL
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateSrc($id, $pathinfo)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Src SET 
	`path` = :pathinfo
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':pathinfo' => $pathinfo
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateSrcListItem($id, $caption, $duration)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE SrcListItem SET 
	`caption` = :caption, 
	`duration` = :duration
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':caption' => $caption, 
		':duration' => $duration
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdatePassword($id, $pw)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Staff SET 
	`password` = :pw,
	`modifieddate` = CURRENT_TIMESTAMP()
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':pw' => $pw
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateLocalPlaying(
	$id
	)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE `LocalPlaying` SET 
	`played` = 1
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

//1.1
function sqlPlayerLaunch(
	$playerid
	)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE `Player` SET 
	`launch` = `launch` + 1
	WHERE id = :playerid";
	 
	// UPDATE article SET pageview = pageview + 1 WHERE id = 234;

	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':playerid' => $playerid
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlUpdateDeviceInfo($id, $name, $memo, $channel)
{
	global $pdo;

	// UPDATE文を変数に格納
	$sql = "UPDATE Player SET 
	name = :name,
	memo = :memo, 
	channel = :channel 
	WHERE id = :id";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id, 
		':name' => $name,
		':memo' => $memo,
		':channel' => $channel
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}


//DELETE
function sqlDelete($id, $table)
{
	global $pdo;
	
	// DELETE文を変数に格納
	$sql = "DELETE FROM {$table} WHERE id = :id";

	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':id' => $id
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}

function sqlDeleteParentFromSrc($parent)
{
	global $pdo;
	
	// UPDATE文を変数に格納
	$sql = "UPDATE Src SET 
	parentSrcListItem = null  
	WHERE parentSrcListItem = :parent";
	 
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	 
	// 値を配列に格納する
	$params = array(
		':parent' => $parent
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}


function sqlDeleteCalendarLoop($dateinfo, $calenderid)
{
	// var_dump($dateinfo);


	// global $pdo;
	
	// // UPDATE文を変数に格納
	// $sql = "DELETE FROM Calender WHERE `date` = :dateinfo";
	// // 値は空のまま、SQL実行の準備をする
	// $stmt = $pdo->prepare($sql);
	 
	// // 値を配列に格納する
	// $params = array(
	// 	':dateinfo' => $dateinfo
	// 	);
	 
	// // 値が入った変数をexecuteにセットしてSQLを実行
	// $flag = $stmt->execute($params);
	 
	// // 完了のメッセージ
	// return $flag;

	global $pdo;
	
	// UPDATE文を変数に格納
	$sql = "DELETE FROM Calender".$calenderid." WHERE `date` = '".$dateinfo."'";
	// 値は空のまま、SQL実行の準備をする
	$stmt = $pdo->prepare($sql);
	
	// 値を配列に格納する
	$params = array(
		);
	 
	// 値が入った変数をexecuteにセットしてSQLを実行
	$flag = $stmt->execute($params);
	 
	// 完了のメッセージ
	return $flag;
}
?>