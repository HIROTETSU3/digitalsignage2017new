<?php

class SimpleClass
{
    // プロパティの宣言
    public $var = 'a default value';

    // メソッドの宣言
    public function displayVar() {
        echo $this->var;
    }
}



class BuildObject
{
	public $id; //int
	public $build; //float
	public $filepath; //string
	public $message; //string
	public $created; //DateTime
}

//ShopSettings

class Src
{
	public $srcid; //int
	public $path; //string
}

class SrcListItem
{
	public $id; //int
	public $src; //Src
	public $duration; //int
	public $caption; //string
}

class SrcListItemWithSrc
{
	public $SrcListItem_id; //int
	public $Src_id; //int
	public $duration; //int
	public $caption; //string
	public $path; //string
}

class Program
{
	public $programid; //int
	public $created; //string

	public $title; //string
	public $subtitle; //string

	public $layout_type; //int
	public $bgcolor; //string
	public $content_type; //int
	public $transition_type; //int
	public $basic_page_time; //int

	public $editorid; //int
	public $shopid; //int
	public $mst_area_id; //int
	public $campany_id; //int
	public $broadcast; //int
	
	public $src_list; //SrcListItem[]

	public $playedid; //int

	//1.1
	public $playable;
}

// class SimpleProgram
// {
// 	public $programid; //int
// 	public $created; //string
// 	public $title; //string
// 	public $subtitle; //string
// 	public $bgcolor; //string
// 	public $editorid; //int
// 	public $basic_page_time; //int
// 	public $content_type; //int
// 	public $layout_type; //int
// 	public $transition_type; //int
// 	public $broadcast; //tinyint
// }

class SimpleTodaysLoop
{
	public $id; //int
	public $title; //string
	public $subtitle;
	public $date; //string
	public $memo;
	public $channel; //int
}

/// <summary>
/// ShopInit
/// </summary>
class ProgramsListItem
{
	public $program; //Program
	public $time; //string
	public $special; //tinyint
	//public $done = false; //bool
}

class TodaysLoop
{
	public $id; //int
	public $specialprograms; //ProgramsListItem[]
	public $programs; //ProgramsListItem[]
	public $created; //string
}

class UserSettings
{
	public $id; //int
	public $shopid; //int
	public $closetime; //string
}
	
class ShopSettings
{
	public $todays_loop; //TodaysLoop
	public $user_setting; //UserSettings
}

/// <summary>
/// ShopPlayerLogin
/// </summary>
class ShopData
{
	public $shopid; //string
	public $name; //string
	public $uuid; //string
	public $campany_id; //int
	public $mst_area_id; //int
}

class ShopPlayerLogin
{
	public $success; //bool
	public $shop_data; //ShopData
}

/// Kanri

class ResponseObject
{
	public $success; //bool
}

/// <summary>
/// getStaff
/// </summary>
class StaffData
{
	public $id; //int(
	public $type; //int
	public $name; //varchar
	public $login; //varchar
	public $mail; //varchar
	public $modifieddate; //datetime
	public $campany_id; //int(
	public $mst_area_id; //int(
	public $shops; //varchar
	public $password; //varchar
}

class UserLogin
{
	public $success; //bool
	public $user; //StaffData
}

class CampanyData
{
	public $id; //int
	public $name; //string
}

class AreaData
{
	public $id; //int
	public $name; //string
}

//Calender
class CalenderItem
{
	public $id; //int
	public $date; //DateTime 
	public $loopset; //SimpleTodaysLoop 
}

class SpecialProgramItem
{
	public $id; //int
	public $dateinfo; //DateTime 
	public $timeinfo; //DateTime 
	public $program; //Program
}

//1.1
class DeviceData
{
	public $id; //int
	public $name; //string
	public $uuid; //string
	public $memo; //string
	public $channel; //int
}

class ChannelData
{
	public $id; //int
	public $name; //string
	public $info; //string
	public $color; //string
}

class CollectionItem extends Program {
	public $collectionid; //int
}

?>