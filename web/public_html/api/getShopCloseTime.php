<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

$exist = false;

$_shop = execSql(sqlGetShop($_GET['shopid']));

$json = new stdClass(); //汎用クラス
$json->success = true;
$json->closetime = $_shop[0]["closetime"];

header("Content-Type: application/json; charset=utf-8");
echo json_encode($json, true); // 配列をJSON形式に変換してくれる
exit();

?>