<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

$exist = false;

$_staff = execSql(sqlGetStaffByUserID($_GET['login']));
if (0 < count($_staff)){
	$exist = true;
}

$json = new stdClass(); //汎用クラス
$json->success = true;
$json->exist = $exist;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($json, true); // 配列をJSON形式に変換してくれる
exit();

?>