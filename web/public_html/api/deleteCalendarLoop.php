<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");


$errflag = false;
$resObj = new ResponseObject();

$dates = explode("_", $_GET['d']);

$calenderid = "";
if (isset($_GET['calenderid'])){
	$calenderid = $_GET['calenderid'];
}

foreach ($dates as $_date) {
	$res = sqlDeleteCalendarLoop(
		$_date, 
		$calenderid
	);

	if ($res == false) {
		$errflag = true;
	}
}


$resObj->success = !$errflag; //いずれかがエラーの場合false
// $resObj->success = true; //いずれかにエラーがあっても、とりあえずtrueにしとく


header("Content-Type: application/json; charset=utf-8");
echo json_encode($resObj, true); // 配列をJSON形式に変換してくれる
exit();

?>
