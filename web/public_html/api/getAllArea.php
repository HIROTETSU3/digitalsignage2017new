<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//GetAllCampanies

	$arealist = array();
	foreach (execSql(sqlGetAllArea()) as $_area) {
		$area = new CampanyData();
		$area->id = (int)$_area["id"];
		$area->name = $_area["name"];
		$arealist[] = $area;
	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($arealist, true); // 配列をJSON形式に変換してくれる
exit();

?>