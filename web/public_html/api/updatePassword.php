<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");


$success = false;
$resObj = new ResponseObject();

$res = sqlUpdatePassword(
	$_GET['userid'],
	$_GET['password']
	);

$resObj->success = $res;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($resObj, true); // 配列をJSON形式に変換してくれる
exit();

?>
