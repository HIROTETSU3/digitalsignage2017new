<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//UserSettings
// $user_setting = new UserSettings();
// $user_setting->id = 123456;
// $user_setting->shopid = 202020;
// $user_setting->closetime = "19:00:00";

// var_dump("test1");

//execSql("SELECT * FROM program WHERE id=1 ORDER BY id ASC");
// echo execSql($pdo, "SELECT * FROM program WHERE id=1 ORDER BY id ASC");
// execSql("SELECT * FROM program ORDER BY id ASC");
//execSql("SELECT * FROM SrcListItem WHERE id=2 ORDER BY id ASC");

// execSql("SELECT id, duration, caption, (SELECT * FROM Src WHERE srcid=2) FROM SrcListItem WHERE id=2");

// execSql("SELECT todaysid FROM TodaysLoop WHERE date = '2017-07-28'");

// $today = "'".$_GET['today']."'";//"'2017-07-28'";
// $shopid = "'".$_GET['shopid']."'";

$programs = array();
$specialprograms = array();
$todaysloop = -1;
$isShopBaseMode = false;

$defaultChannel = 1; //Channel Table ID
if (isset($_GET['shopid'])){
	$shopid = $_GET['shopid'];
	$_shopChannel = execSql(sqlChannelByShopId($shopid));

	//Shop BASE Channel
	if (count($_shopChannel) != 0){
		$defaultChannel = (int)$_shopChannel[0]["id"]; 
		$isShopBaseMode = true;
	}
}

//calendarloopset
$_myChannel = execSql(sqlGetPlayerChannel($_GET['uuid']));
$myChannel = (int)$_myChannel[0]["channel"]; 
//var_dump($myChannel);

$isCollection = false;
if ($myChannel == -1){ //番組コレクションの場合
	$isCollection = true;
	//チャンネルは0に。
	$myChannel = 0;
}

if ($myChannel == 0){
	$myChannelID = "";
}else{
	$myChannelID = $myChannel;
}

if ($isCollection){

	$programlist = array();
	$sqlString = sqlCollectionList($shopid);

	if ($sqlString != ""){
		foreach (execSql($sqlString) as $_collection) {
			$sqlProgString = sqlProgram((int)$_collection["programid"]);
			if ($sqlProgString != ""){
				foreach (execSql($sqlProgString) as $_program) {
				$program = new CollectionItem();
				$program->programid = (int)$_program["id"];
				$program->created = $_program["created"];

				$program->title = $_program["title"];
				$program->subtitle = $_program["subtitle"];

				$program->layout_type = (int)$_program["layout_type"];
				$program->bgcolor = $_program["bgcolor"];
				$ct = (int)$_program["content_type"];
				$program->content_type = $ct;
				$program->transition_type = (int)$_program["transition_type"];
				$program->basic_page_time = (int)$_program["basic_page_time"];

				$program->editorid = (int)$_program["editorid"];
				$program->shopid = $_program["shopid"];
				$program->mst_area_id = (int)$_program["mst_area_id"];
				$program->campany_id = (int)$_program["campany_id"];
				$program->broadcast = (int)$_program["broadcast"];
				
				//20180529
				if ($ct == 1){
					$program->title = "";
					$program->subtitle = "";
				}


				//1.1
				$program->playable = (int)$_program["playable"];

				$program->collectionid = (int)$_collection["id"];

				$_src = execSql(sqlSrcListItem($_program["id"]));
				if (0 < count($_src)){
					$srcpath = $_src[0]["path"];
				}else{
					$srcpath = null;
				}

				$program->firstsrc = $srcpath;

				$program->playedid = 0;
				
				//make SrcListItem
				$srclistitems = array();
				foreach (execSql(sqlSrcListItem($_program["id"])) as $_srclistitem) {
					$src = new Src();
					$srclistitem = new SrcListItem();
					// var_dump($_srclistitem);
					$src->srcid = (int)$_srclistitem["Src_id"];
					$src->path = $_srclistitem["path"];
					$srclistitem->duration = (int)$_srclistitem["duration"];
					$srclistitem->caption = $_srclistitem["caption"];
					$srclistitem->src = $src;

					$srclistitems[] = $srclistitem;
				}

				if (count($srclistitems) == 1){
					$srclistitems[] = $srclistitem;
				}


				$program->src_list = $srclistitems;





				$programslistitem = new ProgramsListItem();
				$programslistitem->time = "00:00:00";
				$programslistitem->special = "0";
				$programslistitem->program = $program;

				$programlist[] = $programslistitem;
				}
			}
		}
	}


	$todays_loop = new TodaysLoop();
	$todays_loop->id = 0;
	$todays_loop->created = "2018-01-01 00:00:00";
	$todays_loop->programs = $programlist;

	$todays_loop->specialprograms = [];

	$json = new stdClass(); //汎用クラス
	$json->todays_loop = $todays_loop;
	// $json->user_setting = $user_setting;


	header("Content-Type: application/json; charset=utf-8");
	echo json_encode($json, true); // 配列をJSON形式に変換してくれる
	exit();


}else{
	$proglistitems = execSql(sqlProgramsListItemByCalendarLoopset($myChannelID));
}

// var_dump($proglistitems);

//BASE
if (count($proglistitems) == 0){
	$proglistitems = execSql(sqlProgramsListItem($defaultChannel)); //Default
}

function getProgram($_program)
{
	// var_dump($_program[0]["title"]);
	$program = new Program();
	$program->programid = (int)$_program[0]["id"];
	$program->created = $_program[0]["created"];
	$program->title = $_program[0]["title"];
	$program->subtitle = $_program[0]["subtitle"];
	$program->bgcolor = $_program[0]["bgcolor"];
	$program->editorid = (int)$_program[0]["editorid"];
	$program->basic_page_time = (int)$_program[0]["basic_page_time"];
	$ct = (int)$_program[0]["content_type"];
	$program->content_type = $ct;
	$program->layout_type = (int)$_program[0]["layout_type"];
	$program->transition_type = (int)$_program[0]["transition_type"];
	$program->playedid = 0;

	//20180529
	if ($ct == 1){
		$program->title = "";
		$program->subtitle = "";
	}

	//make SrcListItem
	$srclistitems = array();
	foreach (execSql(sqlSrcListItem($_program[0]["id"])) as $_srclistitem) {
		$src = new Src();
		$srclistitem = new SrcListItem();
		// var_dump($_srclistitem);
		$src->srcid = (int)$_srclistitem["Src_id"];
		$src->path = $_srclistitem["path"];
		$srclistitem->duration = (int)$_srclistitem["duration"];
		$srclistitem->caption = $_srclistitem["caption"];
		$srclistitem->src = $src;

		$srclistitems[] = $srclistitem;
	}

	if (count($srclistitems) == 1){
		$srclistitems[] = $srclistitem;
	}


	$program->src_list = $srclistitems;

	return $program;
}


//sqlProgramsListItemから通常ループ生成
foreach ($proglistitems as $_proglistitem) {

	//make Program
	//$_program = execSql(sqlProgram($_proglistitem["programid"]));

	if (isset($_GET['preview_program'])){ //プログラムのプレビューモード
		$_program = execSql(sqlProgram($_GET['preview_program']));
	}else{
		$_program = execSql(sqlProgram($_proglistitem["programid"]));
	}
	
	$getProgram = getProgram($_program);

	//make ProgramsListItem
	$programslistitem = new ProgramsListItem();
	$programslistitem->time = $_proglistitem["time"];
	$programslistitem->special = $_proglistitem["special"];
	$programslistitem->program = $getProgram;

	if($_proglistitem["special"] == "1"){
		$specialprograms[] = $programslistitem;
	}else{
		$programs[] = $programslistitem;
	}
    
    $todaysloop = $_proglistitem["parentTodaysLoop"];
}

$_todaysloop = execSql(sqlGetTodaysLoopByID($todaysloop));

$todays_loop = new TodaysLoop();
$todays_loop->id = (int)$_todaysloop[0]["id"];
$todays_loop->created = $_todaysloop[0]["created"];
$todays_loop->programs = $programs;


//Special Program上書き
$sps = execSql(sqlSpecialProgram());

if (isset($_GET['preview_program']) || count($sps) < 1 || $isShopBaseMode){ //プログラムのプレビューモードか特番なしの場合
	$todays_loop->specialprograms = array(); //specialprogramsは[]
}else{
	$calenderSps = array();
	foreach ($sps as $_sps) {
		$programSp = execSql(sqlProgram($_sps["programid"]));
		$getProgramSp = getProgram($programSp);

		$splistitem = new ProgramsListItem();
		$splistitem->time = $_sps["timeinfo"];
		$splistitem->special = 1;
		$splistitem->program = $getProgramSp;

		$calenderSps[] = $splistitem;
	}

	$todays_loop->specialprograms = $calenderSps;
}


$json = new stdClass(); //汎用クラス
$json->todays_loop = $todays_loop;
// $json->user_setting = $user_setting;


header("Content-Type: application/json; charset=utf-8");
echo json_encode($json, true); // 配列をJSON形式に変換してくれる
exit();

/* test

$src = new Src();

$srclistitem = new SrcListItem();
$srclistitem->src = $src;

$program = new Program();
$program->src_list = array($srclistitem);
$program->programid = $tid;

$programslistitem = new ProgramsListItem();
$programslistitem->program = $program;

$programs = array($programslistitem);
$specialprograms = array($programslistitem);


$todays_loop = new TodaysLoop();
$todays_loop->id = "99292";
$todays_loop->date = "07/08/2017";
$todays_loop->programs = $programs;
$todays_loop->specialprograms = $specialprograms;

$user_setting = new UserSettings();
$user_setting->id = 123456;
$user_setting->shopid = 202020;
$user_setting->closetime = "19:00:00";

$json = new stdClass(); //汎用クラス
$json->todays_loop = $todays_loop;
$json->user_setting = $user_setting;

*/

/*
$json = array(
    array('name'=>'Google', 'url'=>'https://www.google.co.jp/'),
    array('name'=>'Yahoo!', 'url'=>'http://www.yahoo.co.jp/'),
);
 */


?>