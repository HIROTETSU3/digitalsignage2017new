<?php
// 4.1.0より前のPHPでは$FILESの代わりに$HTTP_POST_FILESを使用する必要
// があります。
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

$uploaddir = $uploadAssetsDir; //'/home/dreamh/hmjsignage.work/public_html/assets/'; //'/var/www/uploads/';
// $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
$uploadfile = $uploaddir . $_GET['filename'];

/*
echo '<pre>';
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "File is valid, and was successfully uploaded.\n";
} else {
    echo "Possible file upload attack!\n";
}

echo 'Here is some more debugging info:';
print_r($_FILES);

print "</pre>";

phpinfo();
*/


$success = false;
$resObj = new ResponseObject();

if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    $success = true;
}

$resObj->success = $success;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($resObj, true); // 配列をJSON形式に変換してくれる
?>