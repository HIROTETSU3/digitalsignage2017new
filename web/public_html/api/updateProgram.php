<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");


$success = false;
$resObj = new ResponseObject();

$res = sqlUpdateProgram(
	$_GET['id'],
	$_GET['title'],
	$_GET['subtitle'],
	$_GET['layout'],
	$_GET['bg'],
	$_GET['content'],
	$_GET['trans'],
	$_GET['pagetime'],
	$_GET['broadcast'],
	$_GET['playable']
	);

$resObj->success = $res;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($resObj, true); // 配列をJSON形式に変換してくれる
exit();

?>
