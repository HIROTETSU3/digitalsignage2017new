<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//GetAllShop

	$shoplist = array();
	foreach (execSql(sqlGetAllShop()) as $_shop) {
		$shop = new ShopData();
		$shop->id = (int)$_shop["id"];
		$shop->shopid = $_shop["shopid"];
		$shop->name = $_shop["name"];
		$shop->campany_id = (int)$_shop["campany_id"];
		$shop->mst_area_id = (int)$_shop["mst_area_id"];
		$shoplist[] = $shop;
	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($shoplist, true); // 配列をJSON形式に変換してくれる
exit();

?>