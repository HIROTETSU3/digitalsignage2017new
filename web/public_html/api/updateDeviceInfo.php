<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");


$success = false;
$resObj = new ResponseObject();

$res = sqlUpdateDeviceInfo(
	$_GET['id'],
	$_GET['name'],
	$_GET['memo'],
	$_GET['channel']
	);

$resObj->success = $res;

header("Content-Type: application/json; charset=utf-8");
echo json_encode($resObj, true); // 配列をJSON形式に変換してくれる
exit();

?>
