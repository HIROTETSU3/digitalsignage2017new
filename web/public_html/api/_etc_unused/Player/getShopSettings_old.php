{
  "todays_loop": {
    "id": 99292,
    "specialprograms": [
      {
        "program": {
          "programid": 92938392,
          "created": "2017-07-29 21:23:10",
          "title": "X-Serverのapiより",
          "subtitle": "スペシャル０のサブタイトル",
          "bgcolor": "#730103",
          "editorid": 2039303,
          "basic_page_time": 3,
          "transition_type": 0,
          "content_type": 1,
          "layout_type": 0,
          "src_list": [
            {
              "src": {
                "srcid": 8283938,
                "path": "lupin_1.mp4"
              },
              "duration": 0
            }
          ]
        },
        "time": "02:07:00"
      },
      {
        "program": {
          "programid": 92938393,
          "created": "2017-07-29 21:23:10",
          "title": "2鈴鹿8時間耐久観戦レポート",
          "subtitle": "2今年も暑かった",
          "bgcolor": "#666666",
          "editorid": 2039303,
          "basic_page_time": 3,
          "transition_type": 0,
          "content_type": 1,
          "layout_type": 0,
          "src_list": [
            {
              "src": {
                "srcid": 8283938,
                "path": "lupin_2.mp4"
              },
              "duration": 0
            }
          ]
        },
        "time": "02:10:00"
      }
    ],
    "programs": [
      {
        "program": {
          "programid": 92938393,
          "created": "2017-07-29 21:23:10",
          "title": "2鈴鹿8時間耐久観戦レポート",
          "subtitle": "2今年も暑かった",
          "bgcolor": "#484A03",
          "editorid": 2039303,
          "basic_page_time": 3,
          "transition_type": 0,
          "content_type": 1,
          "layout_type": 0,
          "src_list": [
            {
              "src": {
                "srcid": 8283938,
                "path": "8283938_4.mp4"
              },
              "duration": 0
            }
          ]
        }
      },
      {
        "program": {
          "programid": 92938392,
          "created": "2017-07-29 21:23:10",
          "title": "新型GOLDWING2018 発表会 スライドショー",
          "subtitle": "コンパクトにフルモデルチェンジ！",
          "bgcolor": "#440000",
          "editorid": 2039303,
          "basic_page_time": 4,
          "content_type": 2,
          "layout_type": 1,
          "src_list": [
            {
              "src": {
                "srcid": 98393849,
                "path": "98393849.jpg"
              },
              "duration": 3,
              "caption": "今年も始まりました"
            },
            {
              "src": {
                "srcid": 34564212,
                "path": "34564212.jpg"
              },
              "duration": 3,
              "caption": "HRCピット風景"
            },
            {
              "src": {
                "srcid": 34564323,
                "path": "34564323.jpg"
              },
              "duration": 3,
              "caption": "Honda 緑陽会 熊本レーシング\nwith くまモン"
            },
            {
              "src": {
                "srcid": 34554323,
                "path": "34554323.jpg"
              },
              "duration": 0,
              "caption": "予選の様子"
            }
          ]
        }
      },
      {
        "program": {
          "programid": 92938392,
          "created": "2017-07-29 21:23:10",
          "title": "2新型GOLDWING2018 発表会 スライドショー",
          "subtitle": "2コンパクトにフルモデルチェンジ！",
          "bgcolor": "#113366",
          "editorid": 2039304,
          "basic_page_time": 4,
          "content_type": 2,
          "layout_type": 1,
          "src_list": [
            {
              "src": {
                "srcid": 10000199,
                "path": "10000199.jpg"
              },
              "duration": 3,
              "caption": "2今年も始まりました"
            },
            {
              "src": {
                "srcid": 10000200,
                "path": "10000200.jpg"
              },
              "duration": 3,
              "caption": "2HRCピット風景"
            },
            {
              "src": {
                "srcid": 10000201,
                "path": "10000201.jpg"
              },
              "duration": 3,
              "caption": "2Honda 緑陽会 熊本レーシング\nwith くまモン"
            },
            {
              "src": {
                "srcid": 10000202,
                "path": "10000202.jpg"
              },
              "duration": 0,
              "caption": "2予選の様子"
            }
          ]
        }
      }
    ],
    "date": "07/08/2017"
  },
  "user_setting": {
    "id": 120345,
    "shopid": 202020,
    "closetime": "19:00:00"
  }
}