<?php
$json = array(
    array('name'=>'Google', 'url'=>'https://www.google.co.jp/'),
    array('name'=>'Yahoo!', 'url'=>'http://www.yahoo.co.jp/'),
);
 
header("Content-Type: application/json; charset=utf-8");
echo json_encode($json); // 配列をJSON形式に変換してくれる
exit();
?>