<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//Local Playing

	$proglist = array();

	$localplayings = execSql(sqlLocalPlaying($_GET['shopid'], $_GET['playedid']));

	if (0 < count($localplayings)){

		$progid = (int)$localplayings[0]["programid"];
		$playedid = (int)$localplayings[0]["id"];


		if ($progid == -1234){
			$program = new Program();
			$program->programid = $progid;
			$program->playedid = $playedid;
			
			$program->title = "";
			$program->subtitle = "";
			$program->layout_type = 0;
			$program->bgcolor = "";
			$program->content_type = 0;
			$program->transition_type = 0;
			$program->basic_page_time = 0;
			$program->editorid = 0;
			$program->shopid = 0;
			$program->mst_area_id = 0;
			$program->campany_id = 0;
			

			$proglist[] = $program;
		}else{
			foreach (execSql(sqlProgram($progid)) as $_program) {
				$program = new Program();
				$program->programid = (int)$_program["id"];
				$program->created = $_program["created"];
				$program->title = $_program["title"];
				$program->subtitle = $_program["subtitle"];
				$program->bgcolor = $_program["bgcolor"];
				$program->editorid = (int)$_program["editorid"];
				$program->basic_page_time = (int)$_program["basic_page_time"];
				$program->content_type = (int)$_program["content_type"];
				$program->layout_type = (int)$_program["layout_type"];
				$program->transition_type = (int)$_program["transition_type"];

				$program->playedid = $playedid;

				//make SrcListItem
				$srclistitems = array();
				foreach (execSql(sqlSrcListItem($_program["id"])) as $_srclistitem) {
					$src = new Src();
					$srclistitem = new SrcListItem();
					// var_dump($_srclistitem);
					$src->srcid = (int)$_srclistitem["Src_id"];
					$src->path = $_srclistitem["path"];
					$srclistitem->duration = (int)$_srclistitem["duration"];
					$srclistitem->caption = $_srclistitem["caption"];
					$srclistitem->src = $src;

					$srclistitems[] = $srclistitem;
				}
				$program->src_list = $srclistitems;


				$proglist[] = $program;
			}
		}

		// $res = sqlUpdateLocalPlaying(
		// 	(int)$localplayings[0]["id"]
		// );

		//削除式はやめ
		// $res = sqlDelete(
		// 	(int)$localplayings[0]["id"],
		// 	"LocalPlaying"
		// );

	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($proglist, true); // 配列をJSON形式に変換してくれる
exit();

?>