<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//GetAllCampanies

	$channellist = array();
	foreach (execSql(sqlGetChannelList()) as $_channel) {
		$channel = new ChannelData();
		$channel->id = (int)$_channel["id"];
		$channel->name = $_channel["name"];
		$channel->info = $_channel["info"];
		$channel->color = $_channel["color"];
		$channellist[] = $channel;
	}

header("Content-Type: application/json; charset=utf-8");
echo json_encode($channellist, true); // 配列をJSON形式に変換してくれる
exit();

?>