<?php 
require_once("includes/config.php");
require_once("includes/classbox.php");
require_once("includes/database.php");

//Src


//make stafflist
	$programlist = array();
	// foreach (execSql(sqlStaffList($_GET['mytype'], $_GET['type'])) as $_src) {
	$type = $_GET['type'];
	$userid = $_GET['userid'];
	$shopid = $_GET['shopid'];
	$blockid = $_GET['mst_area_id'];
	$campanyid = $_GET['campany_id'];
	$super = $_GET['super'];

	//1.1
	$shopmenu = -1;
	if (isset($_GET['shopmenu'])){
		$shopmenu = $_GET['shopmenu'];
	}

	$sqlString = "";

	if ($type == '0' || $type == '1'){ //kanri user
		if ($super == '1'){
			$sqlString = sqlSuperProgramListForSuperUser($userid); //放送（super）
		}else{
			$sqlString = sqlProgramListForSuperUser($userid); //カセット
		}
	}elseif ($type == '2') { //shop user
		$sqlString = sqlProgramListForBlockUser($userid, $blockid);
	}elseif ($type == '3') { //shop user
		$sqlString = sqlProgramListForCampanyUser($userid, $campanyid);
	}elseif ($type == '4') { //shop user
		$sqlString = sqlProgramListForShopUser($userid, $shopid, $blockid, $campanyid, $shopmenu);
	}


	if ($sqlString != ""){
		foreach (execSql($sqlString) as $_program) {
		$program = new Program();
		$program->programid = (int)$_program["id"];
		$program->created = $_program["created"];

		$program->title = $_program["title"];
		$program->subtitle = $_program["subtitle"];

		$program->layout_type = (int)$_program["layout_type"];
		$program->bgcolor = $_program["bgcolor"];
		$program->content_type = (int)$_program["content_type"];
		$program->transition_type = (int)$_program["transition_type"];
		$program->basic_page_time = (int)$_program["basic_page_time"];

		$program->editorid = (int)$_program["editorid"];
		$program->shopid = $_program["shopid"];
		$program->mst_area_id = (int)$_program["mst_area_id"];
		$program->campany_id = (int)$_program["campany_id"];
		$program->broadcast = (int)$_program["broadcast"];
		
		//1.1
		$program->playable = (int)$_program["playable"];

		$_src = execSql(sqlSrcListItem($_program["id"]));
		if (0 < count($_src)){
			$srcpath = $_src[0]["path"];
		}else{
			$srcpath = null;
		}

		$program->firstsrc = $srcpath;

		$program->playedid = 0;
		
		$programlist[] = $program;
		}
	}




header("Content-Type: application/json; charset=utf-8");
echo json_encode($programlist, true); // 配列をJSON形式に変換してくれる
exit();




?>