﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Database : SingletonMonoBehaviour<Database> {

	public delegate void onComplete( int i, List<StationData> list, bool isMoving );
	public delegate void onCompleteStamp( string ret );


	public void Awake()
	{
		if(this != Instance)
		{
			Destroy(this);
			return;
		}

		DontDestroyOnLoad(this.gameObject);
	}

	void OnDestroy ()
	{
		Destroy(this);
	}

	SqliteDatabase sqlDB;
	public void init(){
		string pathDB = System.IO.Path.Combine (Application.persistentDataPath, "sqlite.db");
		//保存済みのバージョンと違っていたら一度sqlitedbを削除
		string savedVersion = PlayerPrefsX.GetString("SavedAppVersion");
		//Debug.Log("SavedAppVersion:"+PlayerPrefsX.GetString("SavedAppVersion"));
		if(MyUtil.Instance.GetAppVersion() != PlayerPrefsX.GetString("SavedAppVersion")){
			if(PlayerPrefsX.GetString("SavedAppVersion") == ""){
				System.IO.File.Delete( pathDB );
				Debug.Log("Delete sqlite.db");
			}
		}
		sqlDB = new SqliteDatabase("sqlite.db");
			try{
				//Ver2.0用のテーブルやカラムを追加する　エディターではGetAppVersionで比較できないので動かさない
				//ralliesテーブルを追加　
				string query = "SELECT name FROM sqlite_master WHERE type='table' AND name='rallies'";
				//ralliesテーブルがないということはバージョンが古いのでイカを動かす
				DataTable dataTable = sqlDB.ExecuteQuery(query);
				if( dataTable.Rows.Count == 0 ){

					query = "CREATE TABLE IF NOT EXISTS " +
						"v2table( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ) "+
					sqlDB.ExecuteQuery(query);

					query = "CREATE TABLE IF NOT EXISTS " +
						"rallies( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , "+
						"name TEXT NOT NULL, "+
						"image_url TEXT NOT NULL, "+
						"entry_start TEXT NOT NULL, "+
						"entry_end TEXT NOT NULL, "+
						"play_start TEXT NOT NULL, "+
						"play_end TEXT NOT NULL,"+
						"season_id INT ,"+
						"color TEXT ,"+
						"modified TEXT NOT NULL,"+
						"mst_category_id INT," +
						"block_id INT NOT NULL DEFAULT 0 ) ";
						
					sqlDB.ExecuteQuery(query);
					//stations変更
					query = "ALTER TABLE stations ADD COLUMN point INT";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stations ADD COLUMN beacon_uuid TEXT DEFAULT '' ";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stations ADD COLUMN beacon_major INT";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stations ADD COLUMN beacon_minor INT";
					sqlDB.ExecuteQuery(query);
					//stamps変更
					query = "ALTER TABLE stamps ADD COLUMN send_status INT DEFAULT 1 ";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stamps ADD COLUMN status INT DEFAULT 0 ";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stamps ADD COLUMN point INT DEFAULT 0 ";
					sqlDB.ExecuteQuery(query);
					query = "ALTER TABLE stamps ADD COLUMN season_id INT DEFAULT 0 ";
					sqlDB.ExecuteQuery(query);
				}
			}catch( SqliteException e ){
				Debug.Log(e.Message);
			}

	
		Debug.Log("app sqlitepath:"+pathDB);
		PlayerPrefsX.SetString("SavedAppVersion", MyUtil.Instance.GetAppVersion() );
	}

	//Area
	public DataTable getAreaList(){
		if(sqlDB == null){ init(); }
		string selectQuery = "SELECT code, name FROM areas " + 
		"WHERE areas.id " +
		"ORDER BY code";
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return dataTable;

	}
	//Prefecture
	public DataTable getPrefectureList( int area_id = 0 ){
		if(sqlDB == null){ init(); }
		string addOption = " ";
		if( area_id > 0 ){
			addOption += "AND area_id = "+area_id.ToString(); 
		}

		string selectQuery = "SELECT code, name FROM prefectures " + 
			"WHERE prefectures.id " +
			addOption +
			"ORDER BY code";
		
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return dataTable;

	}

	//Rally
	public List<rallyClass> getRallyList( bool stampedOnly = true ){
		List<rallyClass> retList = new List<rallyClass>();
		string addOption = "";
		if( stampedOnly ){
			addOption = "AND stamps.station_id = stations.id AND stations.mst_type_id = rallies.mst_category_id ";
			//addOption = "AND stamps.station_id = stations.id AND stations.mst_type_id = rallies.id ";
		}
		string selectQuery = "SELECT rallies.id as id , rallies.name as name, rallies.image_url as image_url," +
			" rallies.entry_start as entry_start, rallies.entry_end as entry_end, rallies.play_start as play_start, rallies.play_end as play_end, rallies.mst_category_id as mst_category_id" +
			" FROM rallies, stamps, stations "+
			"WHERE rallies.mst_category_id > 2 "+
			addOption+
			"GROUP BY rallies.id "+
			"ORDER BY play_end";
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		Debug.Log(selectQuery);
		foreach( DataRow row in dataTable.Rows ){
			rallyClass rally = new rallyClass();
			rally.id = (int)row["id"];
			rally.name = (string)row["name"];
			rally.start =  DateTime.Parse( row["play_start"].ToString() );
			rally.end = DateTime.Parse( row["play_end"].ToString() );
			rally.url = (string)row["image_url"];
			retList.Add( rally );
		}
		Debug.Log("RallyCount:"+retList.Count);
		return retList;
	}
	//Station
	public DataTable nearStation( double latitude, double longitude, int limit = 0, int prefecture_id = 0, bool ignore_arrived = false, float distance_limit = 0.0f ){

		if (PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE") != 0f){
			//位置をシミュレーション
			MyAPI.Instance.Trace("位置をシミュレート nearStation");
			latitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE");
			longitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LONGITUDE");
		}


		/*
		 *  id:station_id
		 * name:名称
		 * kana:かな
		 * address:住所
		 * url:ウェブページURL
		 * longitude:経度
		 * latitude:緯度
		 * distance:距離
		 * mst_prefecture_id:県番号
		 * prefecture_name:県名
		 * arrived: 0, 1 未獲得、獲得済み
		 */
		if(sqlDB == null){ init(); }

		string addOption = " ";
		if(prefecture_id != 0){
			addOption += "AND stations.mst_prefecture_id = " + prefecture_id + " ";
		}
		if(ignore_arrived){
			//期間
			addOption += "AND NOT EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id AND stamps.rally_id =  "+PlayerPrefsX.GetInt("rally_id")+" ) "; 
		}
		if(distance_limit > 0.0f){
			addOption += "AND ABS(`longitude` - " + longitude + ") + ABS(`latitude` - " + latitude + ")  <  " + distance_limit + " ";
		}
		if(MyAPI.Instance.getDemo()){
			addOption += "AND( stations.mst_type_id = -1 OR stations.mst_type_id = 1 ) ";
		}else{
			addOption += "AND stations.mst_type_id = 1 ";
		}
		string limitOption = " ";
		if(limit > 0){
			limitOption = "LIMIT "+limit;
		}
		string selectQuery = "SELECT stations.id as id, stations.name as name, stations.tel as tel, " +
			"stations.kana as kana, stations.address as address, stations.url as url, stations.mst_type_id as mst_type_id, " +
			"stations.beacon_uuid as beacon_uuid, stations.beacon_major as beacon_major, stations.beacon_minor as beacon_minor, stations.point as point, " +
			"stations.longitude as longitude, stations.latitude as latitude, stations.mst_prefecture_id as mst_prefecture_id, " +
			"prefectures.name as prefecture_name, " +
			"ABS(stations.longitude - " + longitude + ") + ABS(stations.latitude - " + latitude + ") AS distance, " +
			"NULL as datetime, " +
			"EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id  AND stamps.point > 0 AND stamps.season_id = " + PlayerPrefsX.GetInt("season_id") +  ")  AS arrived_thisSeason, " +
			"EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id  )  AS arrived " +
			"FROM stations, prefectures " +
			"WHERE stations.id  " +
			"AND stations.mst_prefecture_id = prefectures.id " +
			"AND stations.code > 0 "+
			addOption +
			"ORDER BY ABS(`longitude` - " + longitude +") + ABS(`latitude` - " + latitude + ") ASC " +
			limitOption;

		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		//Debug.Log("nearStation Query:"+selectQuery);
		//Debug.Log("nearStation result:"+dataTable.Rows.Count);

		return dataTable;
	}

	public DataTable listsStation( double latitude, double longitude ){
		string addOption = " ";
		if(MyAPI.Instance.getDemo()){
			addOption += "AND( stations.mst_type_id = -1 OR stations.mst_type_id = 1 ) ";
		}else{
			addOption += "AND stations.mst_type_id = 1 ";
		}
		string selectQuery = "SELECT stations.id as id, stations.name as name, " +
			"stations.kana as kana, stations.address as address, stations.url as url, " +
			"stations.longitude as longitude, stations.latitude as latitude, stations.mst_prefecture_id as mst_prefecture_id, " +
			"prefectures.name as prefecture_name, " +
			"ABS(`longitude` - " + longitude + ") + ABS(`latitude` - " + latitude + ") AS distance " +
			"FROM stations, prefectures " +
			"WHERE stations.id  " +
			addOption +
			"ORDER BY ABS(`longitude` - " + longitude +") + ABS(`latitude` - " + latitude + ") ASC " +
			"LIMIT 100";
		if(sqlDB == null){ init(); }
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return dataTable;
	}

	public bool todayVisitStation( int station_id ){
		string selectQuery = "SELECT id "+
			"FROM stamps "+
			"WHERE date(modified) = date('now', 'localtime') "+
			"AND station_id="+station_id;
		if(sqlDB == null){ init(); }
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return ( dataTable.Rows.Count > 0 );
	}

	public DataTable listLogs( int station_id, string startdate = "", string enddate = "" ){
		string addOption = " ";

		if(startdate != ""){
			addOption += "AND date(created) >= "+startdate+" ";
		}
		if(enddate != ""){
			addOption += "AND date(created) <= "+enddate+" ";
		}
		string selectQuery = "SELECT stamps.id, stamps.created as datetime, " +
			"(CASE " +
			"WHEN rallies.id = 1 THEN rallies.color " +
			"WHEN stamps.point > 0 THEN rallies.color " +
			"ELSE 'A0A0A0' END) AS color  "+
			"FROM stamps, rallies " +
			"WHERE stamps.id " +
			"AND stamps.station_id ="+station_id+ " AND stamps.rally_id=rallies.id "+
			addOption+
			"ORDER BY stamps.created desc ";
		
		Debug.Log(selectQuery);
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		Debug.Log("finded count:"+dataTable.Rows.Count.ToString());
		return dataTable;
	
	}

	public int rallyVisitStation( int rally_id, int station_id = 0 ){

		string addOption = "";
		if( station_id > 0){
			addOption += "AND station_id="+station_id+" ";
		}
		string selectQuery = "SELECT id "+
			"FROM stamps "+
			"WHERE stamps.rally_id = " + rally_id + " " +
			addOption +
			"GROUP BY date(stamps.modified)";
		if(sqlDB == null){ init(); }
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return dataTable.Rows.Count;
	}

	public int GetTotalStampNum( int prefecture_id ){
		DataTable dt = getMyStampPrefecture( prefecture_id );
		return dt.Rows.Count;
	}


	//指定した都道府県のスタンプリストを返す
	public DataTable getMyStampPrefecture( int prefecture_id = 0, string start="", string end="" ){

		if(sqlDB == null){ init(); }
		string addOption = " ";
		if(MyAPI.Instance.getDemo()){
			addOption += "AND( stations.mst_type_id = -1 OR stations.mst_type_id = 1 ) ";
		}else{
			addOption += "AND stations.mst_type_id = 1 ";
		}
		if( prefecture_id > 0 ){
			addOption += "AND stations.mst_prefecture_id = "+prefecture_id+" ";
		}
		if(start != ""){
			addOption += "AND date(stamps.modified) >= date('" + start + "') ";
		}
		if(end != ""){
			addOption += "AND date(stamps.modified) <= date('" + end + "') ";
		}
		string limitOption = " ";

		string selectQuery = "SELECT stamps.modified as datetime, " +
			"stations.id as id, stations.name, stations.kana, stations.address, stations.url, stations.mst_prefecture_id, " +
			"stamps.longitude as longitude, stamps.latitude as latitude, stamps.modified as modified, 0 as distance, stations.mst_prefecture_id, " +
			"(CASE " +
			"WHEN rallies.id = 1 THEN rallies.color " +
			"WHEN stamps.point > 0 THEN rallies.color " +
			"ELSE 'A0A0A0' END) AS color , "+
			//"rallies.color as color,  " +
			"prefectures.name as prefecture_name, 1 as arrived " +
			"FROM stamps, stations, prefectures, rallies " +
			"WHERE stamps.station_id = stations.id AND stations.mst_prefecture_id = prefectures.id AND stamps.rally_id = rallies.id " +
			addOption +
			"GROUP BY stamps.station_id "+
			"ORDER BY stamps.modified DESC ";

		//Debug.Log(selectQuery);
		DataTable dt = sqlDB.ExecuteQuery(selectQuery);

		return dt;
	}
	//現在登録されている道の駅の数を返す
	public int GetTotalRoadStations( int prefecture_id = 0 ){
		string addOption = "";
		if( prefecture_id > 0){
			addOption += "AND stations.mst_prefecture_id = " + prefecture_id + " ";
		}

		string selectQuery = "SELECT stations.id " +
			"FROM stations, prefectures " +
			"WHERE stations.mst_prefecture_id = prefectures.id "+
			"AND stations.mst_type_id=1 "+
			addOption;

		DataTable dt = sqlDB.ExecuteQuery(selectQuery);
		return dt.Rows.Count;
	}

	//指定したラリーのスタンプリストを返す
	public DataTable getMyStamp( int rally_id = 0, string start="", string end="" ){
		/*
		 *  id:station_id
		 * name:名称
		 * kana:かな
		 * address:住所
		 * url:ウェブページURL
		 * longitude:経度
		 * latitude:緯度
		 * mst_prefecture_id:県番号
		 * prefecture_name:県名
		 */
		if(sqlDB == null){ init(); }
		string addOption = " ";
		if(MyAPI.Instance.getDemo()){
			addOption += "AND( stations.mst_type_id = -1 OR stations.mst_type_id = 1 ) ";
		}else{
			addOption += "AND stations.mst_type_id = 1 ";
		}
		if( rally_id != 0 ){
			addOption += "AND stamps.rally_id = "+rally_id+" " ;
		}
		if(start != ""){
			addOption += "AND date(stamps.modified) >= date('" + start + "') ";
		}
		if(end != ""){
			addOption += "AND date(stamps.modified) <= date('" + end + "') ";
		}
		string limitOption = " ";

		string selectQuery = "SELECT stamps.modified as datetime, " +
			"stations.id as id, stations.name, stations.kana, stations.address, stations.url, stations.mst_prefecture_id, " +
			"stamps.longitude as longitude, stamps.latitude as latitude, stamps.modified as modified, 0 as distance, stations.mst_prefecture_id, " +
			"(CASE " +
			"WHEN rallies.id = 1 THEN rallies.color " +
			"WHEN stamps.point > 0 THEN rallies.color " +
			"ELSE 'A0A0A0' END) AS color , "+
			//"rallies.color as color,  " +
			"prefectures.name as prefecture_name, 1 as arrived " +
			"FROM stamps, stations, prefectures, rallies " +
			"WHERE stamps.station_id = stations.id AND stations.mst_prefecture_id = prefectures.id AND stamps.rally_id = rallies.id " +
			addOption +
			"GROUP BY stamps.station_id "+
			"ORDER BY stamps.point DESC, stamps.modified DESC ";

		Debug.Log(selectQuery);
		DataTable dt = sqlDB.ExecuteQuery(selectQuery);

		return dt;

	}

	//現在のdbに登録されているポイントを返す
	public int getMyPoint( int season_id = 0 ){
		string addOption = "";
		if( season_id > 0 ){
			addOption += "AND stamps.season_id = "+season_id+" ";
		}
		string selectQuery = "SELECT sum(stamps.point) as point "+
			"FROM stamps "+
			//"WHERE stamps.station_id = stations.id AND stations.mst_type_id = rallies.mst_category_id "+
			"WHERE stamps.id "+
			addOption +
			";";

		Debug.Log(selectQuery);
		DataTable dt = sqlDB.ExecuteQuery(selectQuery);
		if( dt.Rows[0]["point"] == null ){
			return 0;
		}else{
			return (int)dt.Rows[0]["point"];
		}

	}

	//指定したラリーのカフェリストを返す
	public DataTable getMyCafe( int rally_id, string start="", string end="" ){
		/*
		 *  id:station_id
		 * name:名称
		 * kana:かな
		 * address:住所
		 * url:ウェブページURL
		 * longitude:経度
		 * latitude:緯度
		 * mst_prefecture_id:県番号
		 * prefecture_name:県名
		 */
		if(sqlDB == null){ init(); }
		string addOption = " ";
		addOption += "AND stations.mst_type_id = 2 ";

		if(start != ""){
			addOption += "AND date(stamps.modified) >=" + start + " ";
		}
		if(end != ""){
			addOption += "AND date(stamps.modified) <=" + end + " ";
		}
		string limitOption = " ";

		string selectQuery = "SELECT stamps.modified as datetime, " +
			"stations.id, stations.name, stations.kana, stations.address, stations.url, stations.mst_prefecture_id, " +
			"stations.longitude, stations.latitude, 0 as distance, stations.mst_prefecture_id, " +
			"prefectures.name as prefecture_name, 1 as arrived " +
			"FROM stamps, stations, prefectures " +
			"WHERE stamps.rally_id = "+rally_id+" " +
			addOption+
			"AND stamps.station_id = stations.id AND stations.mst_prefecture_id = prefectures.id " +
			"GROUP BY stamps.station_id "+
			"ORDER BY stamps.modified DESC ";
		DataTable dt = sqlDB.ExecuteQuery(selectQuery);

		return dt;
	}

	//Cafe
	public DataTable nearEvent( double latitude, double longitude, int limit = 0, int prefecture_id = 0, bool ignore_arrived = false, float distance_limit = 0.0f, int type_id = 0 ){


		if (PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE") != 0f){
			//位置をシミュレーション
			MyAPI.Instance.Trace("位置をシミュレート nearEvent");
			latitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE");
			longitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LONGITUDE");
		}


		/*
		 *  id:station_id
		 * name:名称
		 * kana:かな
		 * address:住所
		 * url:ウェブページURL
		 * longitude:経度
		 * latitude:緯度
		 * distance:距離
		 * mst_prefecture_id:県番号
		 */
		if(sqlDB == null){ init(); }


		string addOption = " ";
		if(ignore_arrived){
			//期間を入れないといけない
			addOption += "AND NOT EXISTS( SELECT *  FROM stamps WHERE stamps.station_id = stations.id  ) "; 
			//期間テスト
			int r_id = PlayerPrefsX.GetInt("rally_id");
			//addOption += "AND NOT EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id AND stamps.rally_id =  "+r_id+" ) "; 
		}
		if(distance_limit > 0.0f){
			addOption += "AND ABS(`longitude` - " + longitude + ") + ABS(`latitude` - " + latitude + ")  <  " + distance_limit + " ";
		}
		if(prefecture_id != 0){
			addOption += "AND stations.mst_prefecture_id = " + prefecture_id + " ";
		}
		addOption += "AND start <= datetime('now', 'localtime') AND end >= datetime('now', 'localtime') ";
		if( type_id != 0 ){
			addOption += "AND stations.mst_type_id = "+type_id+" ";
		}

		string groupOption = "";
		if( type_id == 0 ){
		//	groupOption += "GROUP BY mst_type_id ";
		}

		string limitOption = " ";
		if(limit > 0){
			limitOption = "LIMIT "+limit;
		}

		Debug.Log("Database CheckPosition 2.7:");
		string selectQuery = "SELECT stations.id as id, stations.name as name, kana, address, url, longitude, latitude, mst_prefecture_id, tel, " +
			"stations.kana as kana, stations.address as address, stations.url as url, stations.mst_type_id as mst_type_id, " +
			"stations.beacon_uuid as beacon_uuid, stations.beacon_major as beacon_major, stations.beacon_minor as beacon_minor, stations.point as point, " +
			"stations.longitude as longitude, stations.latitude as latitude, stations.mst_prefecture_id as mst_prefecture_id, " +
			"ABS(`longitude` - " + longitude + ") + ABS(`latitude` - " + latitude + ") AS distance, " +
			"( start > datetime('now', 'localtime') ) as before, (end < datetime('now', 'localtime')) as after, "+
			"( start <= datetime('now', 'localtime') AND end >= datetime('now', 'localtime')) as play, "+
			"prefectures.name as prefecture_name, " +
			"EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id  AND stamps.point > 0 AND stamps.season_id = " + PlayerPrefsX.GetInt("season_id") +  ")  AS arrived_thisSeason, " +
			"EXISTS( SELECT * FROM stamps WHERE stamps.station_id = stations.id  AND stamps.rally_id = " + PlayerPrefsX.GetInt("rally_id") +  ")  AS arrived " +
			"FROM stations, prefectures " +
			"WHERE stations.id  " +
			"AND stations.mst_prefecture_id = prefectures.id " +
			"AND stations.code > 0 "+
			addOption +
			groupOption +
			"ORDER BY ABS(`longitude` - " + longitude +") + ABS(`latitude` - " + latitude + ") ASC " +
			limitOption;

		Debug.Log(selectQuery);
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);

		return dataTable;
	}

	public DataTable todayEvent( int block_id ){

		string addOption = "AND date(start) <= date('now', 'localtime') AND date(end) >= date('now', 'localtime') ";

		string selectQuery = "SELECT stations.id as id, stations.name as name, kana, address, url, longitude, latitude, mst_prefecture_id, tel, start, end, " +
			"prefectures.name as prefecture_name, " +
			"EXISTS(SELECT * FROM stamps WHERE stamps.station_id = stations.id) as arrived " +
			"FROM stations, prefectures " +
			"WHERE stations.id  " +
			addOption +
			"AND stations.mst_prefecture_id = prefectures.id " +
			"AND stations.mst_type_id > 2 " +
			"AND ( stations.block_id = 0 OR stations.block_id = "+block_id+" "+
			"GROUP BY stations.mst_type_id "+
			"LIMIT 1";


		Debug.Log(selectQuery);
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		Debug.Log("count:"+dataTable.Rows.Count.ToString());
		return dataTable;

	}

	public DataTable cafeList( int rally_id ){
		string selectQuery = "SELECT id, name, start, end "+
			"FROM stations " +
			"WHERE code= " + rally_id + " "+
			"AND mst_type_id = 2 ";

		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);
		return dataTable;

	}

	//shop
	public DataTable listsShop(){
		return null;
	}

	public DataTable nearShop( double latitude, double longitude, int limit = 0, int prefecture_id = 0 ){


		if (PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE") != 0f){
			//位置をシミュレーション
			MyAPI.Instance.Trace("位置をシミュレート nearShop");
			latitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE");
			longitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LONGITUDE");
		}


		/*
		 *  id:shop_id
		 * name:名称
		 * kana:かな
		 * address:住所
		 * url:ウェブページURL
		 * tel:電話番号
		 * longitude:経度
		 * latitude:緯度
		 * distance:距離
		 * mst_prefecture_id:県番号
		 */
		if(sqlDB == null){ init(); }
		string addOption = " ";
		if(prefecture_id != 0){
			addOption += "AND shops.mst_prefecture_id = " + prefecture_id + " ";
		}
		string limitOption = " ";
		if(limit > 0){
			limitOption = "LIMIT "+limit;
		}
		string selectQuery = "SELECT id, name, kana, address, url, tel, longitude, latitude, mst_prefecture_id, " +
			"ABS(`longitude` - " + longitude + ") + ABS(`latitude` - " + latitude + ") AS distance, " +
			"0 as arrived, NULL as datetime " +
			"FROM shops " +
			"WHERE shops.id  " +
			"AND shops.code > 0 "+
			addOption +
			"ORDER BY ABS(`longitude` - " + longitude +") + ABS(`latitude` - " + latitude + ") ASC " +
			limitOption;
		Debug.Log(selectQuery);
		DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);

		return dataTable;
	}

	public DataTable myShop( int shop_id = 0 ){
		if(shop_id > 0){
			string selectQuery = "SELECT id, name, latitude, longitude, address, tel, url, mst_prefecture_id FROM shops WHERE id="+shop_id+" AND shop.code > 0";
			DataTable dataTable = sqlDB.ExecuteQuery(selectQuery);

			return dataTable;
		}
		return null;
	}

	//Stamp押す send_statusを0にしておく
	private int dbStampOn( int station_id, int rally_id, int point, int season_id ){

		// hiro 追加
		double latitude = GpsEventManager.Instance.Latitude;
		double longitude = GpsEventManager.Instance.Longitude;

		if (PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE") != 0f){
			//位置をシミュレーション
			MyAPI.Instance.Trace("位置をシミュレート dbStampOn");
			latitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LATITUDE");
			longitude = (double)PlayerPrefsX.GetFloat("DEBUG_LOCATION_LONGITUDE");
		}

		string query = "INSERT INTO stamps "
			+"(longitude, latitude, created, modified, send_status, point, station_id,rally_id,season_id) "
			+"VALUES ("
			+longitude+", "
			+latitude+", "
			+"datetime('now', 'localtime'), datetime('now', 'localtime'),"
			+"0,"+
			+point+","
			+station_id+","
			+rally_id+","
			+season_id+");";
		
		sqlDB.ExecuteNonQuery(query);
		Debug.Log(query);
		//query = "SELECT stamps.id as id FROM stamps WHERE ROWID = last_insert_rowid(); ";
		query = "SELECT max(stamps.id) as id FROM stamps ";
		DataTable dataTable = sqlDB.ExecuteQuery(query);
		Debug.Log(query);
		Debug.Log(dataTable.Rows.Count);
		return (int)dataTable.Rows[0]["id"];
	}

	//GPSとの連携
	private List<StationData> stations;
	public void CheckPosition( onComplete callback ){
		//Debug.Log("Database CheckPosition 1:");
		int _errorflg = 0;
		//ネットワーク通信ができない場合は
		if( Application.internetReachability == NetworkReachability.NotReachable ){
			Debug.Log("no reachable");
			//_errorflg = -1;
		}

		//GPS受信をユーザーが許可していない場合は
		if(!Input.location.isEnabledByUser){
			_errorflg = -2;
		}

		//Unity EditorではGPS受信ができないので-2ばかりになってしまう
		#if UNITY_EDITOR
		_errorflg = 0;
		#endif

		//現在の位置座標から
		float range = PlayerPrefsX.GetFloat("range");
		if(range == 0.0f){
			range = 0.005f;
		}
		bool ignore_arrive = false;
		if( PlayerPrefsX.GetString("TESTMODE2017")=="TEST_STAMPOK" ){
			range = 0.0f;
			ignore_arrive = true;
		}
		//range = 0.0f; //2017 範囲内かどうかは　取ってきてから判定する
		DataTable dt = nearStation( GpsEventManager.Instance.Latitude, GpsEventManager.Instance.Longitude, 1, 0, ignore_arrive, 0.0f);

		//Debug.Log("Database CheckPosition 2:"+dt.Rows.Count);
		stations = new List<StationData>();
		string station_name = "";
		int station_id = 0;
		int total_visit = 0;

		if(dt.Rows.Count > 0){
			//道の駅
			DataRow dr = dt.Rows[0];
			station_name = (string)dr["name"];
			int target_station_id = (int)dr["id"];
			total_visit = rallyVisitStation( PlayerPrefsX.GetInt("rally_id"), target_station_id );
			StationData stData = new StationData();
			stData.id = (int)dr["id"];
			stData.name = (string)dr["name"];
			stData.auth = "GPS";
			stData.beacon_major = 0;
			stData.beacon_minor = 0;
			stData.beacon_uuid = "";
			//stData.category_name = "道の駅"; //<-- ここ道の駅以外の場合は Honda DREAM Cafe
			if( (int)dr["mst_type_id"]  == 1 ){
				stData.category =  "ROAD_STATION";
				stData.category_name = "道の駅";
			}else{
				stData.category =  "EVENT";
				stData.category_name = (string)dr["kana"];
			}
				
			stData.today_stamped =  todayVisitStation( target_station_id );
			stData.season_stamped = ((int)dr["arrived_thisSeason"] == 0) ? false : true;

			//廣追加（今までこの道の駅で押したことがあるかどうか）
			if(stData.category == "ROAD_STATION"){
				stData.etarnal_stamped = ((int)dr["arrived"] == 0) ? false : true;
			} else {
				stData.etarnal_stamped = false;
			}
			stData.visited_num = 0;
			if(dr["point"] != null){
				stData.point = (int)dr["point"];
			}else{
				stData.point = 0;
			}
			//距離と移動していないことをチェックして最終判定
			bool isOK = true;
			double distance = (double)dr["distance"];
			stData.distance = (float) distance;
			Debug.Log("station distance:"+(double)dr["distance"]);

			if( (float)distance > range ){ // ng 
				isOK = false;
			}

			stData.isOK = isOK;
			stations.Add(stData);
		}

		dt = nearEvent( GpsEventManager.Instance.Latitude, GpsEventManager.Instance.Longitude, 0, 0, ignore_arrive, 0.0f );

		Debug.Log("Database CheckPosition 3:"+dt.Rows.Count);
		if(dt.Rows.Count > 0 && MyAPI.Instance.IamEntrant() ){
			//その他イベント
			foreach( DataRow dr in dt.Rows ){
				Debug.Log((string)dr["name"]+":"+(double)dr["distance"]);
				int target_station_id = (int)dr["id"];
				total_visit = rallyVisitStation( PlayerPrefsX.GetInt("rally_id"), target_station_id );
				StationData stData = new StationData();
				stData.id = (int)dr["id"];
				stData.name = (string)dr["name"];
				stData.auth = "GPS";
				if( dr["beacon_major"] != null ){
					stData.beacon_major = (int)dr["beacon_major"];
				}else{
					stData.beacon_major = 0;
				}
				if( dr["beacon_minor"] != null ){
					stData.beacon_minor = (int)dr["beacon_minor"];
				}else{
					stData.beacon_minor = 0;
				}
				if( dr["beacon_uuid"] != null ){
					stData.beacon_uuid = (string)dr["beacon_uuid"];
				}else{
					stData.beacon_uuid = "";
				}
				//stData.category_name = "道の駅"; //<-- ここ道の駅以外の場合は Honda DREAM Cafe
				if( (int)dr["mst_type_id"]  == 1 ){
					stData.category =  "ROAD_STATION";
					stData.category_name = "道の駅";
				}else{
					stData.category =  "EVENT";
					stData.category_name = (string)dr["kana"];
				}

				stData.today_stamped = todayVisitStation( target_station_id );
				stData.season_stamped = ((int)dr["arrived_thisSeason"] == 0) ? false : true;

				//廣追加（今までこの道の駅で押したことがあるかどうか）
				if(stData.category == "ROAD_STATION"){
					stData.etarnal_stamped = ((int)dr["arrived"] == 0) ? false : true;
				} else {
					stData.etarnal_stamped = false;
				}
				stData.visited_num = 0;
				if( dr["point"] != null ){
					stData.point = (int)dr["point"];
				}else{
					stData.point = 0;
				}

				//距離と移動していないこと＆ビーコンをチェックして最終判定
				bool isOK = true;
				double distance = (double)dr["distance"];
				stData.distance = (float) distance;

				/*
				if( stData.beacon_uuid != "" && _beaconScript != null ){
					//GameObject beaconObj = GameObject.Find("IBeacon");
					if( stData.beacon_uuid == _beaconScript.Uuid ){
						//ok
					}else{
						//ng
						isOK = false;
					}
				}
				*/
				if( (float)distance > range ){ // ng 
					isOK = false;
				}
				//Debug.Log("Station beacon:"+stData.beacon_uuid);
				//Debug.Log("beaconScript");
				//Debug.Log(_beaconScript);
				//Debug.Log("Beacon uuid:"+_beaconScript.Uuid);
				stData.isOK = isOK;
				stations.Add(stData);
			}

		}

		int findflg = 0;

		if(_errorflg == 0){
			Debug.Log( "findflg        -> " + findflg );
			Debug.Log( "station_name   -> " + station_name );
			Debug.Log( "total_visit    -> " + total_visit );

			//callback( findflg, station_name, total_visit, today_notvisit );
			findflg = 0;
			//Debug.Log("checkPosition StationCount:"+stations.Count);
			callback( findflg, stations, MyAPI.Instance.isMoving() );
		}else{
			callback( _errorflg, stations, MyAPI.Instance.isMoving());
		}
	}

	private onCompleteStamp _callbackfunction;
	private string _inserted_station_datetime;
	private string _inserted_cafe_datetime;
	public void StampOn(  int station_id, onCompleteStamp callback ){
		_callbackfunction = callback;
		if( station_id > 0 ){
			if( !todayVisitStation( station_id ) ){
				int rally_id = PlayerPrefsX.GetInt("now_rally_id");
				if( rally_id == 0 ){ rally_id = PlayerPrefsX.GetInt( "rally_id" ); }
				string query = "SELECT stations.point as point, mst_type_id FROM stations WHERE stations.id="+station_id+"; ";
				DataTable dt = sqlDB.ExecuteQuery( query );
				int point = (int)dt.Rows[0]["point"]; //stationが持っているポイント
				int mst_type_id = (int)dt.Rows[0]["mst_type_id"];

				query = "SELECT * FROM stamps WHERE stamps.station_id="+station_id+" AND stamps.rally_id = " + PlayerPrefsX.GetInt("rally_id") + "  AND stamps.point > 0; ";
				dt = sqlDB.ExecuteQuery( query );
				int pointed = dt.Rows.Count; //すでに今シーズンのポイントを獲得している
				//2017シーズンにエントリーしていないもしくは期間中ではない場合
				if( MyAPI.Instance.IamEntrant() == false || pointed > 0 || !MyAPI.Instance.IsSeason()){
					point = 0;
				}

				Debug.Log("INSERT point:"+point);
				int season_id = PlayerPrefsX.GetInt("season_id");
				int appdb_id = dbStampOn( station_id,  rally_id, point, season_id ); //ローカルdbに登録
				Hashtable param = new Hashtable();
				param.Add("station_id", station_id);
				param.Add("rally_id", PlayerPrefsX.GetInt( "rally_id" ) );
				param.Add("longitude", GpsEventManager.Instance.Longitude);
				param.Add("latitude", GpsEventManager.Instance.Latitude);
				param.Add("modified", _inserted_station_datetime );
				//2017add
				param.Add("season_id", season_id);
				param.Add("mst_type_id", mst_type_id);
				param.Add("appdb_id", appdb_id);
				param.Add("stack", 0 ); //スタックスタンプではない
				param.Add("point", point );
				//2017end

				MyAPI.Instance.call("members/stampon", param, completeStation);
			}else{
				_callbackfunction( "OK" );
			}
		}
		/*
		if(stations.Count > 0){
			foreach( StationData stationData in stations ){
				if( (!todayVisitStation( stationData.id ) &&  stationData.isOK) || PlayerPrefsX.GetString("TESTMODE2017")=="TEST_STAMPOK"){
					int rally_id = PlayerPrefsX.GetInt("now_rally_id");
					if( rally_id == 0 ){ rally_id = PlayerPrefsX.GetInt( "rally_id" ); }

					string query = "SELECT stations.point as point, mst_type_id FROM stations WHERE stations.id="+stationData.id+"; ";
					DataTable dt = sqlDB.ExecuteQuery( query );
					int point = (int)dt.Rows[0]["point"];
					int mst_type_id = (int)dt.Rows[0]["mst_type_id"];
					Debug.Log("INSERT point:"+point);

					int appdb_id = dbStampOn( stationData.id,  rally_id, point ); //ローカルdbに登録
					Hashtable param = new Hashtable();
					param.Add("station_id", stationData.id);
					param.Add("rally_id", PlayerPrefsX.GetInt( "rally_id" ) );
					param.Add("longitude", GpsEventManager.Instance.Longitude);
					param.Add("latitude", GpsEventManager.Instance.Latitude);
					param.Add("modified", _inserted_station_datetime );
					//2017add
					param.Add("mst_type_id", mst_type_id);
					param.Add("appdb_id", appdb_id);
					param.Add("stack", 0 ); //スタックスタンプではない
					param.Add("point", point );
					//2017end

					MyAPI.Instance.call("members/stampon", param, completeStation);
				}else{
					_callbackfunction( "OK" );
				}
			}
		}
		*/
	}

	public void wwwstation_timeout(){
		//

	}
	public void wwwcafe_timeout(){
		//

	}



	public void ClearAllStamp(){
		//if( MyAPI.Instance.getDemo() ){
			string query = "DELETE FROM stamps";
			sqlDB.ExecuteNonQuery(query);
		//}
	}

	private void completeStation( string ret ){

		try{
			CancelInvoke("wwwstation_timeout");
			LitJson.JsonData jsonData =  LitJson.JsonMapper.ToObject(ret);
			if( (int)jsonData["error"] == 0){
				int input_rally_id = int.Parse((string)jsonData["rally_id"]);
				int appdb_id = int.Parse((string)jsonData["appdb_id"]);

				if( _callbackfunction != null ){
					_callbackfunction( "OK" );
				}
			}
		}catch( UnityException e ){
			Debug.Log( e.Message );
		}
	}

	public string lastUpdate( string tablename ){
		//string queryString = "SELECT date(max(modified)) as max, modified FROM "+tablename;
		string queryString = "SELECT max(modified) as max, modified FROM "+tablename;
		Debug.Log(queryString);
		DataTable dt = sqlDB.ExecuteQuery(queryString);

		Debug.Log("max modified:"+tablename+":"+(string)dt.Rows[0]["modified"]);
		return (string)dt.Rows[0]["max"];
	}

	public void updateStationData( int id, string name, string kana, string address,
		string tel, double latitude, double longitude, string datetime, int mst_prefecture_id,
		int rally_id, int mst_type_id, string start, string end, string zip, string url,
		int point, string beacon_uuid, int beacon_major, int beacon_minor){
		string query = "INSERT OR REPLACE " +
			"INTO stations(" +
			"`id`," +
			" `name`, " +
			"`kana`, " +
			"`address`," +
			"`tel`,  " +
			"`latitude`, " +
			"`longitude`, " +
			"`modified`," +
			"`mst_prefecture_id`, " +
			"`code`, " +
			"`mst_type_id`, " +
			"`start`, " +
			"`end`, " +
			"`zip`," +
			"`url`, " +
			"`point`, " +
			"`beacon_uuid`, " +
			"`beacon_major`, " +
			"`beacon_minor`" +
			") " +
			"VALUES( '"
			+ id +"', '"+
			name+"', '"+
			kana+"', '"+
			address+"', '"+
			tel+"', '"+
			latitude+"', '"+
			longitude+"', '"+
			datetime+"', '"+
			mst_prefecture_id+"', '"+
			rally_id+"', '"+
			mst_type_id+"', '"+
			start+"', '"+
			end+"', '"+
			zip+"', '"+
			url+"', '"+
			point+"', '"+
			beacon_uuid+"', '"+
			beacon_major+"', '"+
			beacon_minor+
			"' )";

		Debug.Log("update station:"+query);
		sqlDB.ExecuteNonQuery(query);



	}

	public void updateRallyData( int id, string name, string url, string entry_start,
		string entry_end, string play_start, string play_end, int season_id, string color, int block_id, string datetime, int mst_category_id ){
		string query = "INSERT OR REPLACE " +
			"INTO rallies(`id`, `name`, `image_url`, `entry_start`, `entry_end`,`play_start`,  `play_end`, `season_id`, `color`, `block_id`, `modified`, `mst_category_id` ) " +
			"VALUES( '"+ id +"', '"+name+"', '"+url+"', '"+entry_start+"', '"+entry_end+"', '"+play_start+"', '"
			+play_end+"', '"+season_id+"', '"+color+"', '"+block_id+"', '"+datetime+"', '"+mst_category_id+"' )";
		Debug.Log("update rally:"+query);
		sqlDB.ExecuteNonQuery(query);

	}

	public void updateShopData( int id, string name, string kana, string address,
		string tel, double latitude, double longitude, string datetime, int mst_prefecture_id, string url, string code){
		string query = "INSERT OR REPLACE " +
			"INTO shops(`id`, `name`, `kana`, `address`,`tel`,  `latitude`, `longitude`, `modified`, `mst_prefecture_id`, `url`, `code`  ) " +
			"VALUES( '"+ id +"', '"+name+"', '"+kana+"', '"+address+"', '"+tel+"', '"
			+latitude+"', '"+longitude+"', '"+datetime+"', '"+mst_prefecture_id+"', '"+url+"', '"+code+"' )";
		Debug.Log("update shop:"+query);
		sqlDB.ExecuteNonQuery(query);

	}


	public void updateStampData(  double latitude, double longitude, string datetime, int station_id, int rally_id, int season_id, int appdb_id = 0 ){
		string query = "";
		if( appdb_id > 0 ){
			query = "INSERT OR REPLACE " +
				"INTO stamps(`id`, `latitude`, `longitude`, `created`, `modified`,`station_id`, `rally_id`, `season_id`, `send_status` ) " +
				"VALUES( '"+ appdb_id +"', '"+latitude+"', '"+longitude+"', '"+datetime+"', '"+datetime+"', '"+station_id+"', '"+rally_id+"', '"+season_id+"', 1 )";
		}else{
			query = "INSERT " +
				"INTO stamps(`latitude`, `longitude`, `created`, `modified`,`station_id`, `rally_id`, `season_id`, `send_status` ) " +
				"VALUES( '"+latitude+"', '"+longitude+"', '"+datetime+"', '"+datetime+"', '"+station_id+"', '"+rally_id+"', '"+season_id+"', 1 )";

		}

		sqlDB.ExecuteNonQuery(query);

	}

	public DataTable getStackStamp(){
		string query = "SELECT * FROM stamps WHERE send_status=0 ORDER BY id ASC LIMIT 1";
		Debug.Log(query);
		DataTable dataTable = sqlDB.ExecuteQuery(query);

		return dataTable;
	}

	public void updateStackStamp( int id ){
		string query = "UPDATE stamps SET send_status=1 WHERE id="+id;
		sqlDB.ExecuteNonQuery(query);
		/*
		string query = "SELECT * FROM stamps WHERE id="+id;
		DataTable dataTable = sqlDB.ExecuteQuery(query);
		DataRow dr = dataTable.Rows[0];

		query = "INSERT OR REPLACE " +
			"INTO stamps(`id`, `longitude`, `latitude`, `created`, `modified`, `station_id`, `rally_id`, `send_status`, `status`, `point`) " +
			"VALUES( '"+ id +"', '"+dr["longitude"]+"', '"+dr["latitude"]+"', '"+dr["created"]+"', '"+dr["modified"]+"', '"+dr["station_id"]+"', '"+dr["rally_id"]+"', '"+"1"+"', '"+dr["status"]+"', '"+dr["point"]+"' )";
		sqlDB.ExecuteNonQuery(query);
		*/
	}



	//シーズンIDからカラー情報を返す
	public string getSeasonColor( int season_id ){
		string query = "SELECT color FROM rallies "+
			"WHERE season_id="+season_id+"; ";

		DataTable dataTable = sqlDB.ExecuteQuery(query);
		string retString = "";
		if( dataTable.Rows.Count > 0 ){
			retString = (string)dataTable.Rows[0]["color"];
		}
		return retString;
	}
}